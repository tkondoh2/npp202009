const path = require('path');
const { makeLibName } = require('./makeLibName');

/**
 * Universal Cランタイム(ver.10)情報
 */
class UniversalCRuntime {
  /**
   * コンストラクタ
   * @param {
   *   rootPath: ルートディレクトリ
   *   bitWidth: BitWidthオブジェクト
   *   ver: バージョン
   *   debug: デバッグ版ならtrue
   * }
   */
  constructor({rootPath, bitWidth, ver, debug}) {
    this.rootPath_ = rootPath;
    this.bitWidth_ = bitWidth;
    this.ver_ = ver;
    this.debug_ = debug;
  }

  /**
   * モジュールがあるディレクトリ
   */
  buildPath() {
    return this.debug_
      ? path.join(
        this.rootPath_,
        'bin',
        this.ver_,
        this.bitWidth_.arch(),
        'ucrt'
      )
      : path.join(
        this.rootPath_,
        'Redist',
        this.ver_,
        'ucrt',
        'DLLs',
        this.bitWidth_.arch()
      );
  }

  /**
   * ソースオブジェクトを作成
   * @param {
   *   files: モジュールファイルのベース名リスト
   * }
   * @returns ソースオブジェクト
   */
  createLibSource({files}) {
    return new UniversalCRTSource({ucrt: this, files});
  }
}

/**
 * Universal Cランタイムのソース情報
 */
class UniversalCRTSource {
  /**
   * コンストラクタ
   * @param {
   *   ucrt: Universal Cランタイム情報オブジェクト
   *   files: モジュールのベース名リスト
   * }
   */
  constructor({ucrt, files}) {
    this.ucrt_ = ucrt;
    this.files_ = files;
  }

  /**
   * コピー元ファイルのディレクトリ
   */
  dir() { return this.ucrt_.buildPath(); }

  /**
   * コピーするファイルリスト
   */
  files() {
    return this.files_.map(basename => makeLibName({
      basename,
      debug: this.ucrt_.debug_,
    }))
  }
}
exports.UniversalCRuntime = UniversalCRuntime;
