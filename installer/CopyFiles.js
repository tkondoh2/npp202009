const path = require('path');
const { mkdirs, existsSync, copyFile } = require("fs-extra");

/**
 * ファイルコピークラス
 * source_ ソースオブジェクト(要dir,filesメソッド)
 * package_ パッケージオブジェクト(要datadirメソッド)
 */
class CopyFiles {
  /**
   * コンストラクタ
   * @param {src: ソースオブジェクト, pkg: パッケージオブジェクト}
   */
  constructor({src, pkg}) {
    this.source_ = src;
    this.package_ = pkg;
  }

  /**
   * コピーの実行
   */
  async copy() {
    try {
      const destdir = this.package_.datadir();
      if (!existsSync(destdir)) {
        await mkdirs(destdir);
      }
      const srcdir = this.source_.dir();
      console.log(`Copying ${srcdir} to ${destdir}`);
      for (const file of this.source_.files()) {
        console.log(`File: ${file}`);
        const srcfile = path.join(srcdir, file);
        const destfile = path.join(destdir, file);
        await copyFile(srcfile, destfile);
      }
    } catch (e) {
      console.error(e);
    }
  }
}
exports.CopyFiles = CopyFiles;
