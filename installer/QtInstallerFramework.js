const path = require('path');
const { mkdirs, existsSync } = require("fs-extra");
const util = require('util');
const childProcess = require('child_process');
const exec = util.promisify(childProcess.exec);
const { os } = require('./os');

/**
 * Qtインストーラフレームワーク情報
 */
class QtInstallerFramework {
  /**
   * コンストラクタ
   * @param {
   *   baseDir: ベースディレクトリ
   *   ver: バージョン
   * }
   */
  constructor({baseDir, ver}) {
    this.baseDir_ = baseDir;
    this.ver_ = ver;
  }

  /**
   * インストーラを作成する
   * @param {
   *   basename: 作成するインストーラのベース名
   * }
   */
  async make({basename}) {
    const targetDir = path.join(process.cwd(), 'installer');
    if (!existsSync(targetDir)) {
      await mkdirs(targetDir);
    }
    const cmdSet = [
      path.join(
        this.baseDir_,
        this.ver_,
        'bin',
        `binarycreator${os.win() ? '.exe' : ''}`
      ),
      '--offline-only',
      '-c',
      path.join('config', 'config.xml'),
      '-p',
      'packages',
      path.join('installer', basename),
    ];
    const cmd = cmdSet.join(' ');
    console.log(cmd);
    await exec(cmd);
  }
}
exports.QtInstallerFramework = QtInstallerFramework;
