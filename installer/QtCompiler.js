const { QtRuntime } = require('./QtRuntime');
const { os } = require('./os');
const { makeLibName } = require('./makeLibName');

/**
 * Qt用コンパイラ情報
 */
class QtCompiler {
  /**
   * コンストラクタ
   * @param {
   *   bitWidth: BitWidthオブジェクト
   *   debug: デバッグ版ならtrue
   *   name: コンパイラ名
   * }
   */
  constructor({bitWidth, debug, name}) {
    this.bitWidth_ = bitWidth;
    this.name_ = name;
    this.debug_ = debug;
  }

  /**
   * デバッグ版ならtrue
   */
  debug() { return this.debug_; }

  /**
   * デバッグ/リリース文字列
   */
  releaseOrDebug() { return this.debug_ ? 'Debug' : 'Release'; }

  /**
   * コンパイラ別Qtルートライブラリ
   */
  qtDir() {
    return this.bitWidth_.arch() === 'x64'
      ? `${this.name_}_64`
      : this.name_;
  }

  /**
   * コンパイラ名とビット幅によるビルドパス名の一部を作成
   */
  buildPath() {
    let parts = [ this.name_.toUpperCase() ];
    if (this.bitWidth_.width() > 32) {
      parts = [...parts, `${this.bitWidth_.width()}bit`];
    }
    return parts.join('_');
  }

  /**
   * ライブラリ名
   * @param {
   *   basename: ベース名
   *   ver: バージョン番号
   * }
   * @returns ライブラリ名
   */
  libName({basename, ver}) {
    return makeLibName({
      basename,
      ver,
      debug: this.debug_
    });
  }

  /**
   * コンパイラに基づくQtランタイム情報の作成
   * @param {
   *   version: Qtバージョン
   *   rootPath: Qtルートディレクトリ
   * }
   * @returns Qtランタイム情報オブジェクト
   */
  createRuntime({version, rootPath}) {
    return new QtRuntime({compiler: this, version, rootPath});
  }
}
exports.QtCompiler = QtCompiler;
