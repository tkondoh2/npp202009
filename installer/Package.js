const path = require('path');
const { makeXml } = require('./makeXml');
const { removeDirRecursive } = require('./removeDirRecursive');

/**
 * インストーラパッケージ情報
 */
class Package {
  /**
   * コンストラクタ
   * @param {
   *   domain: パッケージドメイン名
   *   data: パッケージメタデータ(XML)
   * }
   */
  constructor({domain, data}) {
    this.domain_ = domain;
    this.data_ = data;
  }

  /**
   * パッケージドメイン名
   */
  domain() { return this.domain_; }

  /**
   * パッケージメタデータ
   */
  data() { return this.data_; }

  /**
   * パッケージディレクトリ名
   */
  rootdir() {
    return path.join(process.cwd(), 'packages', this.domain_);
  }

  /**
   * パッケージメタディレクトリ名
   */
  metadir() {
    return path.join(this.rootdir(), 'meta');
  }

  /**
   * パッケージデータディレクトリ名
   */
  datadir() {
    return path.join(this.rootdir(), 'data');
  }

  /**
   * パッケージデータディレクトリのサブディレクトリ名を作成するメソッドを返す
   * @param {
   *   name: サブディレクトリ名
   * }
   * @returns {
   *   datadir: サブディレクトリ名を返すメソッド
   * }
   */
  createSubdir({name}) {
    const v = path.join(this.datadir(), name);
    return {datadir: () => v};
  }

  /**
   * パッケージデータディレクトリ内をクリーンアップする
   */
  async clearData() {
    console.log('clear data of package', this.datadir());
    await removeDirRecursive({dir: this.datadir()});
  }

  /**
   * パッケージ用設定XMLをメタディレクトリに作成する
   */
  async makeXml() {
    await makeXml({
      dir: this.metadir(),
      basename: 'package',
      'data': {
        Package: { ...this.data_ }
      },
    });
  };
}
exports.Package = Package;
