const { os } = require('./os');

/**
 * 共有ライブラリファイル名を作成
 * @param {
 *   basename: ファイルベース名
 *   ver: バージョン番号
 *   debug: デバッグ版ならtrue(ベース名末尾にdが付く)
 * }
 * @returns 共有ライブラリファイル名
 */
function makeLibName({basename, ver, debug}) {
  if (os.win()) {
    return `${basename}${debug ? 'd' : ''}.dll`;
  } else if (os.lnx()) {
    return `lib${basename}.so${!ver ? '' : `.${ver}`}`;
  } else { // mac
    return `${basename}.dylib`
  }
}
exports.makeLibName = makeLibName;
