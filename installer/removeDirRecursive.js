const path = require('path');
const { readdir, rmdir, unlink, stat, existsSync } = require("fs-extra");

/**
 * 指定ディレクトリの中を再帰的に削除する
 * @param {
 *   dir: 中を削除するディレクトリ
 * }
 */
async function removeDirRecursive({dir}) {
  try {
    // 指定されたディレクトリが存在しない場合は何もせずに終了
    if (!existsSync(dir)) return;

    // ディレクトリ内のファイル/ディレクトリリストを取得
    const files = await readdir(dir);
    for (const file of files) {

      // パスを作成
      const filePath = path.join(dir, file);

      // パスが示す情報を取得
      const fstat = await stat(filePath);

      // パスがディレクトリなら
      if (fstat.isDirectory()) {

        // 先に中を削除
        await removeDirRecursive({dir: filePath});

        // ディレクトリを削除
        await rmdir(filePath);
      }

      // パスがファイルなら
      else {
        
        // ファイルを削除
        await unlink(filePath);
      }
    }
  }
  catch (e) {
    console.error(e);
  }
}
exports.removeDirRecursive = removeDirRecursive;