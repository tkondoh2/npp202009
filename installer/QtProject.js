const path = require('path');
const { os } = require('./os');

/**
 * Qtプロジェクトのルート情報
 */
class QtBuildRoot {
  /**
   * コンストラクタ
   * @param {
   *   runtime: Qtランタイム情報
   *   rootPath: Qtプロジェクトのルートディレクトリ
   * }
   */
  constructor({runtime, rootPath}) {
    this.runtime_ = runtime;
    this.rootPath_ = rootPath;
  }

  /**
   * Qtランタイム情報
   */
  runtime() { return this.runtime_; }

  /**
   * Qtプロジェクトのルートディレクトリ
   */
  rootPath() { return this.rootPath_; }

  /**
   * デバッグ版かどうか
   */
  debug() { return this.runtime_.debug(); }

  /**
   * メインプロジェクトのソース情報
   * @param {
   *   name: メインプロジェクト名
   * }
   * @returns メインプロジェクトのソース情報
   */
  createMainBuildSource({name}) {
    return new QtMainBuildSource({base: this, name});
  }
}
exports.QtBuildRoot = QtBuildRoot;

/**
 * Qtメインプロジェクト情報
 */
class QtMainBuildSource {
  /**
   * コンストラクタ
   * @param {
   *   base: Qtプロジェクトルート
   *   name: プロジェクト名
   *   files: ファイル名リスト(拡張子まで含む)
   * }
   */
  constructor({base, name, files}) {
    this.base_ = base;
    this.name_ = name;
    this.files_ = files;
  }

  /**
   * プロジェクト名
   */
  name() { return this.name_; }

  /**
   * デバッグ版か
   */
  debug() { return this.base_.debug(); }

  /**
   * デバッグ/リリース文字列
   */
  releaseOrDebug() { return this.base_.runtime_.compiler_.releaseOrDebug(); }

  /**
   * サブプロジェクトソースオブジェクトを作成
   * @param {
   *   name: サブプロジェクト名
   *   files: ライブラリファイル名リスト(拡張子まで含む)
   * }
   * @returns 
   */
  createSubBuildSource({name, files}) {
    return new QtSubBuildSource({main: this, name, files});
  }

  /**
   * メインプロジェクトのビルドパス
   */
  rootPath() {
    const a = [
      'build',
      this.name_,
      ['Desktop', 'Qt', this.base_.runtime().buildPath()].join('_'),
      this.releaseOrDebug(),
    ].join('-');
    return path.join( this.base_.rootPath(), a );
  }

  /**
   * コピー元ファイルのディレクトリ
   */
  dir() {
    let dirs = [this.rootPath()];
    if (os.win()) {
      dirs = [...dirs, this.releaseOrDebug().toLowerCase()]
    }
    return path.join(...dirs);
  }

  /**
   * コピーするファイルリスト
   */
  files() { return this.files_; }
}

/**
 * Qtサブプロジェクトソースクラス
 */
class QtSubBuildSource {
  /**
   * コンストラクタ
   * @param {
   *   main: Qtメインプロジェクト
   *   name: サブプロジェクト名
   *   files: ファイル名リスト(拡張子まで含む)
   * }
   */
  constructor({main, name, files}) {
    this.main_ = main;
    this.name_ = name;
    this.files_ = files;
  }

  /**
   * コピー元ファイルのディレクトリ
   */
  dir() {
    let dirs = [this.main_.rootPath(), this.name_];
    if (os.win()) {
      dirs = [...dirs, this.main_.releaseOrDebug().toLowerCase()]
    }
    return path.join(...dirs);
  }

  /**
   * コピーするファイルリスト
   */
  files() { return this.files_; }
}
