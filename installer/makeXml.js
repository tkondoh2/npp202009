const path = require('path');
const { mkdirs, writeFile } = require("fs-extra");
const { create } = require('xmlbuilder2');

/**
 * XMLファイルを作成する
 * @param {
 *   dir: ファイルを置くディレクトリ(なければ自動構築)
 *   basename: ファイルベース名
 *   data: XML化するjsオブジェクト
 * }
 */
async function makeXml({dir, basename, data}) {
  const xml = create({version: '1.0', encoding: 'UTF-8'}, data);
  const doc = xml.end({prettyPrint: true});
  await mkdirs(dir);
  await writeFile(path.join(dir, `${basename}.xml`), doc);
};
exports.makeXml = makeXml;
