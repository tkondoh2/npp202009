/**
 * OSプラットフォームの判定
 */
const os = {
  win: () => process.platform === 'win32',
  mac: () => process.platform === 'darwin',
  lnx: () => process.platform === 'linux',
};
exports.os = os;
