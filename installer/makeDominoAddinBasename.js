const { os } = require('./os');

/**
 * Domino固有のアドインファイルのベース名を作成する
 * @param {*} param0 
 * @returns 
 */
function makeDominoAddinBasename({target, majorVer}) {
  const prefix = os.win() ? 'n' : '';
  const suffix = os.win() ? majorVer : '';
  return `${prefix}${target}${suffix}`
}
exports.makeDominoAddinBasename = makeDominoAddinBasename;
