const path = require('path');
const { makeLibName } = require('./makeLibName');

/**
 * Visual C++ 2017 (ver.14.0)情報
 */
class VisualCpp2017_14_0 {
  /**
   * コンストラクタ
   * @param {
   *   rootPath: ルートディレクトリ
   *   bitWidth: BitWidthオブジェクト
   *   debug: デバッグ版ならtrue
   * }
   */
  constructor({rootPaths, bitWidth, debug}) {
    this.path_ = rootPaths.vs2017;
    this.bitWidth_ = bitWidth;
    this.debug_ = debug;
  }

  /**
   * ビルドパスに使用するコンパイラ名
   */
  name() { return 'msvc2017'; }

  /**
   * モジュールがあるディレクトリ
   */
  buildPath() {
    return path.join(
      this.path_,
      'VC',
      'redist',
      this.bitWidth_.arch(),
      'Microsoft.VC140.CRT',
    );
  }

  /**
   * ソースオブジェクトを作成
   * @param {
   *   files: モジュールファイルのベース名リスト
   * }
   * @returns ソースオブジェクト
   */
  createLibSource({files}) {
    return new VisualCppSource({vc: this, files});
  }
}

/**
 * Visual C++ 2017 (ver.14.1)情報
 */
class VisualCpp2017_14_1 extends VisualCpp2017_14_0 {
  /**
   * コンストラクタ
   * @param {
   *   rootPath: ルートディレクトリ
   *   bitWidth: BitWidthオブジェクト
   *   debug: デバッグ版ならtrue
   * }
   */
  constructor({rootPaths, bitWidth, debug}) {
    super({rootPaths, bitWidth, debug});
  }

  /**
   * モジュールがあるディレクトリ
   */
  buildPath() {
    const common = path.join(
      this.path_,
      'VC',
      'Redist',
      'MSVC',
      '14.16.27012',
    )
    return this.debug_
      ? path.join(
        common,
        'debug_nonredist',
        this.bitWidth_.arch(),
        'Microsoft.VC141.DebugCRT',
        )
      : path.join(
        common,
        this.bitWidth_.arch(),
        'Microsoft.VC141.CRT',
      )
  }
}

/**
 * Visual C++のソース情報
 */
class VisualCppSource {
  /**
   * コンストラクタ
   * @param {
   *   vc: Visual C++情報オブジェクト
   *   files: モジュールのベース名リスト
   * }
   */
  constructor({vc, files}) {
    this.vc_ = vc;
    this.files_ = files;
  }

  /**
   * コピー元ファイルのディレクトリ
   */
  dir() {
    return this.vc_.buildPath();
  }

  /**
   * コピーするファイルリスト
   */
  files() {
    return this.files_.map(basename => makeLibName({
      basename,
      debug: this.vc_.debug_
    }));
  }
}

/**
 * Visual C++のバージョン番号によって作成する情報オブジェクトを切り替える
 * @param {
 *   ver: バージョン
 *   rootPath: ルートディレクトリ
 *   bitWidth: BitWidthオブジェクト
 *   debug: デバッグ版ならtrue
 * }
 * @returns Visual C++情報オブジェクト
 */
function createVisualCpp({ver, rootPaths, bitWidth, debug}) {
  if (ver === '14.0') return new VisualCpp2017_14_0({
    rootPaths,
    bitWidth,
  });
  else if (ver === '14.1') return new VisualCpp2017_14_1({
    rootPaths,
    bitWidth,
    debug,
  });
  return null;
}
exports.createVisualCpp = createVisualCpp;
