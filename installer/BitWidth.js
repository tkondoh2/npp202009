/**
 * ビット幅クラス
 */
class BitWidth {
  constructor({width}) {
    this.width_ = width;
  }
  width() { return this.width_; }
  arch() { return this.width_ === 32 ? 'x86' : 'x64'; }
};
exports.BitWidth = BitWidth;
