const path = require('path');
const { os } = require('./os');
const { makeLibName } = require('./makeLibName');

/**
 * Qtランタイム情報
 */
class QtRuntime {
  /**
   * コンストラクタ
   * @param {
   *   compiler: Qt用コンパイラ情報オブジェクト
   *   version: Qtバージョン
   *   rootPath: Qtルートディレクトリ
   * }
   */
  constructor({compiler, version, rootPath}) {
    this.compiler_ = compiler;
    this.version_ = version;
    this.rootPath_ = rootPath;
  }

  /**
   * Qtバージョン
   */
  version() { return this.version_; }

  /**
   * Qtルートディレクトリ
   */
  rootPath() { return this.rootPath_; }

  /**
   * デバッグ版ならtrue
   */
  debug() { return this.compiler_.debug(); }

  /**
   * Qtランタイムのルートディレクトリ
   */
  dir() {
    return path.join(
      this.rootPath_,
      this.version_,
      this.compiler_.qtDir()
    );
  }

  /**
   * コンパイラとQtバージョンによるビルドパス名の一部を作成
   */
   buildPath() {
    return [
      this.version_.replace(/\./g, '_'),
      this.compiler_.buildPath(),
    ].join('_');
  }

  /**
   * Qt共有ライブラリの場所
   */
  libDir() {
    return path.join(this.dir(), os.win() ? 'bin' : 'lib');
  }

  /**
   * Qt共有プラグインの場所
   * @param {
   *   type: プラグインタイプ名(plugins/type)
   * }
   * @returns プラグインディレクトリ
   */
  pluginsDir({type}) {
    return path.join(this.dir(), 'plugins', type);
  }

  /**
   * 共有ライブラリ名の作成
   * @param {
   *   basename: 共有ライブラリベース名
   * }
   * @returns 共有ライブラリ名
   */
  sharedLibFile({basename}) {
    return os.win()
      ? this.compiler_.libName({basename})
      : makeLibName({basename});
  }

  /**
   * ICUライブラリ名の作成
   * @param {
   *   basename: ベース名(プリフィックス'ICU'は不要)
   *   ver: バージョン番号
   * }
   * @returns 
   */
  icuLibFile({basename, ver}) {
    return this.compiler_.libName({
      basename: `icu${basename}`,
      ver
    });
  }

  /**
   * ランタイムライブラリソースオブジェクトを作成
   * @param {
   *   files: ソースファイルベース名リスト
   * }
   * @returns ソースオブジェクト
   */
  createLibSource({files}) {
    return new QtRuntimeLibSource({runtime: this, files});
  }

  /**
   * ICUライブラリソースオブジェクトを作成
   * @param {
   *   files: ソースファイルベース名リスト
   * }
   * @returns ソースオブジェクト
   */
  createICULibSource({files, ver}) {
    return new QtICULibSource({runtime: this, files, ver});
  }

  /**
   * プラグインライブラリソースオブジェクトを作成
   * @param {
   *   type: プラグインタイプ名
   *   files: ソースファイルベース名リスト
   * }
   * @returns ソースオブジェクト
   */
  createPluginsLibSource({type, files}) {
    return new QtPluginsLibSource({runtime: this, type, files});
  }
}
exports.QtRuntime = QtRuntime;

/**
 * Qtランタイムライブラリソースクラス
 */
class QtRuntimeLibSource {
  /**
   * コンストラクタ
   * @param {
   *   runtime: Qtランタイム情報オブジェクト
   *   files: コピーするファイルのベース名リスト
   * }
   */
  constructor({runtime, files}) {
    this.runtime_ = runtime;
    this.files_ = files;
  }

  /**
   * コピー元ファイルのディレクトリ
   */
  dir() {
    return this.runtime_.libDir();
  }

  /**
   * コピーするファイルリスト
   */
  files() {
    return this.files_.map(
      basename => this.runtime_.sharedLibFile({basename})
    );
  }
}

/**
 * Qtプラグインライブラリソースクラス
 */
class QtPluginsLibSource {
  /**
   * コンストラクタ
   * @param {
   *   runtime: Qtランタイム情報オブジェクト
   *   files: コピーするファイルのベース名リスト
   *   type: プラグインタイプ名
   * }
   */
  constructor({runtime, type, files}) {
    this.runtime_ = runtime;
    this.type_ = type;
    this.files_ = files;
    // this.noVer_ = os.lnx();
  }

  /**
   * コピー元ファイルのディレクトリ
   */
  dir() {
    return this.runtime_.pluginsDir({type: this.type_});
  }

  /**
   * コピーするファイルリスト
   */
  files() {
    return this.files_.map(
      basename => this.runtime_.sharedLibFile({
        basename,
        // noVer: this.noVer_,
      })
    );
  }
}

/**
 * ICUライブラリソースクラス
 */
class QtICULibSource {
  /**
   * コンストラクタ
   * @param {
   *   runtime: Qtランタイム情報オブジェクト
   *   files: コピーするファイルのベース名リスト
   *   ver: ICUバージョン
   * }
   */
  constructor({runtime, files, ver}) {
    this.runtime_ = runtime;
    this.files_ = files;
    this.ver_ = ver;
  }

  /**
   * コピー元ファイルのディレクトリ
   */
  dir() {
    return this.runtime_.libDir();
  }

  /**
   * コピーするファイルリスト
   */
  files() {
    return this.files_.map(
      basename => this.runtime_.icuLibFile({
        basename,
        ver: this.ver_
      })
    );
  }
}
