const path = require('path');
const { os } = require('./os');
const { makeLibName } = require('./makeLibName');

/**
 * OpenSSL情報
 */
class OpenSSL {
  /**
   * コンストラクタ
   * @param {
   *   rootPath: OpenSSLライブラリのあるルートディレクトリ
   *   ver: OpenSSLバージョン
   *   bitWidth: BitWidthオブジェクト
   * }
   */
  constructor({rootPath, ver, bitWidth}) {
    this.rootPath_ = rootPath;
    this.ver_ = ver;
    this.bitWidth_ = bitWidth;
  }

  /**
   * ライブラリソースオブジェクトを作成
   * @param {
   *   files: コピーするファイルのベース名リスト
   * }
   * @returns 
   */
  createLibSource({files}) {
    return new OpenSSLSource({base: this, files});
  }
}
exports.OpenSSL = OpenSSL

/**
 * OpenSSL用ソースクラス
 */
class OpenSSLSource {
  /**
   * コンストラクタ
   * @param {
   *   base: OpenSSLオブジェクト
   *   files: コピーするファイルのベース名リスト
   * }
   */
  constructor({base, files}) {
    this.base_ = base;
    this.files_ = files;
  }

  /**
   * コピーするファイルがある場所
   * @returns ディレクトリ
   */
  dir() {
    const local = os.win()
      ? [`Win_${this.base_.bitWidth_.arch()}`, 'bin']
      : ['lib'];
    return path.join(
      this.base_.rootPath_,
      ...local,
    );
  }

  /**
   * コピーするファイルリスト
   * @returns 整形済みファイル名リスト
   */
  files() {
    const vers = this.base_.ver_.split('.').slice(0, 2);
    const ver = vers.join(os.win() ? '_' : '.');
    const fn = os.win()
      ? ({basename}) => {
        const parts = [ `lib${basename}`, ver, this.base_.bitWidth_.arch() ];
        return makeLibName({basename: parts.join('-')});
      }
      : ({basename}) => makeLibName({basename, ver});
    return this.files_.map(basename => fn({basename}));
  }
}
