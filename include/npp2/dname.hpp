#ifndef NPP2_DNAME_HPP
#define NPP2_DNAME_HPP

#include "npp2_global.h"
#include "data/lmbcs.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <dname.h>
// #include <kfm.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp2 {

class NPP2_EXPORT DistinguishedName
{
  data::Lmbcs value_;

public:
  DistinguishedName();

  DistinguishedName(const data::Lmbcs &name) noexcept(false);

  data::Lmbcs value() const;

  data::Lmbcs abbreviated() const;

  DN_COMPONENTS getComponents() const noexcept(false);

  data::Lmbcs orgName() const noexcept(false);

  static DistinguishedName currentUserName() noexcept(false);
};

using DName = DistinguishedName;

} // namespace npp2

#endif // NPP2_DNAME_HPP
