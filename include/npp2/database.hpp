#ifndef NPP2_DATABASE_HPP
#define NPP2_DATABASE_HPP

#include "npp2_global.h"
#include "_private/handleobject.hpp"
#include "data/lmbcs.hpp"
#include "unreadidtable.hpp"
#include "nameslist.hpp"
#include <memory>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfdb.h>
#include <design.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

class _Database;
using Database = std::unique_ptr<_Database>;

struct NPP2_EXPORT NSFDbCloseDeleter {
  void operator ()(DBHANDLE hDb);
};

/**
 * @brief データベースクラス
 */
class NPP2_EXPORT _Database
  : public LockableObject2<DBHANDLE,NSFDbCloseDeleter>
{
  std::unique_ptr<UnreadIdTable> pUnreadIdTable_;

public:
  /**
   * @brief パスネットクラス
   */
  class NPP2_EXPORT PathNet
  {
    data::Lmbcs raw_; ///< 生データ

  public:
    /**
     * @brief コンストラクタ
     * @param raw 生データ
     */
    PathNet(const data::Lmbcs &raw);

    std::tuple<data::Lmbcs,data::Lmbcs,data::Lmbcs> parse() const;

    /**
     * @brief パスネットをパーツから生成する
     * @param path パス
     * @param server サーバ名(省略可)
     * @param port ポート名(省略可)
     * @return パスネットオブジェクト
     */
    static PathNet build(
      const data::Lmbcs &path,
      const data::Lmbcs &server = data::Lmbcs(),
      const data::Lmbcs &port = data::Lmbcs()
    ) noexcept(false);

    friend class _Database;
  };

  struct Paths
  {
    data::Lmbcs canonical_;
    data::Lmbcs expanded_;
  };

  /**
   * @brief デフォルトコンストラクタ
   */
  _Database();

  /**
   * @brief コンストラクタ
   * @param handle データベースハンドル
   */
  _Database(DBHANDLE handle);

  virtual ~_Database();

  Paths getPaths() noexcept(false);

  data::Lmbcs getAccessingUserName();

  /**
   * @brief データベースをパスネットでオープンする
   * @param pathnet パスネットオブジェクト
   * @return データベーススマートポインタ
   */
  static Database open(const PathNet &pathnet) noexcept(false);

  /**
   * @brief データベースをパスパーツでオープンする
   * @param path パス
   * @param server サーバ名(省略可)
   * @param port ポート名(省略可)
   * @return データベーススマートポインタ
   */
  static Database open(
    const data::Lmbcs &path,
    const data::Lmbcs &server = data::Lmbcs(),
    const data::Lmbcs &port = data::Lmbcs()
  ) noexcept(false);

  /**
   * @brief データベースをパスネットでオープンする
   * @param pathnet パスネットオブジェクト
   * @return データベーススマートポインタ
   */
  static Database openEx(
    const NamesListPtr &namesList,
    WORD options,
    const PathNet &pathnet
  ) noexcept(false);

  static Database openEx(
    const NamesListPtr &namesList,
    WORD options,
    const data::Lmbcs &path,
    const data::Lmbcs &server = data::Lmbcs(),
    const data::Lmbcs &port = data::Lmbcs()
  ) noexcept(false);

  static Paths getPaths(DBHANDLE hDB) noexcept(false);

  static data::Lmbcs getAccessingUserName(DBHANDLE hDB) noexcept(false);

  static data::Lmbcs getInfo(DBHANDLE hDB, WORD what) noexcept(false);

  static data::Lmbcs getInfoTitle(DBHANDLE hDB) noexcept(false);

  static data::Lmbcs getInfoCategories(DBHANDLE hDB) noexcept(false);

  static data::Lmbcs getInfoClass(DBHANDLE hDB) noexcept(false);

  static data::Lmbcs getInfoDesignClass(DBHANDLE hDB) noexcept(false);

  data::Lmbcs getInfo(WORD what) noexcept(false);

  data::Lmbcs getInfoTitle() noexcept(false);

  data::Lmbcs getInfoCategories() noexcept(false);

  data::Lmbcs getInfoClass() noexcept(false);

  data::Lmbcs getInfoDesignClass() noexcept(false);

  static DBID getId(DBHANDLE hDB) noexcept(false);

  DBID getId() noexcept(false);

  static DBREPLICAINFO getReplicaInfo(DBHANDLE hDB);

  DBREPLICAINFO getReplicaInfo() const;

  static NOTEID findDesign(DBHANDLE hDb, const char *name, WORD noteClass);

  NOTEID findDesign(const char *name, WORD noteClass);

  UnreadIdTable &getUnreadIdTable();

  static OID generateOid(DBHANDLE hDb);

  OID generateOid();

  static NOTEID createFolder(
    DBHANDLE hDb,
    const data::Lmbcs &name,
    NOTEID formatNoteId,
    DBHANDLE hFormatDb = NULLHANDLE,
    DESIGN_TYPE folderType = DESIGN_TYPE_SHARED
  );

  NOTEID createFolder(
    const data::Lmbcs &name,
    NOTEID formatNoteId,
    DBHANDLE hFormatDb = NULLHANDLE,
    DESIGN_TYPE folderType = DESIGN_TYPE_SHARED
  ) const;

  static void addToFolder(
    DBHANDLE hDb,
    NOTEID folderNoteId,
    DHANDLE hIdTable
  );

  void addToFolder(
    NOTEID folderNoteId,
    DHANDLE hIdTable
  );
};

} // namespace npp2

#endif // NPP2_DATABASE_HPP
