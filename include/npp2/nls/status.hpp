#ifndef NPP2_NLS_STATUS_HPP
#define NPP2_NLS_STATUS_HPP

#include "../npp2_global.h"
#include <exception>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nls.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::nls {

/**
 * @brief NLSステータスクラス
 */
class NPP2_EXPORT Status
  : public std::exception
{
  NLS_STATUS raw_; ///< NLSステータス値

public:
  /**
   * @brief コンストラクタ
   */
  Status();

  /**
   * @brief コンストラクタ
   * @param status NLSステータス
   */
  Status(NLS_STATUS status);

  /**
   * @brief エラー値
   */
  NLS_STATUS raw() const;

  /**
   * @brief 成功していれば真
   */
  operator bool() const;

  /**
   * @brief std::exception::whatをオーバーライド
   */
  virtual const char *what() const noexcept override;
};

} // namespace npp2::nls

#endif // NPP2_NLS_STATUS_HPP
