#ifndef NPP2_NLS_INFO_HPP
#define NPP2_NLS_INFO_HPP

#include "../npp2_global.h"
#include "status.hpp"

namespace npp2::nls {

/**
 * @brief NLS(National Language Service)情報のラッパークラス
 */
class NPP2_EXPORT Info
{
protected:
  NLS_PINFO ptr_; ///< NLSポインタ

public:
  /**
   * @brief コンストラクタ
   * @param ptr NLS情報へのポインタ
   */
  explicit Info(NLS_PINFO ptr);

  /**
   * @brief デストラクタ
   */
  virtual ~Info() = default;

  /**
   * @brief NLS情報へのポインタを返します。
   * @return NLS情報へのポインタ
   */
  NLS_PINFO get() const noexcept;
};

/**
 * @brief ロードして使用するNLS情報クラス
 * @tparam CSID 文字セットID
 */
template <WORD CSID>
class LoadedInfo
  : public Info
{
public:
  /**
   * @brief コンストラクタ
   */
  explicit LoadedInfo() : Info(nullptr) {
    NLS_load_charset(CSID, &ptr_);
  }

  /**
   * @brief デストラクタ
   */
  virtual ~LoadedInfo() {
    NLS_unload_charset(ptr_);
  }
};

/**
 * @brief LMBCS専用NLS情報クラス
 */
class NPP2_EXPORT LmbcsInfo
  : public Info
{
public:
  /**
   * @brief コンストラクタ
   */
  LmbcsInfo();

  /**
   * @brief デストラクタ
   */
  virtual ~LmbcsInfo() = default;
};

/**
 * @brief Native専用NLS情報クラス
 */
class NPP2_EXPORT NativeInfo
  : public Info
{
public:
  /**
   * @brief コンストラクタ
   */
  NativeInfo();

  /**
   * @brief デストラクタ
   */
  virtual ~NativeInfo() = default;
};

} // namespace npp2::nls

#endif // NPP2_NLS_INFO_HPP
