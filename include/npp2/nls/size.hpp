#ifndef NPP2_NLS_SIZE_HPP
#define NPP2_NLS_SIZE_HPP

#include "../npp2_global.h"
#include "status.hpp"

namespace npp2::nls {

/**
 * @brief バイト配列の文字数相当のバイト数を取得します。
 * @param s 対象のバイト配列
 * @param chars バイト数として測りたい文字数
 * @param pInfo バイト配列の文字セット情報
 * @return バイト数
 * @throw Status NLS_STATUSを内包したクラス
 */
WORD NPP2_EXPORT getStringBytes(
  const char *str,
  WORD chars,
  NLS_PINFO pInfo
) noexcept(false);

/**
 * @brief バイト配列の文字数を取得します。
 * @param s 対象のバイト配列
 * @param bytes 文字数として測りたいバイト数
 * @param pInfo バイト配列の文字セット情報
 * @return 文字数
 * @throw Status NLS_STATUSを内包したクラス
 */
WORD NPP2_EXPORT getStringChars(
  const char *str,
  WORD bytes,
  NLS_PINFO pInfo
) noexcept(false);

/**
 * @brief 文字セットに応じて、マルチバイト文字列の区切り位置を調整したバイト数を求めます。
 * @param s 対象のバイト配列
 * @param bytes 計測対象のバイト数
 * @param pInfo バイト配列の文字セット情報
 * @return 調整したバイト数
 * @throw Status NLS_STATUSを内包したクラス
 */
WORD NPP2_EXPORT adjustByteSize(
  const char *str,
  WORD bytes,
  NLS_PINFO pInfo
) noexcept(false);

} // namespace npp2::nls

#endif // NPP2_NLS_SIZE_HPP
