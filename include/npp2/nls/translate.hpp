#ifndef NPP2_NLS_TRANSLATE_HPP
#define NPP2_NLS_TRANSLATE_HPP

#include "../npp2_global.h"
#include "info.hpp"
#include <QByteArray>

namespace npp2::nls {

/**
 * @brief Unicode文字列をLMBCS文字列に変換する
 * @param unicode Unicode文字列
 * @returns 変換後のLMBCS文字列
 */
QByteArray NPP2_EXPORT unicodeToLmbcs(const QByteArray &unicode) noexcept(false);

/**
 * @brief LMBCS文字列をUnicode文字列に変換する
 * @param lmbcs LMBCS文字列
 * @returns 変換後のUnicode文字列
 */
QByteArray NPP2_EXPORT lmbcsToUnicode(const QByteArray &lmbcs) noexcept(false);
/**
 * @brief Native文字列をLMBCS文字列に変換する
 * @param native Native文字列
 * @returns 変換後のLMBCS文字列
 */
QByteArray NPP2_EXPORT nativeToLmbcs(const QByteArray &native) noexcept(false);

/**
 * @brief LMBCS文字列をNative文字列に変換する
 * @param lmbcs LMBCS文字列
 * @returns 変換後のNative文字列
 */
QByteArray NPP2_EXPORT lmbcsToNative(const QByteArray &lmbcs) noexcept(false);

} // namespace npp2::nls

#endif // NPP2_NLS_TRANSLATE_HPP
