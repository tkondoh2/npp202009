#ifndef NPP2_SESSION_HPP
#define NPP2_SESSION_HPP

#include "status.hpp"

namespace npp2 {

class SessionLocker
{
  Status status_;

public:
  explicit SessionLocker(int argc, char *argv[])
    : status_(NotesInitExtended(argc, argv))
  {}

  ~SessionLocker() {
    if (status_) {
      NotesTermThread();
    }
  }
};

} // namespace npp2

#endif // NPP2_SESSION_HPP
