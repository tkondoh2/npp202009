#ifndef NPP2_JWT_BASE64_HPP
#define NPP2_JWT_BASE64_HPP

#include <QByteArray>

namespace npp2::jwt {

inline QByteArray base64(
    const QByteArray &text,
    const QByteArray::Base64Options options
      = QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals
    )
{
  return text.toBase64(options);
}

inline QByteArray decode64(
    const QByteArray &base64,
    const QByteArray::Base64Options options
      = QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals
    )
{
  return QByteArray::fromBase64(base64, options);
}

} // namespace npp2::jwt

#endif // NPP2_JWT_BASE64_HPP
