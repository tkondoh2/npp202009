#ifndef NPP2_JWT_HMAC_HPP
#define NPP2_JWT_HMAC_HPP

#include "base64.hpp"
#include "sha.hpp"
#include <QString>
#include <openssl/hmac.h>
#include <openssl/err.h>

namespace npp2::jwt {

struct HS256 : public npp2::jwt::SHA256 {
  static const char *alg() { return "HS256"; }
};

struct HS384 : public npp2::jwt::SHA384 {
  static const char *alg() { return "HS384"; }
};

struct HS512 : public npp2::jwt::SHA512 {
  static const char *alg() { return "HS512"; }
};

namespace hmac {

template <class T>
QByteArray sign(
    const QByteArray &key,
    const QByteArray &msg,
    bool encoding = true
    ) noexcept(false) {
  char value[T::length() + 1] = "";
  unsigned int len = T::length();
  if (HMAC(
        T::sha(),
        key.constData(),
        key.length(),
        reinterpret_cast<const unsigned char*>(msg.constData()),
        msg.length(),
        reinterpret_cast<unsigned char*>(value),
        &len
        ) == nullptr) {
    throw std::runtime_error(
          QString("HMAC signing error: %1")
          .arg(ERR_get_error(), 0, 16, QChar('0'))
          .toStdString()
        );
  }
  QByteArray binary(value, len);
  return encoding ? base64(binary) : binary;
}

template <class T>
bool verify(
    const QByteArray &key,
    const QByteArray &msg,
    const QByteArray &signature,
    bool encoding = true
    ) noexcept(false) {
  char value[T::length() + 1] = "";
  unsigned int len = T::length();
  if (HMAC(
        T::sha(),
        key.constData(),
        key.length(),
        reinterpret_cast<const unsigned char*>(msg.constData()),
        msg.length(),
        reinterpret_cast<unsigned char*>(value),
        &len
        ) == nullptr) {
    throw std::runtime_error(
          QString("HMAC verifing error: %1")
          .arg(ERR_get_error(), 0, 16, QChar('0'))
          .toStdString()
          );
  }
  QByteArray binary(value, len);
  return signature == (encoding ? base64(binary) : binary);
}

} // namespace hmac
} // namespace npp2::jwt

#endif // NPP2_JWT_HMAC_HPP
