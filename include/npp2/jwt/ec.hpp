#ifndef NPP2_JWT_EC_HPP
#define NPP2_JWT_EC_HPP

#include "base64.hpp"
#include "sha.hpp"
#include "bio.hpp"
#include "evp.hpp"
#include <QString>
#include <openssl/ecdsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>

namespace npp2::jwt {

struct ES256 : public npp2::jwt::SHA256 {
  static const char *alg() { return "ES256"; }
};

struct ES384 : public npp2::jwt::SHA384 {
  static const char *alg() { return "ES384"; }
};

struct ES512 : public npp2::jwt::SHA512 {
  static const char *alg() { return "ES512"; }
};

namespace ec {

struct Deleter {
  static void cleanup(EC_KEY *) {
    // 有効そうなポインタでもクリティカルなエラーになる。(2021-12-7)
    // EC_KEY_free(pEcKey);
  }
};

using Ptr = QScopedPointer<EC_KEY, Deleter>;

inline Ptr createPrivate(const QByteArray &privateKey) {
  bio::Ptr bio = bio::createByBuffer(privateKey);
  auto ec_key = PEM_read_bio_ECPrivateKey(
        bio.data(),
        nullptr,
        nullptr,
        nullptr
        );
  return Ptr(EC_KEY_check_key(ec_key) ? ec_key : nullptr);
}

inline Ptr createPublic(const QByteArray &publicKey) {
  bio::Ptr bio = bio::createByBuffer(publicKey);
  return Ptr(
        PEM_read_bio_EC_PUBKEY(
                bio.data(),
                nullptr,
                nullptr,
                nullptr
                ));
}

template <class T>
QByteArray sign(
    const QByteArray &privateKey,
    const QByteArray &msg,
    bool encoding = true
    ) {
  Ptr eckey = createPrivate(privateKey);
  if (eckey.isNull()) return QByteArray();

  evp::pkey::Ptr pkey = evp::pkey::create();
  if (!EVP_PKEY_assign_EC_KEY(pkey.data(), eckey.data())) return QByteArray();

  evp::pkey::ctx::Ptr pkctx = evp::pkey::ctx::create(pkey.data());

  if (!EVP_PKEY_sign_init(pkctx.data())) return QByteArray();

  if (!EVP_PKEY_CTX_set_signature_md(pkctx.data(), T::sha()))
    return QByteArray();

  size_t sigSize = 0;
  if (!EVP_PKEY_sign(
        pkctx.data(),
        nullptr,
        &sigSize,
        reinterpret_cast<const uchar*>(msg.constData()),
        msg.length())
      )
    return QByteArray();

  QByteArray buffer(static_cast<int>(sigSize), '\0');
  if (!EVP_PKEY_sign(
        pkctx.data(),
        reinterpret_cast<uchar*>(buffer.data()),
        &sigSize,
        reinterpret_cast<const uchar*>(msg.constData()),
        msg.length())
        )
    return QByteArray();

  return encoding ? base64(buffer) : buffer;
}

template <class T>
bool verify(
    const QByteArray &publicKey,
    const QByteArray &msg,
    const QByteArray &signature,
    bool decoding = true
    ) {
  Ptr eckey = createPublic(publicKey);
  if (eckey.isNull()) return false;

  evp::pkey::Ptr pkey = evp::pkey::create();
  if (!EVP_PKEY_assign_EC_KEY(pkey.data(), eckey.data())) return false;

  evp::pkey::ctx::Ptr pkctx = evp::pkey::ctx::create(pkey.data());

  if (!EVP_PKEY_verify_init(pkctx.data()))
    return false;

  if (!EVP_PKEY_CTX_set_signature_md(pkctx.data(), T::sha()))
    return false;

  auto bytes = decoding ? decode64(signature) : signature;
  return !!EVP_PKEY_verify(
        pkctx.data(),
        reinterpret_cast<const uchar*>(bytes.constData()),
        bytes.length(),
        reinterpret_cast<const uchar*>(msg.constData()),
        msg.length()
        );
}

} // namespace ec
} // namespace npp2::jwt

#endif // NPP2_JWT_EC_HPP
