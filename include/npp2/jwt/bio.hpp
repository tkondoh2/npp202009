#ifndef NXPP2_JWT_BIO_HPP
#define NXPP2_JWT_BIO_HPP

#include <openssl/evp.h>
#include <QScopedPointer>
#include <QByteArray>

namespace npp2::jwt::bio {

struct Deleter {
  static void cleanup(BIO *pBio) {
    BIO_free(pBio);
  }
};

using Ptr = QScopedPointer<BIO, Deleter>;

inline Ptr createByBuffer(const QByteArray &buffer) {
  return Ptr(BIO_new_mem_buf(buffer.constData(), buffer.length()));
}

} // namespace npp2::jwt::bio

#endif // NXPP2_JWT_BIO_HPP
