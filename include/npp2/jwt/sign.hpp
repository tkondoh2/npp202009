#ifndef NPP2_JWT_SIGN_HPP
#define NPP2_JWT_SIGN_HPP

#include "header.hpp"
#include "payload.hpp"
#include "base64.hpp"
#include "algorithm.hpp"
#include "hmac.hpp"
#include "rsa.hpp"
#include "ec.hpp"

namespace npp2::jwt {

template <class T>
QByteArray toBase64(const T &obj, bool isCompact = true) {
  auto doc = obj.toDocument();
  auto json = doc.toJson(
        isCompact ? QJsonDocument::Compact : QJsonDocument::Indented
      );
  return base64(json);
}

template <class P = Payload>
QByteArray sign(
    const QByteArray &key,
    const P &payload,
    const QByteArray &algText
    ) noexcept(false) {
  Algorithm alg = textToAlgorithm(algText);
  if (alg == Algorithm::Invalid) {
    throw std::runtime_error("Invalid algorithm.");
  }
  auto header = Header::make(algText);
  auto msg = toBase64(header) + "." + toBase64(payload);
  QByteArray signature;
  switch (alg) {
  case Algorithm::HS256: signature = hmac::sign<HS256>(key, msg); break;
  case Algorithm::HS384: signature = hmac::sign<HS384>(key, msg); break;
  case Algorithm::HS512: signature = hmac::sign<HS512>(key, msg); break;
  case Algorithm::RS256: signature = rsa::sign<RS256>(key, msg); break;
  case Algorithm::RS384: signature = rsa::sign<RS384>(key, msg); break;
  case Algorithm::RS512: signature = rsa::sign<RS512>(key, msg); break;
  case Algorithm::ES256: signature = ec::sign<ES256>(key, msg); break;
  case Algorithm::ES384: signature = ec::sign<ES384>(key, msg); break;
  case Algorithm::ES512: signature = ec::sign<ES512>(key, msg); break;
  default: return "";
  }
  return msg + "." + signature;
}

} // namespace npp2::jwt

#endif // NPP2_JWT_SIGN_HPP
