#ifndef NPP2_JWT_SHA_HPP
#define NPP2_JWT_SHA_HPP

#include <openssl/evp.h>
#include <openssl/sha.h>

namespace npp2::jwt {

struct SHA256 {
  static const EVP_MD *sha() { return EVP_sha256(); }
  static constexpr int length() { return SHA256_DIGEST_LENGTH; }
};

struct SHA384 {
  static const EVP_MD *sha() { return EVP_sha384(); }
  static constexpr int length() { return SHA384_DIGEST_LENGTH; }
};

struct SHA512 {
  static const EVP_MD *sha() { return EVP_sha512(); }
  static constexpr int length() { return SHA512_DIGEST_LENGTH; }
};

} // namespace npp2::jwt

#endif // NPP2_JWT_SHA_HPP
