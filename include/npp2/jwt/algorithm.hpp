#ifndef NPP2_JWT_ALGORITHM_HPP
#define NPP2_JWT_ALGORITHM_HPP

#include <QByteArray>

namespace npp2::jwt {

enum class Algorithm {
  Invalid,
  HS256, HS384, HS512,
  RS256, RS384, RS512,
  ES256, ES384, ES512
};

inline const char *algorithmText(Algorithm alg) {
  switch (alg) {
  case Algorithm::HS256: return "HS256";
  case Algorithm::HS384: return "HS384";
  case Algorithm::HS512: return "HS512";
  case Algorithm::RS256: return "RS256";
  case Algorithm::RS384: return "RS384";
  case Algorithm::RS512: return "RS512";
  case Algorithm::ES256: return "ES256";
  case Algorithm::ES384: return "ES384";
  case Algorithm::ES512: return "ES512";
  default: return "";
  }
}

inline Algorithm textToAlgorithm(const QByteArray &text) {
  if (text == "HS256") return Algorithm::HS256;
  else if (text == "HS384") return Algorithm::HS384;
  else if (text == "HS512") return Algorithm::HS512;
  else if (text == "RS256") return Algorithm::RS256;
  else if (text == "RS384") return Algorithm::RS384;
  else if (text == "RS512") return Algorithm::RS512;
  else if (text == "ES256") return Algorithm::ES256;
  else if (text == "ES384") return Algorithm::ES384;
  else if (text == "ES512") return Algorithm::ES512;
  return Algorithm::Invalid;
}

} // namespace npp2::jwt

#endif // NPP2_JWT_ALGORITHM_HPP
