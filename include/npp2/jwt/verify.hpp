#ifndef NPP2_JWT_VERIFY_HPP
#define NPP2_JWT_VERIFY_HPP

#include "header.hpp"
#include "payload.hpp"
#include "base64.hpp"
#include "algorithm.hpp"
#include "hmac.hpp"
#include "rsa.hpp"
#include "ec.hpp"

namespace npp2::jwt {

template <class T>
T fromBase64(const QByteArray &b64) {
  auto json = decode64(b64);
  auto doc = QJsonDocument::fromJson(json);
  return Object::fromDocument<T>(doc);
}

template <class P = Payload>
std::optional<P> verify(
    const QByteArray &key,
    const QByteArray &jwt
    ) noexcept(false) {
  auto parts = jwt.split('.');
  if (parts.length() != 3) {
    throw std::runtime_error("Invalid token.");
  }
  auto b64header = parts[0];
  auto b64payload = parts[1];
  auto header = fromBase64<Header>(b64header);
  if (header.type() != "JWT") {
    throw std::runtime_error("Invalid header.");
  }
  auto msg = b64header + '.' + b64payload;
  auto signature = parts[2];
  auto algText = header.algorithm();
  auto alg = textToAlgorithm(algText);
  bool result = false;
  switch (alg) {
  case Algorithm::HS256: result = hmac::verify<HS256>(key, msg, signature); break;
  case Algorithm::HS384: result = hmac::verify<HS384>(key, msg, signature); break;
  case Algorithm::HS512: result = hmac::verify<HS512>(key, msg, signature); break;
  case Algorithm::RS256: result = rsa::verify<RS256>(key, msg, signature); break;
  case Algorithm::RS384: result = rsa::verify<RS384>(key, msg, signature); break;
  case Algorithm::RS512: result = rsa::verify<RS512>(key, msg, signature); break;
  case Algorithm::ES256: result = ec::verify<ES256>(key, msg, signature); break;
  case Algorithm::ES384: result = ec::verify<ES384>(key, msg, signature); break;
  case Algorithm::ES512: result = ec::verify<ES512>(key, msg, signature); break;
  default: throw std::runtime_error("Invalid algorithm.");
  }
  if (!result) { return std::nullopt; }
  auto payload = fromBase64<P>(b64payload);
  if (payload.expired()) { return std::nullopt; }
  return payload;
}

} // namespace npp2::jwt

#endif // NPP2_JWT_VERIFY_HPP
