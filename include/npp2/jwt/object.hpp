#ifndef NPP2_JWT_OBJECT_HPP
#define NPP2_JWT_OBJECT_HPP

#include <QVariantMap>
#include <QDateTime>
#include <QJsonObject>
#include <QJsonDocument>
#include <optional>

namespace npp2::jwt {

class Object
    : public QVariantMap
{
public:
  Object() : QVariantMap() {}

  Object(const QVariantMap &map) : QVariantMap(map) {}

  QByteArray type() const {
    return value("typ").toByteArray();
  }

  void setType(const QByteArray &value) {
    insert("typ", value);
  }

  bool isValidType(const QByteArray &_type) const {
    return type() == _type;
  }

  std::optional<QDateTime> dateTime(const QString &key) const {
    auto v = value(key);
    bool ok = false;
    auto n = v.toLongLong(&ok);
    return ok
        ? std::make_optional(QDateTime::fromSecsSinceEpoch(n))
        : std::nullopt;
  }

  QJsonDocument toDocument() const {
   auto json = QJsonObject::fromVariantMap(*this);
   return QJsonDocument(json);
  }

  template <class T>
  static T fromDocument(const QJsonDocument &doc) {
    auto json = doc.object();
    return T(json.toVariantMap());
  }
};

} // namespace npp2::jwt

#endif // NPP2_JWT_OBJECT_HPP
