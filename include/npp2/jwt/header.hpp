#ifndef NPP2_JWT_HEADER_HPP
#define NPP2_JWT_HEADER_HPP

#include "object.hpp"

namespace npp2::jwt {

template <class T>
QJsonDocument createHeader() {
  QJsonObject root;
  root.insert("typ", "JWT");
  root.insert("alg", T::alg());
  return QJsonDocument(root);
}

class Header
    : public Object
{
public:
  Header() : Object() {}

  Header(const QVariantMap &map) : Object(map) {}

  QByteArray algorithm() const {
    return value("alg").toByteArray();
  }

  void setAlgorithm(const QByteArray &value) {
    insert("alg", value);
  }

  bool isValid() const { return type() == "JWT"; }

  static Header make(const QByteArray &alg) {
    QVariantMap map;
    map.insert("typ", "JWT");
    map.insert("alg", alg);
    return Header(map);
  }
};

} // namespace npp2::jwt

#endif // NPP2_JWT_HEADER_HPP
