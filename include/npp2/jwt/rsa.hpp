#ifndef NPP2_JWT_RSA_HPP
#define NPP2_JWT_RSA_HPP

#include "base64.hpp"
#include "sha.hpp"
#include "bio.hpp"
#include "evp.hpp"
#include <QString>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>

namespace npp2::jwt {

struct RS256 : public npp2::jwt::SHA256 {
  static const char *alg() { return "RS256"; }
};

struct RS384 : public npp2::jwt::SHA384 {
  static const char *alg() { return "RS384"; }
};

struct RS512 : public npp2::jwt::SHA512 {
  static const char *alg() { return "RS512"; }
};

namespace rsa {

struct Deleter {
  static void cleanup(RSA *pRsa) { RSA_free(pRsa); }
};

using Ptr = QScopedPointer<RSA, Deleter>;

inline Ptr createPrivate(const QByteArray &privateKey) {
  bio::Ptr bio = bio::createByBuffer(privateKey);
  return Ptr(
        PEM_read_bio_RSAPrivateKey(
                bio.data(),
                nullptr,
                nullptr,
                nullptr
                ));
}

inline Ptr createPublic(const QByteArray &publicKey) {
  bio::Ptr bio = bio::createByBuffer(publicKey);
  return Ptr(
        PEM_read_bio_RSA_PUBKEY(
                bio.data(),
                nullptr,
                nullptr,
                nullptr
                ));
}

template <class T>
QByteArray sign(
    const QByteArray &privateKey,
    const QByteArray &msg,
    bool encoding = true
    ) {
  Ptr rsa = createPrivate(privateKey);
  if (rsa.isNull()) return QByteArray();

  evp::pkey::Ptr pkey = evp::pkey::create();
  if (!EVP_PKEY_set1_RSA(pkey.data(), rsa.data())) return QByteArray();

  evp::md::ctx::Ptr mdctx = evp::md::ctx::create();

  if (!EVP_DigestSignInit(
        mdctx.data(),
        nullptr,
        T::sha(),
        nullptr,
        pkey.data()
        ))
    return QByteArray();

  if (!EVP_DigestSignUpdate(mdctx.data(), msg.constData(), msg.length()))
    return QByteArray();

  size_t sigSize = 0;
  if (!EVP_DigestSignFinal(mdctx.data(), nullptr, &sigSize))
    return QByteArray();

  QByteArray buffer(static_cast<int>(sigSize), '\0');
  if (!EVP_DigestSignFinal(
        mdctx.data(),
        reinterpret_cast<uchar*>(buffer.data()),
        &sigSize
        ))
    return QByteArray();
  return encoding ? base64(buffer) : buffer;
}

template <class T>
bool verify(
    const QByteArray &publicKey,
    const QByteArray &msg,
    const QByteArray &signature,
    bool decoding = true
    ) {
  Ptr rsa = createPublic(publicKey);
  if (rsa.isNull()) return false;

  evp::pkey::Ptr pkey = evp::pkey::create();
  if (!EVP_PKEY_set1_RSA(pkey.data(), rsa.data())) return false;

  evp::md::ctx::Ptr mdctx = evp::md::ctx::create();

  if (!EVP_DigestVerifyInit(
        mdctx.data(),
        nullptr,
        T::sha(),
        nullptr,
        pkey.data()
        ))
    return false;

  if (!EVP_DigestVerifyUpdate(mdctx.data(), msg.constData(), msg.length()))
    return false;

  auto bytes = decoding ? decode64(signature) : signature;
  auto result = EVP_DigestVerifyFinal(
        mdctx.data(),
        reinterpret_cast<const uchar*>(bytes.constData()),
        bytes.length()
        );
  return !!result;
}

} // namespace rsa
} // namespace npp2::jwt

#endif // NPP2_JWT_RSA_HPP
