#ifndef NPP2_JWT_EVP_HPP
#define NPP2_JWT_EVP_HPP

#include <openssl/evp.h>
#include <QScopedPointer>
#include <QByteArray>

namespace npp2::jwt::evp {

namespace pkey {

struct Deleter {
  static void cleanup(EVP_PKEY *pPkey) { EVP_PKEY_free(pPkey); }
};

using Ptr = QScopedPointer<EVP_PKEY, Deleter>;

inline Ptr create() { return Ptr(EVP_PKEY_new()); }

namespace ctx {

struct Deleter {
  static void cleanup(EVP_PKEY_CTX *pCtx) { EVP_PKEY_CTX_free(pCtx); }
};

using Ptr = QScopedPointer<EVP_PKEY_CTX, Deleter>;

inline Ptr create(EVP_PKEY *pkey) {
  return Ptr(EVP_PKEY_CTX_new(pkey, nullptr));
}

} // namespace ctx

} // namespace pkey

namespace md::ctx {

struct Deleter {
  static void cleanup(EVP_MD_CTX *pMDCtx) { EVP_MD_CTX_destroy(pMDCtx); }
};

using Ptr = QScopedPointer<EVP_MD_CTX, Deleter>;

inline Ptr create() { return Ptr(EVP_MD_CTX_create()); }

} // namespace md::ctx

} // namespace npp2::jwt::evp

#endif // EVP_NPP2_JWT_EVP_HPPHPP
