#ifndef NPP2_JWT_PAYLOAD_HPP
#define NPP2_JWT_PAYLOAD_HPP

#include "object.hpp"

namespace npp2::jwt {

class Payload
  : public Object
{
public:
  Payload() : Object() {}

  Payload(const QVariantMap &map) : Object(map) {}

  std::optional<QDateTime> issued() const {
    return dateTime("iat");
  }

  std::optional<QDateTime> expires() const {
    return dateTime("exp");
  }

  bool expired() const {
    auto exp = expires();
    return !exp ? true : (exp.value() < QDateTime::currentDateTime());
  }

  void setIssueAndExpires(qint64 sec, bool expires_in = false) {
    auto now = QDateTime::currentSecsSinceEpoch();
    insert("iat", now);
    insert("exp", now + sec);
    if (expires_in) {
      insert("expires_in", sec);
    }
  }
};

} // namespace npp2::jwt

#endif // NPP2_JWT_PAYLOAD_HPP
