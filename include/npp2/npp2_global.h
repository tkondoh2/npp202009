#ifndef NPP2_GLOBAL_H
#define NPP2_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(NPP2_LIBRARY)
#  define NPP2_EXPORT Q_DECL_EXPORT
#else
#  define NPP2_EXPORT Q_DECL_IMPORT
#endif

#if defined(NT)
#  define NPP2CALLBACK WINAPI
#else
#  define NPP2CALLBACK LNPUBLIC
#endif

#endif // NPP2_GLOBAL_H
