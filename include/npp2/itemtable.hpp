#ifndef NPP2_ITEMTABLE_HPP
#define NPP2_ITEMTABLE_HPP

#include "npp2_global.h"
#include "data/any.hpp"
#include "data/lmbcs.hpp"
#include <QVector>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfdata.h>
// #include <nsfnote.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp2 {

/**
 * @brief 名前付きサマリーバッファクラス
 */
class NPP2_EXPORT NamedItemTable
{
  ITEM_TABLE *pTable_; ///< テーブルポインタ
  QVector<data::Lmbcs> names_; ///< 名前リスト

public:
  /**
   * @brief コンストラクタ
   * @param pTable ITEM_TABLE構造体ポインタ
   */
  NamedItemTable(ITEM_TABLE *pTable);

  /**
   * @brief 名前リストを取得
   * @return
   */
  const QVector<data::Lmbcs> &names() const;

  bool hasItem(const data::Lmbcs &name) const;

  /**
   * @brief 名前に対応した値をVariantで取得
   * @param name アイテム名
   * @return アイテム値
   */
  data::Any value(const data::Lmbcs &name) const;

  /**
   * @brief 名前に対応した値をVariantで取得
   * @param pTable テーブルポインタ
   * @param name アイテム名
   * @return アイテム値
   */
  static data::Any value(ITEM_TABLE *pTable, const data::Lmbcs &name);
};

} // namespace npp2

#endif // NPP2_ITEMTABLE_HPP
