#ifndef NPP2_EXTMGR_HPP
#define NPP2_EXTMGR_HPP

#include "npp2_global.h"
#include "status.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <extmgr.h>
#include <nsferr.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

class ExtensionManagerEntry
{
  HEMREGISTRATION rawHandle_;

public:
  ExtensionManagerEntry() : rawHandle_(NULLHANDLE) {}

  virtual ~ExtensionManagerEntry() {
#if defined(NT)
    if (rawHandle_ != NULLHANDLE) {
      EMDeregister(rawHandle_);
    }
#endif
  }

  template <typename Traits>
  void install(EMHANDLER handler, WORD recId) {
    Status status = EMRegister(
          Traits::eid,
          Traits::regFlags,
          handler,
          recId,
          &rawHandle_
          );
    if (!status) {
      throw status;
    }
  }
};

inline WORD createRecursionID() noexcept(false) {
  WORD RecursionID = 0;
  Status status = EMCreateRecursionID(&RecursionID);
  if (!status) {
    throw status;
  }
  return RecursionID;
}

template <typename Traits>
bool isEnabled(EMRECORD *pRecord, WORD notifyType) {
  // 処理対象イベントかどうか判定
#if defined(NT)
  auto eid = pRecord->EId & 0xffff;
  auto notificationType = pRecord->EId >> 16;
#else
  auto eid = pRecord->EId;
  auto notificationType = pRecord->NotificationType;
#endif
  return eid == Traits::eid && notificationType == notifyType;
}

namespace extmgr {

WORD NPP2_EXPORT createRecursionID();

bool NPP2_EXPORT filter(EMRECORD *pRecord, EID eid, WORD notifyType);

} // namespace extmgr

} // namespace npp2

#endif // EXTMGR_HPP
