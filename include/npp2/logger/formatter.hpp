#ifndef NPP2_LOGGER_FORMATTER_HPP
#define NPP2_LOGGER_FORMATTER_HPP

#include "level.hpp"
#include <QSharedPointer>
#include <QDateTime>
#include <thread>

namespace npp2::logger {

class Formatter
{
public:
  Formatter()
  {}

  virtual ~Formatter()
  {}

  virtual QString operator ()(
    const QString &id,
    const QDateTime &time,
    const std::thread::id &tid,
    Level level,
    const QString &msg
  ) = 0;
};

using FormatterPtr = QSharedPointer<Formatter>;

} // namespace npp2::logger

#endif // NPP2_LOGGER_FORMATTER_HPP
