#ifndef NPP2_LOGGER_LEVEL_HPP
#define NPP2_LOGGER_LEVEL_HPP

#include "../npp2_global.h"
#include <QString>

namespace npp2::logger {

enum class Level: int {
  Off,
  Critical,
  Error,
  Warning,
  Info,
  Debug,
  Trace,
  Max
};

QString NPP2_EXPORT levelText(Level level);

Level NPP2_EXPORT textToLevel(const QString &text);

} // namespace npp2::logger

#endif // NPP2_LOGGER_LEVEL_HPP
