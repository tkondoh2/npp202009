#ifndef NPP2_LOGGER_DEFAULTFORMATTER_HPP
#define NPP2_LOGGER_DEFAULTFORMATTER_HPP

#include "../npp2_global.h"
#include "formatter.hpp"

namespace npp2::logger {

class NPP2_EXPORT DefaultFormatter
  : public Formatter
{
  QString format_;
  std::function<QString(QDateTime)> timeFormatter_;

public:
  DefaultFormatter(
    const QString &format
      = "[${time}] [${thread}] [${id}] [${level}] ${message}",
    const std::function<QString(QDateTime)> &timeFormatter
      = [](const QDateTime &time) {
        return time.toString("yyyy-MM-dd hh:mm:ss.zzz");
      }
  );

  virtual ~DefaultFormatter() override;

  virtual QString operator ()(
    const QString &id,
    const QDateTime &time,
    const std::thread::id &tid,
    Level level,
    const QString &msg
  ) override;
};

} // namespace npp2::logger

#endif // NPP2_LOGGER_DEFAULTFORMATTER_HPP
