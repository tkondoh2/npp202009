#ifndef NPP2_LOGGER_INDEX_HPP
#define NPP2_LOGGER_INDEX_HPP

#include "../npp2_global.h"
#include "level.hpp"
#include "formatter.hpp"
#include "../utils/writer/qtextfilewriter.hpp"
#include <QMutex>

namespace npp2::logger {

using GetMsgFunc = std::function<QString()>;

class Logger;
using LoggerPtr = QSharedPointer<Logger>;

using LoggerWriter = _Writer<QTextStream>;

using WriterPtr = QSharedPointer<LoggerWriter>;

/**
 * @brief ロガークラス
 */
class NPP2_EXPORT Logger
{
  QString id_; ///< ログID
  QMutex mutex_; ///< 書き込み時用ミューテックス
  WriterPtr pWriter_; ///< ライター
  FormatterPtr pFormatter_; ///< フォーマッター
  Level threshold_; ///< レベル閾値

  /**
   * @brief デフォルトコンストラクタ
   */
  Logger();

  /**
   * @brief コンストラクタ
   * @param id ログID
   */
  Logger(const QString &id);

  // シングルトン処理
  Logger(const Logger &) = delete;
  Logger &operator =(const Logger &) = delete;

public:
  /**
   * @brief 有効なログか
   * 
   * @return true 有効
   * @return false 無効
   */
  bool isValid() const;

  /**
   * @return Level レベル閾値を返す。
   */
  Level threshold() const;

  /**
   * @param threshold 設定するレベル閾値
   */
  void setThreshold(Level threshold);

  /**
   * @return WriterPtr& ライターを返す。
   */
  WriterPtr &pWriter();

  /**
   * @return FormatterPtr& フォーマッターを返す。
   */
  FormatterPtr &pFormatter();

  /**
   * @brief 致命的なエラーレベルでメッセージを出力する。
   * @param getMessage メッセージを取得する関数
   */
  void critical(GetMsgFunc getMessage);

  /**
   * @brief エラーレベルでメッセージを出力する。
   * @param getMessage メッセージを取得する関数
   */
  void error(GetMsgFunc getMessage);

  /**
   * @brief 警告レベルでメッセージを出力する。
   * @param getMessage メッセージを取得する関数
   */
  void warning(GetMsgFunc getMessage);

  /**
   * @brief 情報レベルでメッセージを出力する。
   * @param getMessage メッセージを取得する関数
   */
  void info(GetMsgFunc getMessage);

  /**
   * @brief デバッグレベルでメッセージを出力する。
   * @param getMessage メッセージを取得する関数
   */
  void debug(GetMsgFunc getMessage);

  /**
   * @brief トレースレベルでメッセージを出力する。
   * @param getMessage メッセージを取得する関数
   */
  void trace(GetMsgFunc getMessage);

  /**
   * @return LoggerPtr& シングルトンインスタンスを返す。
   */
  static LoggerPtr &instance();

  /**
   * @brief ログファイルを作成する。
   * @param id ログID
   * @param filePath ログファイルパス
   * @param fn 監査用関数
   * @param codecName コーデック名
   * @param bom BOMの有無
   * @return LoggerPtr インスタンス
   */
  static LoggerPtr createFile(
    const QString &id,
    const QString &filePath,
    std::function<void(const QString&)> fn,
    const QByteArray &codecName = "UTF-8",
    bool bom = false
  );

protected:
  /**
   * @brief ログ情報を出力する。
   * @param level ログレベル
   * @param getMessage メッセージ取得関数
   */
  void output(Level level, GetMsgFunc getMessage);
};

} // namespace npp2::logger

#endif // NPP2_LOGGER_INDEX_HPP
