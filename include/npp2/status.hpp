#ifndef NPP2_STATUS_HPP
#define NPP2_STATUS_HPP

#include "npp2_global.h"
#include <exception>
#include <QByteArray>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

/**
 * @brief ステータスクラス
 */
class NPP2_EXPORT Status
    : public std::exception
{
  STATUS raw_; // ステータス値

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Status();

  /**
   * @brief コンストラクタ
   * @param status ステータス値
   */
  Status(STATUS status);

  /**
   * @return STATUS値
   */
  STATUS raw() const noexcept;

  /**
   * @return エラー値の取得
   */
  STATUS error() const noexcept;

  /**
   * @brief 表示済みかどうか
   * @return 表示済みなら真
   */
  bool isDisplayed() const noexcept;

  /**
   * @brief リモート先のエラーかどうか
   * @return リモート先のエラーなら真
   */
  bool isRemote() const noexcept;

  /**
   * @brief エラーが発生したパッケージ値を返す。
   * @return パッケージ値
   */
  STATUS package() const noexcept;

  /**
   * @brief エラー番号を返す。
   * @return エラー番号
   */
  STATUS errnum() const noexcept;

  /**
   * @return エラーがなければ真を返す
   */
  operator bool() const noexcept;

  /**
   * @brief 例外の内容を示す文字列を返す
   * @return Notes C APIエラー文字列(LMBCS)
   */
  virtual const char *what() const noexcept override;

  /**
   * @brief 例外内容をQByteArray型で返す
   * @return 例外メッセージ
   */
  QByteArray message() const noexcept;
};

} // namespace npp2

#endif // NPP2_STATUS_HPP
