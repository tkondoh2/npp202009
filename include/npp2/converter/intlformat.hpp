#ifndef NPP2_CONVERTER_INTLFORMAT_HPP
#define NPP2_CONVERTER_INTLFORMAT_HPP

#include "../npp2_global.h"
#include "../data/lmbcs.hpp"
#include <memory>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <intl.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp2::converter {

/**
 * @brief i18n設定クラス
 */
class NPP2_EXPORT IntlFormat {
  INTLFORMAT value_;

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  IntlFormat();

  /**
   * @return INTLFORMATへのポインタ
   */
  INTLFORMAT *data();

  /**
   * @brief フラグ値から該当フラグの真偽を調べる
   * @param F フラグ
   * @return フラグが真ならtrue
   */
  bool isFlags(WORD F) const;

  /**
   * @brief フラグ値の真偽を設定する
   * @param F フラグ
   * @param f 真にするならtrue
   */
  void setFlags(WORD F, bool f);

  // フラグの真偽取得
  bool isCurrencySuffix() const;
  bool isCurrencySpace() const;
  bool isNumberLeadingZero() const;
  bool isClock24Hour() const;
  bool isDaylightSavings() const;
  bool isDateMDY() const;
  bool isDateDMY() const;
  bool isDateYMD() const;
  bool isDate4DigitYear() const;
  bool isTimeAMPMPrefix() const;
  bool isDateAbbrev() const;

  // フラグの真偽設定
  void setCurrencySuffix(bool f);
  void setCurrencySpace(bool f);
  void setNumberLeadingZero(bool f);
  void setClock24Hour(bool f);
  void setDaylightSavings(bool f);
  void setDateMDY(bool f);
  void setDateDMY(bool f);
  void setDateYMD(bool f);
  void setDate4DigitYear(bool f);
  void setTimeAMPMPrefix(bool f);
  void setDateAbbrev(bool f);

  /**
   * @return 通貨の小数点桁数
   */
  BYTE currencyDigits() const;

  /**
   * @param c 通貨の小数点桁数
   */
  void setCurrencyDigits(BYTE c);

  /**
   * @return 構造体長
   */
  BYTE length() const;

  /**
   * @param l 構造体長
   */
  void setLength(BYTE l);

  /**
   * @return 時差(日本UTC+9なら-9)
   */
  int timeZone() const;

  /**
   * @param tz 時差(日本UTC+9なら-9)
   */
  void setTimeZone(int tz);

  /**
   * @return 午前表記文字列
   */
  data::Lmbcs amString() const;

  /**
   * @return 午後表記文字列
   */
  data::Lmbcs pmString() const;

  /**
   * @return 通貨記号文字列
   */
  data::Lmbcs currencyString() const;

  /**
   * @return 3桁区切り文字列
   */
  data::Lmbcs thousandString() const;

  /**
   * @return 小数点文字列
   */
  data::Lmbcs decimalString() const;

  /**
   * @return 日付区切り文字列
   */
  data::Lmbcs dateString() const;

  /**
   * @return 時間区切り文字列
   */
  data::Lmbcs timeString() const;

  /**
   * @return 「昨日」文字列
   */
  data::Lmbcs yesterdayString() const;

  /**
   * @return 「今日」文字列
   */
  data::Lmbcs todayString() const;

  /**
   * @return 「明日」文字列
   */
  data::Lmbcs tomorrowString() const;

  /**
   * @param src 午前表記文字列
   */
  void setAmString(const data::Lmbcs &src);

  /**
   * @param src 午後表記文字列
   */
  void setPmString(const data::Lmbcs &src);

  /**
   * @param src 通貨記号文字列
   */
  void setCurrencyString(const data::Lmbcs &src);

  /**
   * @param src 3桁区切り文字列
   */
  void setThousandString(const data::Lmbcs &src);

  /**
   * @param src 小数点文字列
   */
  void setDecimalString(const data::Lmbcs &src);

  /**
   * @param src 日付区切り文字列
   */
  void setDateString(const data::Lmbcs &src);

  /**
   * @param src 時間区切り文字列
   */
  void setTimeString(const data::Lmbcs &src);

  /**
   * @param src 「昨日」文字列
   */
  void setYesterdayString(const data::Lmbcs &src);

  /**
   * @param src 「今日」文字列
   */
  void setTodayString(const data::Lmbcs &src);

  /**
   * @param src 「明日」文字列
   */
  void setTomorrowString(const data::Lmbcs &src);

  /**
   * @return デフォルトの設定
   */
  static INTLFORMAT getCurrent();

  /**
   * @brief 文字列バッファサイズを考慮してLmbcsに変換する。
   * @tparam MAX バッファのサイズ
   * @param src 文字列バッファ
   * @return Lmbcs文字列
   */
  template <size_t MAX>
  static data::Lmbcs getString(const char *src) {
    auto len = std::min<size_t>(strlen(src), MAX);
    return data::Lmbcs(src, len);
  }

  /**
   * @brief 文字列バッファサイズを考慮してLmbcs文字列をバッファにコピーする
   * @tparam MAX バッファのサイズ
   * @param dst 文字列バッファ
   * @param src Lmbcs文字列
   */
  template <size_t MAX>
  static void setString(char *dst, const data::Lmbcs &src) {
    strncpy(dst, src.constData(), MAX);
  }
};

using IntlFormatPtr = std::unique_ptr<IntlFormat>;

} // namespace npp2::converter

#endif // NPP2_CONVERTER_INTLFORMAT_HPP
