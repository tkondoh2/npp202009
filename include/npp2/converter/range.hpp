#ifndef NPP2_CONVERTER_RANGE_HPP
#define NPP2_CONVERTER_RANGE_HPP

namespace npp2::converter {

/**
 * @brief 範囲型のデータをテキストにコンバートする
 * @tparam Traits 特性構造体
 * @tparam Format フォーマット設定構造体(NFMT/TFMT)のラップクラス
 * @tparam ItemConverter シングル要素のコンバート関数オブジェクトクラス
 * @tparam PairConverter ペア要素のコンバート関数オブジェクトクラス
 */
template <class Traits, class Format, class ItemConverter, class PairConverter>
class RangeToTextConverter
  : public intl::ConverterBase<Format>
{
  Lmbcs templ_; ///< テンプレートテキスト

public:
  /**
   * @brief コンストラクタ
   * @param templ テンプレートテキスト
   * テンプレートテキストには以下の文字列を含める
   * "{{items}}" シングル要素のリスト文字列に置き換わる
   * "{{pairs}}" ペア要素のリスト文字列に置き換わる
   */
  RangeToTextConverter(const Lmbcs &templ)
    : intl::ConverterBase<Format>()
    , templ_(templ)
  {}

  /**
   * @brief コンストラクタ
   * @param templ テンプレートテキスト
   * テンプレートテキストには以下の文字列を含める
   * "{{items}}" シングル要素のリスト文字列に置き換わる
   * "{{pairs}}" ペア要素のリスト文字列に置き換わる
   * @param iPtr intl::Settingsの共有ポインタ
   * @param tPtr フォーマット設定構造体(NFMT/TFMT)のラップクラスの共有ポインタ
   */
  RangeToTextConverter(
    const Lmbcs &templ,
    intl::SettingsPtr iPtr,
    std::unique_ptr<Format> tPtr
  )
    : intl::ConverterBase<Format>(iPtr, tPtr)
    , templ_(templ)
  {}

  /**
   * @brief 関数オブジェクト実行
   * @param sep 区切り文字列
   * @param range Traits型のRangeクラス
   * @return コンバートした文字列
   */
  Lmbcs operator () (const Lmbcs &sep, const Range<Traits> &range) const {
    // コンバーターでシングル要素を文字列に変換し、TextList型に格納する
    ItemConverter itemConverter(this->intlSettingsPtr_, this->formatPtr_);
    auto itemList = TextList::create(
      range.template map<Lmbcs>([&](typename Traits::item item) {
        return itemConverter(&item);
      }),
      FALSE
    );

    // コンバーターでペア要素を文字列に変換し、TextList型に格納する
    PairConverter pairConverter(this->intlSettingsPtr_, this->formatPtr_);
    auto pairList = TextList::create(
      range.template pairMap<Lmbcs>([&](typename Traits::pair pair) {
        return pairConverter(&pair);
      }),
      FALSE
    );

    // シングル、ペアそれぞれのTextList型をテンプレートに当てはめる
    return Lmbcs(templ_)
      .replace("{{items}}", itemList->join(sep))
      .replace("{{pairs}}", pairList->join(sep));
  }
};

} // namespace npp2::converter

#endif // NPP2_CONVERTER_RANGE_HPP
