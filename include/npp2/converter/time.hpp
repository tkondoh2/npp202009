#ifndef NPP2_CONVERTER_TIME_HPP
#define NPP2_CONVERTER_TIME_HPP

#include "../npp2_global.h"
#include "base.hpp"
#include "../format/time.hpp"
#include "../data/lmbcs.hpp"

namespace npp2::converter {

/**
 * @brief 数値コンバーター関数オブジェクトクラス
 */
class NPP2_EXPORT TimeConverter
    : public Base<format::Time>
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  TimeConverter();

  /**
   * @brief コンストラクタ
   * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
   * @param tPtr NFMTラッパーの共有ポインタ
   */
  TimeConverter(IntlFormatPtr &&iPtr, format::TimePtr &&tPtr);
};

/**
 * @brief 数値→テキストコンバーター関数オブジェクトクラス
 */
class NPP2_EXPORT TimeToTextConverter
    : public TimeConverter
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  TimeToTextConverter();

  /**
   * @brief コンストラクタ
   * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
   * @param tPtr NFMTラッパーの共有ポインタ
   */
  TimeToTextConverter(IntlFormatPtr &&iPtr, format::TimePtr &&tPtr);

  /**
   * @brief 関数呼び出し
   * @param pTime 変換元数値へのポインタ
   * @return フォーマット済み文字列
   */
  data::Lmbcs operator () (TIMEDATE *pTime) const;
};

/**
 * @brief テキスト→数値コンバーター関数オブジェクトクラス
 */
class NPP2_EXPORT TextToTimeConverter
    : public TimeConverter
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  TextToTimeConverter();

  /**
   * @brief コンストラクタ
   * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
   * @param tPtr NFMTラッパーの共有ポインタ
   */
  TextToTimeConverter(IntlFormatPtr &&iPtr, format::TimePtr &&tPtr);

  /**
   * @brief 関数呼び出し
   * @param lmbcs 変換元テキスト
   * @return フォーマット済み文字列
   */
  TIMEDATE operator () (const data::Lmbcs &lmbcs) const;
};



/**
 * @brief 日時コンバーター関数オブジェクトクラス
 */
// class TimeConverter
//     : public intl::ConverterBase<TimeFormat>
// {
// public:
//   /**
//    * @brief デフォルトコンストラクタ
//    */
//   TimeConverter() : intl::ConverterBase<TimeFormat>() {}

//   /**
//    * @brief コンストラクタ
//    * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
//    * @param tPtr TFMTラッパーの共有ポインタ
//    */
//   TimeConverter(
//       intl::SettingsPtr iPtr,
//       TimeFormatPtr tPtr
//       )
//     : intl::ConverterBase<TimeFormat>(iPtr, tPtr)
//   {}
// };

/**
 * @brief 日時→テキストコンバーター関数オブジェクトクラス
 */
// class TimeDateToTextConverter
//     : public TimeConverter
// {
// public:
//   /**
//    * @brief デフォルトコンストラクタ
//    */
//   TimeDateToTextConverter() : TimeConverter() {}

//   /**
//    * @brief コンストラクタ
//    * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
//    * @param tPtr TFMTラッパーの共有ポインタ
//    */
//   TimeDateToTextConverter(intl::SettingsPtr iPtr, TimeFormatPtr tPtr)
//     : TimeConverter(iPtr, tPtr)
//   {}

//   /**
//    * @brief 関数呼び出し
//    * @param pTimeDate 変換元日時へのポインタ
//    * @return フォーマット済み文字列
//    */
//   Lmbcs operator () (TIMEDATE *pTimeDate) const {
//     char buffer[MAXALPHATIMEDATE] = "";
//     WORD len = 0;
//     Status status = ConvertTIMEDATEToText(
//           !intlSettingsPtr_ ? nullptr : intlSettingsPtr_->data(),
//           !formatPtr_ ? nullptr : formatPtr_->data(),
//           pTimeDate,
//           buffer,
//           MAXALPHATIMEDATE,
//           &len
//           );
//     if (!status) { throw status; }
//     return Lmbcs(buffer, len);
//   }
// };

/**
 * @brief テキスト→日時コンバーター関数オブジェクトクラス
 */
// class TextToTimeDateConverter
//     : public TimeConverter
// {
// public:
//   /**
//    * @brief デフォルトコンストラクタ
//    */
//   TextToTimeDateConverter() : TimeConverter() {}

//   /**
//    * @brief コンストラクタ
//    * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
//    * @param tPtr TFMTラッパーの共有ポインタ
//    */
//   TextToTimeDateConverter(intl::SettingsPtr iPtr, TimeFormatPtr tPtr)
//     : TimeConverter(iPtr, tPtr)
//   {}

//   /**
//    * @brief 関数呼び出し
//    * @param lmbcs 変換元テキスト
//    * @return フォーマット済み文字列
//    */
//   TIMEDATE operator () (const Lmbcs &lmbcs) const {
//     char *ptr = const_cast<char*>(lmbcs.constData());
//     TIMEDATE td;
//     Status status = ConvertTextToTIMEDATE(
//           !intlSettingsPtr_ ? nullptr : intlSettingsPtr_->data(),
//           !formatPtr_ ? nullptr : formatPtr_->data(),
//           &ptr,
//           static_cast<WORD>(lmbcs.size()),
//           &td
//           );
//     if (!status) { throw status; }
//     return td;
//   }
// };

} // namespace npp2::converter

#endif // NPP2_CONVERTER_TIME_HPP
