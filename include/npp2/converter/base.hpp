#ifndef NPP2_CONVERTER_BASE_HPP
#define NPP2_CONVERTER_BASE_HPP

#include "intlformat.hpp"

namespace npp2::converter {

/**
 * @brief コンバート関数オブジェクトクラスのテンプレート
 * @tparam T フォーマット設定構造体(NFMT/TFMT)のラップクラス
 */
template <class T>
class Base
{
protected:
  IntlFormatPtr intlFormatPtr_; ///< i18nフォーマットオブジェクトの共有ポインタ
  std::unique_ptr<T> formatPtr_; ///< NFMT/TFMTラッパーの共有ポインタ

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Base() : intlFormatPtr_() , formatPtr_() {}

  /**
   * @brief コンストラクタ
   * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
   * @param tPtr NFMT/TFMTラッパーの共有ポインタ
   */
  Base(IntlFormatPtr &&iPtr, std::unique_ptr<T> &&tPtr)
    : intlFormatPtr_(std::move(iPtr))
    , formatPtr_(std::move(tPtr))
  {}

  /**
   * @brief i18nフォーマットオブジェクトの共有ポインタを設定
   * @param ptr i18nフォーマットオブジェクトの共有ポインタ
   */
  void setIntlSettings(IntlFormatPtr &&iPtr) {
    intlFormatPtr_.swap(iPtr);
  }

  /**
   * @brief NFMT/TFMTラッパーの共有ポインタを設定
   * @param ptr NFMT/TFMTラッパーの共有ポインタ
   */
  void setFormat(std::unique_ptr<T> &ptr) {
    formatPtr_.swap(ptr);
  }
};

} // namespace npp2::converter

#endif // NPP2_CONVERTER_BASE_HPP
