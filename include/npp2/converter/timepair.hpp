#ifndef NPP2_CONVERTER_TIMEPAIR_HPP
#define NPP2_CONVERTER_TIMEPAIR_HPP

#include "time.hpp"

namespace npp2::converter {

/**
 * @brief 数値ペア→テキストコンバーター関数オブジェクトクラス
 */
class NPP2_EXPORT TimeDatePairToTextConverter
    : public TimeToTextConverter
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  TimeDatePairToTextConverter();

  /**
   * @brief コンストラクタ
   * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
   * @param nPtr NFMTラッパーの共有ポインタ
   */
  TimeDatePairToTextConverter(IntlFormatPtr &&iPtr, format::TimePtr &&tPtr);

  /**
   * @brief 関数呼び出し
   * @param pPair 変換元数値ペアへのポインタ
   * @return フォーマット済み文字列
   */
  data::Lmbcs operator () (TIMEDATE_PAIR *pPair) const;
};

/**
 * @brief 日時ペア→テキストコンバーター関数オブジェクトクラス
 */
// class TimeDatePairToTextConverter
//     : public TimeConverter
// {
// public:
//   /**
//    * @brief デフォルトコンストラクタ
//    */
//   TimeDatePairToTextConverter() : TimeConverter() {}

//   /**
//    * @brief コンストラクタ
//    * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
//    * @param tPtr TFMTラッパーの共有ポインタ
//    */
//   TimeDatePairToTextConverter(intl::SettingsPtr iPtr, TimeFormatPtr tPtr)
//     : TimeConverter(iPtr, tPtr)
//   {}

//   /**
//    * @brief 関数呼び出し
//    * @param pPair 変換元日時ペアへのポインタ
//    * @return フォーマット済み文字列
//    */
//   Lmbcs operator () (TIMEDATE_PAIR *pTimeDate) const {
//     char buffer[MAXALPHATIMEDATEPAIR] = "";
//     WORD len = 0;
//     Status status = ConvertTIMEDATEPAIRToText(
//           !intlSettingsPtr_ ? nullptr : intlSettingsPtr_->data(),
//           !formatPtr_ ? nullptr : formatPtr_->data(),
//           pTimeDate,
//           buffer,
//           MAXALPHATIMEDATEPAIR,
//           &len
//           );
//     if (!status) { throw status; }
//     return Lmbcs(buffer, len);
//   }
// };

/**
 * @brief 日時ペア→テキストコンバーター関数オブジェクトクラス
 */
class NPP2_EXPORT TextToTimeDatePairConverter
    : public TimeConverter
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  TextToTimeDatePairConverter();

  /**
   * @brief コンストラクタ
   * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
   * @param tPtr TFMTラッパーの共有ポインタ
   */
  TextToTimeDatePairConverter(
    IntlFormatPtr &&iPtr,
    format::TimePtr &&tPtr
  );

  TIMEDATE_PAIR operator () (const data::Lmbcs &lmbcs) const;
};

} // namespace npp2::converter

#endif // NPP2_CONVERTER_TIMEPAIR_HPP
