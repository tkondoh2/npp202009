#ifndef NPP2_CONVERTER_NUMBERPAIR_HPP
#define NPP2_CONVERTER_NUMBERPAIR_HPP

#include "number.hpp"

namespace npp2::converter {

/**
 * @brief 数値ペア→テキストコンバーター関数オブジェクトクラス
 */
class NPP2_EXPORT NumberPairToTextConverter
    : public NumberToTextConverter
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  NumberPairToTextConverter();

  /**
   * @brief コンストラクタ
   * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
   * @param nPtr NFMTラッパーの共有ポインタ
   */
  NumberPairToTextConverter(IntlFormatPtr &&iPtr, format::NumberPtr &&nPtr);

  /**
   * @brief 関数呼び出し
   * @param pPair 変換元数値ペアへのポインタ
   * @return フォーマット済み文字列
   */
  data::Lmbcs operator () (NUMBER_PAIR *pPair) const;
};

} // namespace npp2::converter

#endif // NPP2_CONVERTER_NUMBERPAIR_HPP
