#ifndef NPP2_CONVERTER_EXTINTLFORMAT_HPP
#define NPP2_CONVERTER_EXTINTLFORMAT_HPP

#include "../data/lmbcs.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <intl.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp2::converter {

template <WORD BufferSize>
class ExtIntlFormat
{
  char buffer_[BufferSize + 1];

  template <char RequestType>
  data::Lmbcs _get(char index = 0) {
    WORD len = OSGetExtIntlFormat(RequestType, index, buffer_, BufferSize);
    return data::Lmbcs(buffer_, len);
  }

public:
  ExtIntlFormat(): buffer_() {}

  data::Lmbcs getNoneCalendarType() {
    return _get<CALENDARTYPE>(CALENDAR_NONE);
  }

  data::Lmbcs getJapanCalendarType() {
    return _get<CALENDARTYPE>(CALENDAR_JAPAN);
  }

  data::Lmbcs getTaiwanCalendarType() {
    return _get<CALENDARTYPE>(CALENDAR_TAIWAN);
  }

  data::Lmbcs getThaiCalendarType() {
    return _get<CALENDARTYPE>(CALENDAR_THAI);
  }

  data::Lmbcs getKoreaCalendarType() {
    return _get<CALENDARTYPE>(CALENDAR_KOREA);
  }

  data::Lmbcs getEraName(char index = 0) {
    return _get<ERANAME>(index);
  }

  data::Lmbcs getAbbreviatedEraName(char index = 0) {
    return _get<ABBRERANAME>(index);
  }

  data::Lmbcs getMonthName(char month) {
    return _get<MONTH_NAMES>(month);
  }

  data::Lmbcs getAbbreviatedMonthName(char month) {
    return _get<ABBR_MONTH_NAMES>(month);
  }

  data::Lmbcs getWeekdayName(char weekday) {
    return _get<WEEKDAY_NAMES>(weekday);
  }

  data::Lmbcs getAbbreviatedWeekdayName(char weekday) {
    return _get<ABBR_WEEKDAY_NAMES>(weekday);
  }

  data::Lmbcs getExtAM() {
    return _get<EXT_AM_STRING>();
  }

  data::Lmbcs getExtPM() {
    return _get<EXT_PM_STRING>();
  }

  data::Lmbcs getExtCurrency() {
    return _get<EXT_CURRENCY_STRING>();
  }
};

} // namespace npp2::converter

#endif // NPP2_CONVERTER_EXTINTLFORMAT_HPP
