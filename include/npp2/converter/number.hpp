#ifndef NPP2_CONVERTER_NUMBER_HPP
#define NPP2_CONVERTER_NUMBER_HPP

#include "../npp2_global.h"
#include "base.hpp"
#include "../format/number.hpp"
#include "../data/lmbcs.hpp"

namespace npp2::converter {

/**
 * @brief 数値コンバーター関数オブジェクトクラス
 */
class NPP2_EXPORT NumberConverter
    : public Base<format::Number>
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  NumberConverter();

  /**
   * @brief コンストラクタ
   * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
   * @param nPtr NFMTラッパーの共有ポインタ
   */
  NumberConverter(IntlFormatPtr &&iPtr, format::NumberPtr &&nPtr);
};

/**
 * @brief 数値→テキストコンバーター関数オブジェクトクラス
 */
class NPP2_EXPORT NumberToTextConverter
    : public NumberConverter
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  NumberToTextConverter();

  /**
   * @brief コンストラクタ
   * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
   * @param nPtr NFMTラッパーの共有ポインタ
   */
  NumberToTextConverter(IntlFormatPtr &&iPtr, format::NumberPtr &&nPtr);

  /**
   * @brief 関数呼び出し
   * @param pNum 変換元数値へのポインタ
   * @return フォーマット済み文字列
   */
  data::Lmbcs operator () (NUMBER *pNum) const;
};

/**
 * @brief テキスト→数値コンバーター関数オブジェクトクラス
 */
class NPP2_EXPORT TextToNumberConverter
    : public NumberConverter
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  TextToNumberConverter();

  /**
   * @brief コンストラクタ
   * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
   * @param nPtr NFMTラッパーの共有ポインタ
   */
  TextToNumberConverter(IntlFormatPtr &&iPtr, format::NumberPtr &&nPtr);

  /**
   * @brief 関数呼び出し
   * @param lmbcs 変換元テキスト
   * @return フォーマット済み文字列
   */
  NUMBER operator () (const data::Lmbcs &lmbcs) const;
};

} // namespace npp2::converter

#endif // NPP2_CONVERTER_NUMBER_HPP
