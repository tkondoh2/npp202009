#ifndef NPP2_IDTABLE_HPP
#define NPP2_IDTABLE_HPP

#include "npp2_global.h"
#include "_private/handleobject.hpp"
#include "status.hpp"
#include <memory>
#include <functional>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfnote.h>
#include <idtable.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

class IdTable;
using IdTablePtr = std::unique_ptr<IdTable>;

struct NPP2_EXPORT IDDestroyTableDeleter {
  void operator ()(DHANDLE handle);
};

class NPP2_EXPORT IdTable
  : public LockableObject2<DHANDLE, IDDestroyTableDeleter>
{
public:
  IdTable(DHANDLE handle);

  virtual ~IdTable();

  template <class T = IdTable>
  static std::unique_ptr<T> create() {
    DHANDLE handle = NULLHANDLE;
    Status status = IDCreateTable(sizeof(NOTEID), &handle);
    if (!status) { throw status; }
    return std::make_unique<T>(handle);
  }

  DWORD count() const;

  bool isEmpty() const;

  void scan(const std::function<bool(NOTEID)> &fn) const;

  bool insert(NOTEID id);

  bool remove(NOTEID id);

  bool contains(NOTEID id) const;

  template <class T = IdTable>
  std::unique_ptr<T> copy() const;

  // void copyToFolder();
};

} // namespace npp2

#endif // NPP2_IDTABLE_HPP
