#ifndef NPP2_UTILS_UNID_HPP
#define NPP2_UTILS_UNID_HPP

#include "../npp2_global.h"
#include <QByteArray>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfdata.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

class NPP2_EXPORT UniversalNoteId
{
  UNID value_;

public:
  UniversalNoteId();

  UniversalNoteId(const UNID &value);

  operator UNID() const;

  QByteArray toString(bool withColon = false) const;
};

using Unid = UniversalNoteId;

} // namespace npp2

#endif // NPP2_UTILS_UNID_HPP
