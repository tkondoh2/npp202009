#ifndef NPP2_UTILS_PROCESS_HPP
#define NPP2_UTILS_PROCESS_HPP

#include "../npp2_global.h"
#include <QByteArray>
#include <QFileInfo>

namespace npp2 {

QByteArray NPP2_EXPORT getProcessFilePath() noexcept;

QFileInfo NPP2_EXPORT getProcessFileInfo() noexcept;

} // namespace npp2

#endif // NPP2_UTILS_PROCESS_HPP
