#ifndef NPP2_UTILS_INDEX_HPP
#define NPP2_UTILS_INDEX_HPP

#include "../npp2_global.h"

#include <QString>
#include <QPair>
#include <QMap>
#include <QList>
#include <QUrl>
#include <functional>

namespace npp2::utils {

/**
 * @brief キーバリューペアリスト型をバリアントマップ型に変換する
 */
template <class K, class V>
QMap<K, V> pairListToMap(QList<QPair<K, V>> pairs) noexcept {
  QMap<K, V> map;
  foreach (auto pair, pairs) {
    map.insert(pair.first, pair.second);
  }
  return map;
}

template <class T, class U>
QList<U> map(const QList<T> &list, std::function<U(const T &)> fn) {
  QList<U> result;
  foreach (auto item, list) {
    result += fn(item);
  }
  return result;
}

/**
 * @brief 複数のマップキーが含まれているかAnd合算で返す
 * @tparam T マップ型
 * @param map マップ値
 * @param keys キーリスト
 * @return true キーリストすべてがマップに含まれている
 * @return false キーリストのいずれかがマップにない
 */
template <typename K, typename V>
bool mapContainsAnd(const QMap<K, V> &map, const QList<K> &keys) {
  int count = 0;
  foreach (auto key, keys) {
    count += (map.contains(key) ? 1 : 0);
  }
  return keys.count() == count;
}

void NPP2_EXPORT addQueryItem(
  QUrl &url,
  const QString &key,
  const QString &value
);

template <typename T>
bool some(const QList<T> &list, std::function<bool(T)> fn) {
  foreach (auto item, list) {
    if (fn(item)) return true;
  }
  return false;
}

template <typename T>
bool every(const QList<T> &list, std::function<bool(T)> fn) {
  foreach (auto item, list) {
    if (!fn(item)) return false;
  }
  return true;
}

/**
 * @brief 条件別に関数を実行する。
 * @tparam T 実行関数共通の戻り値型
 * @param condition 条件結果
 * @param thenFn 条件がtrueの時に実行する関数
 * @param elseFn 条件がfalseの時に実行する関数
 * @return T 戻り値
 */
template <typename T>
T iif(
  bool condition,
  std::function<T()> thenFn,
  std::function<T()> elseFn
) {
  return condition ? thenFn() : elseFn();
}

/**
 * @brief 配列から条件式に合致するアイテムのみの配列を取得する。
 * @tparam T 配列アイテム型
 * @param list 配列
 * @param matchFn 条件式
 * @return QList<T> 条件に合致したアイテムのみの配列
 */
template <typename T>
QList<T> filter(
  const QList<T> &list,
  std::function<bool(const T &)> matchFn
) {
  QList<T> newList;
  foreach (auto item, list) {
    if (matchFn(item)) {
      newList.append(item);
    }
  }
  return newList;
}

} // namespace npp2::utils

#endif // NPP2_UTILS_INDEX_HPP
