#ifndef NPP2_UTILS_RENAMER_COUNT_HPP
#define NPP2_UTILS_RENAMER_COUNT_HPP

#include "index.hpp"
#include <functional>

namespace npp2 {

using CountRenameFunc = std::function<QString(QString,int)>;

/**
 * @brief カウント形式のリネーム処理クラス
 */
class NPP2_EXPORT CountRenamer
  : public Renamer
{
  int maxCount_; ///< 最大カウント数
  CountRenameFunc fn_; ///< フォーマット式

public:
  /**
   * @brief コンストラクタ
   * @param format リネーム元テンプレート
   * @param maxCount 最大カウント数
   * @param fn フォーマット式(デフォルトあり)
   */
  CountRenamer(
    const QString &format,
    int maxCount,
    CountRenameFunc fn = [](QString s, int count) {
      s.replace("${count}", QString::number(count));
      return s;
    }
  );

  /**
   * @brief デストラクタ
   */
  virtual ~CountRenamer() override;

protected:
  /**
   * @brief リネームパスを作成
   * @param filePath リネームするファイルのパス
   * @return QString リネームパス
   */
  virtual QString renamePath(const QString &filePath) const override;
};

} // namespace npp2

#endif // NPP2_UTILS_RENAMER_COUNT_HPP
