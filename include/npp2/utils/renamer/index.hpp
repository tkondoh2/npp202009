#ifndef NPP2_UTILS_RENAMER_INDEX_HPP
#define NPP2_UTILS_RENAMER_INDEX_HPP

#include "../../npp2_global.h"
#include <QSharedPointer>
#include <QString>

namespace npp2 {

class Renamer;
using RenamerPtr = QSharedPointer<Renamer>;

/**
 * @brief リネーム制御インターフェース
 */
class NPP2_EXPORT Renamer
{
protected:
  QString format_; ///< リネーム元テンプレート

public:
  /**
   * @brief コンストラクタ
   * @param format リネーム元テンプレート
   */
  Renamer(const QString &format);

  /**
   * @brief デストラクタ
   */
  virtual ~Renamer();

  /**
   * @brief リネーム処理をする。
   * @param filePath リネームするファイルのパス
   */
  void operator ()(const QString &filePath) const;

protected:
  /**
   * @brief リネームパスを作成
   * @param filePath リネームするファイルのパス
   * @return QString リネームパス
   */
  virtual QString renamePath(const QString &filePath) const = 0;

  /**
   * @brief 指定パスのディレクトリがない時に作成する。
   * @param path パス
   */
  static void makeDir(const QString &path);
};

} // namespace npp2

#endif // NPP2_UTILS_RENAMER_INDEX_HPP
