#ifndef NPP2_UTILS_RENAMER_TIMESTAMP_HPP
#define NPP2_UTILS_RENAMER_TIMESTAMP_HPP

#include "index.hpp"
#include <QDateTime>
#include <functional>

namespace npp2 {

using TimestampRenameFunc = std::function<QString(QString,QDateTime)>;

/**
 * @brief タイムスタンプ形式のリネーム処理クラス
 */
class NPP2_EXPORT TimestampRenamer
    : public Renamer
{
  TimestampRenameFunc fn_; ///< フォーマット式

public:
  /**
   * @brief コンストラクタ
   * @param format リネーム元テンプレート
   * @param fn フォーマット式(デフォルトあり)
   */
  TimestampRenamer(
    const QString &format,
    TimestampRenameFunc fn = [](QString s, const QDateTime &dt) {
      auto x = dt.toString("yyyy,MM,dd,HH,mm,ss,zzz").split(",");
      s.replace("${year}", x[0])
        .replace("${mon}", x[1])
        .replace("${date}", x[2])
        .replace("${hour}", x[3])
        .replace("${min}", x[4])
        .replace("${sec}", x[5])
        .replace("${msec}", x[6]);
      return s;
    }
  );

  /**
   * @brief デストラクタ
   */
  virtual ~TimestampRenamer() override;

protected:
  /**
   * @brief リネームパスを作成
   * @param filePath リネームするファイルのパス
   * @return QString リネームパス
   */
  virtual QString renamePath(const QString &) const override;
};

} // namespace npp2

#endif // NPP2_UTILS_RENAMER_TIMESTAMP_HPP
