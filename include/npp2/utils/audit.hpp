#ifndef NPP2_UTILS_AUDIT_HPP
#define NPP2_UTILS_AUDIT_HPP

#include "../npp2_global.h"

#include <QString>
#include <functional>

namespace npp2::utils {

class Auditor
{
  std::function<void(const QString&)> fn_;

public:
  explicit Auditor(std::function<void(const QString&)> fn)
    : fn_(fn)
  {}

  void audit(const QString &message) const {
    fn_(message);
  }
};

} // namespace npp2::utils

#endif // NPP2_UTILS_AUDIT_HPP
