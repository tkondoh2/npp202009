#ifndef NPP2_UTILS_WRITER_INDEX_HPP
#define NPP2_UTILS_WRITER_INDEX_HPP

#include <QSharedPointer>

namespace npp2 {

/**
 * @brief ストリームライターテンプレートクラス
 * @tparam T ストリームクラス
 */
template <class T>
class _Writer
{
protected:
  QSharedPointer<T> pStream_; ///< ストリーム

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  _Writer()
    : pStream_()
  {}

  /**
   * @brief デストラクタ
   */
  virtual ~_Writer()
  {}

  /**
   * @return T& ストリーム参照を返す。
   */
  virtual T &operator ()() {
    return *pStream_;
  }

  /**
   * @brief ストリームを閉じる。
   */
  virtual void close()
  {}

protected:
  const QSharedPointer<T> &pStream() const {
    return pStream_;
  }
};

} // namespace npp2

#endif // NPP2_UTILS_WRITER_INDEX_HPP
