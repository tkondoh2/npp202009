#ifndef NPP2_UTILS_WRITER_FILEWRITER_HPP
#define NPP2_UTILS_WRITER_FILEWRITER_HPP

#include "index.hpp"
#include "../audit.hpp"
#include "../rotator/size.hpp"
#include "../renamer/count.hpp"
#include <QString>
#include <QFileInfo>
#include <QDir>

namespace npp2 {

/**
 * @brief ファイルライターテンプレートクラス
 * @tparam T ストリームクラス
 */
template <class T>
class _FileWriter
  : public _Writer<T>
  , public utils::Auditor
{
protected:
  QString filePath_; ///< ファイルパス
  RotatorPtr pRotate_; ///< ローテーション制御
  RenamerPtr pRename_; ///< リネーム制御

public:
  /**
   * @brief コンストラクタ
   * @param filePath ファイルパス
   * @param fn 監査用関数
   */
  _FileWriter(
    const QString &filePath,
    std::function<void(const QString&)> fn
  )
    : _Writer<T>()
    , utils::Auditor(fn)
    , filePath_(filePath)
    , pRotate_(new SizeRotator(1024 * 1024)) // 1MiB
    , pRename_(new CountRenamer("", 100))
  {}

  /**
   * @return ローテーション制御を返す。
   */
  RotatorPtr &pRotate() {
    return pRotate_;
  }

  /**
   * @return リネーム制御を返す。
   */
  RenamerPtr &pRename() {
    return pRename_;
  }

  /**
   * @brief ファイルを開く。
   * @return true 成功
   * @return false 失敗
   */
  virtual bool open() = 0;

protected:
  /**
   * @brief ファイルオープン後に実行する処理
   */
  virtual void afterOpenFile(bool emptyFile) = 0;

public:
  /**
   * @brief ファイルは開いているか
   * @return true 開いている
   * @return false 閉じている
   */
  virtual bool isOpen() const = 0;

  /**
   * @return qint64 ファイルサイズを返す。
   */
  virtual qint64 size() const = 0;
  const QString &filePath() const { return filePath_; }

  /**
   * @brief ファイルを閉じる。
   */
  virtual void close() = 0;

  /**
   * @brief ローテーション条件処理後にストリーム参照を返す。
   * @return T& ストリーム参照
   */
  virtual T &operator ()() override {
    // ファイルパスが設定されているか？
    if (!filePath_.isEmpty()) {
      // ローテーションの条件に当てはまるか
      // 当てはまればファイルをローテーションする
      // ローテーション/1 ファイルが開いていれば閉じる
      // ローテーション/2 ファイル名をリネームする
      // ローテーション/3 ログファイルの先頭に必要な処理があれば初期化

      // ファイルが存在し、ローテーション条件に当てはまるか？
      QFileInfo fileInfo(filePath_);
      auto emptyFile = true;

      // ファイルがあってサイズが0以上
      if (fileInfo.exists() && fileInfo.size() > 0) {
        if ((*pRotate_)(filePath_)) {

          // ファイルを閉じて、リネーム処理をする。
          close();
          (*pRename_)(filePath_);
        }
        else {
          emptyFile = false;
        }
      }

      // ファイルは未オープンか？
      if (!isOpen()) {

        // ファイルまでのパスは存在するか？
        auto dir = fileInfo.dir();
        if (!dir.exists()) {

          // ファイルまでのパスを構築する。
          dir.mkpath(dir.path());
        }

        // ファイルをオープンする
        if (open()) {

          // 成功したら行う処理をする。
          afterOpenFile(emptyFile);
        }
      }
    }
    // ストリーム参照を返す。
    return *this->pStream_;
  }
};

} // namespace npp2

#endif // NPP2_UTILS_WRITER_FILEWRITER_HPP
