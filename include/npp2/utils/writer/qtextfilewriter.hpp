#ifndef NPP2_UTILS_WRITER_QTEXTFILEWRITER_HPP
#define NPP2_UTILS_WRITER_QTEXTFILEWRITER_HPP

#include "../../npp2_global.h"
#include "qfilewriter.hpp"
#include <QTextStream>
#include <QByteArray>
#include <QTextCodec>

namespace npp2 {

/**
 * @brief QTextStreamを使ったライタークラス
 */
class NPP2_EXPORT QTextFileWriter
  : public _QFileWriter<QTextStream>
{
protected:
  QByteArray codecName_; ///< コーデック名
  bool bom_; ///< BOMの有無

public:
  /**
   * @brief コンストラクタ
   * @param filePath ファイルパス
   * @param fn 監査用関数
   * @param codecName コーデック名(デフォルトUTF-8)
   * @param bom BOMの有無(デフォルトなし)
   */
  QTextFileWriter(
    const QString &filePath,
    std::function<void(const QString&)> fn,
    const QByteArray &codecName = "UTF-8",
    bool bom = false
  );

  const QByteArray &codecName() const { return codecName_; }
  const bool bom() const { return bom_; }

protected:
  /**
   * @return QTextCodec* コーデックへのポインタを返す。
   */
  QTextCodec *codecPtr() const;

  /**
   * @brief ファイルオープン後に実行する処理
   * ストリームに対し、コーデック設定、BOM設定を追加する。
   */
  virtual void afterOpenFile(bool emptyFile) override;
};

} // namespace npp2

#endif // NPP2_UTILS_WRITER_QTEXTFILEWRITER_HPP
