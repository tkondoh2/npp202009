#ifndef NPP2_UTILS_WRITER_STDFILEWRITER_HPP
#define NPP2_UTILS_WRITER_STDFILEWRITER_HPP

#include "../../npp2_global.h"
#include "filewriter.hpp"
#include <iostream>
#include <fstream>

namespace npp2 {

using StdStream = std::ofstream;

/**
 * @brief 標準ファイル出力ストリームを使ったライタークラス
 */
class NPP2_EXPORT StdFileWriter
  : public _FileWriter<StdStream>
{
public:
  /**
   * @brief コンストラクタ
   * @param filePath ファイルパス
   * @param fn 監査用関数
   */
  StdFileWriter(
    const QString &filePath,
    std::function<void(const QString&)> fn
  );

  /**
   * @brief ファイルを開く。
   * @return true 成功
   * @return false 失敗
   */
  virtual bool open() override;

  /**
   * @brief ファイルは開いているか
   * @return true 開いている
   * @return false 閉じている
   */
  virtual bool isOpen() const override;

  /**
   * @return qint64 ファイルサイズを返す。
   */
  virtual qint64 size() const override;

  /**
   * @brief ファイルを閉じる。
   */
  virtual void close() override;

  /**
   * @brief ファイルオープン後に実行する処理
   */
  virtual void afterOpenFile(bool) override;
};

} // namespace npp2

#endif // NPP2_UTILS_WRITER_STDFILEWRITER_HPP
