#ifndef NPP2_UTILS_WRITER_QDATAFILEWRITER_HPP
#define NPP2_UTILS_WRITER_QDATAFILEWRITER_HPP

#include "../../npp2_global.h"
#include "qfilewriter.hpp"
#include <QDataStream>

namespace npp2 {

/**
 * @brief QDataStreamを使ったライタークラス
 */
class NPP2_EXPORT QDataFileWriter
  : public _QFileWriter<QDataStream>
{
public:
  /**
   * @brief コンストラクタ
   * @param filePath ファイルパス
   * @param fn 監査用関数
   */
  QDataFileWriter(
    const QString &filePath,
    std::function<void(const QString&)> fn
  );

  /**
   * @brief ファイルオープン後に実行する処理
   */
  virtual void afterOpenFile(bool) override;
};

} // namespace npp2

#endif // NPP2_UTILS_WRITER_QDATAFILEWRITER_HPP
