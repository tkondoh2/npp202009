#ifndef NPP2_UTILS_WRITER_QFILEWRITER_HPP
#define NPP2_UTILS_WRITER_QFILEWRITER_HPP

#include "filewriter.hpp"
#include <QFile>

using QFilePtr = QSharedPointer<QFile>;

namespace npp2 {

/**
 * @brief QFileを使ったライターテンプレートクラス
 * @tparam T ストリームクラス
 */
template <class T>
class _QFileWriter
  : public _FileWriter<T>
{
protected:
  QFilePtr pFile_; ///< ファイル

public:
  /**
   * @brief コンストラクタ
   * @param filePath ファイルパス
   * @param fn 監査用関数
   */
  _QFileWriter(
    const QString &filePath,
    std::function<void(const QString&)> fn
  )
    : _FileWriter<T>(filePath, fn)
    , pFile_(new QFile)
  {}

  /**
   * @brief ファイルを開く。
   * @return true 成功
   * @return false 失敗
   */
  virtual bool open() override {
    pFile_.reset(new QFile(this->filePath_));
    if (pFile_->open(QFile::Text | QFile::WriteOnly | QFile::Append)) {
      this->pStream_.reset(new T(pFile_.data()));
      return true;
    }
    return false;
  }

  /**
   * @brief ファイルは開いているか
   * @return true 開いている
   * @return false 閉じている
   */
  virtual bool isOpen() const override {
    return pFile_ && pFile_->isOpen();
  }

  /**
   * @return qint64 ファイルサイズを返す。
   */
  virtual qint64 size() const override {
    // return pFile_->size();
    QFileInfo info(filePath_);
    return info.size();
  }

  /**
   * @brief ファイルを閉じる。
   */
  virtual void close() override {
    if (isOpen()) {
      pFile_->close();
      this->pStream_.reset();
      pFile_.reset();
    }
  }
};

} // namespace npp2

#endif // NPP2_UTILS_WRITER_QFILEWRITER_HPP
