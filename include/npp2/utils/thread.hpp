#ifndef NPP2_UTILS_THREAD_HPP
#define NPP2_UTILS_THREAD_HPP

#include "../npp2_global.h"
#include <QString>
// #include <sstream>
#include <thread>

namespace npp2 {

QString NPP2_EXPORT threadIdToText(const std::thread::id &id);

} // namespace npp2

#endif // NPP2_UTILS_THREAD_HPP
