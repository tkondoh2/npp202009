#ifndef NPP2_UTILS_DATA_HPP
#define NPP2_UTILS_DATA_HPP

#if defined(NT)
#pragma pack(push, 1)
#endif

#include "global.h"

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

template <typename T>
T *dataptr(void *ptr, BOOL fPrefix) {
  return reinterpret_cast<T*>(
    reinterpret_cast<WORD*>(ptr) + (fPrefix ? 1 : 0)
  );
}

} // namespace npp2

#endif // NPP2_UTILS_DATA_HPP
