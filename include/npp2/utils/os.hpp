#ifndef NPP2_UTILS_OS_HPP
#define NPP2_UTILS_OS_HPP

#include "../data/lmbcs.hpp"
#include <QString>

namespace npp2::os {

template <class T> T unifySep(T path) {
  path.replace("\\", "/");
  return path;
}

} // namespace npp2::os

#endif // NPP2_UTILS_OS_HPP
