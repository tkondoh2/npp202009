#ifndef NPP2_UTILS_FUNCTION_HPP
#define NPP2_UTILS_FUNCTION_HPP

#include <QString>
#include <QStringList>
#include <QByteArrayList>

/**
 * @brief @式に関するユーティリティ
 */
namespace npp2::function {

/**
 * @brief 文字列リストを@関数文字列リストリテラルに変換
 * @param list 文字列リスト
 * @return @関数文字列リストリテラル
 */
template <class T>
T toStringList(const QList<T> &list) {
  QList<T> result;
  const T quot(R"(")");
  foreach (T item, list) {
    item.replace(quot, R"(\")");
    result.append(quot + item + quot);
  }
  return result.join(':');
}

/**
 * @brief @式に含まれるバックスラッシュをエスケープする
 * @return エスケープ済み@式文字列
 */
template <class T>
T escape(const T &text) {
  T result(text);
  result.replace(R"(\)", R"(\\)");
  return result;
}

template <class T>
T sanitize(const T &text) {
  T result(text);
  result.replace("\"", "\\\"");
  return result;
}

} // namespace npp2::function

#endif // NPP2_UTILS_FUNCTION_HPP
