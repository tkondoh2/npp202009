#ifndef NPP2_UTILS_HEXINT_HPP
#define NPP2_UTILS_HEXINT_HPP

#include <QByteArray>

namespace npp2 {

template <typename In, typename CastTo>
QByteArray formatHexInt(In n) {
  size_t w = sizeof(In) * 2; // 1バイト当たり2文字計算
  QByteArray s = QByteArray("0").repeated(w - 1);
  s += QByteArray::number(static_cast<CastTo>(n), 16);
  return s.right(w).toUpper();
}

} // namespace npp2

#endif // NPP2_UTILS_HEXINT_HPP
