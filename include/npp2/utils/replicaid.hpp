#ifndef NPP2_UTILS_REPLICAID_HPP
#define NPP2_UTILS_REPLICAID_HPP

#include "../npp2_global.h"
#include <QByteArray>
// #include "hexint.hpp"
// #include "time.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfdata.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

class NPP2_EXPORT ReplicaId
{
  DBID value_;

public:
  ReplicaId();

  ReplicaId(const DBREPLICAINFO &repInfo);

  operator DBID() const;

  QByteArray toString() const;
};

} // namespace npp2

#endif // NPP2_UTILS_REPLICAID_HPP
