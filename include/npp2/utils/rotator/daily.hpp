#ifndef NPP2_UTILS_ROTATOR_DAILY_HPP
#define NPP2_UTILS_ROTATOR_DAILY_HPP

#include "index.hpp"

namespace npp2 {

/**
 * @brief 日次判定型ローテーションクラス
 * 
 */
class NPP2_EXPORT DailyRotator
  : public Rotator
{
public:
  /**
   * @brief コンストラクタ
   */
  DailyRotator();

  /**
   * @brief デストラクタ
   */
  virtual ~DailyRotator() override;

  /**
   * @brief ローテーション判定を実行する
   * @param filePath ファイルパス
   * @return true ファイルの最終更新日が昨日以前
   * @return false ファイルの最終更新日が今日
   */
  virtual bool operator ()(const QString &filePath) override;
};

} // namespace npp2

#endif // NPP2_UTILS_ROTATOR_DAILY_HPP
