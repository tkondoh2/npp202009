#ifndef NPP2_UTILS_ROTATOR_SIZE_HPP
#define NPP2_UTILS_ROTATOR_SIZE_HPP

#include "index.hpp"

namespace npp2 {

/**
 * @brief サイズ判定型ローテーションクラス
 */
class NPP2_EXPORT SizeRotator
  : public Rotator
{
  qint64 limit_; ///< 上限サイズ

public:
  /**
   * @brief コンストラクタ
   * @param limit 上限サイズ
   */
  SizeRotator(qint64 limit);

  /**
   * @brief デストラクタ
   */
  virtual ~SizeRotator() override;

  /**
   * @return qint64 設定済み上限サイズ
   */
  qint64 limit() const;

  /**
   * @brief ローテーション判定を実行する
   * @param filePath ファイルパス
   * @return true ファイルが上限サイズに達している
   * @return false ファイルが上限サイズに達していない
   */
  virtual bool operator ()(const QString &filePath) override;
};

} // namespace npp2

#endif // NPP2_UTILS_ROTATOR_SIZE_HPP
