#ifndef NPP2_UTILS_ROTATOR_INDEX_HPP
#define NPP2_UTILS_ROTATOR_INDEX_HPP

#include "../../npp2_global.h"
#include <QSharedPointer>

namespace npp2 {

/**
 * @brief ローテーション制御インターフェース
 */
class NPP2_EXPORT Rotator
{
public:
  /**
   * @brief コンストラクタ
   */
  Rotator();

  /**
   * @brief デストラクタ
   */
  virtual ~Rotator();

  /**
   * @brief ローテーション判定を実行する
   * @param filePath ファイルパス
   * @return true ローテーションの条件に合っている
   * @return false ローテーションの条件に合っていない
   */
  virtual bool operator ()(const QString &filePath) = 0;
};

using RotatorPtr = QSharedPointer<Rotator>;

} // namespace npp2

#endif // NPP2_UTILS_ROTATOR_INDEX_HPP
