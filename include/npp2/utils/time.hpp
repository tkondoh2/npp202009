#ifndef NPP2_UTILS_TIME_HPP
#define NPP2_UTILS_TIME_HPP

#include "../npp2_global.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

TIMEDATE NPP2_EXPORT getMinTimeDate();

TIMEDATE NPP2_EXPORT getMaxTimeDate();

} // namespace npp2

#endif // NPP2_UTILS_TIME_HPP
