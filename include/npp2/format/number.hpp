#ifndef NPP2_FORMAT_NUMBER_HPP
#define NPP2_FORMAT_NUMBER_HPP

#include "../npp2_global.h"
#include <memory>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <misc.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp2::format {

/**
 * @brief 数値フォーマットクラス
 */
class NPP2_EXPORT Number
{
  NFMT value_;

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Number();

  /**
   * @brief コンストラクタ
   * @param digits 小数点以下の桁数
   * @param format フォーマット値(NFMT_xxx)
   * @param attributes フォーマット属性(NATTR_xxx)
   */
  Number(BYTE digits, BYTE format, BYTE attributes);

  /**
   * @return NFMTへのポインタ
   */
  NFMT *data();

  /**
   * @return 小数点以下の桁数
   */
  BYTE digits() const;

  /**
   * @param v 設定する小数点以下の桁数
   */
  void setDigits(BYTE v);

  /**
   * @return フォーマット値(NFMT_xxx)
   */
  BYTE format() const;

  /**
   * @param v 設定するフォーマット値(NFMT_xxx)
   */
  void setFormat(BYTE v);

  /**
   * @brief 属性値から該当属性の真偽を調べる
   * @param A 属性
   * @return 属性が真ならtrue
   */
  bool isAttribute(WORD A) const;

  /**
   * @brief 属性値の真偽を設定する
   * @param A 属性
   * @param a 真にするならtrue
   */
  void setAttribute(WORD A, bool a);

  /**
   * @return 3桁ごとに分離記号を付けるか
   */
  bool isPunctuated() const;

  /**
   * @return 負のときに括弧を付けるか
   */
  bool isParen() const;

  /**
   * @return パーセント表示をするか
   */
  bool isPercent() const;

  /**
   * @return 小数点位置を可変にするか
   */
  bool isVarying() const;

  /**
   * @return バイト表示するか
   */
  bool isBytes() const;

  /**
   * @param a 3桁ごとに分離記号を付けるかを設定
   */
  void setPunctuated(bool a);

  /**
   * @param a 負のときに括弧を付けるかを設定
   */
  void setParen(bool a);

  /**
   * @param a パーセント表示をするかを設定
   */
  void setPercent(bool a);

  /**
   * @param a 小数点位置を可変にするかを設定
   */
  void setVarying(bool a);

  /**
   * @param a バイト表示するかを設定
   */
  void setBytes(bool a);
};

using NumberPtr = std::unique_ptr<Number>;

} // namespace npp2::format

#endif // NPP2_FORMAT_NUMBER_HPP
