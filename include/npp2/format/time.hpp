#ifndef NPP2_FORMAT_TIME_HPP
#define NPP2_FORMAT_TIME_HPP

#include "../npp2_global.h"
#include <memory>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <misc.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp2::format {

/**
 * @brief 日時フォーマットクラス
 */
class NPP2_EXPORT Time
{
  TFMT value_;

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Time();

  /**
   * @brief コンストラクタ
   * @param date 日付フォーマット値(TDFMT_xxx)
   * @param time 時刻フォーマット値(TTFMT_xxx)
   * @param zone タイムゾーンフォーマット値(TZFMT_xxx)
   * @param structure 構成フォーマット値(TSFMT_xxx)
   */
  Time(BYTE date, BYTE time, BYTE zone, BYTE structure);

  /**
   * @return TFMTへのポインタ
   */
  TFMT *data();

  /**
   * @return 日付フォーマット
   */
  BYTE date() const;

  /**
   * @return 時刻フォーマット
   */
  BYTE time() const;

  /**
   * @return タイムゾーンフォーマット
   */
  BYTE zone() const;

  /**
   * @return 構成フォーマット
   */
  BYTE structure() const;

  /**
   * @param v 設定する日付フォーマット値
   */
  void setDate(BYTE v);

  /**
   * @param v 設定する時刻フォーマット値
   */
  void setTime(BYTE v);

  /**
   * @param v 設定するタイムゾーンフォーマット値
   */
  void setZone(BYTE v);

  /**
   * @param v 設定する構成フォーマット値
   */
  void setStructure(BYTE v);
};

using TimePtr = std::unique_ptr<Time>;

} // namespace npp2::format

#endif // NPP2_FORMAT_TIME_HPP
