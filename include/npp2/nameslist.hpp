#ifndef NPP2_NAMESLIST_HPP
#define NPP2_NAMESLIST_HPP

#include "npp2_global.h"
#include "_private/handleobject.hpp"
#include <memory>
#include "data/lmbcs.hpp"
// #include "status.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
// #include <nsfdb.h>
// #include <acl.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

class NamesList;
using NamesListPtr = std::unique_ptr<NamesList>;

class NamesList
  : public LockableObject3<DHANDLE>
{
public:
  NamesList();
  
  NamesList(DHANDLE handle);

  virtual ~NamesList();

  static NamesListPtr build(const data::Lmbcs &name);
};

} // namespace npp2

#endif // NPP2_NAMESLIST_HPP
