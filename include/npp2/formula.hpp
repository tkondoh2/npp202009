#ifndef NPP2_FORMULA_HPP
#define NPP2_FORMULA_HPP

#include "npp2_global.h"
#include "_private/handleobject.hpp"
#include "status.hpp"
#include "data/lmbcs.hpp"
#include <memory>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfsearc.h>
// #include <nsferr.h>
// #include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

class _Formula;
using Formula = std::unique_ptr<_Formula>;

class NPP2_EXPORT _Formula
    : public LockableObject3<FORMULAHANDLE>
{

public:
  _Formula();

  _Formula(FORMULAHANDLE handle);

  virtual ~_Formula();

  class NPP2_EXPORT CompileError
    : public Status
  {
    WORD line_;
    WORD col_;
    WORD offset_;
    WORD length_;

  public:
    CompileError();

    CompileError(STATUS status, WORD line, WORD col, WORD offset, WORD length);

    WORD line() const;

    WORD col() const;

    WORD offset() const;

    WORD length() const;
  };

  static Formula compile(
    const data::Lmbcs &query,
    const data::Lmbcs &column = ""
  ) noexcept(false);
};

} // namespace npp2

#endif // NPP2_FORMULA_HPP
