#ifndef NPP2_ATTACHMENTITEM_HPP
#define NPP2_ATTACHMENTITEM_HPP

#include "item.hpp"

namespace npp2 {

class NPP2_EXPORT AttachmentItem
  : public Item
{
  OBJECT_DESCRIPTOR header_;	/* object header */
  WORD fileNameLength_;
  WORD hostType_;		 		/* identifies type of text file delimeters (HOST_) */
  WORD compressionType_;		/* compression technique used (COMPRESS_) */
  WORD fileAttributes_;		/* original file attributes (ATTRIB_) */
  WORD flags_;					/* miscellaneous flags (FILEFLAG_, ENCODE_) */
  DWORD fileSize_;				/* original file size */
  TIMEDATE fileCreated_;		/* original file date/time of creation, 0 if unknown */
  TIMEDATE fileModified_;		/* original file date/time of modification */
  data::Lmbcs fileName_;

public:
  AttachmentItem();

  AttachmentItem(
    BLOCKID itemId,
    WORD dataType,
    BLOCKID valueId,
    DWORD valueSize
  );

  OBJECT_DESCRIPTOR header() const;
  WORD fileNameLength() const;
  WORD hostType() const;
  WORD compressionType() const;
  WORD fileAttributes() const;
  WORD flags() const;
  DWORD fileSize() const;
  TIMEDATE fileCreated() const;
  TIMEDATE fileModified() const;
  data::Lmbcs fileName() const;
};

} // namespace npp2

#endif // NPP2_ATTACHMENTITEM_HPP
