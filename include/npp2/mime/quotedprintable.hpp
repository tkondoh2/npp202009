#ifndef NPP2_MIME_QUOTEDPRINTABLE_HPP
#define NPP2_MIME_QUOTEDPRINTABLE_HPP

#include "../npp2_global.h"
#include <QByteArray>

namespace npp2::mime {

class NPP2_EXPORT QuotedPrintable
{
public:
  static QByteArray encode(const QByteArray &latin1) noexcept;

  static QByteArray decode(const QByteArray &org_input) noexcept;
};

} // namespace npp2::mime

#endif // NPP2_MIME_QUOTEDPRINTABLE_HPP
