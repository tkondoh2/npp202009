#ifndef NPP2_MIME_CODEC_HPP
#define NPP2_MIME_CODEC_HPP

#include "../npp2_global.h"
#include <QString>
#include <QByteArray>
#include <optional>

using QOptionalString = std::optional<QString>;

namespace npp2::mime {

class NPP2_EXPORT Codec
{
  QString pattern_;

public:
  Codec(const QString &pattern);

  QOptionalString operator ()(const QByteArray &from) const;

  template <typename T>
  QOptionalString to(const QByteArray &from) const {
    if (T::compare(pattern_)) {
      return T::codec(from);
    }
    else {
      return std::nullopt;
    }
  }

  static bool equals(const QString &lhs, const QString &rhs);

  static QString standardize(const QString &s);

  struct NPP2_EXPORT US_ASCII {
    static bool compare(const QString &pattern);
    static QString codec(const QByteArray &from);
  };

  struct NPP2_EXPORT UTF8 {
    static bool compare(const QString &pattern);
    static QString codec(const QByteArray &from);
  };

  struct NPP2_EXPORT ShiftJIS {
    static bool compare(const QString &pattern);
    static QString codec(const QByteArray &from);
  };

  struct NPP2_EXPORT JIS {
    static bool compare(const QString &pattern);
    static QString codec(const QByteArray &from);
  };
};

} // namespace npp2::mime {

#endif // NPP2_MIME_CODEC_HPP
