#ifndef NPP2_MIME_DECODE_HPP
#define NPP2_MIME_DECODE_HPP

#include "../npp2_global.h"
#include <QString>
#include <QByteArray>
#include <optional>

using QOptionalByteArray = std::optional<QByteArray>;

namespace npp2::mime {

struct NPP2_EXPORT EncodeBase64
{
  static const char pattern;
  static QByteArray decode(const QByteArray &bytes);
};

struct NPP2_EXPORT EncodeQuoted
{
  static const char pattern;
  static QByteArray decode(const QByteArray &bytes);
};

class NPP2_EXPORT Decode
{
public:
  static QString header(const QString &source) noexcept(false);

  static QString headerType1(const QString &source);

  static QString headerType2(const QString &source);

  static QString encodingPart(
      const QString &charSet,
      const QString &encoding,
      const QString &source
      ) noexcept(false);

  template <class T>
  static QOptionalByteArray decodeByteArray(
    const QString &pattern,
    const QByteArray &from
  ) {
    if (pattern.toUpper().at(0).toLatin1() == T::pattern) {
      return T::decode(from);
    }
    else {
      return std::nullopt;
    }
  }
};

} // namespace npp2::mime

#endif // NPP2_MIME_DECODE_HPP
