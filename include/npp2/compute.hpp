#ifndef NPP2_COMPUTE_HPP
#define NPP2_COMPUTE_HPP

#include "npp2_global.h"
#include <functional>
#include "data/any.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

void NPP2_EXPORT compute(
    FORMULAHANDLE hFormula,
    std::function<void(HCOMPUTE hCompute)> fn
    ) noexcept(false);

data::Any NPP2_EXPORT evaluate(
    HCOMPUTE hCompute,
    NOTEHANDLE hNote,
    bool *pNoteMatchesFormula = nullptr,
    bool *pNoteShouldBeDeleted = nullptr,
    bool *pNoteModified = nullptr
    ) noexcept(false);

} // namespace npp2

#endif // NPP2_COMPUTE_HPP
