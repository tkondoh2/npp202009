#ifndef NPP2_SEC_VERIFYPASSWORD_HPP
#define NPP2_SEC_VERIFYPASSWORD_HPP

#include "../data/lmbcs.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <misc.h> // SECVerifyPassword

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::sec {

inline bool verifyPassword(const data::Lmbcs &input, const data::Lmbcs &digest) {
  Status status = SECVerifyPassword(
    static_cast<WORD>(input.size()),
    reinterpret_cast<BYTE*>(const_cast<char*>(input.constData())),
    static_cast<WORD>(digest.size()),
    reinterpret_cast<BYTE*>(const_cast<char*>(digest.constData())),
    0, nullptr
  );
  return status;
}

} // namespace npp2::sec

#endif // NPP2_SEC_VERIFYPASSWORD_HPP
