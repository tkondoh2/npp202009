#ifndef NPP2_DATA_LIST_v1_HPP
#define NPP2_DATA_LIST_v1_HPP

#include "lmbcs.hpp"
#include "../_private/handleobject.hpp"
#include <QList>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <textlist.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::data::v1 {

/**
 * @brief テキストリストクラス
 */
template <WORD DATATYPE>
class List
    : public LockableObject<DHANDLE>
    , public Variant
{
  BOOL fPrefix_;

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  List(BOOL fPrefix = FALSE)
    : LockableObject<DHANDLE>(NULLHANDLE)
    , Variant()
    , fPrefix_(fPrefix)
  {
    allocate();
  }

  /**
   * @brief コンストラクタ
   * @param pList LISTポインタ
   * @param fPrefix データタイププレフィックスの有無
   */
  List(LIST *pList, BOOL fPrefix)
    : LockableObject<DHANDLE>(NULLHANDLE)
    , Variant()
    , fPrefix_(fPrefix)
  {
    DHANDLE handle = NULLHANDLE;
    ListDuplicate(pList, fPrefix_, &handle);
    setHandle(handle);
  }

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  List(const List &other)
    : LockableObject<DHANDLE>(NULLHANDLE)
    , Variant()
    , fPrefix_(other.fPrefix_)
  {
    Locker<DHANDLE> lock(other.rawHandle());
    DHANDLE handle = NULLHANDLE;
    ListDuplicate(lock.ptr<LIST>(), fPrefix_, &handle);
    setHandle(handle);
  }

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return このオブジェクトの参照
   */
  List &operator =(const List &other) {
    if (&other != this) {
      if (!isNull()) {
        OSMemFree(rawHandle());
        setHandle(NULLHANDLE);
      }
      Locker<DHANDLE> lock(other.rawHandle());
      fPrefix_ = other.fPrefix_;
      DHANDLE handle = NULLHANDLE;
      ListDuplicate(lock.ptr<LIST>(), fPrefix_, &handle);
      setHandle(handle);
    }
    return *this;
  }

  /**
   * @brief デストラクタ
   */
  virtual ~List() {
    if (!isNull()) {
      OSMemFree(rawHandle());
    }
  }

  /**
   * @return データタイププレフィックスの有無
   */
  BOOL fPrefix() const { return fPrefix_; }

  /**
   * @return データタイプ
   */
  virtual WORD dataType() const override {
    Locker<DHANDLE> lock(rawHandle());
    return getDataType(lock.ptr<LIST>(), fPrefix_, TYPE_TEXT_LIST);
  }

  /**
   * @return 現在の要素数
   */
  WORD size() const {
    if (isNull()) return 0;
    Locker<DHANDLE> lock(rawHandle());
    return getSize(lock.ptr<LIST>(), fPrefix_);
  }

  bool isEmpty() const {
    return size() == 0;
  }

  /**
   * @return 現在の全体のデータサイズ
   */
  WORD byteSize() const {
    Locker<DHANDLE> lock(rawHandle());
    return getByteSize(lock.ptr<LIST>(), fPrefix_);
  }

  /**
   * @brief 要素アクセス
   * @param index 要素インデックス
   * @return インデックスに該当する要素(テキスト)
   */
  Lmbcs at(WORD index, bool fReplace = true) const {
    Locker<DHANDLE> lock(rawHandle());
    return getAt(index, lock.ptr<LIST>(), fPrefix_, fReplace);
  }

  /**
   * @brief 要素アクセス(atと同じ)
   * @param index 要素インデックス
   * @return インデックスに該当する要素(テキスト)
   */
  Lmbcs operator [](WORD index) const {
    return at(index);
  }

  /**
   * @return 先頭要素
   */
  Lmbcs first() const {
    Locker<DHANDLE> lock(rawHandle());
    return getFirst(lock.ptr<LIST>(), fPrefix_);
  }

  /**
   * @return 末尾要素
   */
  Lmbcs last() const {
    Locker<DHANDLE> lock(rawHandle());
    return getLast(lock.ptr<LIST>(), fPrefix_);
  }

  /**
   * @param sep 区切り文字列
   * @return リスト中の文字列が1つに繋がった文字列
   */
  Lmbcs join(const Lmbcs &sep) const {
    Locker<DHANDLE> lock(rawHandle());
    return getJoin(sep, lock.ptr<LIST>(), fPrefix_);
  }

  /**
   * @brief 要素を指定した位置への挿入
   * @param index 挿入位置
   * @param item 挿入するテキスト
   * @return 例外以外の失敗はfalseを返す
   */
  bool insert(WORD index, const Lmbcs &item) noexcept(false) {
    if (isNull() && !allocate()) {
      return false;
    }
    if (index > size()) { return false; }
    auto curSize = byteSize();
    Status status = ListAddEntry(
      rawHandle(),
      fPrefix_,
      &curSize,
      index,
      item.constData(),
      static_cast<WORD>(item.size())
    );
    if (!status) { throw status; }
    return true;
  }

  /**
   * @brief 末尾への要素追加
   * @param item 追加するテキスト
   */
  bool append(const Lmbcs &item) noexcept(false) {
    return insert(size(), item);
  }

  /**
   * @brief 指定した位置の要素を削除
   * @param index 削除する要素のインデックス
   * @return 成功すればtrue
   */
  bool removeAt(WORD index) noexcept(false) {
    if (isNull() || index >= size()) { return false; }
    auto curSize = byteSize();
    Status status = ListRemoveEntry(
      rawHandle(),
      fPrefix_,
      &curSize,
      index
    );
    if (!status) { throw status; }
    return true;
  }

  /**
   * @brief 末尾の要素削除
   * @return 成功すればtrue
   */
  bool removeLast() noexcept(false) {
    return removeAt(size() - 1);
  }

  /**
   * @brief すべての要素を削除
   */
  void removeAll() noexcept(false) {
    if (isNull() || size() == 0) { return; }
    auto curSize = byteSize();
    Status status = ListRemoveAllEntries(
          rawHandle(),
          fPrefix_,
          &curSize
          );
    if (!status) { throw status; }
  }

  void clear() noexcept(false) { removeAll(); }

  /**
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   * @return データタイプ
   */
  static WORD getDataType(
      LIST *pList,
      BOOL fPrefix,
      WORD defaultType = TYPE_INVALID_OR_UNKNOWN
      ) {
    return fPrefix
        ? *reinterpret_cast<WORD*>(pList)
        : defaultType;
  }

  /**
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   * @return 現在の要素数
   */
  static WORD getSize(LIST *pList, BOOL fPrefix) {
    return ListGetNumEntries(pList, fPrefix);
  }

  /**
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   * @return 現在の全体のデータサイズ
   */
  static WORD getByteSize(LIST *pList, BOOL fPrefix) {
    return ListGetSize(pList, fPrefix);
  }

  /**
   * @brief 要素アクセス
   * @param index 要素インデックス
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   * @return インデックスに該当する要素(テキスト)
   */
  static Lmbcs getAt(WORD index, LIST *pList, BOOL fPrefix, bool fReplace = true) {
    auto count = getSize(pList, fPrefix);
    if (count == 0 || index >= count) {
      return Lmbcs();
    }
    char *ptr = nullptr;
    WORD len = 0;
    Status status = ListGetText(pList, fPrefix, index, &ptr, &len);
    if (!status) { throw status; }
    return Lmbcs(ptr, len, FALSE, fReplace);
  }

  /**
   * @return 先頭要素
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   */
  static Lmbcs getFirst(LIST *pList, BOOL fPrefix, bool fReplace = true) {
    return getAt(0, pList, fPrefix, fReplace);
  }

  /**
   * @return 末尾要素
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   */
  static Lmbcs getLast(LIST *pList, BOOL fPrefix, bool fReplace = true) {
    auto count = getSize(pList, fPrefix);
    if (count == 0) { return Lmbcs(); }
    return getAt(count - 1, pList, fPrefix, fReplace);
  }

  /**
   * @param sep 区切り文字列
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   * @return リスト中の文字列が1つに繋がった文字列
   */
  static Lmbcs getJoin(const Lmbcs &sep, LIST *pList, BOOL fPrefix, bool fReplace = true) {
    auto count = getSize(pList, fPrefix);
    auto joined = getFirst(pList, fPrefix);
    for (auto i = 1; i < count; ++i) {
      joined += sep + getAt(i, pList, fPrefix, fReplace);
    }
    return joined;
  }

private:
  /**
   * @brief LIST領域の確保
   * @param count 初期要素数
   * @param quota 初期割当サイズ
   * @return 成功すればTRUE
   */
  bool allocate(WORD count = 0, WORD quota = 0) {
    DHANDLE handle = NULLHANDLE;
    void *ptr = nullptr;
    WORD size = 0;
    Status status = ListAllocate(
      count,
      quota,
      fPrefix_,
      &handle,
      &ptr,
      &size
    );
    if (!status) { throw status; }
    auto pType = reinterpret_cast<WORD*>(ptr);
    if (fPrefix_) { *pType = DATATYPE; }
    OSUnlockObject(handle);
    setHandle(handle);
    return true;
  }
};

using TextList = List<TYPE_TEXT_LIST>;

} // namespace npp2::data::v1

inline QStringList fromLmbcsList(const npp2::data::v1::TextList &list) {
  QStringList result;
  for (WORD i = 0; i < list.size(); ++i) {
    result << fromLmbcs(list.at(i));
  }
  return result;
}

inline npp2::data::v1::TextList toLmbcsList(const QStringList &list) {
  npp2::data::v1::TextList result;
  foreach (auto item, list) {
    result.append(toLmbcs(item));
  }
  return result;
}

inline bool contains(
  const npp2::data::v1::TextList &list,
  const npp2::data::Lmbcs &item,
  Qt::CaseSensitivity cs = Qt::CaseSensitive,
  bool fReplace = true
) {
  auto v = fromLmbcs(item);
  for (WORD i = 0; i < list.size(); ++i) {
    auto s = fromLmbcs(list.at(i, fReplace));
    if (s.compare(v, cs) == 0) return true;
  }
  return false;
}

inline void append(
  npp2::data::v1::TextList &list,
  const npp2::data::v1::TextList &others,
  std::function<npp2::data::Lmbcs(const npp2::data::Lmbcs &)> t
    = [](const npp2::data::Lmbcs &a) { return a; }
) {
  for (WORD i = 0; i < others.size(); ++i) {
    list.append(t(others.at(i)));
  }
}

inline npp2::data::v1::TextList split(const npp2::data::Lmbcs &text, char sep) {
  npp2::data::v1::TextList result;
  auto list = text.raw().split(sep);
  foreach (auto item, list) {
    result.append(npp2::data::Lmbcs(item));
  }
  return result;
}

inline npp2::data::v1::TextList createTextList(
  const std::initializer_list<npp2::data::Lmbcs> &list
) {
  npp2::data::v1::TextList result;
  for (auto it: list) {
    result.append(it);
  }
  return result;
}

#endif // NPP2_DATA_LIST_v1_HPP
