#ifndef NPP2_DATA_LMBCS_HPP
#define NPP2_DATA_LMBCS_HPP

#include "../npp2_global.h"
#include "variant.hpp"
#include <QString>

namespace npp2::data {

/**
 * @brief LMBCS文字列クラス
 */
class NPP2_EXPORT Lmbcs
  : public Variant
{
  QByteArray raw_;

public:
  virtual WORD dataType() const override;

  /**
   * @brief デフォルトコンストラクタ
   */
  Lmbcs();

  /**
   * @brief コンストラクタ
   * @param p ヌル終端文字列
   */
  Lmbcs(const char *p);

  /**
   * @brief コンストラクタ
   * @param p 文字列
   * @param n サイズ
   * @param fPrefix データタイププレフィックス分ずらす場合はTRUE
   * @param fReplace NULL文字を改行に置き換える時はtrue
   */
  Lmbcs(const char *p, int n, BOOL fPrefix = FALSE, bool fReplace = false);

  /**
   * @brief コンストラクタ
   * @param str QByteArrayデータ(あくまでLMBCS文字列が入っている前提)
   */
  Lmbcs(const QByteArray &str);

  virtual ~Lmbcs();
  
  /**
   * @return QByteArrayデータ
   */
  const QByteArray &raw() const;

  /**
   * @return C文字列ポインタ
   */
  char *data();

  /**
   * @return 定数C文字列ポインタ
   */
  const char *constData() const;

  /**
   * @return データ長
   */
  int size() const;

  /**
   * @return データ長が0ならtrue
   */
  bool isEmpty() const;

  /**
   * @param str 検索文字列
   * @param from 検索開始位置
   * @return 文字列が見つかった位置
   */
  int indexOf(const Lmbcs &str, int from = 0) const;

  /**
   * @brief 部分文字列の置き換え
   * @param from 置き換え元の文字列
   * @param to 置き換え先の文字列
   * @return 置き換え済みの自身の参照
   */
  Lmbcs &replace(const Lmbcs &before, const Lmbcs &after);

  /**
   * @brief 加算代入演算子(文字列結合)
   * @param rhs 結合する文字列
   * @return 結合済み文字列(自身)
   */
  Lmbcs &operator +=(const Lmbcs &rhs);

  /**
   * @brief 加算演算子(文字列結合)
   * @param lhs 左辺値
   * @param rhs 右辺値
   * @return 左辺値と右辺値を結合した文字列
   */
  friend NPP2_EXPORT Lmbcs operator +(const Lmbcs &lhs, const Lmbcs &rhs);

  /**
   * @brief ＜(小なり)演算子
   * @param lhs 左辺値
   * @param rhs 右辺値
   * @return 左辺値が右辺値より小さい場合true
   */
  friend NPP2_EXPORT bool operator <(const Lmbcs &lhs, const Lmbcs &rhs);

  /**
   * @brief 等価演算子
   * @param lhs 左辺値
   * @param rhs 右辺値
   * @return 左辺値と右辺値が等価の場合true
   */
  friend NPP2_EXPORT bool operator ==(const Lmbcs &lhs, const Lmbcs &rhs);
};

} // namespace npp2::data

namespace npp2 {

/**
 * @brief STATUS値からエラー内容のテキストに変換する
 * @param status ステータス値
 * @return エラーテキスト(LMBCS文字列)
 */
data::Lmbcs NPP2_EXPORT fromStatus(STATUS status);

} // namespace npp2

/**
 * @brief QString文字列をLMBCS文字列に変換する
 * @param qstr QString
 * @return LMBCS文字列
 */
npp2::data::Lmbcs NPP2_EXPORT toLmbcs(const QString &qstr);

/**
 * @brief LMBCS文字列をQString文字列に変換する
 * @param lmbcs LMBCS文字列
 * @return QString
 */
QString NPP2_EXPORT fromLmbcs(const npp2::data::Lmbcs &lmbcs);

/**
 * @brief QString文字列をLMBCS文字列に変換する
 * @param qstr QString
 * @return LMBCS文字列
 */
npp2::data::Lmbcs NPP2_EXPORT nativeTo(const QByteArray &native);

/**
 * @brief LMBCS文字列をQString文字列に変換する
 * @param lmbcs LMBCS文字列
 * @return QString
 */
QByteArray NPP2_EXPORT toNative(const npp2::data::Lmbcs &lmbcs);

#endif // NPP2_DATA_LMBCS_HPP
