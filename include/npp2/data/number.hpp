#ifndef NPP2_DATA_NUMBER_HPP
#define NPP2_DATA_NUMBER_HPP

#include "../npp2_global.h"
#include "variant.hpp"
#include "lmbcs.hpp"

// #if defined(NT)
// #pragma pack(push, 1)
// #endif

// #include <misc.h>

// #if defined(NT)
// #pragma pack(pop)
// #endif

namespace npp2::data {

/**
 * @brief 数値型(NUMBER)のラッパークラス
 */
class NPP2_EXPORT Number
  : public Variant
{
  NUMBER raw_;

public:
  virtual WORD dataType() const override;

  using type = NUMBER;
  static NUMBER defaultRaw();

  /**
   * @brief デフォルトコンストラクタ
   */
  Number();

  /**
   * @brief コンストラクタ
   * @param num 初期値
   */
  Number(NUMBER num);

  /**
   * @brief コンストラクタ(フィールドデータから構築)
   * @param ptr 数値型へのポインタ
   * @param fPrefix データタイププレフィックスの有無
   */
  Number(const void *ptr, BOOL fPrefix);

  /**
   * @brief NUMBER型へのキャスト演算子
   */
  operator NUMBER() const;

  /**
   * @brief アドレス取得演算子
   * @return NUMBERデータへのポインタ
   */
  NUMBER *operator &();

  Lmbcs toString() const noexcept(false);

  static Lmbcs toString(NUMBER n) noexcept(false);
};

} // namespace npp2::data

#endif // NPP2_DATA_NUMBER_HPP
