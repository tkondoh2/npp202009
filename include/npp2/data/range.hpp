#ifndef NPP2_DATA_RANGE_HPP
#define NPP2_DATA_RANGE_HPP

#include "variant.hpp"
#include "../_private/handleobject.hpp"
#include "../_private/range.hpp"
#include "../status.hpp"
#include <functional>
#include <QList>

namespace npp2::data {

/**
 * @brief RANGE構造体ラップクラス
 * @tparam Item
 *   Item::type typename シングル要素データ型(NUMBER|TIMEDATE)
 *   Item::defaultRaw static-method シングル要素のデフォルト値
 *   Pair::type typename ペア要素データ型(NUMBER_PAIR|TIMEDATE_PAIR)
 *   Pair::defaultRaw static-method ペア要素のデフォルト値
 */
template <typename Item, typename Pair>
class Range
  : public LockableObject3<DHANDLE>
  , public Variant
{
  BOOL fPrefix_; ///< データタイププレフィックスの有無

public:
  /**
   * @brief コンストラクタ
   * @param fPrefix データタイププレフィックスの有無
   */
  Range(BOOL fPrefix = FALSE)
    : LockableObject3<DHANDLE>(NULLHANDLE)
    , Variant()
    , fPrefix_(fPrefix)
  {
    allocate();
  }

  /**
   * @brief コンストラクタ 指定ポインタの範囲型データをコピーする
   * @param pRange RANGEポインタ
   * @param fPrefix データタイププレフィックスの有無
   */
  Range(RANGE *pRange, BOOL fPrefix)
    : LockableObject3<DHANDLE>(NULLHANDLE)
    , Variant()
    , fPrefix_(fPrefix)
  {
    DHANDLE handle = NULLHANDLE;
    RangeDuplicate<Item, Pair>(pRange, fPrefix_, &handle);
    setHandle(handle);
  }

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  Range(const Range<Item, Pair> &other)
    : LockableObject3<DHANDLE>(NULLHANDLE)
    , Variant()
    , fPrefix_(other.fPrefix_)
  {
    Locker<DHANDLE> lock(other.rawHandle());
    DHANDLE handle = NULLHANDLE;
    RangeDuplicate<Item, Pair>(lock.ptr<RANGE>(), fPrefix_, &handle);
    setHandle(handle);
  }

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return このオブジェクトの参照
   */
  Range &operator =(const Range &other) {

    // 自身を代入していないことを確認
    if (&other != this) {
      if (!isNull()) {
        OSMemFree(rawHandle());
        setHandle(NULLHANDLE);
      }

      // 相手のハンドルをロック
      Locker<DHANDLE> lock(other.rawHandle());
      fPrefix_ = other.fPrefix_;
      DHANDLE handle = NULLHANDLE;
      RangeDuplicate<Item, Pair>(lock.ptr<RANGE>(), fPrefix_, &handle);
      setHandle(handle);
    }
    return *this;
  }

  /**
   * @brief デストラクタ
   */
  virtual ~Range() {}

  // virtual const std::string &name() const override {
  //   static std::string n("Range");
  //   return n;
  // }

  /**
   * @return データタイププレフィックスの有無
   */
  BOOL fPrefix() const { return fPrefix_; }

  /**
   * @return シングル要素の数
   */
  WORD itemSize() const {
    Locker<DHANDLE> lock(rawHandle());
    return getItemSize(lock.ptr<RANGE>(), fPrefix_);
  }

  /**
   * @return ペア要素の数
   */
  WORD pairSize() const {
    Locker<DHANDLE> lock(rawHandle());
    return getPairSize(lock.ptr<RANGE>(), fPrefix_);
  }

  /**
   * @return 全体のバイト数
   */
  DWORD byteSize() const {
    Locker<DHANDLE> lock(rawHandle());
    return getByteSize(lock.ptr<RANGE>(), fPrefix_);
  }

  /**
   * @param index インデックス
   * @return 指定したインデックスのシングル要素
   */
  typename Item::type itemAt(WORD index) const {
    Locker<DHANDLE> lock(rawHandle());
    return getItemAt(index, lock.ptr<RANGE>(), fPrefix_);
  }

  /**
   * @return 先頭シングル要素
   */
  typename Item::type firstItem() const {
    auto count = itemSize();
    if (count == 0) { return Item::defaultRaw(); }
    return itemAt(0);
  }

  /**
   * @return 末尾シングル要素
   */
  typename Item::type lastItem() const {
    auto count = itemSize();
    if (count == 0) { return Item::defaultRaw(); }
    return itemAt(count - 1);
  }

  /**
   * @brief []演算子のオーバーロード
   * @param index インデックス
   * @return 指定したインデックスのシングル要素
   */
  typename Item::type operator [](WORD index) const {
    return itemAt(index);
  }

  /**
   * @param index インデックス
   * @return 指定したインデックスのペア要素
   */
  typename Pair::type pairAt(WORD index) const {
    Locker<DHANDLE> lock(rawHandle());
    return getPairAt(index, lock.ptr<RANGE>(), fPrefix_);
  }

  /**
   * @return 先頭ペア要素
   */
  typename Pair::type firstPair() const {
    return pairAt(0);
  }

  /**
   * @return 末尾ペア要素
   */
  typename Pair::type lastPair() const {
    auto count = pairSize();
    if (count == 0) { return Pair::defaultRaw(); }
    return pairAt(count - 1);
  }

  /**
   * @brief シングル要素をマップ関数で変換して取り出す
   * @tparam T 変換後のデータ型
   * @param fn マップ関数
   * @return 変換後のデータリスト
   */
  template <class T>
  QList<T> itemMap(std::function<T(typename Item::type)> fn) const {
    Locker<DHANDLE> lock(rawHandle());
    return getItemMap(fn, lock.ptr<RANGE>(), fPrefix_);
  }

  /**
   * @brief ペア要素をマップ関数で変換して取り出す
   * @tparam T 変換後のデータ型
   * @param fn マップ関数
   * @return 変換後のデータリスト
   */
  template <class T>
  QVector<T> pairMap(std::function<T(typename Pair::type)> fn) const {
    Locker<DHANDLE> lock(rawHandle());
    return getPairMap(fn, lock.ptr<RANGE>(), fPrefix_);
  }

  template <class T>
  T reduceItems(std::function<T(T, typename Item::type)> fn, T seed) const {
    Locker<DHANDLE> lock(rawHandle());
    return getReduceItems(fn, seed, lock.ptr<RANGE>(), fPrefix_);
  }

  template <class T>
  T reducePairs(std::function<T(T, typename Pair::type)> fn, T seed) const {
    Locker<DHANDLE> lock(rawHandle());
    return getReducePairs(fn, seed, lock.ptr<RANGE>(), fPrefix_);
  }

  /**
   * @brief シングル要素の挿入
   * @param index 挿入位置(末尾はsize())
   * @param item 挿入したいシングル要素
   * @return 成功すればtrue
   */
  bool insertItem(WORD index, typename Item::type item) noexcept(false) {
    // 領域未確保の場合、先に領域を確保する
    if (isNull() && !allocate()) return false;

    // 挿入位置が末尾を超えていたら失敗
    if (index > itemSize()) return false;

    // シングル要素を挿入
    Status status = RangeInsertItem<Item, Pair>(rawHandle(), fPrefix_, index, &item);
    if (!status) { throw status; }
    return true;
  }

  /**
   * @brief ペア要素の追加
   * @param item 追加したいシングル要素
   * @return 成功すればtrue
   */
  bool appendItem(typename Item::type item) noexcept(false) {
    return insertItem(itemSize(), item);
  }

  /**
   * @brief ペア要素の挿入
   * @param index 挿入位置(末尾はpairSize())
   * @param item 挿入したいペア要素
   * @return 成功すればtrue
   */
  bool insertPair(WORD index, typename Pair::type pair) {
    if (isNull() && !allocate()) return false;

    if (index > pairSize()) return false;

    Status status = RangeInsertPair<Item, Pair>(this->handle_, fPrefix_, index, &pair);
    if (!status) { throw status; }
    return true;
  }

  /**
   * @brief ペア要素の追加
   * @param item 追加したいペア要素
   * @return 成功すればtrue
   */
  bool appendPair(typename Pair::type pair) {
    return insertPair(pairSize(), pair);
  }

  /**
   * @brief ペア要素の挿入
   * @param index 挿入位置(末尾はpairSize())
   * @param lower 挿入したいペアの下位要素
   * @param upper 挿入したいペアの上位要素
   * @return 成功すればtrue
   */
  bool insertPair(
      WORD index,
      typename Item::type lower,
      typename Item::type upper
      ) {
    return insertPair(index, {lower, upper});
  }

  /**
   * @brief ペア要素の追加
   * @param lower 追加したいペアの下位要素
   * @param upper 追加したいペアの上位要素
   * @return 成功すればtrue
   */
  bool appendPair(typename Item::type lower, typename Item::type upper) {
    return insertPair(pairSize(), lower, upper);
  }

  /**
   * @brief 指定した位置のシングル要素を削除
   * @param index 削除する要素のインデックス
   * @return 成功すればtrue
   */
  bool removeItemAt(WORD index) {
    if (handle_ == NULLHANDLE || index >= itemSize()) { return false; }
    Status status = RangeRemoveItem<Item, Pair>(this->handle_, fPrefix_, index);
    if (!status) { throw status; }
    return true;
  }

  /**
   * @brief シングル要素の末尾を削除
   * @return 成功すればtrue
   */
  bool removeLastItem() {
    auto index = itemSize();
    if (index == 0) { return false; }
    return removeItemAt(index - 1);
  }

  /**
   * @brief 指定した位置のペア要素を削除
   * @param index 削除する要素のインデックス
   * @return 成功すればtrue
   */
  bool removePairAt(WORD index) {
    if (this->handle_ == NULLHANDLE || index >= pairSize()) { return false; }
    Status status = RangeRemovePair<Item, Pair>(this->handle_, fPrefix_, index);
    if (!status) { throw status; }
    return true;
  }

  /**
   * @brief ペア要素の末尾を削除
   * @return 成功すればtrue
   */
  bool remoteLastPair() {
    auto index = pairSize();
    if (index == 0) { return false; }
    return removePairAt(index - 1);
  }

  /**
   * @brief すべてのシングル要素を削除
   */
  void removeAllItems() {
    if (this->handle_ == NULLHANDLE || itemSize() == 0) { return; }
    Status status = RangeRemoveAllItems<Item, Pair>(this->handle_, fPrefix_);
    if (!status) { throw status; }
  }

  /**
   * @brief すべてのペア要素を削除
   */
  void removeAllPairs() {
    if (this->handle_ == NULLHANDLE || pairSize() == 0) { return; }
    Status status = RangeRemoveAllPairs<Item, Pair>(this->handle_, fPrefix_);
    if (!status) { throw status; }
  }

  /**
   * @brief すべてのシングル要素、ペア要素を削除
   */
  void clear() {
    if (this->handle_ == NULLHANDLE || (itemSize() == 0 && pairSize() == 0)) { return; }
    Status status = RangeRemoveAll(this->handle_, fPrefix_);
    if (!status) { throw status; }
  }

  /**
   * @param pRange RANGEへのポインタ
   * @param fPrefix データタイププレフィックスの有無
   * @return シングル要素の数
   */
  static WORD getItemSize(RANGE *ptr, BOOL fPrefix) {
    return RangeGetNumItem(ptr, fPrefix);
  }

  /**
   * @param pRange RANGEへのポインタ
   * @param fPrefix データタイププレフィックスの有無
   * @return ペア要素の数
   */
  static WORD getPairSize(RANGE *ptr, BOOL fPrefix) {
    return RangeGetNumPair(ptr, fPrefix);
  }

  /**
   * @param pRange RANGEへのポインタ
   * @param fPrefix データタイププレフィックスの有無
   * @return 全体のバイト数
   */
  static DWORD getByteSize(RANGE *ptr, BOOL fPrefix) {
    return RangeGetSize<Item, Pair>(ptr, fPrefix);
  }

  /**
   * @param index インデックス
   * @param pRange RANGEへのポインタ
   * @param fPrefix データタイププレフィックスの有無
   * @return 指定したインデックスのシングル要素
   */
  static typename Item::type getItemAt(
      WORD index,
      RANGE *pRange,
      BOOL fPrefix
      ) {
    auto count = getItemSize(pRange, fPrefix);
    if (count == 0 || index >= count) {
      return Item::defaultRaw();
    }
    typename Item::type data;
    RangeGetItem<Item>(pRange, fPrefix, index, &data);
    return data;
  }

  /**
   * @param pRange 範囲型ポインタ
   * @param fPrefix データタイププレフィックスの有無
   * @return 先頭のシングル要素
   */
  static typename Item::type getFirstItem(RANGE *pRange, BOOL fPrefix) {
    return getItemAt(0, pRange, fPrefix);
  }

  /**
   * @param pRange 範囲型ポインタ
   * @param fPrefix データタイププレフィックスの有無
   * @return 末尾のシングル要素
   */
  static typename Item::type getLastItem(RANGE *pRange, BOOL fPrefix) {
    auto count = getItemSize(pRange, fPrefix);
    if (count == 0) { return Item::defaultRaw(); }
    return getItemAt(count - 1, pRange, fPrefix);
  }

  /**
   * @param index インデックス
   * @param pRange RANGEへのポインタ
   * @param fPrefix データタイププレフィックスの有無
   * @return 指定したインデックスのペア要素
   */
  static typename Pair::type getPairAt(
      WORD index,
      RANGE *pRange,
      BOOL fPrefix
      ) {
    auto count = getPairSize(pRange, fPrefix);
    if (count == 0 || index >= count) {
      return Pair::defaultRaw();
    }
    typename Pair::type data;
    RangeGetPair<Item, Pair>(pRange, fPrefix, index, &data);
    return data;
  }

  /**
   * @param pRange RANGEへのポインタ
   * @param fPrefix データタイププレフィックスの有無
   * @return 先頭のペア要素
   */
  static typename Pair::type getFirstPair(RANGE *pRange, BOOL fPrefix) {
    return getPairAt(0, pRange, fPrefix);
  }

  /**
   * @param pRange RANGEへのポインタ
   * @param fPrefix データタイププレフィックスの有無
   * @return 末尾のペア要素
   */
  static typename Pair::type getLastPair(RANGE *pRange, BOOL fPrefix) {
    auto count = getPairSize(pRange, fPrefix);
    if (count == 0) { return Pair::defaultRaw(); }
    return getPairAt(count - 1, pRange, fPrefix);
  }

  /**
   * @brief シングル要素をマップ関数で変換して取り出す
   * @tparam T 変換後のデータ型
   * @param fn マップ関数
   * @param pRange RANGEへのポインタ
   * @param fPrefix データタイププレフィックスの有無
   * @return 変換後のデータリスト
   */
  template <class T>
  static QList<T> getItemMap(
      std::function<T(typename Item::type)> fn,
      RANGE *pRange,
      BOOL fPrefix
      ) {
    QList<T> list;
    auto count = getItemSize(pRange, fPrefix);
    for (auto i = 0; i < count; ++i) {
      auto v = getItemAt(i, pRange, fPrefix);
      auto s = fn(v);
      list.append(s);
    }
    return list;
  }

  /**
   * @brief ペア要素をマップ関数で変換して取り出す
   * @tparam T 変換後のデータ型
   * @param fn マップ関数
   * @param pRange RANGEへのポインタ
   * @param fPrefix データタイププレフィックスの有無
   * @return 変換後のデータリスト
   */
  template <class T>
  static QList<T> getPairMap(
      std::function<T(typename Pair::type)> fn,
      RANGE *pRange,
      BOOL fPrefix
      ) {
    QList<T> list;
    auto count = getPairSize(pRange, fPrefix);
    for (auto i = 0; i < count; ++i) {
      auto v = getPairAt(i, pRange, fPrefix);
      auto s = fn(v);
      list.append(s);
    }
    return list;
  }

  template <class T>
  static T getReduceItems(
      std::function<T(T, typename Item::type)> fn,
      T seed,
      RANGE *pRange,
      BOOL fPrefix
      ) {
    auto count = getItemSize(pRange, fPrefix);
    auto acc = seed;
    for (auto i = 0; i < count; ++i) {
      acc = fn(acc, getItemAt(i, pRange, fPrefix));
    }
    return acc;
  }

  template <class T>
  static T getReducePairs(
      std::function<T(T, typename Pair::type)> fn,
      T seed,
      RANGE *pRange,
      BOOL fPrefix
      ) {
    auto count = getPairSize(pRange, fPrefix);
    auto acc = seed;
    for (auto i = 0; i < count; ++i) {
      acc = fn(acc, getPairAt(i, pRange, fPrefix));
    }
    return acc;
  }

private:
  /**
   * @brief RANGE領域の確保
   * @param count 初期シングル要素数
   * @param pairCount 初期ペア要素数
   * @return 成功すればTRUE
   */
  bool allocate(WORD count = 0, WORD pairCount = 0) noexcept(false) {
    void *ptr = nullptr;
    DWORD size = 0;
    DHANDLE handle = NULLHANDLE;
    Status status = RangeAllocate<Item, Pair>(
      count,
      pairCount,
      fPrefix_,
      &handle,
      &ptr,
      &size
    );
    if (!status) {
      throw status;
    }
    if (fPrefix_) {
      auto pType = reinterpret_cast<WORD*>(ptr);
      *pType = dataType();
    }
    OSUnlockObject(handle);
    setHandle(handle);
    return true;
  }
};

} // namespace npp2::data

#endif // NPP2_DATA_RANGE_HPP
