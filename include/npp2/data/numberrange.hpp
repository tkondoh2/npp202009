#ifndef NPP2_DATA_NUMBERRANGE_HPP
#define NPP2_DATA_NUMBERRANGE_HPP

#include "../npp2_global.h"
#include "number.hpp"
#include "range.hpp"

namespace npp2::data {

class NPP2_EXPORT NumberPair
{
public:
  using type = NUMBER_PAIR;
  static NUMBER_PAIR defaultRaw();
};

class NPP2_EXPORT NumberRange
  : public Range<Number, NumberPair>
{
public:
  virtual WORD dataType() const;

  NumberRange();

  NumberRange(RANGE *pRange, BOOL fPrefix);
};

} // namespace npp2::data

#endif // NPP2_DATA_NUMBERRANGE_HPP
