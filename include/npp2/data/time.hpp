#ifndef NPP2_DATA_TIME_HPP
#define NPP2_DATA_TIME_HPP

#include "../npp2_global.h"
#include <memory>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <misc.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp2::data {

/**
 * @brief 時間クラス(TIME型のプロキシークラス)
 */
class NPP2_EXPORT Time
{
  int year_; ///< 年
  int month_; ///< 月
  int day_; ///< 日
  int hour_; ///< 時
  int minute_; ///< 分
  int second_; ///< 秒
  int millisecond_; ///< ミリ秒
  bool isDaylightTime_; ///< 夏時間の有無
  int offsetSecond_; ///< UTCオフセット秒

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Time();

  /**
   * @brief コンストラクタ
   * @param year 年
   * @param month 月
   * @param day 日
   * @param hour 時
   * @param minute 分
   * @param second 秒
   * @param millisecond ミリ秒
   * @param isDaylightTime 夏時間の有無
   * @param offsetSecond UTCオフセット秒
   */
  Time(int year, int month, int day,
       int hour, int minute, int second, int millisecond,
       bool isDaylightTime, int offsetSecond
       );

  /**
   * @return 年
   */
  int year() const;

  /**
   * @return 月
   */
  int month() const;

  /**
   * @return 日
   */
  int day() const;

  /**
   * @return 時
   */
  int hour() const;

  /**
   * @return 分
   */
  int minute() const;

  /**
   * @return 秒
   */
  int second() const;

  /**
   * @return ミリ秒
   */
  int millisecond() const;

  /**
   * @return 夏時間の有無
   */
  bool isDaylightTime() const;

  /**
   * @return UTCオフセット秒
   */
  int offsetSeconds() const;

  TIME toTIME() const;

  static Time fromTIME(const TIME &time);

  /**
   * @brief UTCオフセット秒(夏時間を含む)からTIME::zone値(夏時間を含まない)に変換する
   * @param secs UTCオフセット秒
   * @param isDaylightTime 夏時間の有無
   * @return TIME::zone値
   */
  static int offsetSecondsToZone(int secs, bool isDaylightTime = false);

  /**
   * @brief TIME::zone値(夏時間を含まない)からUTCオフセット秒(夏時間を含む)に変換する
   * @param zone TIME::zone値
   * @param isDaylightTime 夏時間の有無
   * @return UTCオフセット秒
   */
  static int zoneToOffsetSeconds(int zone, bool isDaylightTime = false);
};

using TimePtr = std::unique_ptr<Time>;

} // namespace npp2::data

#endif // NPP2_DATA_TIME_HPP
