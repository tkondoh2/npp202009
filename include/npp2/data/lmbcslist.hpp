#ifndef NPP2_DATA_LMBCSLIST_HPP
#define NPP2_DATA_LMBCSLIST_HPP

#include "../npp2_global.h"
#include "lmbcs.hpp"
#include <QList>
#include <QStringList>

namespace npp2::data {

class NPP2_EXPORT LmbcsList
  : public QList<Lmbcs>
{
public:
  LmbcsList();

  LmbcsList(const std::initializer_list<npp2::data::Lmbcs> &list);

  QString atAsQStr(int i) const;

  bool containsByQStr(
    const Lmbcs &search,
    Qt::CaseSensitivity cs = Qt::CaseSensitive
  ) const;

  Lmbcs join(const Lmbcs &sep) const;

  template <class List, class Item = Lmbcs>
  List map(std::function<Item(Lmbcs)> fn) const {
    List result;
    foreach (auto item, *this) {
      result.append(fn(item));
    }
    return result;
  }

  QStringList toQStrList() const;

  static LmbcsList split(const Lmbcs &target, char sep);
};

} // namespace npp2::data

#endif // NPP2_DATA_LMBCSLIST_HPP
