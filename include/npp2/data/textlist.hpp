#ifndef NPP2_DATA_TEXTLIST_HPP
#define NPP2_DATA_TEXTLIST_HPP

#include "../npp2_global.h"
#include "lmbcslist.hpp"
#include "../_private/handleobject.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::data {

class _TextList;
using TextList = std::unique_ptr<_TextList>;

/**
 * @brief テキストリストクラス
 */
class NPP2_EXPORT _TextList
  : public LockableObject3<DHANDLE>
  , public Variant
{
  bool allocated_;
  BOOL fPrefix_;

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  _TextList(BOOL fPrefix = FALSE);

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  _TextList(const _TextList &other) = delete;
  _TextList(_TextList &&other) = delete;

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return このオブジェクトの参照
   */
  _TextList &operator =(const _TextList &other) = delete;
  _TextList &operator =(_TextList &&other) = delete;

  virtual ~_TextList();
;
  void duplicate(const LIST *from, BOOL fPrefix);

  /**
   * @return データタイププレフィックスの有無
   */
  BOOL fPrefix() const;

  /**
   * @return データタイプ
   */
  virtual WORD dataType() const override;

  /**
   * @return 現在の要素数
   */
  WORD size() const;

  bool isEmpty() const;

  /**
   * @return 現在の全体のデータサイズ
   */
  WORD byteSize() const;

  /**
   * @brief 要素アクセス
   * @param index 要素インデックス
   * @return インデックスに該当する要素(テキスト)
   */
  Lmbcs at(WORD index, bool fReplace = true) const;

  /**
   * @brief 要素アクセス(atと同じ)
   * @param index 要素インデックス
   * @return インデックスに該当する要素(テキスト)
   */
  Lmbcs operator [](WORD index) const;

  /**
   * @return 先頭要素
   */
  Lmbcs first() const;

  /**
   * @return 末尾要素
   */
  Lmbcs last() const;

  /**
   * @param sep 区切り文字列
   * @return リスト中の文字列が1つに繋がった文字列
   */
  Lmbcs join(const Lmbcs &sep) const;

  /**
   * @brief 要素を指定した位置への挿入
   * @param index 挿入位置
   * @param item 挿入するテキスト
   * @return 例外以外の失敗はfalseを返す
   */
  bool insert(WORD index, const Lmbcs &item) noexcept(false);

  /**
   * @brief 末尾への要素追加
   * @param item 追加するテキスト
   */
  bool append(const Lmbcs &item) noexcept(false);

  /**
   * @brief 指定した位置の要素を削除
   * @param index 削除する要素のインデックス
   * @return 成功すればtrue
   */
  bool removeAt(WORD index) noexcept(false);

  /**
   * @brief 末尾の要素削除
   * @return 成功すればtrue
   */
  bool removeLast() noexcept(false);

  /**
   * @brief すべての要素を削除
   */
  void removeAll() noexcept(false);

  void clear() noexcept(false);

  /**
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   * @return データタイプ
   */
  static WORD getDataType(
    LIST *pList,
    BOOL fPrefix,
    WORD defaultType = TYPE_INVALID_OR_UNKNOWN
  );

  /**
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   * @return 現在の要素数
   */
  static WORD getSize(LIST *pList, BOOL fPrefix);

  /**
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   * @return 現在の全体のデータサイズ
   */
  static WORD getByteSize(LIST *pList, BOOL fPrefix);

  /**
   * @brief 要素アクセス
   * @param index 要素インデックス
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   * @return インデックスに該当する要素(テキスト)
   */
  static Lmbcs getAt(
    WORD index,
    LIST *pList,
    BOOL fPrefix,
    bool fReplace = true
  );

  /**
   * @return 先頭要素
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   */
  static Lmbcs getFirst(LIST *pList, BOOL fPrefix, bool fReplace = true);

  /**
   * @return 末尾要素
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   */
  static Lmbcs getLast(LIST *pList, BOOL fPrefix, bool fReplace = true);

  /**
   * @param sep 区切り文字列
   * @param pList LISTへのポインタ
   * @param fPrefix データタイププレフィックス
   * @return リスト中の文字列が1つに繋がった文字列
   */
  static Lmbcs getJoin(
    const Lmbcs &sep,
    LIST *pList,
    BOOL fPrefix,
    bool fReplace = true
  );

  LmbcsList toLmbcsList() const;

private:
  /**
   * @brief LIST領域の確保
   * @param count 初期要素数
   * @param quota 初期割当サイズ
   * @return 成功すればTRUE
   */
  bool allocate(WORD count = 0, WORD quota = 0);
};

} // namespace npp2::data

QStringList NPP2_EXPORT fromTextList(const npp2::data::TextList &list);

npp2::data::TextList NPP2_EXPORT toTextList(const QStringList &qlist);

bool NPP2_EXPORT contains(
  const npp2::data::TextList &list,
  const npp2::data::Lmbcs &item,
  Qt::CaseSensitivity cs = Qt::CaseSensitive,
  bool fReplace = true
);

void NPP2_EXPORT append(
  npp2::data::TextList &list,
  const npp2::data::TextList &others,
  std::function<npp2::data::Lmbcs(const npp2::data::Lmbcs &)> t
    = [](const npp2::data::Lmbcs &a) { return a; }
);

npp2::data::TextList NPP2_EXPORT split(const npp2::data::Lmbcs &text, char sep);

npp2::data::TextList NPP2_EXPORT createTextList(
  const std::initializer_list<npp2::data::Lmbcs> &list
);

npp2::data::TextList NPP2_EXPORT lmbcsToTextList(
  const npp2::data::LmbcsList &lmbcsList
);

#endif // NPP2_DATA_TEXTLIST_HPP
