#ifndef NPP2_DATA_TIMEDATERANGE_HPP
#define NPP2_DATA_TIMEDATERANGE_HPP

#include "../npp2_global.h"
#include "timedate.hpp"
#include "range.hpp"

namespace npp2::data {

class NPP2_EXPORT TimeDatePair
{
public:
  using type = TIMEDATE_PAIR;
  static TIMEDATE_PAIR defaultRaw();
};

class NPP2_EXPORT TimeDateRange
  : public Range<TimeDate, TimeDatePair>
{
public:
  virtual WORD dataType() const;

  TimeDateRange();

  TimeDateRange(RANGE *pRange, BOOL fPrefix);
};

} // namespace npp2::data

#endif // NPP2_DATA_TIMEDATERANGE_HPP
