#ifndef NPP2_DATA_ANY_HPP
#define NPP2_DATA_ANY_HPP

#include "../npp2_global.h"
#include "lmbcs.hpp"
#include "textlist.hpp"
#include "number.hpp"
#include "numberrange.hpp"
#include "timedate.hpp"
#include "timedaterange.hpp"
#include "rfc822text.hpp"

namespace npp2::data {

class NPP2_EXPORT Any
  : public Variant
{
  WORD type_;
  QByteArray raw_;

public:
  Any();

  Any(const char *ptr, size_t size, BOOL fPrefix = TRUE);

  Any(WORD type, const char *ptr, size_t size);

  virtual WORD dataType() const override;

  const QByteArray &raw() const;

  Lmbcs toText(bool fReplace = true) const;

  TextList toTextList(bool fReplace = true) const;

  Number toNumber() const;

  NumberRange toNumberRange() const;

  TimeDate toTimeDate() const;

  TimeDateRange toTimeDateRange() const;

  Rfc822Text toRfc822Text() const;

  Lmbcs toString() const;
};

} // namespace npp2::data

#endif // NPP2_DATA_ANY_HPP
