#ifndef NPP2_DATA_VARIANT_HPP
#define NPP2_DATA_VARIANT_HPP

#include "../npp2_global.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::data {

class NPP2_EXPORT Variant
{
public:
  Variant();

  virtual WORD dataType() const = 0;
};

} // namespace npp2::data

#endif // NPP2_DATA_VARIANT_HPP
