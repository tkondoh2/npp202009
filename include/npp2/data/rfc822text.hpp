#ifndef NPP2_DATA_RFC822TEXT_HPP
#define NPP2_DATA_RFC822TEXT_HPP

#include "../npp2_global.h"
#include "textlist.hpp"
#include "timedate.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <mimeods.h> // RFC822ITEMDESC

#ifdef NT
#pragma pack(pop)
#endif

namespace npp2::data {

class NPP2_EXPORT Rfc822Text
  : public Variant
{
  RFC822ITEMDESC desc_;
  QByteArray native_;
  QByteArray name_;
  QByteArray delim_;
  QByteArray body_;

public:
  virtual WORD dataType() const override;

  Rfc822Text();

  WORD version() const;
  DWORD flags() const;
  DWORD type() const;

  WORD nativeLen() const;
  WORD nameLen() const;
  WORD delimLen() const;
  WORD bodyLen() const;

  const QByteArray &native() const;
  const QByteArray &name() const;
  const QByteArray &delim() const;
  const QByteArray &body() const;

  bool isAddress() const;
  bool isDate() const;
  bool isText() const;
  bool isStorageStrict() const;
  bool isTextList() const;
  bool isUnused() const;

  Lmbcs text() const;

  TimeDate date() const;

  TextList textList() const;

  static Rfc822Text fromBytes(const void *ptr, BOOL fPrefix = FALSE);

  static void *copyODStoRFC822ITEMDESC(
    void *ppSrc,
    RFC822ITEMDESC *pDest,
    WORD iterations
  );

  static QByteArray getAndInc(char **pp, WORD len);

  static QByteArray trimLastCRLF(const QByteArray &bytes);

#ifndef NT
  template <typename T>
  static char *_copy(char* from, void *to) {
  memcpy(to, from, sizeof(T));
  return from + sizeof(T);
}
#endif
};

} // namespace npp2::data

#endif // NPP2_DATA_RFC822TEXT_HPP
