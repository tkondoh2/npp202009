#ifndef NPP2_DATA_TIMEDATE_HPP
#define NPP2_DATA_TIMEDATE_HPP

#include "../npp2_global.h"
#include "variant.hpp"
#include "time.hpp"
#include "lmbcs.hpp"
#include <QDateTime>
// #include <QTimeZone>

// #if defined(NT)
// #pragma pack(push, 1)
// #endif

// #include <ostime.h>

// #if defined(NT)
// #pragma pack(pop)
// #endif

namespace npp2::data {

/**
 * @brief 日時クラス(TIMEDATE型ラッパー)
 */
class NPP2_EXPORT TimeDate
  : public Variant
{
  TIMEDATE raw_;

public:
  virtual WORD dataType() const override;

  using type = TIMEDATE;
  static TIMEDATE defaultRaw();

  /**
   * @brief デフォルトコンストラクタ
   */
  TimeDate();

  TimeDate(const TIMEDATE &td);

  /**
   * @brief コンストラクタ(フィールドデータから構築)
   * @param ptr 数値型へのポインタ
   * @param fPrefix データタイププレフィックスの有無
   */
  TimeDate(const void *ptr, BOOL fPrefix);

  /**
   * @brief TIMEDATE型へのキャスト演算子
   */
  operator TIMEDATE() const;

  /**
   * @brief アドレス取得演算子
   * @return TIMEDATEデータへのポインタ
   */
  TIMEDATE *operator &();

  /**
   * @brief 現在の日時を指定したUTCオフセットの時間に変換する
   * @param offsetSeconds UTCオフセット秒(夏時間込み)
   * @param isDaylightTime 夏時間の有無
   * @return TIMEDATE型のデータ
   */
  TIMEDATE toTimeZone(int offsetSeconds, bool isDaylightTime) const;

  Time toTime() const;

  /**
   * @brief 現在の日時を取得
   * @return 現在の日時
   */
  static TIMEDATE getCurrent();

  Lmbcs toString() const noexcept(false);

  static Lmbcs toString(TIMEDATE td) noexcept(false);

  /**
   * @return ミニマム定数を返す
   */
  static TIMEDATE getMinimum();

  /**
   * @return マキシマム定数を返す
   */
  static TIMEDATE getMaximum();

  /**
   * @return ワイルドカード定数を返す
   */
  static TIMEDATE getWildcard();

  static TimeDate fromTime(const Time &t);

  static TIMEDATE fromTIMEToTIMEDATE(const TIME &time);

  static TIME fromTIMEDATEToTIME(const TIMEDATE &td);

  static TimeDate fromQDateTime(const QDateTime &dateTime) noexcept(false);

private:
  /**
   * @brief 日時定数の取得
   * @tparam T 取得する定数の設定値
   * @return 指定した日時定数
   */
  static TIMEDATE getConstant(WORD type);
};

using TimeDatePtr = std::unique_ptr<TimeDate>;

} // namespace npp2::data

#endif // NPP2_DATA_TIMEDATE_HPP
