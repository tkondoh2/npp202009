#ifndef NPP2_COLLECTION_HPP
#define NPP2_COLLECTION_HPP

#include "npp2_global.h"
#include "_private/handleobject.hpp"
#include <memory>
// #include "status.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nif.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

class NPP2_EXPORT Entries
  : public npp2::LockableObject3<DHANDLE>
{
public:
  Entries();

  Entries(DHANDLE handle);

  virtual ~Entries();
};

struct ReadEntriesResult
{
  COLLECTIONPOSITION pos_;
  Entries entries_; 
  WORD bufferLength_;
  DWORD numEntriesSkipped_;
  DWORD numEntriesReturned_;
  WORD signalFlags_;
};

class _Collection;
using Collection = std::unique_ptr<_Collection>;

struct NPP2_EXPORT NIFCloseCollectionDeleter {
  void operator ()(HCOLLECTION hCol);
};

class NPP2_EXPORT _Collection
  : public LockableObject2<HCOLLECTION,NIFCloseCollectionDeleter>
{
  UNID unid_;
  DHANDLE hCollapsedList_;
  DHANDLE hSelectedList_;

public:
  _Collection(
    HCOLLECTION hCol,
    UNID unid,
    DHANDLE hCollapsedList,
    DHANDLE hSelectedList
  );
  
  virtual ~_Collection();

  static Collection open(
    DBHANDLE hViewDb,
    DBHANDLE hDataDb,
    NOTEID viewNoteId,
    WORD openFlags,
    DHANDLE hUnread = NULLHANDLE
  );

  static Collection open(
    DBHANDLE hDb,
    NOTEID viewNoteId,
    WORD openFlags,
    DHANDLE hUnread = NULLHANDLE
  );

  static DWORD entriesCount(HCOLLECTION hCol);

  DWORD entriesCount() const;

  static void readEntries(
    HCOLLECTION hCol,
    WORD skipNavigator,
    DWORD skipCount,
    WORD returnNavigator,
    DWORD returnCount,
    DWORD returnMask,
    ReadEntriesResult *result
  );

  void readEntries(
    WORD skipNavigator,
    DWORD skipCount,
    WORD returnNavigator,
    DWORD returnCount,
    DWORD returnMask,
    ReadEntriesResult *result
  ) const;
};

} // namespace npp2

#endif // NPP2_COLLECTION_HPP
