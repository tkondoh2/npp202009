#ifndef NPP2_ADDIN_LOGMESSAGE_HPP
#define NPP2_ADDIN_LOGMESSAGE_HPP

#include "../status.hpp"
#include "../data/lmbcs.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <addin.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::addin {

template <class... Args>
void logMessageText(
  const data::Lmbcs &msg,
  STATUS AdditionalErrorCode = NOERROR,
  Args... args
) {
  AddInLogMessageText(
    const_cast<char*>(msg.constData()),
    ERR(AdditionalErrorCode),
    args...
  );
}

} // namespace npp2::addin

#endif // NPP2_ADDIN_LOGMESSAGE_HPP
