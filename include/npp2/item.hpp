#ifndef NPP2_ITEM_HPP
#define NPP2_ITEM_HPP

#include "npp2_global.h"
#include "_private/block.hpp"
#include "data/lmbcs.hpp"
#include "data/any.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <pool.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

class NPP2_EXPORT Item
{
  BLOCKID itemId_;
  WORD dataType_;
  BLOCKID valueId_;
  DWORD valueSize_;

public:
  Item();

  Item(BLOCKID itemId, WORD dataType, BLOCKID valueId, DWORD valueSize);

  const BLOCKID itemId() const;
  WORD dataType() const;
  const BLOCKID valueId() const;
  DWORD valueSize() const;

  bool isNull() const;
  operator bool() const;

  Block itemBlock() const;
  Block valueBlock() const;

  data::Any value() const;

  data::Lmbcs convertToText(
    const data::Lmbcs &lineDelimiter = "\n",
    WORD charsPerLine = MAXWORD,
    BOOL fStripTabs = FALSE
  ) const;

  data::Lmbcs convertToText(
    WORD valueType,
    char separator = '\0' // => ';'に変換
  ) const;
};

} // namespace npp2

#endif // NPP2_ITEM_HPP
