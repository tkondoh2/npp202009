#ifndef NPP2_NAMELOOKUP_HPP
#define NPP2_NAMELOOKUP_HPP

#include "npp2_global.h"
#include "data/lmbcs.hpp"
#include "data/lmbcslist.hpp"
#include "data/any.hpp"
#include <QByteArray>
#include <QList>
#include <QMap>

namespace npp2 {

class NPP2_EXPORT NameLookup
{
  class InnerList
  {
    QByteArray bytes_;
    int size_;

  public:
    InnerList(QByteArray &&bytes, int size);

    const char *constData() const;
    template <typename T> T size() const {
      return static_cast<T>(size_);
    }
  };

  static InnerList serialize(const data::LmbcsList &list);

  data::Lmbcs server_;
  data::LmbcsList viewList_;

public:
  struct Result
  {
    data::Lmbcs view_;
    data::Lmbcs name_;
    int match_;
    QMap<data::Lmbcs, data::Any> items_;
  };

  NameLookup(const data::Lmbcs &server, const data::LmbcsList &viewList);

  QList<Result> operator ()(
    const data::LmbcsList &nameList,
    const data::LmbcsList &itemList,
    WORD flags = 0
  );
};

} // namespace npp2

#endif // NPP2_NAMELOOKUP_HPP
