#ifndef NPP2_UNREADIDTABLE_HPP
#define NPP2_UNREADIDTABLE_HPP

#include "idtable.hpp"

namespace npp2 {

class NPP2_EXPORT UnreadIdTable
  : public IdTable
{
  DBHANDLE hDb_;
  DHANDLE hOriginalTable_;

  UnreadIdTable(const UnreadIdTable &) = delete;
  UnreadIdTable& operator=(const UnreadIdTable &) = delete;

  explicit UnreadIdTable(DBHANDLE hDb, DHANDLE handle, DHANDLE hOrigin);

  static UnreadIdTable *build(DBHANDLE hDb);

public:

  virtual ~UnreadIdTable();

  void update();

  friend class _Database;
};

} // namespace npp2

#endif // NPP2_UNREADIDTABLE_HPP
