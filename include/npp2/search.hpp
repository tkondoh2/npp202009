#ifndef NPP2_SEARCH_HPP
#define NPP2_SEARCH_HPP

#include "npp2_global.h"
#include "data/lmbcs.hpp"
#include <functional>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

using SearchCallback = std::function<STATUS(const SEARCH_MATCH&,ITEM_TABLE*)>;

class NPP2_EXPORT Search
{
public:
  Search();

  TIMEDATE operator ()(
    DBHANDLE hDb,
    FORMULAHANDLE hFormula,
    const data::Lmbcs &viewTitle,
    WORD flags,
    WORD noteClassMask,
    SearchCallback fn,
    TIMEDATE *pSince = nullptr
  ) noexcept(false);

  TIMEDATE operator ()(
    DBHANDLE hDb,
    const data::Lmbcs &query,
    const data::Lmbcs &viewTitle,
    WORD flags,
    WORD noteClassMask,
    SearchCallback fn,
    TIMEDATE *pSince = nullptr
  ) noexcept(false);
};

} // namespace npp2

#endif // NPP2_SEARCH_HPP
