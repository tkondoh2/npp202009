#ifndef NPP2_HTTP1_EXEC_HPP
#define NPP2_HTTP1_EXEC_HPP

#include "./filter.hpp"

namespace npp2::http1 {

inline bool exec(const Filter &filter, const Request &req) {
  return filter.matchEventType(req->eventType())
    ? filter(req)
    : false;
}

} // namespace npp2::http1

#endif // NPP2_HTTP1_EXEC_HPP
