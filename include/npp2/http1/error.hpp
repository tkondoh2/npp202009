#ifndef NPP2_HTTP1_ERROR_HPP
#define NPP2_HTTP1_ERROR_HPP

#include <QtGlobal>
#include <dsapi.h>
#include <exception>

namespace npp2::http1 {

class Error
  : public std::exception
{
  uint id_;

public:
  Error(uint id)
    : std::exception()
    , id_(id)
  {}

  uint id() const { return id_; }

  virtual const char *what() const noexcept override {
    switch (id_) {
    case DSAPI_BUFFER_TOO_SMALL: return "[npp2::http1] Buffer too small";
    case DSAPI_INTERNAL_ERROR: return "[npp2::http1] Internal error";
    case DSAPI_INVALID_ARGUMENT: return "[npp2::http1] Invalid argument";
    case DSAPI_MEMORY_ERROR: return "[npp2::http1] Memory error";
    case DSAPI_REQUEST_ALREADY_OWNED: return "[npp2::http1] Request already owned";
    case 0: return "[npp2::http1] No error";
    default: return "[npp2::http1] Unknown error";
    }
  }
};

} // namespace npp2::http1

#endif // NPP2_HTTP1_ERROR_HPP
