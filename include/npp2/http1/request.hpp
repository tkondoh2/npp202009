#ifndef NPP2_HTTP1_REQUEST_HPP
#define NPP2_HTTP1_REQUEST_HPP

#include "./authenticateduser.hpp"
#include "./authenticate.hpp"
#include "../utils.hpp"
#include <QString>
#include <QByteArray>
#include <QPair>
#include <QList>
#include <QVariantMap>
#include <QUrl>
#include <QUrlQuery>
#include <memory>
#include <optional>

using QStringMap = QMap<QString,QString>;

inline QVariantMap strMapToVarMap(const QStringMap &map) {
  QVariantMap result;
  foreach (auto key, map.keys()) {
    result.insert(key, map.value(key));
  }
  return result;
}

namespace npp2::http1 {

class _Request;
using Request = std::unique_ptr<_Request>;

class _Request
{
  FilterContext *pCtx_;
  uint eventType_;
  void *ptr_;
  FilterRequest req_;

public:
  _Request(FilterContext *pCtx, uint eventType, void *ptr)
    : pCtx_(pCtx)
    , eventType_(eventType)
    , ptr_(ptr)
    , req_()
  {
    uint errId = 0;
    pCtx_->GetRequest(pCtx_, &req_, &errId);
  }

  uint eventType() const { return eventType_; }

	uint method() const { return req_.method; }
  const char *version() const { return req_.version; }

  QUrl url(const QString &dummyHost = "http://dummy") const {
    return QUrl(QString("%1%2").arg(dummyHost, req_.URL));
  }

  QString path(
    QUrl::ComponentFormattingOption options = QUrl::FullyDecoded
  ) const {
    return url().path(options);
  }

  bool matchUrl(
    const QString &path_,
    QUrl::ComponentFormattingOption options = QUrl::FullyDecoded
  ) const {
    return path(options) == path_;
  }

  QVariantMap queryMap(
    QUrl::ComponentFormattingOption options = QUrl::FullyDecoded
  ) const {
    QUrlQuery q(url());
    auto strmap = utils::pairListToMap(q.queryItems(options));
    return strMapToVarMap(strmap);
  }

  QByteArray rawBody() const {
    return QByteArray(
      req_.contentRead,
      static_cast<int>(req_.contentReadLen)
    );
  }

  QString body() const {
    return QString::fromUtf8(rawBody());
  }

  QStringMap bodyByUrlEncoded(
    QUrl::ComponentFormattingOption option = QUrl::FullyDecoded
  ) const {
    QUrlQuery q(body());
    auto pairs = q.queryItems(option);
    return utils::pairListToMap(pairs);
  }

  bool writeClient(const QByteArray &bytes) const {
    uint errId = 0;
    if (!pCtx_->WriteClient(
      pCtx_,
      const_cast<char*>(bytes.constData()),
      static_cast<uint>(bytes.size()),
      0,
      &errId
      )
    || errId != 0) {
      return false;
    }
    return true;
  }

  template <uint FuncType, class T>
  void serverSupport(T *dataPtr) const {
    uint ErrID = 0;
    auto result = pCtx_->ServerSupport(
      pCtx_,
      FuncType,
      dataPtr,
      nullptr,
      0,
      &ErrID
    );
    if (!result || ErrID != 0) { throw Error(ErrID); }
  }

  AuthenticatedUser getAuthenticatedUser(uint flags) const {
    FilterAuthenticatedUser data;
    data.fieldFlags = static_cast<FilterAuthenticatedUserFields>(flags);
    serverSupport<kGetAuthenticatedUserInfo>(&data);
    return AuthenticatedUser(data);
  }

  std::optional<Authenticate> getAuthenticate() const {
    if (eventType() == kFilterAuthenticate) {
      return Authenticate(
        pCtx_,
        reinterpret_cast<FilterAuthenticate*>(ptr_)
      );
    }
    return std::nullopt;
  }
};

} // namespace npp2::http1

#endif // NPP2_HTTP1_REQUEST_HPP
