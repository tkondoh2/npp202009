#ifndef NPP2_HTTP1_EXCEPTION_HPP
#define NPP2_HTTP1_EXCEPTION_HPP

#include "./directresponse.hpp"
#include <QVariantMap>
#include <QString>
#include <QStringList>
#include <QUrl>
#include <QUrlQuery>
#include <QJsonDocument>
#include <exception>

namespace npp2::http1 {

inline QByteArray encodeForQueryItem(const QString &value) {
  return QUrl::toPercentEncoding(value, "*", " ");
}

class Exception
  : public std::exception
{
  QVariantMap headers_;
  uint responseCode_;
  QString reasonText_;
  QString errorCode_;
  QString description_;
  QVariantMap options_;

  Exception(
    QVariantMap &&headers,
    uint responseCode,
    QString &&reasonText,
    QString &&errorCode,
    QString &&description,
    QVariantMap &&options = QVariantMap()
  )
    : std::exception()
    , headers_(std::move(headers))
    , responseCode_(responseCode)
    , reasonText_(std::move(reasonText))
    , errorCode_(std::move(errorCode))
    , description_(std::move(description))
    , options_(std::move(options))
  {}

public:
  Exception(const Exception &other) = default;

  const QString &reasonText() const { return reasonText_; }
  const QString &errorCode() const { return errorCode_; }
  const QString &description() const { return description_; }

  QString contentType() const {
    return headers_.value("Content-Type").toString();
  }

  bool isContentType(const QString &type) const {
    auto ct = contentType().left(type.length());
    return type.compare(ct, Qt::CaseInsensitive) == 0;
  }

  bool isRedirect() const {
    return isContentType("text/html") && headers_.contains("Location");
  }

  bool isJson() const {
    return isContentType("application/json");
  }

  static Exception makeHtml(
    uint responseCode,
    QString &&reasonText,
    QString &&errorCode,
    QString &&description,
    QVariantMap &&options = QVariantMap(),
    const QVariantMap &headers = QVariantMap()
  ) {
    QVariantMap h(headers);
    if (!h.contains("Content-Type")) {
      h.insert("Content-Type", "text/html; charset=utf-8");
    }
    return Exception(
      std::move(h),
      responseCode,
      std::move(reasonText),
      std::move(errorCode),
      std::move(description),
      std::move(options)
    );
  }

  bool responseHtmlError(
    const Request &req,
    QString &&page
  ) const {
    if (page.isEmpty()) {
      page = R"(<!DOCTYPE html>
<html><head><title>${response_code} ${reason_text}</title></head>
<body>
<h1>${response_code} ${reason_text}</h1>
<p>code: ${error_code}</p>
<p>${error_description}</p>
${options}
</body> </html>)";
    }
    page.replace("${response_code}", QString::number(responseCode_));
    page.replace("${reason_text}", reasonText_);
    page.replace("${error_code}", errorCode_);
    page.replace("${error_description}", description_);

    QStringList optList;
    foreach (auto key, options_.keys()) {
      auto value = options_.value(key).toString();
      optList.append(
        QString("<dt>%1</dt><dd>%2</dd>").arg(key).arg(value)
      );
    }

    if (!optList.isEmpty()) {
      page.replace(
        "${options}",
        QString("<dl>%1</dl>").arg(optList.join(""))
      );
    }

    return DirectResponse(
      responseCode_,
      QString(reasonText_),
      std::move(page)
    ).response(req);
  }

  static Exception makeRedirect(
      QString &&redirectUri,
      QString &&errorCode,
      QString &&description,
      QVariantMap &&options = QVariantMap(),
      const QVariantMap &headers = QVariantMap()
      ) {
    QVariantMap h(headers);
    if (!h.contains("Content-Type")) {
      h.insert("Content-Type", "text/html; charset=utf-8");
    }
    if (!h.contains("Location")) {
      h.insert("Location", std::move(redirectUri));
    }
    return Exception(
      std::move(h),
      302,
      "Found",
      std::move(errorCode),
      std::move(description),
      std::move(options)
    );
  }

  bool responseRedirectError(const Request &req) const {
    if (!isRedirect()) return false;

    DirectResponse res(responseCode_, QString(reasonText_), "");
    res.uniteToHeaders(headers_);
    QUrl url(res.takeFromHeader("Location"));
    QUrlQuery query(url);
    query.addQueryItem(
      "error",
      encodeForQueryItem(errorCode_)
    );
    query.addQueryItem(
      "error_description",
      encodeForQueryItem(description_)
    );

    foreach (auto key, options_.keys()) {
      query.addQueryItem(
        key,
        encodeForQueryItem(options_.value(key).toString())
      );
    }

    url.setQuery(query);
    res.insertToHeader("Location", url.url(QUrl::FullyEncoded));
    return res.response(req);
  }

  static Exception makeJson(
    uint responseCode,
    QString &&reasonText,
    QString &&errorCode,
    QString &&description,
    QVariantMap &&options = QVariantMap(),
    const QVariantMap &headers = QVariantMap()
  ) {
    QVariantMap h(headers);
    if (!h.contains("Content-Type")) {
      h.insert("Content-Type", "application/json; charset=utf-8");
    }
    return Exception(
      std::move(h),
      responseCode,
      std::move(reasonText),
      std::move(errorCode),
      std::move(description),
      std::move(options)
    );
  }

  bool responseJsonError(const Request &req) const {
    if (!isJson()) return false;

    QVariantMap data {
      { "error", errorCode_ },
      { "error_description", description_ }
    };

    foreach (QString key, options_.keys()) {
      data.insert(key, options_.value(key));
    }

    QJsonDocument json = QJsonDocument::fromVariant(data);
    DirectResponse res(
      responseCode_,
      QString(reasonText_),
      QString::fromUtf8(json.toJson(QJsonDocument::Compact))
    );
    res.uniteToHeaders(headers_);
    return res.response(req);
  }
};

} // namespace npp2::http1

#endif // NPP2_HTTP1_EXCEPTION_HPP
