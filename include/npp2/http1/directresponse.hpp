#ifndef NPP2_HTTP1_DIRECTRESPONSE_HPP
#define NPP2_HTTP1_DIRECTRESPONSE_HPP

#include "./request.hpp"
#include <QString>
#include <QMap>
#include <QTextStream>
#include <QIODevice>
#include <QByteArray>

namespace npp2::http1 {

using Headers = QMap<QString,QString>;

class DirectResponse
{
  uint code_;
  QString reasonText_;
  Headers headers_;
  QString body_;

public:
  DirectResponse()
    : code_(200)
    , reasonText_("OK")
    , headers_()
    , body_()
  {}

  DirectResponse(QString &&body)
    : code_(200)
    , reasonText_("OK")
    , headers_()
    , body_(std::move(body))
  {}

  DirectResponse(uint code, QString &&reasonText, QString &&body)
    : code_(code)
    , reasonText_(std::move(reasonText))
    , headers_()
    , body_(std::move(body))
  {}

  void insertToHeader(const QString &key, const QString &value) {
    headers_.insert(key, value);
  }

  void uniteToHeaders(const QVariantMap &others) {
    foreach (auto key, others.keys()) {
      headers_.insert(key, others.value(key).toString());
    }
  }

  QString takeFromHeader(const QString &key) {
    return headers_.contains(key)
      ? headers_.take(key)
      : QString();
  }

  void setContentType(const QString &value) {
    insertToHeader("Content-Type", value);
  }

  bool response(const Request &req) const {
    auto bytes = render(req);
    return req->writeClient(bytes);
  }

  QByteArray render(const Request &req) const {
    QString buffer;
    QTextStream stream(&buffer, QIODevice::WriteOnly);

    // ステータス行
    stream << QString("%1 %2 %3")
      .arg(req->version())
      .arg(code_)
      .arg(reasonText_)
      << endl;

    // レスポンスヘッダー
    foreach (QString key, headers_.keys()) {
      stream << QString("%1: %2")
        .arg(key, headers_.value(key))
        << endl;
    }

    // ボディ
    if (!body_.isEmpty()) {
      stream << endl << body_;
    }

    stream.flush();
    return buffer.toUtf8();
  }
};

} // namespace npp2::http1

#endif // NPP2_HTTP1_DIRECTRESPONSE_HPP
