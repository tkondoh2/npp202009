#ifndef NPP2_HTTP1_AUTHENTICATEDUSER_HPP
#define NPP2_HTTP1_AUTHENTICATEDUSER_HPP

#include "./error.hpp"

namespace npp2::http1 {

class AuthenticatedUser
{
  FilterAuthenticatedUser value_;

public:
  AuthenticatedUser(const FilterAuthenticatedUser &value)
    : value_(value)
  {}

  template <class T>
  T userCanonicalName() const {
    return T(value_.pUserCannonicalName);
  }

  template <class T>
  T webUserName() const {
    return T(value_.pWebUserName);
  }

  template <class T>
  T userPassword() const {
    return T(value_.pUserPassword);
  }

  template <class T>
  T userGroupList() const {
    return T(value_.pUserGroupList);
  }
};

} // namespace npp2::http1

#endif // NPP2_HTTP1_AUTHENTICATEDUSER_HPP
