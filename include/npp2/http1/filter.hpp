#ifndef NPP2_HTTP1_FILTER_HPP
#define NPP2_HTTP1_FILTER_HPP

#include "./request.hpp"

namespace npp2::http1 {

class Filter
{
public:
  virtual bool matchEventType(uint eventType) const = 0;

  virtual bool operator ()(const Request &req) const = 0;
};

} // namespace npp2::http1

#endif // NPP2_HTTP1_FILTER_HPP
