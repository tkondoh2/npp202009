#ifndef NPP2_HTTP1_AUTHENTICATE_HPP
#define NPP2_HTTP1_AUTHENTICATE_HPP

#include "../data/lmbcs.hpp"
#include <dsapi.h>
#include <QByteArray>

namespace npp2::http1 {

class Authenticate
{
  FilterContext *pCtx_;
  FilterAuthenticate *ptr_;

public:
  Authenticate(FilterContext *pCtx, FilterAuthenticate *ptr)
    : pCtx_(pCtx)
    , ptr_(ptr)
  {}

  bool foundInCache() const {
    return ptr_->foundInCache != 0;
  }

  template <size_t BufferSize = 1024>
  QList<data::Lmbcs> getUserNameList() const {
    QList<data::Lmbcs> list;
    char buffer[BufferSize];
    uint count = 0, ErrID = 0;
    int size = ptr_->GetUserNameList(
      pCtx_,
      reinterpret_cast<LMBCS*>(buffer),
      static_cast<uint>(BufferSize),
      &count,
      0,
      &ErrID
    );
    int p = 0, i = 0;
    while (
      ((size >= 0 && p < size) || (size < 0 && p < BufferSize))
      && i++ < static_cast<int>(count)
    ) {
      list.append(data::Lmbcs(buffer + p));
      p += qstrlen(buffer + p) + 1;
    }
    return list;
  }

  template <size_t BufferSize = 1024>
  QByteArray getHeader(const QByteArray &name) const {
    char buffer[BufferSize] = "";
    uint ErrID = 0;
    int bytes = ptr_->GetHeader(
      pCtx_,
      const_cast<char*>(name.constData()),
      buffer,
      BufferSize,
      &ErrID
    );
    if (ErrID != 0) {
      throw Error(ErrID);
    }
    else if (bytes < 1) {
      return QByteArray();
    }
    else {
      return QByteArray(buffer, bytes - 1);
    }
  }

  char *setAuthName(const data::Lmbcs &authName) const {
    return qstrncpy(
      reinterpret_cast<char*>(ptr_->authName),
      authName.constData(),
      ptr_->authNameSize
    );
  }

  void setAuthType(FilterAuthenticationTypes type) const {
    ptr_->authType = type;
  }
};

} // namespace npp2::http1

#endif // NPP2_HTTP1_AUTHENTICATE_HPP
