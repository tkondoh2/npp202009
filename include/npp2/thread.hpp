#ifndef NPP2_THREAD_HPP
#define NPP2_THREAD_HPP

#include "npp2_global.h"
#include "status.hpp"

namespace npp2 {

class NPP2_EXPORT ThreadLocker
{
  Status status_;

public:
  ThreadLocker();

  virtual ~ThreadLocker();
};

} // namespace npp2

#endif // NPP2_THREAD_HPP
