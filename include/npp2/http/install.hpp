#ifndef NPP2_HTTP_INSTALL_HPP
#define NPP2_HTTP_INSTALL_HPP

#include "error.hpp"
#include <QByteArray>

namespace npp2::http {

inline void install(
  FilterInitData *pInitData,
  const char *title,
  unsigned int eventFlags
) {
  pInitData->appFilterVersion = kInterfaceVersion;
  pInitData->eventFlags = eventFlags;
  qstrcpy(pInitData->filterDesc, title);
}

/**
 * @brief DSAPIのeventFlagsのOR演算子で合成する。
 * @tparam T eventFlagsの型(通常uint)
 * @tparam Head eventFlags静的メンバ定数を持つ先頭のクラス
 * @tparam Tails eventFlags静的メンバ定数を持つ2番目以降のクラス
 * @return 合成されたeventFlags値
 */
template <class T, class Head, class... Tails>
T filterEventSummary() {
  return Head::eventFlags() | filterEventSummary<T, Tails...>();
}

/**
 * @brief DSAPIのeventFlags合成で終端値を返す。
 * 不定数のクラスを処理するためのテンプレート関数
 * @tparam T eventFlagsの型(通常uint)
 * @return 0
 */
template <class T>
T filterEventSummary() {
  return 0;
}

} // namespace npp2::http

#endif // NPP2_HTTP_INSTALL_HPP
