#ifndef NPP2_HTTP_REQUEST_HPP
#define NPP2_HTTP_REQUEST_HPP

#include "error.hpp"
#include "../utils/pairlisttomap.hpp"
#include <dsapi.h>
#include <QSharedPointer>
#include <QRegExp>
#include <QUrl>
#include <QUrlQuery>

namespace npp2::http {

class Request
{
  FilterContext *ctx_;
  unsigned int eventType_;
  void *ptr_;
  FilterRequest request_;
  QString path_;
  QStringList params_;
  QString query_;
  QMap<QString,QString> queryMap_;

public:
  /**
   * @brief コンストラクタ
   * @param ctx コンテキスト
   * @param eventType イベントタイプ
   * @param ptr イベントごとの追加アクセスポイント
   */
  explicit Request(
      FilterContext *ctx,
      uint eventType,
      void *ptr,
      QUrl::ComponentFormattingOption options = QUrl::FullyDecoded
      )
    : ctx_(ctx)
    , eventType_(eventType)
    , ptr_(ptr)
  {
    uint errId = 0;
    if (!ctx_->GetRequest(ctx_, &request_, &errId) || errId != 0) {
      throw Error(errId);
    }
    auto aUrl = url();
    path_ = aUrl.path(options);
    params_ = path_.split("/");
    query_ = aUrl.query(options);
    QUrlQuery urlQuery(aUrl);
    queryMap_ = utils::pairListToMap<QString,QString>(
          urlQuery.queryItems(options)
          );
  }

  /**
   * @brief コンテキスト
   * @return FilterContext*
   */
  FilterContext *ctx() const { return ctx_; }

  /**
   * @brief イベントタイプ
   * @return uint
   */
  uint eventType() const { return eventType_; }

  void *ptr() const { return ptr_; }

  /**
   * @brief HTTPバージョン
   * @return QString
   */
  QString version() const {
    return request_.version;
  }

  /**
   * @brief HTTPメソッドコード
   * @return uint
   */
  uint method() const {
    return request_.method;
  }

  /**
   * @brief リクエストパス
   * @param options フォーマットオプション
   * @return QString
   */
  const QString &path() const { return path_; }

  QStringList params() const { return params_; }

  /**
   * @brief リクエストクエリ
   * @param options フォーマットオプション
   * @return QString
   */
  QString query() const { return query_; }

  const QMap<QString,QString> queryMap() const { return queryMap_; }

  /**
   * @brief 生ボディ
   * @return QByteArray
   */
  QByteArray rawBody() const {
    return QByteArray(
          request_.contentRead,
          static_cast<int>(request_.contentReadLen)
          );
  }

  /**
   * @brief パスが正規表現にマッチするか
   * @param regexp 検索する正規表現
   * @param options フォーマットオプション
   * @return true 正規表現にマッチしている
   * @return false 正規表現にマッチしていない
   */
  bool matchPath(const QRegExp &regexp) const {
    return regexp.exactMatch(path_);
  }

private:
  /**
   * @brief リクエストURLを取得する
   * host部はダミー値(QUrlの仕様による)
   * @return QUrl
   */
  QUrl url() const {
    QString dummyHost("http://dummy");
    return QUrl(QString("%1%2").arg(dummyHost, request_.URL));
  }
};

using RequestPtr = QSharedPointer<Request>;

} // namespace npp2::http

#endif // NPP2_HTTP_REQUEST_HPP
