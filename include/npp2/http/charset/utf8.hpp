#ifndef NPP2_HTTP_CHARSET_UTF8_HPP
#define NPP2_HTTP_CHARSET_UTF8_HPP

#include <QString>

namespace npp2::http::charset {

/**
 * @brief レスポンス時の本体の文字セット定義(UTF-8版)
 */
struct Utf8
{
  /**
   * @brief charset=***の文字列を返す
   * @return QString
   */
  static QString charSet() {
    return QString("utf-8");
  }

  /**
   * @brief 本体データを文字コードに合わせて変換
   * @param body 本体(QString)
   * @return QByteArray 文字コードに変換されたバイト文字列
   */
  static QByteArray translate(const QString &body) {
    return body.toUtf8();
  }
};

} // namespace npp2::http::charset

#endif // NPP2_HTTP_CHARSET_UTF8_HPP
