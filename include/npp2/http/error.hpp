#ifndef NPP2_HTTP_ERROR_HPP
#define NPP2_HTTP_ERROR_HPP

#include <dsapi.h>
#include <exception>

namespace npp2::http {

class Error
    : public std::exception
{
  unsigned int raw_;

public:
  explicit Error(unsigned int raw) : std::exception() , raw_(raw) {}

  unsigned int raw() const { return raw_; }

  virtual const char *what() const noexcept override {
    switch (raw_) {
    case DSAPI_BUFFER_TOO_SMALL: return "[npp2::http] Buffer too small.";
    case DSAPI_INTERNAL_ERROR: return "[npp2::http] Internal error.";
    case DSAPI_INVALID_ARGUMENT: return "[npp2::http] Invalid argument.";
    case DSAPI_MEMORY_ERROR: return "[npp2::http] Memory error.";
    case DSAPI_REQUEST_ALREADY_OWNED: return "[npp2::http] Request already owned.";
    case 0: return "[npp2::http] No error.";
    default: return "[npp2::http] Unknown error.";
    }
  }
};

} // namespace npp2::http

#endif // NPP2_HTTP_ERROR_HPP
