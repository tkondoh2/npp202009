#ifndef NPP2_HTTP_EXCEPTION_HPP
#define NPP2_HTTP_EXCEPTION_HPP

#include "request.hpp"
#include "responsestatus.hpp"

namespace npp2::http {

class Exception
{
  unsigned int status_;
  QString statusText_;
  QMap<QString,QString> headers_;
  QVariantMap data_;

public:
  Exception()
    : status_(static_cast<unsigned int>(ResponseStatus::Ok))
    , statusText_(npp2::http::statusText(ResponseStatus::Ok))
    , headers_()
    , data_()
  {}

  Exception(
      const ResponseStatus &status,
      const QVariantMap &data,
      const QMap<QString,QString> &headers = {}
      )
    : status_(static_cast<unsigned int>(status))
    , statusText_(npp2::http::statusText(status))
    , headers_(headers)
    , data_(data)
  {}

  Exception(
      unsigned int status,
      const QString &statusText,
      const QVariantMap &data,
      const QMap<QString,QString> &headers = {}
      )
    : status_(status)
    , statusText_(statusText)
    , headers_(headers)
    , data_(data)
  {}

  unsigned int status() const { return status_; }
  const QString &statusText() const { return statusText_; }
  const QMap<QString,QString> &headers() const { return headers_; }
  const QVariantMap &data() const { return data_; }

  bool isOk() const { return status_ == 200; }
};

} // namespace npp2::http

#endif // NPP2_HTTP_EXCEPTION_HPP
