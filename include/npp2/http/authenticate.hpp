#ifndef NPP2_HTTP_AUTHENTICATE_HPP
#define NPP2_HTTP_AUTHENTICATE_HPP

#include "error.hpp"
#include "../data/lmbcs.hpp"
#include <QList>

namespace npp2::http {

class Authenticate
{
  FilterContext *ctx_;
  FilterAuthenticate *ptr_;

public:
  Authenticate(FilterContext *ctx, void *ptr)
    : ctx_(ctx)
    , ptr_(reinterpret_cast<FilterAuthenticate*>(ptr))
  {}

  data::Lmbcs userName() const {
    return data::Lmbcs(reinterpret_cast<const char*>(ptr_->userName));
  }

  bool foundInCache() const {
    return ptr_->foundInCache != 0;
  }

  template <int BufferSize = 1024>
  QList<data::Lmbcs> getUserNameList() const noexcept(false) {
    QList<data::Lmbcs> result;
    char buffer[BufferSize];
    uint count = 0, ErrID = 0;
    int size = ptr_->GetUserNameList(
          ctx_,
          reinterpret_cast<LMBCS*>(buffer),
          static_cast<uint>(BufferSize),
          &count,
          0,
          &ErrID
          );
    if (ErrID != 0) { throw Error(ErrID); }
    int p = 0, i = 0;
    while (((size >= 0 && p < size) || (size < 0 && p < BufferSize))
           && i++ < static_cast<int>(count)
           ) {
      result.append(data::Lmbcs(buffer + p));
      p += qstrlen(buffer + p) + 1;
    }
    return result;
  }
};

} // namespace npp2::http

#endif // NPP2_HTTP_AUTHENTICATE_HPP
