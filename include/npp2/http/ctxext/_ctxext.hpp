#ifndef NPP2_HTTP__CTXEXT_HPP
#define NPP2_HTTP__CTXEXT_HPP

#include "../request.hpp"

namespace npp2::http::ctxext {

template <typename T>
class _ContextExtension
{
protected:
  FilterContext *ctx_;
  T *ptr_;

public:
  _ContextExtension(const Request &req)
    : ctx_(req.ctx())
    , ptr_(reinterpret_cast<T*>(req.ptr()))
  {}
};

} // namespace npp2::http::ctxext

#endif // NPP2_HTTP__CTXEXT_HPP
