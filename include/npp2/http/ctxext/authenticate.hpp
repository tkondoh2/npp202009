#ifndef NPP2_HTTP_CTXEXT_AUTHENTICATE_HPP
#define NPP2_HTTP_CTXEXT_AUTHENTICATE_HPP

#include "_ctxext.hpp"
#include "../../data/lmbcs.hpp"
#include <functional>

namespace npp2::http::ctxext {

class Authenticate
    : public _ContextExtension<FilterAuthenticate>
{
public:
  Authenticate(const Request &req)
    : _ContextExtension<FilterAuthenticate>(req)
  {}

  data::Lmbcs userName() const {
    return data::Lmbcs(reinterpret_cast<const char*>(ptr_->userName));
  }

  bool foundInCache() const {
    return ptr_->foundInCache != 0;
  }

  template <int BufferSize = 1024>
  void getUserNameList(
      std::function<void(data::Lmbcs&&)> fn
      ) const noexcept(false) {
    char buffer[BufferSize];
    uint count = 0, ErrID = 0;
    int size = ptr_->GetUserNameList(
          ctx_,
          reinterpret_cast<LMBCS*>(buffer),
          static_cast<uint>(BufferSize),
          &count,
          0,
          &ErrID
          );
    if (ErrID != 0) { throw Error(ErrID); }
    int p = 0, i = 0;
    while (((size >= 0 && p < size) || (size < 0 && p < BufferSize))
           && i++ < static_cast<int>(count)
           ) {
      fn(Lmbcs(buffer + p));
      p += qstrlen(buffer + p) + 1;
    }
  }
};

} // namespace npp2::http::ctxext

#endif // NPP2_HTTP_CTXEXT_AUTHENTICATE_HPP
