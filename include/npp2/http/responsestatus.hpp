#ifndef NPP2_HTTP_RESPONSESTATUS_HPP
#define NPP2_HTTP_RESPONSESTATUS_HPP

#include <QString>

namespace npp2::http {

/**
 * @brief レスポンスステータス
 * レスポンス時のHTTPステータス
 */
enum class ResponseStatus: unsigned int {
  Ok = 200,
  Found = 302,
  BadRequest = 400,
  Unauthorized = 401,
  Forbidden = 403,
  InternalServerError = 500
};

inline QString statusText(const ResponseStatus &status) {
  switch (status) {
//  case ResponseStatus::Ok: return "OK";
  case ResponseStatus::Found: return "Found";
  case ResponseStatus::BadRequest: return "Bad Request";
  case ResponseStatus::Unauthorized: return "Unauthorized";
  case ResponseStatus::Forbidden: return "Forbidden";
  case ResponseStatus::InternalServerError: return "Internal Server Error";
  default: return "OK";
  }
}

} // namespace npp2::http

#endif // NPP2_HTTP_RESPONSESTATUS_HPP
