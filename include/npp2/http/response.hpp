#ifndef NPP2_HTTP_RESPONSE_HPP
#define NPP2_HTTP_RESPONSE_HPP

#include "request.hpp"
#include "responsestatus.hpp"
#include <QTextStream>

namespace npp2::http {

class Response
{
  unsigned int status_; ///< ステータス
  QString statusText_; ///< ステータステキスト
  QString type_; ///< 一般的なコンテンツタイプ
  QString contentType_; ///< カスタムコンテンツタイプ
  QMap<QString,QString> headers_; ///< コンテンツタイプ以外のレスポンスヘッダー
  QString body_; ///< 本体データ
  QTextStream stream_; ///< 本体データに追記するストリーム
  bool ended_; ///< レスポンス済みならばtrue

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Response()
    : status_(200)
    , statusText_("OK")
    , type_("text/plain")
    , contentType_()
    , headers_()
    , body_()
    , stream_(&body_, QIODevice::WriteOnly)
    , ended_(false)
  {}

  bool ended() const { return ended_; }

  /**
   * @brief ステータスの設定(enum定義がない時)
   * @param status ステータス値
   * @param text ステータステキスト
   */
  Response &setStatus(uint status, const QString &text) {
    if (ended_) throw std::runtime_error("Already ended.");
    status_ = status;
    statusText_ = text;

    return *this;
  }

  /**
   * @brief ステータスの設定(enum定義がある時)
   * @param status ステータスenum型
   */
  Response &setStatus(ResponseStatus status) {
    if (ended_) {
      throw std::runtime_error("Already ended.");
    }

    status_ = static_cast<uint>(status);
    statusText_ = statusText(status);
    return *this;
  }

  /**
   * @brief コンテンツタイプの設定
   * charsetと自動で組み合わせる一般的な仕様
   * @param type コンテンツタイプ
   */
  Response &setType(const QString &type) {
    if (ended_) throw std::runtime_error("Already ended.");
    type_ = type;

    return *this;
  }

  Response &addHeaders(const QMap<QString,QString> &map) {
    foreach (auto key, map.keys()) {
      addHeader(key, map.value(key));
    }

    return *this;
  }

  Response &addHeader(const QString &key, const QString &value) {
    headers_.insert(key, value);

    return *this;
  }

  /**
   * @brief 本体データを追記する
   * @param data 本体データ
   */
  Response &send(const QString &data) {
    if (ended_) throw std::runtime_error("Already ended.");
    stream_ << data;

    return *this;
  }

  /**
   * @brief レスポンスデータをクライアントに返す
   * @param req リクエストオブジェクト
   */
  template <typename T>
  Response &end(QSharedPointer<Request> req) noexcept(false) {
    if (ended_) throw std::runtime_error("Already ended.");

    QString buffer;
    QTextStream s(&buffer, QIODevice::WriteOnly);

    // ステータス行
    s << QString("%1 %2 %3")
         .arg(req->version())
         .arg(status_, 3)
         .arg(statusText_)
      << endl;

    // コンテンツタイプをヘッダーに組み込む
    headers_.insert(
          "Content-Type",
          contentType_.isEmpty()
            ? QString("%1; charset=%2").arg(type_, T::charSet())
            : contentType_
          );

    // ヘッダー
    foreach (QString key, headers_.keys()) {
      s << QString("%1: %2").arg(key, headers_.value(key)) << endl;
    }

    // 空行
    s << endl;

    // ここまでをLatin1に変換して出力データに追加
    QByteArray bytes = buffer.toLatin1();

    // ボディデータを指定の文字セットに変換して出力データに追加
    if (!body_.isEmpty()) {
      bytes += T::translate(body_);
    }

    // 出力の実行
    uint errId = 0;
    if (!req->ctx()->WriteClient(
          req->ctx(),
          bytes.data(),
          static_cast<uint>(bytes.length()),
          0,
          &errId
          ) || errId != 0) {
      throw Error(errId);
    }

    // 2度目を禁止
    ended_ = true;

    return *this;
  }
};

using ResponsePtr = QSharedPointer<Response>;

} // namespace npp2::http

#endif // NPP2_HTTP_RESPONSE_HPP
