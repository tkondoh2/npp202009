#ifndef NPP2_HTTP_FILTER_HPP
#define NPP2_HTTP_FILTER_HPP

#include "request.hpp"
#include "response.hpp"

namespace npp2::http {

template <unsigned int EVENT_FLAGS>
class Filter
{
public:
  static unsigned int eventFlags() { return EVENT_FLAGS; }

  virtual unsigned int operator ()(
      RequestPtr req,
      ResponsePtr res
      ) noexcept = 0;
};

} // namespace npp2::http

#endif // NPP2_HTTP_FILTER_HPP
