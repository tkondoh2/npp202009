#ifndef NPP2_HTTP_EXCEPTIONSENDER_HPP
#define NPP2_HTTP_EXCEPTIONSENDER_HPP

#include "exception.hpp"
#include "response.hpp"
#include "charset/utf8.hpp"
#include <QJsonDocument>
#include <QVariant>

namespace npp2::http {

class ExceptionSender
{
  QSharedPointer<Request> req_;
  QSharedPointer<Response> res_;

public:
  ExceptionSender(QSharedPointer<Request> req, QSharedPointer<Response> res)
    : req_(req)
    , res_(res)
  {}

  void byJson(const Exception &ex) {
    auto json = QJsonDocument::fromVariant(ex.data());
    auto body = QString::fromUtf8(json.toJson());
    res_->setStatus(ex.status(), ex.statusText())
        .setType("application/json")
        .addHeaders(ex.headers())
        .send(body)
        .end<charset::Utf8>(req_);
  }

  void byJson(const std::exception &ex) {
    const auto status = ResponseStatus::InternalServerError;
    const QString statusText_(statusText(status));
    QVariantMap data;
    data.insert("message", ex.what());
    auto json = QJsonDocument::fromVariant(data);
    auto body = QString::fromUtf8(json.toJson());
    res_->setStatus(static_cast<unsigned int>(status), statusText_)
        .setType("application/json")
        .send(body)
        .end<charset::Utf8>(req_);
  }

  void byHtml(const Exception &ex) {
    QString buffer;
    QTextStream s(&buffer);
    foreach (auto key, ex.data().keys()) {
      s << "<dt>" << key << "</dt>"
        << "<dd>" << ex.data().value(key).toString() << "</dd>"
        << endl;
    }
    const QString html(R"(<html>
  <body>
    <h1>%1 %2</h1>
    <dl>%3</dl>
  </body>
</html>)");
    res_->setStatus(ex.status(), ex.statusText())
        .setType("text/html")
        .addHeaders(ex.headers())
        .send(html.arg(ex.status()).arg(ex.statusText(), buffer))
        .end<charset::Utf8>(req_);
  }

  void byHtml(const std::exception &ex) {
    QString buffer;
    QTextStream s(&buffer);
    const auto status = ResponseStatus::InternalServerError;
    const QString statusText_(statusText(status));
    s << "<dt>Message</dt>"
      << "<dd>" << ex.what() << "</dd>"
      << endl;
    const QString html(R"(<html>
  <body>
    <h1>%1 %2</h1>
    <dl>%3</dl>
  </body>
</html>)");
    res_->setStatus(static_cast<unsigned int>(status), statusText_)
        .setType("text/html")
        .send(html
              .arg(static_cast<unsigned int>(status))
              .arg(statusText_, buffer)
              )
        .end<charset::Utf8>(req_);
  }

  void byRedirect(const Exception &ex) {
    QUrlQuery query;
    foreach (auto key, ex.data().keys()) {
      query.addQueryItem(key, ex.data().value(key).toString());
    }
    QUrl url(ex.headers().value("Location"));
    url.setQuery(query);
    auto headers = ex.headers();
    headers.insert("Location", url.toString(QUrl::FullyEncoded));
    res_->setStatus(ResponseStatus::Found)
        .addHeaders(headers)
        .end<charset::Utf8>(req_);
  }

  void byRedirect(const QString &location, const std::exception &ex) {
    QUrlQuery query;
    const ResponseStatus status = ResponseStatus::InternalServerError;
    const QString statusText_(statusText(status));
    query.addQueryItem(
          "status",
          QString::number(static_cast<unsigned int>(status))
          );
    query.addQueryItem("statusText", statusText_);
    query.addQueryItem("message", ex.what());
    QUrl url(location);
    url.setQuery(query);
    QMap<QString,QString> headers;
    headers.insert("Location", url.toString(QUrl::FullyEncoded));
    res_->setStatus(ResponseStatus::Found)
        .addHeaders(headers)
        .end<charset::Utf8>(req_);
  }
};

} // namespace npp2::http

#endif // NPP2_HTTP_EXCEPTIONSENDER_HPP
