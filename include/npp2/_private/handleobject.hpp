#ifndef NPP2_PRIVATE_HANDLEOBJECT_HPP
#define NPP2_PRIVATE_HANDLEOBJECT_HPP

#include "../npp2_global.h"
// #include "../status.hpp"
// #include <functional>
// #include <iostream>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

template <typename T>
class HandleObject
{
protected:
  T handle_;

public:
  HandleObject() : handle_(NULLHANDLE) {}

  HandleObject(T handle) : handle_(handle) {}

  T rawHandle() const { return handle_; }

  void setHandle(T handle) { handle_ = handle; }

  bool isNull() const { return handle_ == NULLHANDLE; }
};

template <typename T, class Deleter>
class LockableObject2
  : public HandleObject<T>
{
public:
  LockableObject2() : HandleObject<T>() {}

  LockableObject2(T handle) : HandleObject<T>(handle) {}

  virtual ~LockableObject2() {
    if (!this->isNull()) {
      Deleter()(this->rawHandle());
      this->setHandle(NULLHANDLE);
      // std::cout << "Free " << name() << std::endl;
    }
    // this->free();
  }
};

struct NPP2_EXPORT OSMemFreeDeleter {
  void operator ()(DHANDLE handle);
};

template <typename T>
class LockableObject3
  : public LockableObject2<T, OSMemFreeDeleter>
{
  public:
  LockableObject3()
    : LockableObject2<T, OSMemFreeDeleter>()
  {}

  LockableObject3(T handle)
    : LockableObject2<T, OSMemFreeDeleter>(handle)
  {}

  virtual ~LockableObject3() {}
};

template <typename T>
class Locker {
  T handle_;
  void *ptr_;
public:
  Locker(): handle_(NULLHANDLE), ptr_(nullptr) {}
  Locker(T handle): handle_(handle), ptr_(OSLockObject(handle_)) {}
  ~Locker() {
    if (handle_ != NULLHANDLE) {
      OSUnlockObject(handle_);
    }
  }

  template <typename P>
  P *ptr() const { return reinterpret_cast<P*>(ptr_); }
};

} // namespace npp2

#endif // NPP2_PRIVATE_HANDLEOBJECT_HPP
