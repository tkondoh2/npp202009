﻿#ifndef NPP2_PRIVATE_RANGE_HPP
#define NPP2_PRIVATE_RANGE_HPP

#include "../npp2_global.h"
#include "../utils/data.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include "global.h"
#include "osmem.h"

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

/**
 * @brief 範囲型シングルリストの要素数を取得
 * @param pRange 範囲型のロック済みポインタ
 * @param fPrefix データタイププレフィックスの有無
 * @return 範囲型シングルリストの要素数
 */
WORD NPP2_EXPORT RangeGetNumItem(void *pRange, BOOL fPrefix);

/**
 * @brief 範囲型ペアリストの要素数を取得
 * @param pRange 範囲型のロック済みポインタ
 * @param fPrefix データタイププレフィックスの有無
 * @return 範囲型ペアリストの要素数
 */
WORD NPP2_EXPORT RangeGetNumPair(void *pRange, BOOL fPrefix);

/**
 * @brief 範囲型用メモリを確保し、ハンドルとロック済みポインタを返す。
 * @tparam Traits 範囲型特性構造体
 * @param itemCount シングル要素数
 * @param pairCount ペア要素数
 * @param fPrefix データタイププレフィックスの有無
 * @param rethRange ハンドルへのポインタ
 * @param retpRange ロック済みメモリポインタへのポインタ
 * @param retRangeSize 確保済みメモリサイズへのポインタ
 * @return OSMemAllocが返したステータス値
 */
template <typename Item, typename Pair>
STATUS RangeAllocate(
    WORD itemCount,
    WORD pairCount,
    BOOL fPrefix,
    DHANDLE *rethRange,
    void **retpRange,
    DWORD *retRangeSize
    ) {
  // 割り当てるサイズを算出
  DWORD listSize
      = sizeof(WORD) * (fPrefix ? 1 : 0) // データタイププレフィックス
      + sizeof(RANGE) // RANGE構造体
      + sizeof(typename Item::type) * itemCount // シングルリスト
      + sizeof(typename Pair::type) * pairCount; // ペアリスト

  // OSMemAllocを実行
  STATUS status = OSMemAlloc(MEM_GROWABLE, listSize, rethRange);

  if (ERR(status) != NOERROR) {
    // エラー処理
    *rethRange = NULLHANDLE;
    *retpRange = nullptr;
    *retRangeSize = 0;
  }
  else {
    // ハンドルをロックしてポインタを取得
    *retpRange = OSLockObject(*rethRange);
    memset(*retpRange, '\0', listSize);

    // RANGEデータにエントリー数を設定
    auto pRange = dataptr<RANGE>(*retpRange, fPrefix);
    pRange->ListEntries = itemCount;
    pRange->RangeEntries = pairCount;

    // 算出したサイズも返す。
    *retRangeSize = listSize;
  }
  return status;
}

/**
 * @brief 指定した位置のアイテムエントリーを取得する。
 * @tparam Traits 特性構造体
 * @param pRange ロック済みメモリポインタへのポインタ
 * @param fPrefix データタイププレフィックスの有無
 * @param itemIndex アイテムのエントリー位置
 * @param pItem 取得したアイテムデータのコピー先
 */
template <typename Item>
void RangeGetItem(
    void *pRange,
    BOOL fPrefix,
    WORD itemIndex,
    typename Item::type *pItem
    ) {
  auto pr = dataptr<RANGE>(pRange, fPrefix);
  auto pEntry = reinterpret_cast<char*>(pr + 1)
      + sizeof(typename Item::type) * itemIndex;
  *pItem = *reinterpret_cast<typename Item::type*>(pEntry);
}

/**
 * @brief 指定した位置のペアエントリーを取得する。
 * @tparam Traits 特性構造体
 * @param pRange ロック済みメモリポインタへのポインタ
 * @param fPrefix データタイププレフィックスの有無
 * @param pairIndex ペアのエントリー位置
 * @param pPair 取得したペアデータのコピー先
 */
template <typename Item, typename Pair>
void RangeGetPair(
    void *pRange,
    BOOL fPrefix,
    WORD pairIndex,
    typename Pair::type *pPair
    ) {
  auto pr = dataptr<RANGE>(pRange, fPrefix);
  auto pEntry = reinterpret_cast<char*>(pr + 1)
      + sizeof(typename Item::type) * pr->ListEntries
      + sizeof(typename Pair::type) * pairIndex;
  *pPair = *reinterpret_cast<typename Pair::type*>(pEntry);
}

/**
 * @brief 範囲型の全体サイズを取得する。
 * @tparam Traits 特性構造体
 * @param pRange ロック済みメモリポインタへのポインタ
 * @param fPrefix データタイププレフィックスの有無
 * @return 範囲型の全体サイズ
 */
template <typename Item, typename Pair>
WORD RangeGetSize(void *pRange, BOOL fPrefix) {
  auto pr = dataptr<RANGE>(pRange, fPrefix);
  return static_cast<WORD>(
        sizeof(WORD) * (fPrefix ? 1 : 0)
        + sizeof(RANGE)
        + sizeof(typename Item::type) * pr->ListEntries
        + sizeof(typename Pair::type) * pr->RangeEntries
        );
}

/**
 * @brief 範囲型にシングル要素を挿入する。
 * @tparam Traits 特性構造体
 * @param hRange 範囲型ハンドル
 * @param fPrefix データタイププレフィックスの有無
 * @param itemIndex 挿入先インデックス
 * @param pItem 挿入データ
 * @return API関数が返したステータス値
 */
template <typename Item, typename Pair>
STATUS RangeInsertItem(
    DHANDLE hRange,
    BOOL fPrefix,
    WORD itemIndex,
    const typename Item::type *pItem
    ) {
  // メモリハンドルの全体サイズを取得
  DWORD memsize = 0;
  STATUS status = OSMemGetSize(hRange, &memsize);
  if (ERR(status) != NOERROR) {
    return status;
  }

  // RANGE型が持っているはずのサイズを取得
  WORD realsize = RangeGetSize<Item, Pair>(
        OSLockObject(hRange),
        fPrefix
        );
  OSUnlockObject(hRange);

  // ハンドルのサイズが持つべきサイズを下回った場合拡張する
  if (memsize < (realsize + sizeof(typename Item::type))) {
    // メモリをアイテムサイズ1つ分増やす
    memsize = realsize + sizeof(typename Item::type);
    status = OSMemRealloc(hRange, memsize);
    if (ERR(status) != NOERROR) {
      return status;
    }
  }

  // メモリハンドルをロックしてポインタを取得
  char *p0 = reinterpret_cast<char*>(OSLockObject(hRange));

  // RANGE型データポインタを取得
  DWORD offset = sizeof(WORD) * (fPrefix ? 1 : 0);
  auto pRange = dataptr<RANGE>(p0, fPrefix);

  // 挿入位置を0～エントリー数までとする
  itemIndex = (itemIndex > pRange->ListEntries)
      ? pRange->ListEntries
      : itemIndex;

  // 挿入位置までのオフセットを求める
  offset += sizeof(RANGE) + sizeof(typename Item::type) * itemIndex;
  char *pos = p0 + offset;

  // 挿入位置から後ろのメモリをアイテムサイズ1つ分ずらす
  memmove(pos + sizeof(typename Item::type), pos, realsize - offset);

  // 挿入位置にデータをコピーする
  memcpy(pos, pItem, sizeof(typename Item::type));

  // RANGE::ListEntriesを1つ増やす
  ++pRange->ListEntries;

  // メモリハンドルをアンロックする
  OSUnlockObject(hRange);

  return status;
}

/**
 * @brief 範囲型にペア要素を挿入する。
 * @tparam Traits 特性構造体
 * @param hRange 範囲型ハンドル
 * @param fPrefix データタイププレフィックスの有無
 * @param pairIndex 挿入先インデックス
 * @param pPair 挿入データ
 * @return API関数が返したステータス値
 */
template <typename Item, typename Pair>
STATUS RangeInsertPair(
    DHANDLE hRange,
    BOOL fPrefix,
    WORD pairIndex,
    const typename Pair::type *pPair
    ) {
  // メモリハンドルの全体サイズを取得
  DWORD memsize = 0;
  STATUS status = OSMemGetSize(hRange, &memsize);
  if (ERR(status) != NOERROR) {
    return status;
  }

  // RANGE型が持っているはずのサイズを取得
  WORD realsize = RangeGetSize<Item, Pair>(
        OSLockObject(hRange),
        fPrefix
        );
  OSUnlockObject(hRange);

  // ハンドルのサイズが持つべきサイズを下回った場合拡張する
  if (memsize < (realsize + sizeof(Pair::type))) {
    // メモリをペアサイズ1つ分増やす
    memsize = realsize + sizeof(Pair::type);
    status = OSMemRealloc(hRange, memsize);
    if (ERR(status) != NOERROR) {
      return status;
    }
  }

  // メモリハンドルをロックしてポインタを取得
  char *p0 = reinterpret_cast<char*>(OSLockObject(hRange));

  // RANGE型データポインタを取得
  DWORD offset = sizeof(WORD) * (fPrefix ? 1 : 0);
  auto pRange = dataptr<RANGE>(p0, fPrefix);

  // 挿入位置を0～エントリー数までとする
  pairIndex = (pairIndex > pRange->RangeEntries)
      ? pRange->RangeEntries
      : pairIndex;

  // 挿入位置までのオフセットを求める
  offset += sizeof(RANGE) + sizeof(Pair::type) * pairIndex
      + sizeof(Item::type) * pRange->ListEntries;
  char *pos = p0 + offset;

  // 挿入位置から後ろのメモリをペアサイズ1つ分ずらす
  memmove(pos + sizeof(Pair::type), pos, realsize - offset);

  // 挿入位置にデータをコピーする
  memcpy(pos, pPair, sizeof(Pair::type));

  // RANGE::RangeEntriesを1つ増やす
  ++pRange->RangeEntries;

  // メモリハンドルをアンロックする
  OSUnlockObject(hRange);

  return status;
}

/**
 * @brief 範囲型からシングル要素を削除する。
 * @tparam Traits 特性構造体
 * @param hRange 範囲型ハンドル
 * @param fPrefix データタイププレフィックスの有無
 * @param itemIndex 削除要素のインデックス
 * @return API関数が返したステータス値
 */
template <typename Item, typename Pair>
STATUS RangeRemoveItem(DHANDLE hRange, BOOL fPrefix, WORD itemIndex) {
  // メモリハンドルをロックしてポインタを取得
  void *p0 = OSLockObject(hRange);
  WORD realsize = RangeGetSize<Item, Pair>(p0, fPrefix);

  // RANGE型データポインタを取得
  WORD offset = sizeof(WORD) * (fPrefix ? 1 : 0);
  auto pRange = dataptr<RANGE>(p0, fPrefix);

  // 元々削除できるエントリーがなければ返す。
  if (pRange->ListEntries == 0) {
    OSUnlockObject(hRange);
    return NOERROR;
  }

  // 削除位置を0～(エントリー数-1)までとする
  itemIndex = (itemIndex >= pRange->ListEntries)
      ? (pRange->ListEntries - 1)
      : itemIndex;

  // 削除位置までのオフセットを求める
  WORD offset1 = offset + sizeof(RANGE) + sizeof(Item::type) * itemIndex;
  WORD offset2 = offset1 + sizeof(Item::type);
  char *pos1 = reinterpret_cast<char*>(p0) + offset1;
  char *pos2 = reinterpret_cast<char*>(p0) + offset2;
  size_t delta = realsize - offset2;

  // エントリカウントを1つ減らす
  --pRange->ListEntries;

  // 削除位置まで後ろのデータを詰める
  memmove(pos1, pos2, delta);

  // メモリハンドルをアンロックする
  OSUnlockObject(hRange);

  // メモリをアイテムサイズ1つ分減らす(必ず減るとは限らない)
  DWORD newSize = realsize - sizeof(Item::type);
  return OSMemRealloc(hRange, newSize);
}

/**
 * @brief 範囲型からペア要素を削除する。
 * @tparam Traits 特性構造体
 * @param hRange 範囲型ハンドル
 * @param fPrefix データタイププレフィックスの有無
 * @param pairIndex 削除要素のインデックス
 * @return API関数が返したステータス値
 */
template <typename Item, typename Pair>
STATUS RangeRemovePair(DHANDLE hRange, BOOL fPrefix, WORD pairIndex) {
  // メモリハンドルをロックしてポインタを取得
  char *p0 = reinterpret_cast<char*>(OSLockObject(hRange));
  WORD realsize = RangeGetSize<Item, Pair>(p0, fPrefix);

  // RANGE型データポインタを取得
  WORD offset = sizeof(WORD) * (fPrefix ? 1 : 0);
  auto pRange = dataptr<RANGE>(p0, fPrefix);

  // 元々削除できるエントリーがなければ返す。
  if (pRange->RangeEntries == 0) {
    OSUnlockObject(hRange);
    return NOERROR;
  }

  // 削除位置を0～(エントリー数-1)までとする
  pairIndex = (pairIndex >= pRange->RangeEntries)
      ? (pRange->RangeEntries - 1)
      : pairIndex;

  // 削除位置までのオフセットを求める
  offset += sizeof(RANGE)
      + sizeof(Pair::type) * pairIndex
      + sizeof(Item::type) * pRange->ListEntries;
  char *pos = p0 + offset;

  // エントリカウントを1つ減らす
  --pRange->RangeEntries;

  // 削除位置まで後ろのデータを詰める
  memmove(pos, pos + sizeof(Pair::type),
          realsize - offset - sizeof(Pair::type));

  // メモリハンドルをアンロックする
  OSUnlockObject(hRange);

  // メモリをアイテムサイズ1つ分減らす(必ず減るとは限らない)
  DWORD newSize = realsize - sizeof(Pair::type);
  return OSMemRealloc(hRange, newSize);
}

/**
 * @brief 範囲型からシングル要素をすべて削除する。
 * @tparam Traits 特性構造体
 * @param hRange 範囲型ハンドル
 * @param fPrefix データタイププレフィックスの有無
 * @return API関数が返したステータス値
 */
template <typename Item, typename Pair>
STATUS RangeRemoveAllItems(DHANDLE hRange, BOOL fPrefix) {
  // メモリハンドルをロックしてポインタを取得
  void *p0 = reinterpret_cast<char*>(OSLockObject(hRange));
  WORD realsize = RangeGetSize<Item, Pair>(p0, fPrefix);

  // RANGE型データポインタを取得
  DWORD offset = sizeof(WORD) * (fPrefix ? 1 : 0);
  auto pRange = dataptr<RANGE>(p0, fPrefix);

  // 元々削除できるエントリーがなければ返す。
  if (pRange->ListEntries == 0) {
    OSUnlockObject(hRange);
    return NOERROR;
  }

  // ペアデータがなければ移動データがないので最低限のサイズにして返す。
  if (pRange->RangeEntries == 0) {
    pRange->ListEntries = 0;
    OSUnlockObject(hRange);
    return OSMemRealloc(hRange, offset + sizeof(RANGE));
  }

  // 減らすサイズを求める。
  size_t delta = sizeof(Item::type) * pRange->ListEntries;

  // 削除位置の先端から終端まで求める
  char *pos = reinterpret_cast<char*>(p0) + offset + sizeof(RANGE);
  char *pos2 = pos + delta;

  // カウントを0にし、ペアデータをまるごと移動する。
  pRange->ListEntries = 0;
  memmove(pos, pos2, sizeof(Pair::type) * pRange->RangeEntries);

  // メモリハンドルをアンロックする
  OSUnlockObject(hRange);

  // メモリをアイテムサイズ分減らす
  return OSMemRealloc(hRange, realsize - delta);
}

/**
 * @brief 範囲型からペア要素をすべて削除する。
 * @tparam Traits 特性構造体
 * @param hRange 範囲型ハンドル
 * @param fPrefix データタイププレフィックスの有無
 * @return API関数が返したステータス値
 */
template <typename Item, typename Pair>
STATUS RangeRemoveAllPairs(DHANDLE hRange, BOOL fPrefix) {
  // メモリハンドルをロックしてポインタを取得
  void *p0 = reinterpret_cast<char*>(OSLockObject(hRange));
  WORD realsize = RangeGetSize<Item, Pair>(p0, fPrefix);

  // RANGE型データポインタを取得
  auto pRange = dataptr<RANGE>(p0, fPrefix);

  // 元々削除できるエントリーがなければ返す。
  if (pRange->RangeEntries == 0) {
    OSUnlockObject(hRange);
    return NOERROR;
  }

  // 削除後の新しいサイズを求める(ペアリストは末端なので移動する必要なし)
  DWORD newSize = realsize - sizeof(Pair::type) * pRange->RangeEntries;
  pRange->RangeEntries = 0;

  // メモリハンドルをアンロックする
  OSUnlockObject(hRange);

  // メモリをペアサイズ分減らす
  return OSMemRealloc(hRange, newSize);
}

/**
 * @brief 範囲型からシングル/ペア要素すべての削除する。
 * @param hRange 範囲型ハンドル
 * @param fPrefix データタイププレフィックスの有無
 * @return API関数が返したステータス値
 */
STATUS NPP2_EXPORT RangeRemoveAll(DHANDLE hRange, BOOL fPrefix);

/**
 * @brief 範囲型データを複製する
 * @param pRange 範囲型のロック済みポインタ
 * @param fPrefix データタイププレフィックスの有無
 * @param rethRange ハンドルへのポインタ
 * @param retpRange ロック済みメモリポインタへのポインタ
 * @return OSMemAllocが返したステータス値
 */
template <typename Item, typename Pair>
STATUS RangeDuplicate(void *pRange, BOOL fPrefix, DHANDLE *rethRange) {
  WORD realsize = RangeGetSize<Item, Pair>(pRange, fPrefix);
  STATUS status = OSMemAlloc(MEM_GROWABLE, realsize, rethRange);
  if (ERR(status) == NOERROR) {
    void *pDest = OSLockObject(*rethRange);
    memcpy(pDest, pRange, realsize);
    OSUnlockObject(*rethRange);
  }
  return status;
}

} // namespace npp2

#endif // NPP2_PRIVATE_RANGE_HPP
