#ifndef NPP2_PRIVATE_BLOCK_HPP
#define NPP2_PRIVATE_BLOCK_HPP

#include "../npp2_global.h"
#include <functional>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <pool.h>
// #include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

#define NullBlockID (BLOCKID{NULLHANDLE,NULLBLOCK})

namespace npp2 {

/**
 * @brief ブロックIDラップクラス
 */
class NPP2_EXPORT Block
{
  BLOCKID id_; ///< ブロックID

public:
  /**
   * @brief コンストラクタ
   * @param id ブロックID
   */
  Block(BLOCKID id);

  /**
   * @return ブロックIDがヌルを示している場合はtrue
   */
  bool isNull() const;

  /**
   * @brief ブロックIDが有効ならtrue
   */
  operator bool() const;

  operator BLOCKID() const;
};

class NPP2_EXPORT BlockLocker
{
  BLOCKID id_;
  char *ptr_;

public:
  BlockLocker();

  BlockLocker(BLOCKID id);

  ~BlockLocker();

  template <typename P = char>
  P *ptr() const {
    return reinterpret_cast<P*>(ptr_);
  }
};

} // namespace npp2

#endif // NPP2_PRIVATE_BLOCK_HPP
