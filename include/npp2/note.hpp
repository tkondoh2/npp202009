#ifndef NPP2_NOTE_HPP
#define NPP2_NOTE_HPP

#include "npp2_global.h"
#include "_private/handleobject.hpp"
#include "data/lmbcs.hpp"
#include "data/any.hpp"
#include "database.hpp"
#include "item.hpp"
#include "attachmentitem.hpp"
#include <memory>
#include <functional>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfdata.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

class _Note;
using Note = std::unique_ptr<_Note>;

using ScanItemsFn = std::function<STATUS(const data::Lmbcs&,const data::Any&,WORD)>;

struct NPP2_EXPORT NSFNoteCloseDeleter {
  void operator ()(NOTEHANDLE hNote);
};

class NPP2_EXPORT _Note
  : public LockableObject2<NOTEHANDLE,NSFNoteCloseDeleter>
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  _Note();

  /**
   * @brief コンストラクタ
   * @param handle 文書ハンドル
   */
  _Note(NOTEHANDLE handle);

  virtual ~_Note();

  static UNID getUnid(NOTEHANDLE hNote) noexcept;

  UNID getUnid() noexcept;

  static WORD getNoteClass(NOTEHANDLE hNote) noexcept;

  WORD getNoteClass() noexcept;

  static DBHANDLE getDbHandle(NOTEHANDLE hNote) noexcept;

  DBHANDLE getDbHandle() noexcept;

  static NOTEID getNoteId(NOTEHANDLE hNote) noexcept;

  NOTEID getNoteId() const noexcept;

  static OID getOID(NOTEHANDLE hNote) noexcept;

  OID getOID() const noexcept;

  // template <WORD Type, class T>
  // static T getNoteInfo(NOTEHANDLE hNote) noexcept {
  //   T value;
  //   NSFNoteGetInfo(hNote, Type, &value);
  //   return value;
  // }

  // template <WORD Type, class T>
  // T getNoteInfo() noexcept {
  //   return _Note::getNoteInfo<Type, T>(rawHandle());
  // }

  // template <WORD Type, class T>
  // static void setNoteInfo(NOTEHANDLE hNote, T value) {
  //   NSFNoteSetInfo(hNote, Type, &value);
  // }

  // template <WORD Type, class T>
  // void setNoteInfo(T value) {
  //   setNoteInfo<Type, T>(rawHandle() &value);
  // }

  static Note open(
    const Database &dbPtr,
    NOTEID noteId,
    WORD flags = 0
  ) noexcept(false);

  static NOTEHANDLE open(
    DBHANDLE hDB,
    NOTEID noteId,
    WORD flags = 0
  ) noexcept(false);

  static void close(NOTEHANDLE hNote) noexcept(false);

  data::Lmbcs text256(const data::Lmbcs &field) const noexcept;

  static data::Lmbcs text256(NOTEHANDLE hNote, const data::Lmbcs &field) noexcept;

  // template <class T = Item>
  // static void _items(
  //     NOTEHANDLE hNote,
  //     const data::Lmbcs &name,
  //     std::function<bool(const T&)> fn
  //     ) noexcept(false) {
  //   BLOCKID itemId, valueId, prevId;
  //   WORD dataType = 0;
  //   DWORD valueSize = 0;
  //   Status status = NSFItemInfo(
  //         hNote,
  //         name.constData(),
  //         static_cast<WORD>(name.size()),
  //         &itemId,
  //         &dataType,
  //         &valueId,
  //         &valueSize
  //         );
  //   while (status.error() != ERR_ITEM_NOT_FOUND) {
  //     if (!status) { throw status; }
  //     T item(itemId, dataType, valueId, valueSize);
  //     if (!fn(item)) { break; }
  //     prevId = itemId;
  //     status = NSFItemInfoNext(
  //           hNote,
  //           prevId,
  //           name.constData(),
  //           static_cast<WORD>(name.size()),
  //           &itemId,
  //           &dataType,
  //           &valueId,
  //           &valueSize
  //           );
  //   }
  // }

  static void items(
      NOTEHANDLE hNote,
      const data::Lmbcs &name,
      std::function<bool(const Item&)> fn
      ) noexcept(false);

  static WORD getTextListEntries(NOTEHANDLE hNote, const data::Lmbcs &name) noexcept;

  WORD getTextListEntries(const data::Lmbcs &name) const noexcept;

  static data::Lmbcs getTextListEntry(
    NOTEHANDLE hNote,
    const data::Lmbcs &name,
    WORD index
  ) noexcept;

  data::Lmbcs getTextListEntry(
    const data::Lmbcs &name,
    WORD index
  ) const noexcept;

  static void attachmentItems(
      NOTEHANDLE hNote,
      std::function<bool(const AttachmentItem &attItem)> fn
      ) noexcept(false);

  void items(
      const data::Lmbcs &name,
      std::function<bool(const Item&)> fn
      ) const noexcept(false);

  void attachmentItems(
      std::function<bool(const AttachmentItem &attItem)> fn
      ) const noexcept(false);

  QByteArray extractAttachment(
      const AttachmentItem &attItem
      ) const noexcept(false);

  static STATUS LNPUBLIC _extractAttachmentCallback(
      const BYTE *bytes,
      DWORD length,
      void *ptr
      );

  bool isUnread(const Database &dbPtr) const;

  void setUnread(const Database &dbPtr, bool bUnread);

  static NOTEHANDLE copy(
    NOTEHANDLE hSrc,
    bool isReplica,
    DBHANDLE hDestDb
  );

  Note copy(
    bool isReplica,
    DBHANDLE hDestDb = NULLHANDLE
  ) const;

  static void update(NOTEHANDLE hNote, WORD updateFlags = 0);

  void update(WORD updateFlags = 0);

  static void updateEx(NOTEHANDLE hNote, DWORD updateFlags = 0);

  void updateEx(DWORD updateFlags = 0);

  static void setText(
    NOTEHANDLE hNote,
    const data::Lmbcs &name,
    data::Lmbcs value,
    bool bSummary
  );

  void setText(
    const data::Lmbcs &name,
    const data::Lmbcs &value,
    bool bSummary
  );

  static STATUS LNPUBLIC scanItemsCallback(
    WORD, WORD itemFlags,
    char *name, WORD nameLength,
    void *pValue, DWORD valueLength,
    void *routineParameter
  );

  static void scanItems(NOTEHANDLE hNote, ScanItemsFn fn);

  void scanItems(const ScanItemsFn &fn) const;

  data::Lmbcs convertToText(
    const data::Lmbcs &name,
    char separator = '\0'
  ) const;

  static data::Lmbcs convertToText(
    NOTEHANDLE hNote,
    const data::Lmbcs &name,
    char separator = '\0' // => ';'に変換
  );

  static bool hasItem(NOTEHANDLE hNote, const data::Lmbcs &field);

  bool hasItem(const data::Lmbcs &field) const;
};

class NPP2_EXPORT NoteClass
{
  WORD raw_;

public:
  NoteClass();

  NoteClass(WORD v);

  WORD raw() const;

  bool match(WORD v) const;

  bool isDesign() const;
};

} // namespace npp2

#endif // NPP2_NOTE_HPP
