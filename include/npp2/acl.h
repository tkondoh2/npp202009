#ifndef NPP2_ACL_H
#define NPP2_ACL_H

#include "npp2_global.h"
#include "_private/handleobject.hpp"
#include "data/lmbcs.hpp"
#include "data/lmbcslist.hpp"
#include <QList>
#include <functional>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfdb.h>
#include <acl.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

/**
 * @brief ACLエントリー
 */
class NPP2_EXPORT AclEntry
{
  data::Lmbcs name_; ///< エントリ名
  WORD level_; ///< アクセスレベル ACL_LEVEL_***
  ACL_PRIVILEGES privileges_; ///< ロールビット値構造体
  WORD flags_; ///< エントリフラグ ACL_FLAG_***

public:
  /**
   * @brief コンストラクタ
   * @param pName エントリ名へのポインタ
   * @param level アクセスレベル
   * @param pPrivileges ロールビット値構造体へのポインタ
   * @param flags エントリフラグ
   */
  AclEntry(
    const char *pName,
    WORD level,
    const ACL_PRIVILEGES *pPrivileges,
    WORD flags
  );

  /**
   * @brief エントリ名
   * @return const data::Lmbcs& エントリ名
   */
  const data::Lmbcs &name() const {
    return name_;
  }

  /**
   * @brief アクセスレベル
   * @return WORD アクセスレベル
   */
  WORD level() const {
    return level_;
  }

  /**
   * @brief ロールビット値構造体
   * @return const ACL_PRIVILEGES& ロールビット値構造体
   */
  const ACL_PRIVILEGES &privileges() const {
    return privileges_;
  }

  /**
   * @brief エントリフラグ ACL_FLAG_***
   * @return WORD エントリフラグ
   */
  WORD flags() const {
    return flags_;
  }
};

class _Acl;
using Acl = std::unique_ptr<_Acl>;

/**
 * @brief アクセス制御リスト(ACL)
 */
class NPP2_EXPORT _Acl
  : public LockableObject3<DHANDLE>
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  _Acl();

  /**
   * @brief コンストラクタ
   * @param hAcl ACLハンドル
   */
  _Acl(DHANDLE hAcl);

  /**
   * @brief デストラクタ
   */
  virtual ~_Acl();

  /**
   * @brief ACLを読み取る
   * @param hDB データベースハンドル
   * @return Acl ACLスマートポインタ
   */
  static Acl read(DBHANDLE hDB);

  /**
   * @brief このデータベースのレプリカはすべて共通のアクセス制御リストを用いるか
   * @param hAcl ACLハンドル
   * @return true このデータベースのレプリカはすべて共通のアクセス制御リストを用いる
   * @return false このデータベースのレプリカはすべて共通のアクセス制御リストを用いない
   */
  static bool isUniformAccess(DHANDLE hAcl);

  /**
   * @brief このデータベースのレプリカはすべて共通のアクセス制御リストを用いるか
   * @return true このデータベースのレプリカはすべて共通のアクセス制御リストを用いる
   * @return false このデータベースのレプリカはすべて共通のアクセス制御リストを用いない
   */
  bool isUniformAccess() const {
    return isUniformAccess(handle_);
  }

  /**
   * @brief システム管理サーバ名
   * @param hAcl ACLハンドル
   * @return data::Lmbcs システム管理サーバ名
   */
  static data::Lmbcs getAdminServer(DHANDLE hAcl);

  /**
   * @brief システム管理サーバ名
   * @return data::Lmbcs システム管理サーバ名
   */
  data::Lmbcs getAdminServer() const {
    return getAdminServer(handle_);
  }

  /**
   * @brief ACLエントリを取得する
   * @param hAcl ACLハンドル
   * @param callback 取得したACLエントリを受け取る関数(複数回呼び出される)
   */
  static void getEntries(
    DHANDLE hAcl,
    std::function<void(AclEntry &&)> callback
  );

  /**
   * @brief ACLエントリを取得する
   * @param callback 取得したACLエントリを受け取る関数(複数回呼び出される)
   */
  void getEntries(
    std::function<void(AclEntry &&)> callback
  ) const {
    getEntries(handle_, callback);
  }

  /**
   * @brief ロール名リストを取得する
   * @param hAcl ACLハンドル
   * @param privileges ロールビット値構造体
   * @return data::LmbcsList ロール名リスト
   */
  static data::LmbcsList getRoles(
    DHANDLE hAcl,
    const ACL_PRIVILEGES &privileges
  );

  /**
   * @brief ロール名リストを取得する
   * @param privileges ロールビット値構造体
   * @return data::LmbcsList ロール名リスト
   */
  data::LmbcsList getRoles(
    const ACL_PRIVILEGES &privileges
  ) const {
    return getRoles(handle_, privileges);
  }

  /**
   * @brief 変更履歴を取得する
   * @param hAcl ACLハンドル
   * @param callback 取得した変更履歴を受け取る関数(複数回呼び出される)
   */
  static void getHistory(
    DHANDLE hAcl,
    std::function<void(data::Lmbcs &&)> callback
  );

  /**
   * @brief 変更履歴を取得する
   * @param callback 取得した変更履歴を受け取る関数(複数回呼び出される)
   */
  void getHistory(
    std::function<void(data::Lmbcs &&)> callback
  ) const {
    getHistory(handle_, callback);
  }
};

} // namespace npp2

#endif // NPP2_ACL_H
