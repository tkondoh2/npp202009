#ifndef NPP2_ENV_HPP
#define NPP2_ENV_HPP

#include "npp2_global.h"
#include <QByteArray>
#include <functional>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <osenv.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::env {

/**
 * @brief 環境変数から文字列値を取得する
 * @param key 環境変数名
 * @param fn 環境変数名が見つからない時にデフォルト値を返す式
 * @return 文字列値
 */
QByteArray NPP2_EXPORT getString(
  const char *key,
  std::function<QByteArray()> fn = [](){return QByteArray();}
) noexcept;

/**
 * @brief 環境変数に文字列値を設定する
 * @param key 環境変数名
 * @param value 文字列値
 */
void NPP2_EXPORT setString(const char *key, const QByteArray &value) noexcept;

/**
 * @brief 環境変数に変数が設定されているかテストする
 * @param key 環境変数名
 * @return 設定されていればtrue
 */
bool NPP2_EXPORT has(const char *key) noexcept;

/**
 * @brief 環境変数から数値を取得する
 * @param key 環境変数名
 * @param fn 環境変数名が見つからない時にデフォルト値を返す式
 * @return 数値
 */
template <class T = int>
T getNumber(const char *key, std::function<T()> fn) noexcept {
  return has(key)
    ? static_cast<T>(OSGetEnvironmentLong(key))
    : fn();
}

/**
 * @brief 環境変数に数値を設定する
 * @param key 環境変数名
 * @param value 数値
 */
template <class T>
void setNumber(const char *key, T value) noexcept {
  OSSetEnvironmentInt(key, static_cast<int>(value));
}

/**
 * @return データディレクトリ
 */
QByteArray NPP2_EXPORT getDataDirectory() noexcept;

/**
 * @return プログラムディレクトリ
 */
QByteArray NPP2_EXPORT getProgramDirectory() noexcept;

} // namespace npp2::env

#endif // NPP2_ENV_HPP
