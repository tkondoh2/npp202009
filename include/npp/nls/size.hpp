#ifndef NPP202009_NPP_NLS_SIZE_HPP
#define NPP202009_NPP_NLS_SIZE_HPP

#include "./status.hpp"
#include <algorithm>

namespace npp202009::npp::nls {

/**
 * @brief バイト配列の文字数相当のバイト数を取得します。
 * @param s 対象のバイト配列
 * @param chars バイト数として測りたい文字数
 * @param pInfo バイト配列の文字セット情報
 * @return バイト数
 * @throw Status NLS_STATUSを内包したクラス
 */
WORD getStringBytes_w(
    const char *s,
    WORD chars,
    NLS_PINFO pInfo
    ) noexcept(false);

/**
 * @brief バイト配列の文字数を取得します。
 * @param s 対象のバイト配列
 * @param bytes 文字数として測りたいバイト数
 * @param pInfo バイト配列の文字セット情報
 * @return 文字数
 * @throw Status NLS_STATUSを内包したクラス
 */
WORD getStringChars_w(
    const char *s,
    WORD bytes,
    NLS_PINFO pInfo
    ) noexcept(false);

/**
 * @brief バイト配列の文字数を取得します。
 * @param s 対象のバイト配列
 * @param bytes 文字数として測りたいバイト数
 * @param pInfo バイト配列の文字セット情報
 * @return 文字数
 * @throw Status NLS_STATUSを内包したクラス
 */
int getStringChars(
    const char *s,
    int bytes,
    NLS_PINFO pInfo
    ) noexcept(false);

/**
 * @brief バイト配列の文字数相当のバイト数を取得します。
 * @param s 対象のバイト配列
 * @param chars バイト数として測りたい文字数
 * @param pInfo バイト配列の文字セット情報
 * @return バイト数
 * @throw Status NLS_STATUSを内包したクラス
 */
int getStringBytes(
    const char *s,
    int chars,
    NLS_PINFO pInfo
    ) noexcept(false);

/**
 * @brief 文字セットに応じて、マルチバイト文字列の区切り位置を調整したバイト数を求めます。
 * @param s 対象のバイト配列
 * @param bytes 計測対象のバイト数
 * @param pInfo バイト配列の文字セット情報
 * @return 調整したバイト数
 * @throw Status NLS_STATUSを内包したクラス
 */
int adjustByteSize(
    const char *s,
    int bytes,
    NLS_PINFO pInfo
    ) noexcept(false);


inline WORD getStringBytes_w(
    const char *s,
    WORD chars,
    NLS_PINFO pInfo
    ) noexcept(false) {
  if (chars < 1) return 0;
  WORD result = 0;
  Status status = NLS_string_bytes(
        reinterpret_cast<const BYTE*>(s),
        chars,
        &result,
        pInfo
        );
  if (status.failure()) throw status;
  return result;
}

inline WORD getStringChars_w(
    const char *s,
    WORD bytes,
    NLS_PINFO pInfo
    ) noexcept(false)
{
  if (bytes < 1) return 0;
  auto b = bytes;
  WORD result = 0;
  while (b > 0) {
    Status status = NLS_string_chars(
          reinterpret_cast<const BYTE*>(s),
          b,
          &result,
          pInfo
          );
    if (status.failure()) {
      if (status.error() == NLS_INVALIDDATA) {
        if (b > 1) { --b; continue; }
        else return 0;
      }
      else throw status;
    }
    else break;
  }
  return result;
}

inline int getStringChars(
    const char *s,
    int bytes,
    NLS_PINFO pInfo
    ) noexcept(false)
{
  if (bytes < 1) return 0;
  int p = 0, i = 0;
  while (p < bytes) { // bytes - p は必ず正
    // 測りたい幅とWORD最大値を比較
    auto w = std::min<int>(bytes - p, MAXWORD - 1);
    // 測りたい幅の切れ目ごとに文字数に加算する。
    auto c = getStringChars_w(s + p, static_cast<WORD>(w), pInfo);
    if (c > 0) {
      i += static_cast<int>(c);
      // 切れ目に応じたバイト数分移動する。
      p += static_cast<int>(getStringBytes_w(s + p, c, pInfo));
    }
    else break;
  }
  return i;
}

inline int getStringBytes(
    const char *s,
    int chars,
    NLS_PINFO pInfo
    ) noexcept(false)
{
  if (chars < 1) return 0;
  int p = 0, i = 0;
  while (i < chars) {
    auto c = std::min<int>(chars - i, MAXWORD / NLS_MAXBYTESPERCHAR);
    i += c;
    p += static_cast<int>(getStringBytes_w(s + p, static_cast<WORD>(c), pInfo));
  }
  return p;
}

inline int adjustByteSize(
    const char *s,
    int bytes,
    NLS_PINFO pInfo
    ) noexcept(false) {
  auto c = getStringChars(s, bytes, pInfo);
  return getStringBytes(s, c, pInfo);
}


} // namespace npp202009::npp::nls

#endif // NPP202009_NPP_NLS_SIZE_HPP
