#ifndef NPP202009_NPP_NLS_INFO_HPP
#define NPP202009_NPP_NLS_INFO_HPP

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nls.h>
#include <osmisc.h> // OSGetLMBCSCLS

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp::nls {

/**
 * @brief NLS(National Language Service)情報のラッパークラスです。
 */
class Info
{
protected:
  NLS_PINFO ptr_; ///< NLSポインタ

public:
  /**
   * @brief コンストラクタです。
   * @param ptr NLS情報へのポインタ
   */
  explicit Info(NLS_PINFO ptr) noexcept;

  /**
   * @brief デストラクタです。
   */
  virtual ~Info() = default;

  /**
   * @brief NLS情報へのポインタを返します。
   * @return NLS情報へのポインタ
   */
  NLS_PINFO get() const noexcept;
};

/**
 * @brief ロードして使用するNLS情報クラスです。
 * @tparam CSID 文字セットID
 */
template <WORD CSID>
class LoadingInfo
    : public Info
{
public:
  /**
   * @brief コンストラクタです。
   */
  explicit LoadingInfo() noexcept;

  /**
   * @brief デストラクタです。
   */
  virtual ~LoadingInfo();
};

/**
 * @brief LMBCS専用NLS情報クラスです。
 */
class LmbcsInfo
    : public Info
{
public:
  /**
   * @brief コンストラクタです。
   */
  LmbcsInfo() noexcept;

  /**
   * @brief デストラクタです。
   */
  virtual ~LmbcsInfo() = default;
};


inline Info::Info(NLS_PINFO ptr) noexcept
  : ptr_(ptr)
{}

inline NLS_PINFO Info::get() const noexcept {
  return ptr_;
}

template <WORD CSID>
LoadingInfo<CSID>::LoadingInfo() noexcept
  : Info(nullptr)
{
  NLS_load_charset(CSID, &ptr_);
}

template <WORD CSID>
LoadingInfo<CSID>::~LoadingInfo() {
  NLS_unload_charset(ptr_);
}

inline LmbcsInfo::LmbcsInfo() noexcept
  : Info(OSGetLMBCSCLS())
{}

} // namespace npp202009::npp::nls

#endif // NPP202009_NPP_NLS_INFO_HPP
