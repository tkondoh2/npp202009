#ifndef NPP202009_NPP_NLS_TRANSLATE_HPP
#define NPP202009_NPP_NLS_TRANSLATE_HPP

#include "./size.hpp"
#include "./info.hpp"
#include <QByteArray>

namespace npp202009::npp::nls {

/**
 * @brief キロバイトをバイトにします。
 * @tparam T 数値型
 * @param n キロバイト
 * @return バイト
 */
template <typename T>
T KBtoB(T n) { return n * 1024; }

/**
 * @brief バイト配列を元の文字セットから目的の文字セットに変換します。
 * @tparam mode 変換モード(NLSベース)
 * @tparam ratio 準備するバッファの、元のバイト配列と比較した倍率
 * @param p 元のバイト配列
 * @param maxSize 変換対象バイト数(通常、バイト配列の長さ)
 * @param pFrom 元の文字セットのNLS情報へのポインタ
 * @param pTo 目的の文字セットのNLS情報へのポインタ
 * @return ステータスと変換結果
 */
template <WORD mode, int ratio>
QByteArray translate(
    const QByteArray &array,
    const Info *fromInfo,
    const Info *toInfo
    ) noexcept(false);

/**
 * @brief Unicode(UTF16)バイト配列をLMBCSバイト配列に変換します。
 * @param src Unicodeバイト配列
 * @param size 変換するバイトサイズ
 * @return ステータスと変換結果
 */
QByteArray unicodeToLmbcs(const QByteArray &unicode) noexcept(false);

/**
 * @brief LMBCSバイト配列をUnicode(UTF16)バイト配列に変換します。
 * @param src LMBCSバイト配列
 * @param size 変換するバイトサイズ
 * @return ステータスと変換結果
 */
QByteArray lmbcsToUnicode(const QByteArray &lmbcs) noexcept(false);


template <WORD mode, int ratio>
QByteArray translate(
    const QByteArray &array,
    const Info *fromInfo,
    const Info *toInfo
    ) noexcept(false) {
  // 変換する単位バイト数
  int maxSize = array.length();
  int CHUNK_SIZE = (maxSize > KBtoB<int>(8)) ? KBtoB<int>(8) : KBtoB<int>(1);

  int offset = 0;
  QByteArray result;
  while (offset < maxSize) {

    // チャンクサイズの候補を計算
    auto chunkSize = std::min<int>(maxSize - offset, CHUNK_SIZE);

    // 切れ目に応じてチャンクサイズを調整
    chunkSize = adjustByteSize(
          array.constData() + offset,
          chunkSize,
          fromInfo->get()
          );

    // 処理する文字列がない場合はループを抜ける。
    if (chunkSize <= 0) break;

    // 文字列からチャンクサイズ分だけ取得
    auto chunk = array.mid(offset, chunkSize);

    // 変換した文字列を格納するバッファを準備する
    auto bufSize = chunkSize * ratio;
    QByteArray buffer(bufSize, '\0');

    // 変換する
    auto len = static_cast<WORD>(bufSize);
    Status status = NLS_translate(
            reinterpret_cast<BYTE*>(chunk.data()),
            static_cast<WORD>(chunkSize),
            reinterpret_cast<BYTE*>(buffer.data()),
            &len,
            mode,
            toInfo->get()
            );
    if (status.failure()) throw status;
    result.append(buffer.left(static_cast<int>(len)));

    offset += chunkSize;
  }
  return result;
}

inline QByteArray unicodeToLmbcs(
    const QByteArray &unicode
    ) noexcept(false) {
  nls::LoadingInfo<NLS_CS_UNICODE> unicodeInfo;
  nls::LmbcsInfo lmbcsInfo;
  return translate<
      NLS_NONULLTERMINATE | NLS_SOURCEISUNICODE | NLS_TARGETISLMBCS,
      NLS_MAXRATIO_XLATE_TO_LMBCS>(
        unicode,
        &unicodeInfo,
        &lmbcsInfo
        );
}

inline QByteArray lmbcsToUnicode(
    const QByteArray &lmbcs
    ) noexcept(false) {
  nls::LoadingInfo<NLS_CS_UNICODE> unicodeInfo;
  nls::LmbcsInfo lmbcsInfo;
  return translate<
      NLS_NONULLTERMINATE | NLS_SOURCEISLMBCS | NLS_TARGETISUNICODE,
      NLS_MAXRATIO_XLATE_FROM_LMBCS>(
        lmbcs,
        &lmbcsInfo,
        &unicodeInfo
        );
}


} // namespace npp202009::npp::nls

#endif // NPP202009_NPP_NLS_TRANSLATE_HPP
