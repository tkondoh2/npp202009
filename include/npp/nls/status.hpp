#ifndef NPP202009_NPP_NLS_STATUS_HPP
#define NPP202009_NPP_NLS_STATUS_HPP

#include <exception>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nls.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp::nls {

/**
 * @brief The Status class
 */
class Status
    : public std::exception
{
  NLS_STATUS status_;

public:
  Status() noexcept;

  Status(NLS_STATUS status) noexcept;

  NLS_STATUS error() const noexcept { return status_; }
  bool success() const noexcept;
  bool failure() const noexcept;

  virtual const char *what() const noexcept override;
};


inline Status::Status() noexcept
  : std::exception()
  , status_(NLS_SUCCESS)
{}

inline Status::Status(NLS_STATUS status) noexcept
  : std::exception()
  , status_(status)
{}

inline bool Status::success() const noexcept {
  return error() == NLS_SUCCESS;
}

inline bool Status::failure() const noexcept {
  return !success();
}

inline const char *Status::what() const noexcept {
  switch (status_) {
  case NLS_SUCCESS: return "Success.";
  case NLS_BADPARM: return "Bad Parameters.";
  case NLS_BUFFERTOOSMALL: return "Buffer too small.";
  case NLS_CHARSSTRIPPED: return "Chars stripped.";
  case NLS_ENDOFSTRING: return "End of string.";
  case NLS_FALLBACKUSED: return "Fallback used.";
  case NLS_FILEINVALID: return "File invalid.";
  case NLS_FILENOTFOUND: return "File not found.";
  case NLS_FINDFAILED: return "Find failed.";
  case NLS_INVALIDCHARACTER: return "Invalid character.";
  case NLS_INVALIDDATA: return "Invalid data.";
  case NLS_INVALIDENTRY: return "Invalid entry.";
  case NLS_INVALIDTABLE: return "Invalid table.";
  case NLS_PROPNOTFOUND: return "Prop not found.";
  case NLS_STARTOFSTRING: return "Start of string.";
  case NLS_STRINGSIZECHANGED: return "String size changed.";
  case NLS_TABLEHEADERINVALID: return "Table header invalid.";
  case NLS_TABLENOTFOUND: return "Table not found.";
  default: return "Unknown NLS status.";
  }
}

} // namespace npp202009::npp::nls

#endif // NPP202009_NPP_NLS_STATUS_HPP
