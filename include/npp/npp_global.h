#ifndef NPP202009_NPP_NPP_GLOBAL_H
#define NPP202009_NPP_NPP_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(NPP_LIBRARY)
#  define NPP_EXPORT Q_DECL_EXPORT
#else
#  define NPP_EXPORT Q_DECL_IMPORT
#endif

#endif // NPP202009_NPP_NPP_GLOBAL_H
