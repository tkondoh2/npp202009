#ifndef NPP202009_NPP_ADDIN_LOGMESSAGE_HPP
#define NPP202009_NPP_ADDIN_LOGMESSAGE_HPP

//#include <QString>
#include "../type/lmbcs.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <addin.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp::addin {

template <class... Args>
void logMessage(
    STATUS StringID,
    STATUS AdditionalErrorCode = NOERROR,
    Args... args
    )
{
  AddInLogMessage(StringID, ERR(AdditionalErrorCode), args...);
}

template <class... Args>
void logMessageText(
    const Lmbcs &String,
    STATUS AdditionalErrorCode = NOERROR,
    Args... args
    )
{
  AddInLogMessageText(
        const_cast<char*>(String.constData()),
        ERR(AdditionalErrorCode),
        args...
        );
}

inline void logMsg(
    STATUS StringID,
    const Lmbcs &Arg = Lmbcs()
    )
{
  AddInLogMsg(
        StringID,
        Arg.isEmpty()
          ? nullptr
          : const_cast<char*>(Arg.constData())
        );
}

} // namespace npp202009::npp::addin

#endif // NPP202009_NPP_ADDIN_LOGMESSAGE_HPP
