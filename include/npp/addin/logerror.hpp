#ifndef NPP202009_NPP_ADDIN_LOGERROR_HPP
#define NPP202009_NPP_ADDIN_LOGERROR_HPP

//#include <QString>
#include "../type/lmbcs.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <addin.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp::addin {

inline void logError(
    STATUS StringID,
    STATUS AdditionalErrorCode = NOERROR,
    const Lmbcs &Arg = Lmbcs()
    )
{
  AddInLogError(
        StringID,
        ERR(AdditionalErrorCode),
        Arg.isEmpty()
          ? nullptr
          : const_cast<char*>(Arg.constData())
        );
}

inline void logErrorText(
    const Lmbcs &String,
    STATUS AdditionalErrorCode = NOERROR,
    const Lmbcs &Arg = Lmbcs()
    )
{
  AddInLogErrorText(
        const_cast<char*>(String.constData()),
        ERR(AdditionalErrorCode),
        Arg.isEmpty()
          ? nullptr
          : const_cast<char*>(Arg.constData())
        );
}

} // namespace npp202009::npp::addin

#endif // NPP202009_NPP_ADDIN_LOGERROR_HPP
