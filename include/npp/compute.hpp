#ifndef NPP202009_NPP_COMPUTE_HPP
#define NPP202009_NPP_COMPUTE_HPP

#include "./formula.hpp"
#include "./handle/object.hpp"
#include "./item.hpp"

namespace npp202009::npp {

struct ComputeHandleTraits
{
  using handle_type = HCOMPUTE;
  static bool is_null(HCOMPUTE h) { return h == nullptr; }
  static void deleter(HCOMPUTE h) { NSFComputeStop(h); }
};

class Compute;
using ComputePtr = QSharedPointer<Compute>;
using ComputeHandle = handle::Base<ComputeHandleTraits>;

class Compute
    : public ComputeHandle
{
public:
  Compute() : ComputeHandle(nullptr) {}

  Compute(HCOMPUTE &&handle) : ComputeHandle(std::move(handle)) {}

  observable<Item::Value> evaluate(
      NOTEHANDLE hNote,
      bool *pNoteMatchesFormula = nullptr,
      bool *pNoteShouldBeDeleted = nullptr,
      bool *pNoteModified = nullptr
      ) {
    return observable<>::create<Item::Value>(
          [this, hNote, pNoteMatchesFormula, pNoteShouldBeDeleted, pNoteModified](
          subscriber<Item::Value> s
          ) {
      DHANDLE valueHandle = NULLHANDLE;
      WORD valueLen = 0;
      Status status = NSFComputeEvaluate(
            handle(),
            hNote,
            &valueHandle,
            &valueLen,
            reinterpret_cast<BOOL*>(pNoteMatchesFormula),
            reinterpret_cast<BOOL*>(pNoteShouldBeDeleted),
            reinterpret_cast<BOOL*>(pNoteModified)
            );
      if (status.failure()) {
        s.on_error(std::make_exception_ptr(status));
        return;
      }
      handle::ObjectPtr vPtr = handle::make_ptr<handle::Object>(std::move(valueHandle));
      QByteArray data(
            vPtr->lock<char>(),
            static_cast<int>(valueLen)
            );
      s.on_next(Item::Value(std::move(data)));
      s.on_completed();
    });
  }

  Item::Value evaluate_x(
      NOTEHANDLE hNote,
      bool *pNoteMatchesFormula = nullptr,
      bool *pNoteShouldBeDeleted = nullptr,
      bool *pNoteModified = nullptr
      ) {
    DHANDLE valueHandle = NULLHANDLE;
    WORD valueLen = 0;
    Status status = NSFComputeEvaluate(
          handle(),
          hNote,
          &valueHandle,
          &valueLen,
          reinterpret_cast<BOOL*>(pNoteMatchesFormula),
          reinterpret_cast<BOOL*>(pNoteShouldBeDeleted),
          reinterpret_cast<BOOL*>(pNoteModified)
          );
    if (status.failure()) { throw status; }
    handle::ObjectPtr vPtr = handle::make_ptr<handle::Object>(
          std::move(valueHandle)
          );
    QByteArray data(
          vPtr->lock<char>(),
          static_cast<int>(valueLen)
          );
    return Item::Value(std::move(data));
  }

  static observable<ComputePtr> compute(FormulaPtr f) noexcept {
    return observable<>::create<ComputePtr>([f](subscriber<ComputePtr> s) {
      HCOMPUTE handle = nullptr;
      void *pFormula = f->lock<void>();
      Status status = NSFComputeStart(0, pFormula, &handle);
      if (status.failure()) {
        s.on_error(std::make_exception_ptr(status));
        return;
      }
      s.on_next(handle::make_ptr<Compute>(std::move(handle)));
      s.on_completed();
    });
  }

  static ComputePtr compute_x(FormulaPtr f) noexcept(false) {
    HCOMPUTE handle = nullptr;
    void *pFormula = f->lock<void>();
    Status status = NSFComputeStart(0, pFormula, &handle);
    if (status.failure()) { throw status; }
    return handle::make_ptr<Compute>(std::move(handle));
  }
};

} // namespace npp202009::npp

#endif // NPP202009_NPP_COMPUTE_HPP
