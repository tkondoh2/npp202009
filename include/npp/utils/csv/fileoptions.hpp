#ifndef NPP202009_NPP_CSV_FILEOPTIONS_HPP
#define NPP202009_NPP_CSV_FILEOPTIONS_HPP

#include "./record.hpp"
#include "../writer/file.hpp"

namespace npp202009::npp::csv {

/**
 * @brief CSV用ファイルオプションクラス(CSVヘッダーを付加する)
 */
class FileOptions
{
  QStringList headers_; ///< CSVヘッダーリスト

public:
  /**
   * @brief コンストラクタ
   */
  FileOptions() : headers_() {}

  /**
   * @brief ヘッダーを追加する。
   * @param header ヘッダー文字列
   * @return 自身への参照
   */
  FileOptions &add(QString &&header) {
    headers_.append(
          Field::quote(Field::escape(std::move(header)))
          );
    return *this;
  }

  /**
   * @brief 文書オープン後にフックするメソッド
   *   ファイルサイズが0(何も書き込みがない)の時、CSVヘッダーを書き込む。
   * @param pFile writer::Fileへのポインタ
   */
  virtual void afterOpen(writer::File *pFile) {
    if (pFile->info().size() == 0) {
      pFile->_write(headers_.join(","), true);
    }
  }
};

} // namespace npp202009::npp::csv

#endif // NPP202009_NPP_CSV_FILEOPTIONS_HPP
