#ifndef NPP202009_NPP_CSV_FIELDS_LMBCS_HPP
#define NPP202009_NPP_CSV_FIELDS_LMBCS_HPP

#include "../field.hpp"
#include "../../../type/lmbcs.hpp"

namespace npp202009::npp::csv::field {

struct Lmbcs
{
  using field_type = npp::Lmbcs;
  static QString encode(const npp::Lmbcs &value) {
    return Field::quote(lq(value));
  }
};

} // namespace npp202009::npp::csv::field

#endif // NPP202009_NPP_CSV_FIELDS_LMBCS_HPP
