#ifndef NPP202009_NPP_CSV_FIELDS_NUMBER_HPP
#define NPP202009_NPP_CSV_FIELDS_NUMBER_HPP

#include "../field.hpp"
#include "../../../type/number.hpp"

namespace npp202009::npp::csv::field {

struct Number
{
  using field_type = npp::Number;
  static QString encode(const npp::Number &value) {
    QString result;
    value.convertToText().subscribe([&result](npp::Lmbcs lmbcs) {
      result = lq(lmbcs);
    });
    return Field::escape(std::move(result));
  }
};

} // namespace npp202009::npp::csv::field

#endif // NPP202009_NPP_CSV_FIELDS_NUMBER_HPP
