#ifndef NPP202009_NPP_CSV_FIELDS_DATETIME_HPP
#define NPP202009_NPP_CSV_FIELDS_DATETIME_HPP

#include "../field.hpp"
#include <QDateTime>

namespace npp202009::npp::csv::field {

struct DateTime
{
  using field_type = QDateTime;
  static QString encode(const QDateTime &value) {
    return Field::quote(value.toString("yyyy/MM/dd HH:mm:ss"));
  }
};

struct DateTimeISO8601
{
  using field_type = QDateTime;
  static QString encode(const QDateTime &value) {
    return Field::quote(value.toString(Qt::ISODate));
  }
};

} // namespace npp202009::npp::csv::field

#endif // NPP202009_NPP_CSV_FIELDS_DATETIME_HPP
