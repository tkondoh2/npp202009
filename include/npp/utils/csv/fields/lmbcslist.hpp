#ifndef NPP202009_NPP_CSV_FIELDS_LMBCSLIST_HPP
#define NPP202009_NPP_CSV_FIELDS_LMBCSLIST_HPP

#include "../field.hpp"
#include "../../../type/lmbcslist.hpp"

namespace npp202009::npp::csv::field {

struct LmbcsList
{
  using field_type = npp::LmbcsList;
  static QString encode(const npp::LmbcsList &value) {
    return Field::quote(lq(value.join(';')));
  }
};

} // namespace npp202009::npp::csv::field

#endif // NPP202009_NPP_CSV_FIELDS_LMBCSLIST_HPP
