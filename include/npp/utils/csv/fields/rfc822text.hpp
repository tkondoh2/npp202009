#ifndef NPP202009_NPP_CSV_FIELDS_RFC822TEXT_HPP
#define NPP202009_NPP_CSV_FIELDS_RFC822TEXT_HPP

#include "../field.hpp"
#include "../../../mime/rfc822text.hpp"
#include "../../../type/lmbcslist.hpp"

namespace npp202009::npp::csv::field {

struct Rfc822Text
{
  using field_type = npp::mime::Rfc822Text;
  static QString encode(const npp::mime::Rfc822Text &value) {
    QString result;
    observable<>::just(value)
    .flat_map([](npp::mime::Rfc822Text value) {

//      QString x = QString::fromLocal8Bit(value.name());
//      x += QString::fromLocal8Bit(value.delim());
//      x += QString::fromLocal8Bit(value.body());
//      observable<npp::Lmbcs> r
//          = observable<>::just(npp::ql(x));

      observable<npp::Lmbcs> r
          = observable<>::just(npp::Lmbcs());
      if (value.isTextList()) {
        r = value.textList().map([](npp::LmbcsList list) {
          return list.join(';');
        });
      }
      else if (value.isText() || value.isAddress()) {
        r = value.text();
      }
      else if (value.isDate()) {
        r = value.date().flat_map([](npp::TimeDate td) {
          return td.convertToText();
        });
      }

      return r;
    })
    .subscribe([&result](npp::Lmbcs text) {
      result = lq(text);
    });
    return Field::quote(result);
  }
};

} // namespace npp202009::npp::csv::field

#endif // NPP202009_NPP_CSV_FIELDS_RFC822TEXT_HPP
