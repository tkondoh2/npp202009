#ifndef NPP202009_NPP_CSV_FIELDS_TIMEDATE_HPP
#define NPP202009_NPP_CSV_FIELDS_TIMEDATE_HPP

#include "../field.hpp"
#include "../../../type/timedate.hpp"

namespace npp202009::npp::csv::field {

struct TimeDate
{
  using field_type = npp::TimeDate;
  static QString encode(const npp::TimeDate &value) {
    QString result;
    value.convertToText().subscribe([&result](npp::Lmbcs lmbcs) {
      result = lq(lmbcs);
    });
    return Field::quote(std::move(result));
  }
};

} // namespace npp202009::npp::csv::field

#endif // NPP202009_NPP_CSV_FIELDS_TIMEDATE_HPP
