#ifndef NPP202009_NPP_CSV_FIELDS_NUMBERRANGE_HPP
#define NPP202009_NPP_CSV_FIELDS_NUMBERRANGE_HPP

#include "../field.hpp"
#include "../../../type/numberrange.hpp"

namespace npp202009::npp::csv::field {

struct NumberRange
{
  using field_type = npp::NumberRange;
  static QString encode(const npp::NumberRange &value) {
    QString result;
    value.convertToTextList()
    .map([](npp::LmbcsList list) {
      return list.join(';');
    })
    .subscribe([&result](npp::Lmbcs lmbcs) {
      result = lq(lmbcs);
    });
    return Field::quote(result);
  }
};

} // namespace npp202009::npp::csv::field

#endif // NPP202009_NPP_CSV_FIELDS_NUMBERRANGE_HPP
