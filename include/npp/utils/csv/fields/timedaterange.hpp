#ifndef NPP202009_NPP_CSV_FIELDS_TIMEDATERANGE_HPP
#define NPP202009_NPP_CSV_FIELDS_TIMEDATERANGE_HPP

#include "../field.hpp"
#include "../../../type/timedaterange.hpp"
#include "../../../type/lmbcslist.hpp"

namespace npp202009::npp::csv::field {

struct TimeDateRange
{
  using field_type = npp::TimeDateRange;
  static QString encode(const npp::TimeDateRange &value) {
    QString result;
    value.convertToTextList()
    .map([](npp::LmbcsList list) {
      return list.join(";");
    })
    .subscribe([&result](npp::Lmbcs lmbcs) {
      result = lq(lmbcs);
    });
    return Field::quote(result);
  }
};

} // namespace npp202009::npp::csv::field

#endif // NPP202009_NPP_CSV_FIELDS_TIMEDATERANGE_HPP
