#ifndef NPP202009_NPP_CSV_FIELDS_STRING_HPP
#define NPP202009_NPP_CSV_FIELDS_STRING_HPP

#include "../field.hpp"

namespace npp202009::npp::csv::field {

struct String
{
  using field_type = QString;
  static QString encode(const QString &value) {
    return Field::quote(value);
  }
};

} // namespace npp202009::npp::csv::field

#endif // NPP202009_NPP_CSV_FIELDS_STRING_HPP
