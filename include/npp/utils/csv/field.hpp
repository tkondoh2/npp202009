#ifndef NPP202009_NPP_CSV_FIELD_HPP
#define NPP202009_NPP_CSV_FIELD_HPP

#include <QString>
#include <QSharedPointer>

namespace npp202009::npp::csv {

/**
 * @class Field
 * @brief CSVフィールド抽象クラス
 */
class Field
{
public:
  /**
   * デストラクタ
   */
  virtual ~Field() = default;

  /**
   * @brief CSVに書き込む文字列を取得する。
   * @return フィールドデータのCSV文字列
   */
  virtual QString encode() const = 0;

  /**
   * @brief CSV文字列化する時に行うエスケープシーケンス処理
   * @param value 元の文字列
   * @return エスケープシーケンス処理された文字列
   */
  static QString escape(QString &&value) {
    return value.replace(R"(")", R"("")");
  }

  /**
   * @brief 文字列をクォーテーションで囲む。
   * @param value クォーテーションで囲む文字列
   * @return クォーテーションで囲まれた文字列
   */
  static QString quote(const QString &value) {
    return QString(R"("%1")").arg(escape(QString(value)));
  }
};

using FieldPtr = QSharedPointer<Field>;

/**
 * @class FieldBase
 * @brief エンコード処理にテンプレートを利用するクラス
 * @tparam データの型とエンコード処理を提供するクラス
 */
template <class T>
class FieldBase
    : public Field
{
  typename T::field_type value_; ///< フィールドのデータ型

public:
  /**
   * @brief コンストラクタ
   * @param value フィールドデータ
   */
  FieldBase(typename T::field_type &&value)
    : value_(std::move(value))
  {}

  /**
   * @brief デストラクタ
   */
  virtual ~FieldBase() = default;

  /**
   * @brief CSVに書き込む文字列を取得する。
   * @return フィールドデータのCSV文字列
   */
  virtual QString encode() const override {
    return T::encode(value_);
  }
};

/**
 * @fn make_field
 * @brief データからCSVフィールドオブジェクトの共有ポインタを作成する。
 * @tparam T フィールド型サポートクラス
 */
template <class T>
FieldPtr make_field(typename T::field_type &&value) {
  Field *p = new FieldBase<T>(std::move(value));
  return FieldPtr(p);
}

} // namespace npp202009::npp::csv

#endif // NPP202009_NPP_CSV_FIELD_HPP
