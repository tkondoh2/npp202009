#ifndef NPP202009_NPP_CSV_RECORD_HPP
#define NPP202009_NPP_CSV_RECORD_HPP

#include "field.hpp"
#include <QList>
#include "../../item.hpp"
#include "fields/lmbcs.hpp"
#include "fields/lmbcslist.hpp"
#include "fields/number.hpp"
#include "fields/numberrange.hpp"
#include "fields/timedate.hpp"
#include "fields/timedaterange.hpp"
#include "fields/rfc822text.hpp"
#include "fields/lmbcs.hpp"

namespace npp202009::npp::csv {

/**
 * @class Record
 * @brief 1行分のCSVフィールドデータ集合体
 */
class Record
    : public QList<FieldPtr>
{
public:
  /**
   * @brief フィールドを追加する。
   * @tparam T フィールド型
   * @param value 追加するフィールドのデータ型
   */
  template <class T>
  void appendField(typename T::field_type &&value) {
    append(make_field<T>(std::move(value)));
  }

  /**
   * @brief 1行分の文字列に変換する。
   * @return CSV文字列
   */
  QString toLine() const {
    QStringList list;
    for (int i = 0; i < count(); ++i) {
      list.append(at(i)->encode());
    }
    return list.join(",");
  }

  /**
   * @brief Itemからフィールドオブジェクトを作成する。
   * @tparam T 変換先フィールド型
   * @param item 変換元Itemオブジェクト
   * @return フィールドオブジェクトポインタ
   */
  template <class T>
  static FieldPtr makeField(const npp::Item &item) {
    auto opt = item.value().get<typename T::field_type>();
    return make_field<T>(opt ? opt.value() : (typename T::field_type()));
  }

  /**
   * @brief Itemの型に応じてフィールドオブジェクトを作成する。
   * @param item 変換元Itemオブジェクト
   * @return フィールドオブジェクトポインタ
   */
  static FieldPtr itemToField(const npp::Item &item) {

//    QString buffer;
//    QTextStream str(&buffer);
//    str << "Type:"
//        << QString::number(item.type(), 16).right(4)
//        << ",";
//    npp::Item::Value value = item.value();
//    QByteArray bytes = value.value();
//    for (int i = 0; i < bytes.size(); ++i) {
//      str << " x"
//          << QString::number(static_cast<uint>(bytes.at(i)), 16).right(2);
//    }
//    str << endl;
//    return make_field<field::Lmbcs>(npp::ql(buffer));

        switch (item.type()) {
    case npp::Lmbcs::value_type:
      return makeField<field::Lmbcs>(item);
    case npp::LmbcsList::value_type:
      return makeField<field::LmbcsList>(item);
    case npp::Number::value_type:
      return makeField<field::Number>(item);
    case npp::NumberRange::value_type:
      return makeField<field::NumberRange>(item);
    case npp::TimeDate::value_type:
      return makeField<field::TimeDate>(item);
    case npp::TimeDateRange::value_type:
      return makeField<field::TimeDateRange>(item);
    case npp::mime::Rfc822Text::value_type:
      return makeField<field::Rfc822Text>(item);

//    case TYPE_TEXT: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: TEXT>>"));
//    case TYPE_TEXT_LIST: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: TEXT_LIST>>"));
//    case TYPE_NUMBER: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: NUMBER>>"));
//    case TYPE_NUMBER_RANGE: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: NUMBER_RANGE>>"));
//    case TYPE_TIME: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: TIME>>"));
//    case TYPE_TIME_RANGE: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: TIME_RANGE>>"));
//    case TYPE_RFC822_TEXT: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: RFC822_TEXT>>"));
    case TYPE_ERROR: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: ERROR>>"));
    case TYPE_UNAVAILABLE: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: UNAVAILABLE>>"));
    case TYPE_FORMULA: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: FORMULA>>"));
    case TYPE_USERID: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: USERID>>"));
    case TYPE_INVALID_OR_UNKNOWN: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: INVALID_OR_UNKNOWN>>"));
    case TYPE_COMPOSITE: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: COMPOSITE>>"));
    case TYPE_COLLATION: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: COLLATION>>"));
    case TYPE_OBJECT: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: OBJECT>>"));
    case TYPE_NOTEREF_LIST: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: NOTEREF_LIST>>"));
    case TYPE_VIEW_FORMAT: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: VIEW_FORMAT>>"));
    case TYPE_ICON: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: ICON>>"));
    case TYPE_NOTELINK_LIST: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: NOTELINK_LIST>>"));
    case TYPE_SIGNATURE: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: SIGNATURE>>"));
    case TYPE_SEAL: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: SEAL>>"));
    case TYPE_SEALDATA: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: SEALDATA>>"));
    case TYPE_SEAL_LIST: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: SEAL_LIST>>"));
    case TYPE_HIGHLIGHTS: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: HIGHLIGHTS>>"));
    case TYPE_WORKSHEET_DATA: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: WORKSHEET_DATA>>"));
    case TYPE_USERDATA: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: USERDATA>>"));
    case TYPE_QUERY: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: QUERY>>"));
    case TYPE_ACTION: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: ACTION>>"));
    case TYPE_ASSISTANT_INFO: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: ASSISTANT_INFO>>"));
    case TYPE_VIEWMAP_DATASET: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: VIEWMAP_DATASET>>"));
    case TYPE_VIEWMAP_LAYOUT: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: VIEWMAP_LAYOUT>>"));
    case TYPE_LSOBJECT: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: LSOBJECT>>"));
    case TYPE_HTML: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: HTML>>"));
    case TYPE_SCHED_LIST: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: SCHED_LIST>>"));
    case TYPE_CALENDAR_FORMAT: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: CALENDAR_FORMAT>>"));
    case TYPE_MIME_PART: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: MIME_PART>>"));
    case TYPE_SEAL2: return make_field<field::Lmbcs>(npp::Lmbcs("<<undefine_field_type: SEAL2>>"));
    default:
      return make_field<field::Lmbcs>(npp::Lmbcs("<<unknown_field_type>>"));
    }
  }
};

} // namespace npp202009::npp::csv

#endif // NPP202009_NPP_CSV_RECORD_HPP
