#ifndef NPP202009_NPP_CSV_FILE_HPP
#define NPP202009_NPP_CSV_FILE_HPP

#include "./record.hpp"

namespace npp202009::npp::csv {

/**
 * @class File
 * @brief CSVファイルインターフェース
 */
class File
{
public:
  /**
   * @brief デストラクタ
   */
  virtual ~File() = default;

  /**
   * @brief CSVレコード(1行分のデータ)を書き込む。
   * @param record CSVレコード
   * @param flush 即時フラッシュするかどうか
   */
  virtual void writeRecord(const Record &record) = 0;
//  virtual void writeRecord(
//      const Record &record,
//      bool flush = true
//      ) = 0;

  virtual void flush() = 0;
};

} // namespace npp202009::npp::csv

#endif // NPP202009_NPP_CSV_FILE_HPP
