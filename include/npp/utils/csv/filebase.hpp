#ifndef NPP202009_NPP_CSV_FILEBASE_HPP
#define NPP202009_NPP_CSV_FILEBASE_HPP

#include "./file.hpp"
#include "./record.hpp"
#include "../writer/filebase.hpp"

namespace npp202009::npp::csv {

/**
 * @class FileBase
 * @brief writer::FileBaseを用いたCSVファイルクラス
 * @tparam Options オプションオブジェクトクラス
 */
template <class Options>
class FileBase
    : public File
{
  writer::FileBase<Options> file_; ///< ファイルオブジェクト

public:
  /**
   * @brief コンストラクタ
   * @param options オプションオブジェクト
   */
  FileBase(
      Options &&options
//      ,
//      QMutex *pFileMutex,
//      QReadWriteLock *pReadWriteMutex
      )
    : File()
    , file_(std::move(options)
//            , pFileMutex
//            , pReadWriteMutex
            )
  {}

  /**
   * @brief デストラクタ
   */
  virtual ~FileBase() = default;

  /**
   * @brief CSVレコード(1行分のデータ)を書き込む。
   * @param record CSVレコード
   * @param flush 即時フラッシュするかどうか
   */
  virtual void writeRecord( const Record &record ) override {
    if (!file_.isOpen()) {
      file_.open();
    }
    file_.write(record.toLine(), true);
  }
//  virtual void writeRecord(
//      const Record &record,
//      bool flush = true
//      ) override {
//    if (!file_.isOpen()) {
//      file_.open();
//    }
//    file_.write(record.toLine(), true, flush);
//  }

  virtual void flush() override {
    file_.flush();
  }
};

} // namespace npp202009::npp::csv

#endif // NPP202009_NPP_CSV_FILEBASE_HPP
