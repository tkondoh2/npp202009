#ifndef NPP202009_NPP_UTILS_HPP
#define NPP202009_NPP_UTILS_HPP

#include <QVariantMap>
#include <QList>
#include <QPair>
#include <QString>
#include <QUrl>
#include <QFileInfo>
#include <rxcpp/rx.hpp>
#include <sstream>

#ifdef NT
#include <Windows.h>
#else
#include <unistd.h>
#endif

using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp {

/**
 * @brief キーバリューペアリスト型をバリアントマップ型に変換する
 */
template <class K, class V>
QVariantMap pairsToMap(QList<QPair<K, V>> pairs) noexcept {
  QVariantMap map;
  foreach (auto pair, pairs) {
    map.insert(pair.first, pair.second);
  }
  return map;
}

/**
 * @brief クエリーのキーや値をエンコードする
 * @param value 対象文字列
 * @return エンコードバイト文字列
 */
inline QByteArray encodeForQueryItem(const QString &value) noexcept {
  return QUrl::toPercentEncoding(value, "*", " ");
}

inline QByteArray getProcessFilePath() noexcept {
#ifdef NT
  char buffer[MAX_PATH] = "";
  return GetModuleFileNameA(nullptr, buffer, MAX_PATH)
      ? QByteArray(buffer)
      : QByteArray();
#else
  char path[1024] = "";
  ssize_t len = readlink("/proc/self/exe", path, sizeof(path) - 1);
  return QByteArray(path, static_cast<int>(len));
#endif
}

inline QFileInfo getProcessFileInfo() noexcept {
  return QFileInfo(QString::fromLocal8Bit(getProcessFilePath()));
}

template <class T = int> observable<T> for_each(T start, T count, T step = 1) {
  return observable<>::create<T>([start, count, step](subscriber<T> s) {
    for (T i = start; i < start + count; i += step) {
      s.on_next(i);
    }
    s.on_completed();
  });
}

template <class T> observable<T> for_qlist(QList<T> list) {
  return observable<>::create<T>([list](subscriber<T> s) {
    foreach (T item, list) {
      s.on_next(item);
    }
    s.on_completed();
  });
}

inline QString getThreadIdText() {
  std::ostringstream oss;
  oss << std::this_thread::get_id();
  return QString::fromStdString(oss.str());
}

template <class T>
T getSeparator() noexcept {
  return
#ifdef NT
    "\\"
#else
    "/"
#endif
  ;
}

} // namespace npp202009::npp

#endif // NPP202009_NPP_UTILS_HPP
