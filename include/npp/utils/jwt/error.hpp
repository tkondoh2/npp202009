#ifndef NPP202009_NPP_JWT_ERROR_HPP
#define NPP202009_NPP_JWT_ERROR_HPP

#include <QByteArray>
#include <openssl/err.h>

namespace npp202009::npp::jwt {

template <size_t SIZE = 256>
bool throwErrors() {
  uint code = ERR_get_error();
  if (code > 0) {
    QByteArray buffer(static_cast<int>(SIZE + 1), '\0');
    ERR_error_string_n(code, buffer.data(), SIZE);
    throw std::runtime_error(buffer.toStdString());
  }
  return false;
}

} // namespace npp202009::npp::jwt

#endif // NPP202009_NPP_JWT_ERROR_HPP
