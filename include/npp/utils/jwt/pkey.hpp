#ifndef NPP202009_NPP_JWT_PKEY_HPP
#define NPP202009_NPP_JWT_PKEY_HPP

#include "./rsa.hpp"
#include "./error.hpp"

namespace npp202009::npp::jwt {

/**
 * @brief EVP_PKEY解放用ハンドラ(QScopedPointer用)
 */
struct PKeyDeleter {
  static void cleanup(EVP_PKEY *pPKey) { EVP_PKEY_free(pPKey); }
};

using PKeyPtr = QScopedPointer<EVP_PKEY, PKeyDeleter>;

/**
 * @brief OpenSSL EVP_PKEY型のラッパークラス
 */
class PKey
{
  PKeyPtr ptr_; /// EVP_PKEYポインタ

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  PKey() : ptr_(EVP_PKEY_new()) {}

  /**
   * @brief EVP_PKEYオリジナルポインタ
   * @return EVP_PKEYオリジナルポインタ
   */
  EVP_PKEY *operator ()() {
    return ptr_.data();
  }

  /**
   * @brief RSAキーの設定
   * @tparam T RSAファンクタ型
   * @param rsa RSAキー
   */
  template <class T>
  bool setRSA(Rsa<T> &rsa) noexcept(false) {
    if (EVP_PKEY_set1_RSA(ptr_.data(), rsa.data()) != 0) {
      return true;
    }
    return throwErrors();
  }

  /**
   * @brief EVP_PKEYのサイズ
   * @return EVP_PKEYのサイズ
   */
  uint size() const {
    return EVP_PKEY_size(ptr_.data());
  }
};

} //namespace npp202009::npp::jwt

#endif // NPP202009_NPP_JWT_PKEY_HPP
