#ifndef NPP202009_NPP_JWT_MESSAGEDIGEST_HPP
#define NPP202009_NPP_JWT_MESSAGEDIGEST_HPP

#include "./pkey.hpp"
#include <rxcpp/rx.hpp>

using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp::jwt {

/**
 * @brief メッセージダイジェスト解放用ハンドラ(QScopedPointer用)
 */
struct MDCtxDeleter {
  static void cleanup(EVP_MD_CTX *pMDCtx) { EVP_MD_CTX_destroy(pMDCtx); }
};

using MDCtxPtr = QScopedPointer<EVP_MD_CTX, MDCtxDeleter>;

/**
 * @brief メッセージダイジェスト用ラッパークラス
 */
class MessageDigest
{
  const EVP_MD *pType_; /// メッセージダイジェスト

public:
  /**
   * @brief メッセージダイジェストコンテキスト用ラッパークラス
   */
  class Context
  {
    MDCtxPtr ptr_; /// メッセージダイジェストコンテキスト

  public:
    /**
     * @brief コンストラクタ
     */
    Context() : ptr_(EVP_MD_CTX_create()) {}

    /**
     * @brief メッセージダイジェストコンテキストオリジナルポインタ
     * @return メッセージダイジェストコンテキストオリジナルポインタ
     */
    EVP_MD_CTX *operator ()() { return ptr_.data(); }

    /**
     * @brief 署名用に初期化
     * @param pMD メッセージダイジェスト
     * @param pKey EVP_PKEY
     * @param ppPKCtx EVP_PKEYコンテキスト
     * @param impl エンジン
     * @return 成功すれば1
     */
    bool initSign(
        const EVP_MD *pMD,
        EVP_PKEY *pKey,
        EVP_PKEY_CTX **ppPKCtx = nullptr,
        ENGINE *impl = nullptr
        ) noexcept(false) {
      if (EVP_DigestSignInit(ptr_.data(), ppPKCtx, pMD, impl, pKey)) {
        return true;
      }
      return throwErrors();
    }

    /**
     * @brief 署名処理のアップデート
     * @param pMessage 署名メッセージ
     * @param pMessageSize 署名メッセージサイズ
     * @return 成功すれば1
     */
    int updateSign(const void *pMessage, size_t pMessageSize) noexcept(false) {
      if (EVP_DigestSignUpdate(ptr_.data(), pMessage, pMessageSize)) {
        return true;
      }
      return throwErrors();
    }

    /**
     * @brief 署名処理の終了
     * @param pSignature 署名格納先
     * @param pSigSize 署名格納先サイズ
     * @return 成功すれば1
     */
    int finalSign(uchar *pSignature, size_t *pSigSize) noexcept(false) {
      if (EVP_DigestSignFinal(ptr_.data(), pSignature, pSigSize)) {
        return true;
      }
      return throwErrors();
    }

    /**
     * @brief 検証用に初期化
     * @param pMD メッセージダイジェスト
     * @param pKey EVP_PKEY
     * @param ppPKCtx EVP_PKEYコンテキスト
     * @param impl エンジン
     * @return 成功すれば1
     */
    int initVerify(
        const EVP_MD *pMD,
        EVP_PKEY *pKey,
        EVP_PKEY_CTX **ppPKCtx = nullptr,
        ENGINE *impl = nullptr
        ) noexcept(false) {
      if (EVP_DigestVerifyInit(ptr_.data(), ppPKCtx, pMD, impl, pKey)) {
        return true;
      }
      return throwErrors();
    }

    /**
     * @brief 検証処理のアップデート
     * @param pMessage 検証メッセージ
     * @param pMessageSize 検証メッセージサイズ
     * @return 成功すれば1
     */
    int updateVerify(const void *pMessage, size_t messageSize) noexcept(false) {
      if (EVP_DigestVerifyUpdate(ptr_.data(), pMessage, messageSize)) {
        return true;
      }
      return throwErrors();
    }

    /**
     * @brief 検証処理の終了
     * @param pSignature 署名格納先
     * @param pSigSize 署名格納先サイズ
     * @return 成功すれば1
     */
    int finalVerify(const uchar *pSignature, size_t sigSize) noexcept(false) {
      return EVP_DigestVerifyFinal(ptr_.data(), pSignature, sigSize);
//      if (EVP_DigestVerifyFinal(ptr_.data(), pSignature, sigSize)) {
//        return true;
//      }
//      return throwErrors();
    }
  };

  /**
   * @brief コンストラクタ
   * @param name アルゴリズム名
   */
  MessageDigest(const QString &name) : pType_(nullptr) {
    if (name == "RS256") { pType_ = EVP_sha256(); }
    else if (name == "RS384") { pType_ = EVP_sha384(); }
    else if (name == "RS512") { pType_ = EVP_sha512(); }
  }

  const EVP_MD *pType() { return pType_; }

  /**
   * @brief 有効なメッセージダイジェストか
   * @return 有効なメッセージダイジェストか
   */
  bool isValid() const { return pType_ != nullptr; }

  /**
   * @brief デジタル署名をする
   * @param source 署名するバイト列
   * @param privateKey プライベートキー
   * @return 署名されたバイト列
   */
  QByteArray sign(
      const QByteArray &source,
      const QByteArray &privateKey
      ) const noexcept(false) {
    Bio bioPrivateKey(privateKey);
    Rsa<rsa::Private> rsaPrivateKey(bioPrivateKey);
    if (!rsaPrivateKey.isNull()) {

      PKey pkey;
      pkey.setRSA(rsaPrivateKey);

      Context ctx;
      ctx.initSign(pType_, pkey());
      ctx.updateSign(source.data(), source.size());

      size_t sigSize = 0;
      ctx.finalSign(nullptr, &sigSize);

      QByteArray buffer(static_cast<int>(sigSize), '\0');
      ctx.finalSign(
            reinterpret_cast<uchar*>(buffer.data()),
            &sigSize
            );
      return buffer;
    }
    return QByteArray();
  }

  /**
   * @brief デジタル署名を検証する
   * @param source 署名前のバイト列
   * @param signature 検証するバイト列
   * @param publicKey パブリックキー
   * @return 検証に成功すれば真
   */
  bool verify(
      const QByteArray &source,
      const QByteArray &signature,
      const QByteArray &publicKey
      ) const noexcept(false) {
    Bio bioPublicKey(publicKey);
    Rsa<rsa::Public> rsaPublicKey(bioPublicKey);
    if (!rsaPublicKey.isNull()) {

      PKey pkey;
      pkey.setRSA(rsaPublicKey);

      Context ctx;
      ctx.initVerify(pType_, pkey());
      ctx.updateVerify(source.data(), source.size());
      return ctx.finalVerify(
            reinterpret_cast<const uchar*>(signature.constData()),
            static_cast<size_t>(signature.size())
            );
    }
    return false;
  }
};

} // namespace npp202009::npp::jwt

#endif // NPP202009_NPP_JWT_MESSAGEDIGEST_HPP
