#ifndef NPP202009_NPP_JWT_PAYLOADJSON_HPP
#define NPP202009_NPP_JWT_PAYLOADJSON_HPP

#include "./json.hpp"

namespace npp202009::npp::jwt {

/**
 * @brief JsonWebTokenペイロード型
 */
class PayloadJson
    : public Json
{
public:
  /**
   * @brief コンストラクタ
   */
  PayloadJson() : Json() {}

  /**
   * @brief 発行日時
   * @return 発行日時オプショナル
   */
  std::optional<QDateTime> issued() const {
    return dateTime("iat");
  }

  /**
   * @brief 失効日時
   * @return 失効日時オプショナル
   */
  std::optional<QDateTime> expires() const {
    return dateTime("exp");
  }

  /**
   * @brief 失効しているか
   * @return 失効しているか
   */
  bool isExpired() const {
    std::optional<QDateTime> exp = expires();
    return exp ? (exp.value() < QDateTime::currentDateTime()) : true;
  }

  /**
   * @brief 発行日時と失効日時を設定
   * @param sec 現在から失効までの秒数
   */
  void setIssueAndExpires(qint64 sec) {
    qint64 now = QDateTime::currentSecsSinceEpoch();
    insert("iat", now);
    insert("exp", now + sec);
  }
};

} // namespace npp202009::npp::jwt

#endif // NPP202009_NPP_JWT_PAYLOADJSON_HPP
