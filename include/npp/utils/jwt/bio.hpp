#ifndef NPP202009_NPP_JWT_BIO_HPP
#define NPP202009_NPP_JWT_BIO_HPP

#include <openssl/evp.h>
#include <QScopedPointer>
#include <QByteArray>

namespace npp202009::npp::jwt {

/**
 * @brief BIO解放用ハンドラ(QScopedPointer用)
 */
struct BIODeleter
{
  static void cleanup(BIO *pBIO) { BIO_free(pBIO); }
};

using BIOPtr = QScopedPointer<BIO, BIODeleter>;

/**
 * @brief OpenSSL BIO型のラッパークラス
 */
class Bio
{
  BIOPtr ptr_; /// BIOポインタ

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Bio() : ptr_(BIO_new(BIO_s_mem())) {}

  /**
   * @brief コンストラクタ
   * @param bytes 元のバイト列
   */
  Bio(const QByteArray &bytes)
    : ptr_(BIO_new_mem_buf(bytes.constData(), bytes.length()))
  {}

  /**
   * @brief BIOオリジナルポインタ
   * @return BIOオリジナルポインタ
   */
  BIO *operator ()() const {
    return ptr_.data();
  }

  /**
   * @brief バイト列に変換
   * @return バイト列
   */
  QByteArray toBytes() const {
    char* ptr;
    int len = BIO_get_mem_data(ptr_.data(), &ptr);
    return QByteArray(ptr, len);
  }

  /**
   * @brief BIOからデータを読み込む
   * @param pBuffer 読み出し先バッファ
   * @param bufferSize 読み出し先バッファのサイズ
   * @return  読み出したバイト数
   */
  int read(void *pBuffer, int bufferSize) const {
    return BIO_read(ptr_.data(), pBuffer, bufferSize);
  }

  /**
   * @brief write BIOにデータを書き込む
   * @param pBuffer 書き込み先バッファ
   * @param bufferSize 書き込み先バッファのサイズ
   * @return 書き込んだバイト数
   */
  int write(const void *pBuffer, int bufferSize) {
    return BIO_write(ptr_.data(), pBuffer, bufferSize);
  }
};

} // namespace npp202009::npp::jwt

#endif // NPP202009_NPP_JWT_BIO_HPP
