#ifndef NPP202009_NPP_JWT_HEADERJSON_HPP
#define NPP202009_NPP_JWT_HEADERJSON_HPP

#include "./json.hpp"

namespace npp202009::npp::jwt {

/**
 * @brief JsonWebTokenヘッダ型
 */
class HeaderJson
    : public Json
{
public:
  /**
   * @brief コンストラクタ
   */
  explicit HeaderJson() : Json() {}

  /**
   * @brief デジタル署名アルゴリズム
   * @return デジタル署名アルゴリズム値
   */
  QString algorithm() const {
    return value("alg").toString();
  }

  /**
   * @brief デジタル署名アルゴリズムの設定
   * @param value
   */
  void setAlgorithm(QString &&value) {
    insert("alg", value);
  }

  /**
   * @brief 有効なオブジェクトか
   * @return 有効なオブジェクトか
   */
  bool isValid() const {
    return type() == "JWT";
  }

  /**
   * @brief ファクトリー
   * @param alg デジタル署名アルゴリズム値
   * @return JsonWebTokenヘッダオブジェクト
   */
  static HeaderJson makeHeader(QString &&alg) {
    HeaderJson json;
    json.setType("JWT");
    json.setAlgorithm(std::move(alg));
    return json;
  }
};

} // namespace npp202009::npp::jwt

#endif // NPP202009_NPP_JWT_HEADERJSON_HPP
