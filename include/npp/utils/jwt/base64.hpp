#ifndef NPP202009_NPP_JWT_BASE64_HTPP
#define NPP202009_NPP_JWT_BASE64_HTPP

#include <QByteArray>

namespace npp202009::npp::jwt {

inline QByteArray fromBase64ToBytes(
    const QByteArray &base64,
    const QByteArray::Base64Options options
      = QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals
    )
{
  return QByteArray::fromBase64(base64, options);
}

inline QByteArray fromBytesToBase64(
    const QByteArray &text,
    const QByteArray::Base64Options options
      = QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals
    )
{
  return text.toBase64(options);
}

} // namespace npp202009::npp::jwt

#endif // NPP202009_NPP_JWT_BASE64_HTPP
