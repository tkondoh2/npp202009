#ifndef NPP202009_NPP_JWT_JSONWEBTOKEN_HPP
#define NPP202009_NPP_JWT_JSONWEBTOKEN_HPP

#include "./headerjson.hpp"
#include "./payloadjson.hpp"
#include "./base64.hpp"
#include "./messagedigest.hpp"

namespace npp202009::npp::jwt {

using PrivateKey = QByteArray;
using PublicKey = QByteArray;

/**
 * @brief JsonWebToken型
 */
class JsonWebToken
{
  HeaderJson header_; /// ヘッダ
  PayloadJson payload_; /// ペイロード
  QByteArray signature_; /// 署名

public:
  /**
   * @brief コンストラクタ
   * @param header ヘッダ
   * @param payload ペイロード
   * @param signature 署名、デフォルトは空
   */
  JsonWebToken(
      HeaderJson &&header,
      PayloadJson &&payload,
      const QByteArray &signature = QByteArray()
      )
    : header_(std::move(header))
    , payload_(std::move(payload))
    , signature_(signature)
  {}

  /**
   * @brief コンストラクタ
   * @param header ヘッダ
   * @param payload ペイロード
   * @param signature 署名、デフォルトは空
   */
  JsonWebToken(
      const HeaderJson &header,
      const PayloadJson &payload,
      const QByteArray &signature = QByteArray()
      )
    : header_(header)
    , payload_(payload)
    , signature_(signature)
  {}

  /**
   * @brief ヘッダ
   * @return ヘッダ
   */
  const HeaderJson &header() const { return header_; }

  /**
   * @brief ペイロード
   * @return ペイロード
   */
  const PayloadJson &payload() const { return payload_; }

  /**
   * @brief 署名
   * @return 署名
   */
  const QByteArray &signature() const { return signature_; }

  /**
   * @brief 署名前の値
   * @return 署名前の値
   */
  QByteArray beforeSignature() const {
    return toBase64(header_) + "." + toBase64(payload_);
  }

  /**
   * @brief トークンの生成
   * @return トークン値
   */
  QByteArray toToken() const {
    return beforeSignature() + "." + signature_;
  }

  /**
   * @brief 有効なJWTか(失効しているかは関知しない)
   * @return 有効なJWTか
   */
  bool isValid() const {
    return !header_.isEmpty() && !payload_.isEmpty() && !signature_.isEmpty();
  }

  /**
   * @brief Base64→QJsonDocument→T型(ヘッダかペイロード)に変換
   * @tparam T 変換先の型(ヘッダかペイロード)
   * @param base64 Base64値
   * @return 変換したT型オブジェクト
   */
  template <class T>
  static T fromBase64(const QByteArray &base64) {
    QJsonDocument doc = QJsonDocument::fromJson(fromBase64ToBytes(base64));
    return Json::fromDocument<T>(doc);
  }

  /**
   * @brief T型(ヘッダかペイロード)→QJsonDocument→Base64に変換
   * @tparam T 変換元の型(ヘッダかペイロード)
   * @param json 変換元のT型オブジェクト
   * @param isCompact コンパクトかインデントか(デフォルトはコンパクト)
   * @return Base64値
   */
  template <class T>
  static QByteArray toBase64(const T &json, bool isCompact = true) {
    QJsonDocument doc = json.toDocument();
    return fromBytesToBase64(
      doc.toJson(isCompact ? QJsonDocument::Compact : QJsonDocument::Indented)
    );
  }

  /**
   * @brief 署名済みJsonWebTokenを発行する
   * @param privateKey 署名用プライベートキー
   * @param payload ペイロード
   * @param header ヘッダ(デフォルトはアルゴリズムRSA SHA-256)
   * @return 署名済みJsonWebTokenのオブザーバブル
   */
  static observable<JsonWebToken> encode(
      const PrivateKey &privateKey,
      PayloadJson &&payload,
      HeaderJson &&header = HeaderJson::makeHeader("RS256")
      ) {
    return observable<>::create<JsonWebToken>(
          [=](subscriber<JsonWebToken> s) {
      JsonWebToken jwt(std::move(header), std::move(payload));
      QByteArray before = jwt.beforeSignature();
      MessageDigest md(jwt.header().algorithm());
      if (!md.isValid()) {
        s.on_error(
              std::make_exception_ptr(
                std::runtime_error("[JsonWebToken] Not a valid message digest")
                )
              );
        return;
      }
      try {
        jwt.signature_ = fromBytesToBase64(md.sign(before, privateKey));
        s.on_next(jwt);
        s.on_completed();
      }
      catch (std::exception &ex) {
        s.on_error(std::make_exception_ptr(ex));
      }
    });
  }

  static JsonWebToken encode_x(
      const PrivateKey &privateKey,
      PayloadJson &&payload,
      HeaderJson &&header = HeaderJson::makeHeader("RS256")
      ) {
    JsonWebToken jwt(std::move(header), std::move(payload));
    QByteArray before = jwt.beforeSignature();
    MessageDigest md(jwt.header().algorithm());
    if (!md.isValid()) {
      throw std::runtime_error("[JsonWebToken] Not a valid message digest");
    }
    jwt.signature_ = fromBytesToBase64(md.sign(before, privateKey));
    return jwt;
  }

  /**
   * @brief JsonWebTokenトークン文字列を検証する
   * @param publicKey 検証用パブリックキー
   * @param token トークン文字列
   * @return 署名済みJsonWebTokenのオブザーバブル(失敗時はエラー)
   */
  static observable<JsonWebToken> decode(
      const PublicKey &publicKey,
      const QByteArray &token
      ) {
    return observable<>::create<QByteArrayList>(
          [token](subscriber<QByteArrayList> s) {
      if (token.count('.') != 2) {
        s.on_error(
              std::make_exception_ptr(
                std::runtime_error("[JsonWebToken] Not a valid token.")
                )
              );
        return;
      }
      s.on_next(token.split('.'));
      s.on_completed();
    })
    .map([publicKey](QByteArrayList tokens) {
      QJsonParseError error;
      QJsonDocument doc = QJsonDocument::fromJson(
            fromBase64ToBytes(tokens[0]),
            &error
            );
      if (error.error != QJsonParseError::NoError) {
        throw std::runtime_error(error.errorString().toStdString());
      }
      MessageDigest md(doc.object().value("alg").toString());
      if (!md.isValid()) {
        throw std::runtime_error("[JsonWebToken] Not a valid message digest");
      }
      QByteArray before = tokens[0] + "." + tokens[1];
      QByteArray after = fromBase64ToBytes(tokens[2]);
      if (!md.verify(before, after, publicKey)) {
        throw std::runtime_error("[JsonWebToken] Verification failed.");
      }
      return JsonWebToken(
              fromBase64<HeaderJson>(tokens[0]),
              fromBase64<PayloadJson>(tokens[1]),
              tokens[2]
            );
    });
  }

  static JsonWebToken decode_x(
      const PublicKey &publicKey,
      const QByteArray &token
      ) noexcept(false) {
    if (token.count('.') != 2) {
      throw std::runtime_error("[JsonWebToken] Not a valid token.");
    }
    auto tokens = token.split('.');
    QJsonParseError error;
    auto doc = QJsonDocument::fromJson(
        fromBase64ToBytes(tokens[0]),
        &error
        );
    if (error.error != QJsonParseError::NoError) {
      throw std::runtime_error(error.errorString().toStdString());
    }
    MessageDigest md(doc.object().value("alg").toString());
    if (!md.isValid()) {
      throw std::runtime_error("[JsonWebToken] Not a valid message digest");
    }
    auto before = tokens[0] + "." + tokens[1];
    auto after = fromBase64ToBytes(tokens[2]);
    if (!md.verify(before, after, publicKey)) {
      throw std::runtime_error("[JsonWebToken] Verification failed.");
    }
    return JsonWebToken(
        fromBase64<HeaderJson>(tokens[0]),
        fromBase64<PayloadJson>(tokens[1]),
        tokens[2]
        );
  }
};

} // namespace npp202009::npp::jwt

#endif // NPP202009_NPP_JWT_JSONWEBTOKEN_HPP
