#ifndef NPP202009_NPP_JWT_RSA_HPP
#define NPP202009_NPP_JWT_RSA_HPP

#include "./bio.hpp"
#include <openssl/rsa.h>
#include <openssl/pem.h>

namespace npp202009::npp::jwt {

/**
 * @brief RSA解放用ハンドラ(QScopedPointer用)
 */
struct RSADeleter
{
  static void cleanup(RSA *pRSA) { RSA_free(pRSA); }
};

using RSAPtr = QScopedPointer<RSA, RSADeleter>;

/**
 * @brief OpenSSL RSA型のラッパークラス
 * @tparam T プライベートまたはパブリック用ファンクタ型
 */
template <class T>
class Rsa
    : public RSAPtr
{
public:
  /**
   * @brief コンストラクタ
   * @param key BIO型のキー
   */
  Rsa(const Bio &key) : RSAPtr() {
    RSA *p = T()(key(), nullptr, nullptr, nullptr);
    if (p) {
      reset(p);
    }
  }
};

namespace rsa {

/**
 * @brief RSAプライベートキー用ファンクタ
 */
struct Private
{
  RSA *operator ()(BIO *pBio, RSA **ppRsa, pem_password_cb *cb, void *u) {
    return PEM_read_bio_RSAPrivateKey(pBio, ppRsa, cb, u);
  }
};

/**
 * @brief RSAパブリックキー用ファンクタ
 */
struct Public
{
  RSA *operator ()(BIO *pBio, RSA **ppRsa, pem_password_cb *cb, void *u) {
    return PEM_read_bio_RSA_PUBKEY(pBio, ppRsa, cb, u);
  }
};

} // namespace rsa

} // namespace npp202009::npp::jwt

#endif // NPP202009_NPP_JWT_RSA_HPP
