#ifndef NPP202009_NPP_JWT_JSON_HPP
#define NPP202009_NPP_JWT_JSON_HPP

#include <optional>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariant>
#include <QDateTime>

namespace npp202009::npp::jwt {

/**
 * @brief JWTで使用するJSONの基本型
 */
class Json
    : public QJsonObject
{
public:
  /**
   * @brief コンストラクタ
   */
  explicit Json() : QJsonObject() {}

  /**
   * @brief "typ"パラメータ
   * @return "type"値
   */
  QString type() const {
    return value("typ").toString();
  }

  /**
   * @brief "typ"パラメータの設定
   * @param value "type"値
   */
  void setType(QString &&value) {
    insert("typ", value);
  }

  /**
   * @brief 有効なタイプ値か？
   * @param _type 想定するタイプ値
   * @return 有効なタイプ値なら真
   */
  bool isValidType(const QString &_type) const {
    return type() == _type;
  }

  /**
   * @brief 日時値の取得
   * @param key 日時値のキー
   * @return QDateTimeのオプショナル型
   */
  std::optional<QDateTime> dateTime(const QString &key) const {
    QJsonValue v = value(key);
    bool ok = false;
    qint64 n = v.toVariant().toLongLong(&ok);
    return ok
        ? std::make_optional(QDateTime::fromSecsSinceEpoch(n))
        : std::nullopt;
  }

  /**
   * @brief JSON文書に変換
   * @return JSON文書
   */
  QJsonDocument toDocument() const {
    return QJsonDocument(*static_cast<const QJsonObject*>(this));
  }

  /**
   * @brief JSON文書から変換
   * @tparam T 変換先の型(QJsonObject由来)
   * @param doc 変換元のJSON文書
   */
  template <class T>
  static T fromDocument(const QJsonDocument &doc) {
    T json; QJsonObject o = doc.object();
    json.swap(o);
    return json;
  }
};

} // namespace npp202009::npp::jwt

#endif // NPP202009_NPP_JWT_JSON_HPP
