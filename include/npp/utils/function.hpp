#ifndef NPP202009_NPP_FUNCTION_HPP
#define NPP202009_NPP_FUNCTION_HPP

#include <QString>
#include <QStringList>

namespace npp202009::npp::function {

/**
 * @brief 文字列リストを@関数文字列リストリテラルに変換
 * @param list 文字列リスト
 * @return @関数文字列リストリテラル
 */
template <class T = QString>
T toStringList(const QList<T> &list) {
  QList<T> result;
  const T quot(R"(")");
  foreach (T item, list) {
    item.replace(quot, R"(\")");
    result.append(quot + item + quot);
  }
  return result.join(':');
}

template <class T = QString>
T escape(const T &text) {
  T result(text);
  result.replace(R"(\)", R"(\\)");
  return result;
}

} // namespace npp202009::npp::function

#endif // NPP202009_NPP_FUNCTION_HPP
