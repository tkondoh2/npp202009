#ifndef NPP202009_NPP_WRITER_FILE_HPP
#define NPP202009_NPP_WRITER_FILE_HPP

#include <QMutex>
#include <QFileInfo>
#include <QRunnable>
#include <QTextCodec>

namespace npp202009::npp::writer {

/**
 * @class File
 * @brief 書き込みファイル用インターフェース
 */
class File
{
public:
  /**
   * @brief ファイル操作用ミューテックスを取得する。
   * @return ファイル操作用ミューテックスへのポインタ
   */
//  virtual QMutex *fileMutex() = 0;

  /**
   * @brief ファイルは開いているか。
   * @return 開いていれば真を返す。
   */
  virtual bool isOpen() const = 0;

  /**
   * @brief ファイル情報を取得する。
   * @return ファイル情報(QFileInfo型)
   */
  virtual QFileInfo info() const = 0;

  /**
   * @brief ファイルを開く。
   */
  virtual void open() = 0;

  /**
   * @brief ファイルを閉じる。
   */
  virtual void close() = 0;

  /**
   * @brief コーデックを設定する。
   * @param pCodec コーデックへのポインタ
   */
  virtual void setCodec(QTextCodec* pCodec) = 0;

  /**
   * @brief ファイルにテキストを書き込む。(より高機能な処理を推奨)
   * @param text 書き込むテキスト
   * @param endl 改行する場合は真
   * @param flush 即時フラッシュする場合は真
   */
//  virtual void write(QString &&text, bool endl, bool flush = true) = 0;
  virtual void write(QString &&text, bool endl) = 0;

  /**
   * @brief ファイルにテキストを書き込む。(より低レベルな処理を推奨)
   * @param text 書き込むテキスト
   * @param endl 改行する場合は真
   * @param flush 即時フラッシュする場合は真
   */
//  virtual void _write(QString &&text, bool endl, bool flush = true) = 0;
  virtual void _write(QString &&text, bool endl) = 0;

  virtual void flush() = 0;
};

} // namespace npp202009::npp::writer

#endif // NPP202009_NPP_WRITER_FILE_HPP
