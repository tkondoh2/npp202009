#ifndef NPP202009_NPP_WRITER_VERIFIER_DAILYVERIFIER_HPP
#define NPP202009_NPP_WRITER_VERIFIER_DAILYVERIFIER_HPP

#include <QFileInfo>
#include <QDateTime>

namespace npp202009::npp::writer::verifier {

/**
 * @brief ファイルの更新日が今日より前の時、偽を返す関数オブジェクトクラス
 */
class DailyVerifier
{
public:
  bool operator ()(const QFileInfo &info) const {
    QDateTime now = QDateTime::currentDateTime();
    QDateTime modified = info.lastModified();
    return now.daysTo(modified) >= 0; // 前日なら-1でfalseを返す
  }
};

} // namespace npp202009::npp::writer::verifier

#endif // NPP202009_NPP_WRITER_VERIFIER_DAILYVERIFIER_HPP
