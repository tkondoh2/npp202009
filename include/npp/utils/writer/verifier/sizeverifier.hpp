#ifndef NPP202009_NPP_WRITER_VERIFIER_SIZEVERIFIER_HPP
#define NPP202009_NPP_WRITER_VERIFIER_SIZEVERIFIER_HPP

#include <QFileInfo>

namespace npp202009::npp::writer::verifier {

/**
 * @brief ファイルのサイズが閾値を超えている時、偽を返す関数オブジェクトクラス
 */
class SizeVerifier
{
  qint64 maxSize_; ///< ファイルサイズの判定閾値

public:
  /**
   * @brief コンストラクタ
   * @param maxSize ファイルサイズの判定閾値
   */
  SizeVerifier(qint64 maxSize)
    : maxSize_(maxSize)
  {}

  bool operator ()(const QFileInfo &info) const {
    return info.size() <= maxSize_; // 閾値を上回っていたらfalseを返す
  }
};

} // namespace npp202009::npp::writer::verifier

#endif // NPP202009_NPP_WRITER_VERIFIER_SIZEVERIFIER_HPP
