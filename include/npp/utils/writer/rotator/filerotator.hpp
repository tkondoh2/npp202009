#ifndef NPP202009_NPP_WRITER_ROTATOR_FILEROTATOR_HPP
#define NPP202009_NPP_WRITER_ROTATOR_FILEROTATOR_HPP

#include "../file.hpp"
#include <QDir>

namespace npp202009::npp::writer {

/**
 * @class FileRotator
 * @brief ローテーション処理を付加するwriter::FileBase用オプションクラス(抽象クラス)
 * @tparam Verifier ローテーションするかどうかの判定をする関数オブジェクトクラス
 */
template <class Verifier>
class FileRotator
{
protected:
  QDir dir_; ///< ディレクトリ
  QString currentFilename_; ///< ファイル名
  Verifier verifier_; ///< 判定用関数オブジェクト
  mutable QFile::OpenMode openMode_; ///< オープンモード

public:
  /**
   * @brief コンストラクタ
   * @param dir ディレクトリ
   * @param currentFilename ファイル名
   * @param verifier 判定用関数オブジェクト
   */
  FileRotator(
      QDir &&dir,
      QString &&currentFilename,
      Verifier &&verifier
      )
    : dir_(std::move(dir))
    , currentFilename_(std::move(currentFilename))
    , verifier_(std::move(verifier))
  {}

  /**
   * @brief デストラクタ
   */
  virtual ~FileRotator() = default;

  /**
   * @brief ファイルパスを返す。
   * @return 絶対パス
   */
  QString filePath() const {
    return dir_.absoluteFilePath(currentFilename_);
  }

  /**
   * @brief オープンモードを返す。
   * @return オープンモード
   */
  QFile::OpenMode openMode() const { return openMode_; }

  /**
   * @brief オープン前フック
   */
  void beforeOpen() {
    if (!dir_.exists()) {
      dir_.mkpath("dummy");
      dir_.rmdir("dummy");
    }
    openMode_ = QFile::Text | QFile::WriteOnly;
    if (dir_.exists(currentFilename_)) {
      openMode_ |= QFile::Append;
    }
  }

  /**
   * @brief オープン後フック
   */
  void afterOpen(writer::File *) {}

  /**
   * @brief 書き込み前フック
   * @param pFile ファイルオブジェクトへのポインタ
   */
  void beforeWrite(File *pFile) {
    if (!pFile->isOpen()) {
      pFile->open();
    }
    if (!verifier_(pFile->info())) {
      rotate(pFile);
    }
  }

  /**
   * @brief ローテーション処理をする。
   * @param pFile ファイルオブジェクトへのポインタ
   */
  virtual void rotate(File *pFile) = 0;
};

} // namespace npp202009::npp::writer

#endif // NPP202009_NPP_WRITER_FILEROTATOR_HPP
