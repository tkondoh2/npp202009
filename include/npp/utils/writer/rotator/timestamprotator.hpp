#ifndef NPP202009_NPP_WRITER_ROTATOR_TIMESTAMPROTATOR_HPP
#define NPP202009_NPP_WRITER_ROTATOR_TIMESTAMPROTATOR_HPP

#include "./filerotator.hpp"
#include <QDateTime>

namespace npp202009::npp::writer::rotator {

/**
 * @class TimestampRotator
 * @brief タイムスタンプでローテーションするオプションクラス
 * @tparam Verifier ローテーションするかどうかの判定をする関数オブジェクトクラス
 */
template <class Verifier>
class TimestampRotator
    : public FileRotator<Verifier>
{
  QString rotateFilename_; ///< ローテーション用ファイル名テンプレート
  QString timestampFormat_; ///< タイムスタンプ用フォーマット(QDateTime::toString準拠)

public:
  /**
   * @brief コンストラクタ
   * @param dir ディレクトリ
   * @param currentFilename ファイル名
   * @param verifier 判定用関数オブジェクト
   * @param rotateFilename ローテーション用ファイル名テンプレート
   * @param timestampFormat タイムスタンプ用フォーマット(QDateTime::toString準拠)
   */
  TimestampRotator(
      QDir &&dir,
      QString &&currentFilename,
      Verifier &&verifier,
      QString &&rotateFilename,
      QString &&timestampFormat
      )
    : FileRotator<Verifier>(
      std::move(dir),
      std::move(currentFilename),
      std::move(verifier)
      )
    , rotateFilename_(std::move(rotateFilename))
    , timestampFormat_(std::move(timestampFormat))
  {}

  /**
   * @brief デストラクタ
   */
  virtual ~TimestampRotator() = default;

  /**
   * @brief ローテーション処理をする。
   * @param pFile ファイルオブジェクトへのポインタ
   */
  virtual void rotate(File *pFile) override {
    pFile->close();
    QFile file(this->filePath());
    if (file.exists()) {
//      QMutexLocker lock(pFile->fileMutex());
      QDateTime now = QDateTime::currentDateTime();
      QString newFileName = rotateFilename_.arg(now.toString(timestampFormat_));
      file.rename(this->dir_.absoluteFilePath(newFileName));
    }
    pFile->open();
  }

};

} // namespace npp202009::npp::writer::rotator

#endif // NPP202009_NPP_WRITER_ROTATOR_TIMESTAMPROTATOR_HPP
