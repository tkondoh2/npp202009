#ifndef NPP202009_NPP_WRITER_ROTATOR_FILESCOUNTROTATOR_HPP
#define NPP202009_NPP_WRITER_ROTATOR_FILESCOUNTROTATOR_HPP

#include "./filerotator.hpp"
#include "../../utils.hpp"
#include <QDateTime>

namespace npp202009::npp::writer::rotator {

/**
 * @class FilesCountRotator
 * @brief ファイル数でローテーションするオプションクラス
 * @tparam Verifier ローテーションするかどうかの判定をする関数オブジェクトクラス
 */
template <class Verifier>
class FilesCountRotator
    : public FileRotator<Verifier>
{
  int maxFiles_; ///< 最大ファイル数

  /**
   * @struct FileCompare
   * @brief ファイル比較用構造体
   */
  struct FileCompare
  {
    qint64 timestamp_; ///< タイムスタンプ
    QString filePath_; ///< ファイルパス
  };

public:
  /**
   * @brief コンストラクタ
   * @param dir ディレクトリ
   * @param currentFilename ファイル名
   * @param verifier 判定用関数オブジェクト
   * @param maxFiles 最大ファイル数
   */
  FilesCountRotator(
      QDir &&dir,
      QString &&currentFilename,
      Verifier &&verifier,
      int maxFiles
      )
  : FileRotator<Verifier>(
      std::move(dir),
      std::move(currentFilename),
      std::move(verifier)
      )
  , maxFiles_(maxFiles)
  {}

  /**
   * デストラクタ
   */
  virtual ~FilesCountRotator() = default;

  /**
   * @brief ローテーション先となる次のパスを取得する。
   * @return 次のパス
   */
  QString pathToNext() const {

    QFileInfo info(this->filePath());
    QString base = info.completeBaseName();

    QString ext = info.suffix();
    QVector<FileCompare> v;
    QString rotatePath;
    for (int i = 0; i < maxFiles_; ++i) {
      QString path = this->dir_.absoluteFilePath(
            QString("%1.%2.%3").arg(base).arg(i + 1).arg(ext)
            );
      QFileInfo info(path);
      if (info.exists()) {
        FileCompare fc {
          info.lastModified().currentMSecsSinceEpoch(),
          path
        };
        v.append(std::move(fc));
      }
      else {
        rotatePath = path;
        break;
      }
    }
    if (rotatePath.isEmpty()) {
      qint64 ts = v.at(0).timestamp_;
      int index = 0;
      for (int i = 1; i < maxFiles_; ++i) {
        if (ts > v.at(i).timestamp_) {
          ts = v.at(i).timestamp_;
          index = i;
        }
      }
      rotatePath = v.at(index).filePath_;
    }
    return rotatePath;
  }

  /**
   * @brief ローテーション処理をする。
   * @param pFile ファイルオブジェクトへのポインタ
   */
  virtual void rotate(File *pFile) override {
    pFile->close();
    {
//      QMutexLocker lock(pFile->fileMutex());
      QString rotatePath = pathToNext();
      QFile rotateFile(rotatePath);
      if (rotateFile.exists()) {
        rotateFile.remove();
      }
      QFile file(this->filePath());
      file.rename(rotatePath);
      // ここでミューテックスのロックを外す
    }
    pFile->open();
  }

};

} // namespace npp202009::npp::writer::rotator

#endif // NPP202009_NPP_WRITER_ROTATOR_FILESCOUNTROTATOR_HPP
