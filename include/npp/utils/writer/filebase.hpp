#ifndef NPP202009_NPP_WRITER_FILEBASE_HPP
#define NPP202009_NPP_WRITER_FILEBASE_HPP

#include "./file.hpp"

#include <QTextStream>
#include <QMutexLocker>
#include <QReadWriteLock>
#include <QWriteLocker>
#include "../utils.hpp"

namespace npp202009::npp::writer {

/**
 * @class FileBase
 * @brief 書き込みファイル用ベースクラス
 * @tparam Options オプションクラス
 */
template <class Options>
class FileBase
    : public File
{
  Options options_; ///< オプションオブジェクト
  QScopedPointer<QFile> pFile_; ///< ファイルオブジェクト
  QScopedPointer<QTextStream> pStream_; ///< ストリームオブジェクト
//  QMutex *pFileMutex_; ///< ファイル操作用ミューテックスオブジェクト
//  QReadWriteLock *pReadWriteMutex_; ///< 読み書き用ミューテックスオブジェクト
//  mutable QMutex fileMutex_; ///< ファイル操作用ミューテックスオブジェクト
//  mutable QReadWriteLock readWriteMutex_; ///< 読み書き用ミューテックスオブジェクト

  /**
   * @brief コピーコンストラクタは無効にする。
   */
  FileBase(const FileBase<Options> &) = delete;

public:
  /**
   * @brief コンストラクタ
   * @param options オプションオブジェクトへの右辺値参照
   */
  FileBase(
      Options &&options
//      ,
//      QMutex *pFileMutex,
//      QReadWriteLock *pReadWriteMutex
      )
    : options_(std::move(options))
    , pFile_()
    , pStream_()
//    , pFileMutex_(pFileMutex)
//    , pReadWriteMutex_(pReadWriteMutex)
  {}

  /**
   * @brief ムーブコンストラクタ
   * @param obj ムーブ元オブジェクト
   */
//  FileBase(FileBase<Options> &&obj) = delete;
  FileBase(FileBase<Options> &&obj)
    : options_(std::move(obj.options_))
    , pFile_()
    , pStream_()
//    , pFileMutex_(obj.pFileMutex_)
//    , pReadWriteMutex_(obj.pReadWriteMutex_)
  {}

  /**
   * @brief デストラクタ
   */
  virtual ~FileBase() {
    close();
  }

  /**
   * @brief ファイル操作用ミューテックスを取得する。
   * @return ファイル操作用ミューテックスへのポインタ
   */
//  virtual QMutex *fileMutex() override {
//    return pFileMutex_;
//  }

  /**
   * @brief ファイルは開いているか。
   * @return 開いていれば真を返す。
   */
  virtual bool isOpen() const override {
    return pFile_ && pFile_->isOpen();
  }

  /**
   * @brief ファイル情報を取得する。
   * @return ファイル情報(QFileInfo型)
   */
  virtual QFileInfo info() const override {
    return QFileInfo(*pFile_);
  }

  /**
   * @brief ファイルを開く。
   */
  virtual void open() override {
    if (!pFile_) {
//      QMutexLocker lock(pFileMutex_);
      options_.beforeOpen();
      pFile_.reset(new QFile(options_.filePath()));
      if (pFile_->open(options_.openMode())) {
        pStream_.reset(new QTextStream(pFile_.get()));
        options_.afterOpen(this);
      }
    }
  }

  /**
   * @brief ファイルを閉じる。
   */
  virtual void close() override {
    if (pFile_) {
      if (pStream_) {
//        QWriteLocker lock(pReadWriteMutex_);
//        pStream_->flush();
        pStream_.reset();
      }
//      QMutexLocker lock(pFileMutex_);
      pFile_->close();
      pFile_.reset();
    }
  }

  /**
   * @brief コーデックを設定する。
   * @param pCodec コーデックへのポインタ
   */
  virtual void setCodec(QTextCodec* pCodec) override {
    pStream_->setCodec(pCodec);
  }

  /**
   * @brief ファイルにテキストを書き込む。(より高機能な処理を推奨)
   *   _writeを呼び出す前に、options_.beforeWriteを呼びだしている。
   * @param text 書き込むテキスト
   * @param endl 改行する場合は真
   * @param flush 即時フラッシュする場合は真
   */
  virtual void write(QString &&text, bool endl) override {
    options_.beforeWrite(this);
    _write(std::move(text), endl);
  }
//  virtual void write(QString &&text, bool endl, bool flush = true) override {
//    options_.beforeWrite(this);
//    _write(std::move(text), endl, flush);
//  }

  /**
   * @brief ファイルにテキストを書き込む。(より低レベルな処理を推奨)
   * @param text 書き込むテキスト
   * @param endl 改行する場合は真
   * @param flush 即時フラッシュする場合は真
   */
  virtual void _write(QString &&text, bool endl) override {
//    QWriteLocker lock(pReadWriteMutex_);
    if (!text.isNull()) {
      (*pStream_) << text;
    }
    if (endl) {
      (*pStream_) << '\n';
    }
  }
//  virtual void _write(QString &&text, bool endl, bool flush = true) override {
//    QWriteLocker lock(pReadWriteMutex_);
//    if (!text.isNull()) {
//      (*pStream_) << text;
//    }
//    if (endl) {
//      (*pStream_) << '\n';
//    }
//    if (flush) {
//      pStream_->flush();
//    }
//  }
  virtual void flush() override {
    pStream_->flush();
  }
};

} // namespace npp202009::npp::writer

#endif // NPP202009_NPP_WRITER_FILEBASE_HPP
