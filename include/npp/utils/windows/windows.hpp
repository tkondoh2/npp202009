#ifndef NPP202009_NPP_WINDOWS_WINDOWS_HPP
#define NPP202009_NPP_WINDOWS_WINDOWS_HPP

#include <QString>
#include <QScopedArrayPointer>

#ifdef NT

#include <Windows.h> // Windows

namespace npp202009::npp::windows {

struct Ansi {
  using char_type = char;
  using string = std::string;
  static long getShortPathNameFunc(
      const char *pSrc,
      char *pDst,
      long size
      ) {
    return GetShortPathNameA(pSrc, pDst, size);
  }
  static std::string fromQString(const QString &qs) {
    return qs.toStdString();
  }
  static QString toQString(const std::string &ss) {
    return QString::fromStdString(ss);
  }
};

struct WChar {
  using char_type = wchar_t;
  using string = std::wstring;
  static long getShortPathNameFunc(
      const wchar_t *pSrc,
      wchar_t *pDst,
      long size
      ) {
    return GetShortPathNameW(pSrc, pDst, size);
  }
  static std::wstring fromQString(const QString &qs) {
    return qs.toStdWString();
  }
  static QString toQString(const std::wstring &ss) {
    return QString::fromStdWString(ss);
  }
};

template <class T>
QString getShortPathName(const QString &longPath) {

  auto s = T::fromQString(longPath);
  long length = T::getShortPathNameFunc(s.data(), nullptr, 0);
  if (length > 0) {
    QScopedArrayPointer<T::char_type> buffer(new T::char_type[length]);
    length = T::getShortPathNameFunc(s.data(), buffer.data(), length);
    if (length > 0) {
      return T::toQString(T::string(buffer.data(), length));
    }
  }
  return QString();
}

} // namespace npp202009::npp::windows

#endif

#endif // NPP202009_NPP_WINDOWS_WINDOWS_HPP
