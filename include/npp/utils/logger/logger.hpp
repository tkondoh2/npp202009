#ifndef NPP202009_NPP_LOGGER_LOGGER_HPP
#define NPP202009_NPP_LOGGER_LOGGER_HPP

#include <QMap>
#include <QString>
#include <QDateTime>
#include <sstream>
#include <thread>

namespace npp202009::npp::logger {

enum class LevelEnum: int
{
  Trace = 0,
  Debug,
  Info,
  Warning,
  Error,
  Critical,
  Off,
  MAX_LEVEL
};

class Level
{
public:
  static const QMap<LevelEnum, QString> names() {
    return {
      { LevelEnum::Trace, "Trace" },
      { LevelEnum::Debug, "Debug" },
      { LevelEnum::Info, "Info" },
      { LevelEnum::Warning, "Warning" },
      { LevelEnum::Error, "Error" },
      { LevelEnum::Critical, "Critical" },
      { LevelEnum::Off, "Off" },
    };
  }
};

class DefaultFormatter
{
  QString format_;

public:
  DefaultFormatter(QString &&format)
    : format_(std::move(format))
  {
    if (format_.isEmpty()) {
      format_ = "[${time}] [${thread}] [${id}] [${level}] ${message}";
    }
  }

  QString getThreadId() {
    std::ostringstream str;
    str << std::this_thread::get_id();
    return QString::fromStdString(str.str());
  }

  QString format(const QString &id, const QString &level, QString &&msg) {
    auto s = format_;
    auto time = QDateTime::currentDateTime()
        .toString("yyyy-MM-dd hh:mm:ss.zzz");
    auto thread = getThreadId();
    s.replace("${time}", time)
        .replace("${thread}", thread)
        .replace("${id}", id)
        .replace("${level}", level)
        .replace("${message}", msg);
    return s;
  }
};

template <class Writer, class Formatter = DefaultFormatter>
class Logger
{
  QString id_;
  Writer writer_;
  LevelEnum threshold_;
  Formatter formatter_;

public:
  Logger(QString &&id, Writer &&writer, LevelEnum threshold, QString &&format = QString())
    : id_(id)
    , writer_(std::move(writer))
    , threshold_(threshold)
    , formatter_(std::move(format))
  {}

  virtual ~Logger() = default;

  const QString &id() const { return id_; }

  void trace(QString &&msg) { output(LevelEnum::Trace, std::move(msg)); }
  void debug(QString &&msg) { output(LevelEnum::Debug, std::move(msg)); }
  void info(QString &&msg) { output(LevelEnum::Info, std::move(msg)); }
  void warning(QString &&msg) { output(LevelEnum::Warning, std::move(msg)); }
  void error(QString &&msg) { output(LevelEnum::Error, std::move(msg)); }
  void critical(QString &&msg) { output(LevelEnum::Critical, std::move(msg)); }

protected:
  virtual void output(LevelEnum level, QString &&msg) {
    if (level >= threshold_) {
      writer_.write(
            formatter_.format(id_, Level::names().value(level), std::move(msg)),
            true
            );
      writer_.flush();
    }
  }
};

} // namespace npp202009::npp::logger

#endif // NPP202009_NPP_LOGGER_LOGGER_HPP
