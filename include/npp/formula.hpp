#ifndef NPP202009_NPP_FORMULA_HPP
#define NPP202009_NPP_FORMULA_HPP

#include "./handle/lockable.hpp"
#include "./type/lmbcs.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfsearc.h>

#ifdef NT
#pragma pack(pop)
#endif

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp {

struct FormulaHandleTraits
{
  using handle_type = FORMULAHANDLE;
  static bool is_null(FORMULAHANDLE h) { return h == NULLHANDLE; }
  static void deleter(FORMULAHANDLE h) { OSMemFree(h); }
};

class Formula;
using FormulaPtr = QSharedPointer<Formula>;
using FormulaHandle = handle::Lockable<FormulaHandleTraits>;

class Formula
    : public FormulaHandle
{
public:

  class CompileError
      : public Status
  {
    WORD line_;
    WORD col_;
    WORD offset_;
    WORD length_;

  public:
    CompileError()
      : Status()
      , line_(0)
      , col_(0)
      , offset_(0)
      , length_(0)
    {}

    CompileError(STATUS status, WORD line, WORD col, WORD offset, WORD length)
      : Status(status)
      , line_(line)
      , col_(col)
      , offset_(offset)
      , length_(length)
    {}

    WORD line() const { return line_; }

    WORD col() const { return col_; }

    WORD offset() const { return offset_; }

    WORD length() const { return length_; }
  };

  Formula() : FormulaHandle(NULLHANDLE) {}

  Formula(FORMULAHANDLE &&handle) : FormulaHandle(std::move(handle)) {}

  operator FORMULAHANDLE() const noexcept { return handle(); }

  static observable<FormulaPtr> compile(
      const Lmbcs &statement,
      const Lmbcs &column = ""
      ) noexcept {
    return observable<>::create<FormulaPtr>(
          [=](subscriber<FormulaPtr> s
          ) {
      FORMULAHANDLE handle = NULLHANDLE;
      STATUS compileStatus = NOERROR;
      WORD formulaLen = 0, line = 0, col = 0, offset = 0, length = 0;
      Status status = NSFFormulaCompile(
            column.isEmpty() ? nullptr : const_cast<char*>(column.constData()),
            static_cast<WORD>(column.size()),
            const_cast<char*>(statement.constData()),
            static_cast<WORD>(statement.size()),
            &handle,
            &formulaLen,
            &compileStatus,
            &line,
            &col,
            &offset,
            &length
            );
      if (status.failure()) {
        s.on_error(std::make_exception_ptr(status));
        return;
      }
      CompileError compileError { compileStatus, line, col, offset, length };
      if (compileError.failure()) {
        s.on_error(std::make_exception_ptr(compileError));
        return;
      }
      s.on_next(handle::make_ptr<Formula>(std::move(handle)));
      s.on_completed();
    });
  }

  static FormulaPtr compile_x(
      const Lmbcs &statement,
      const Lmbcs &column = ""
      ) noexcept(false) {
    FORMULAHANDLE handle = NULLHANDLE;
    STATUS compileStatus = NOERROR;
    WORD formulaLen = 0, line = 0, col = 0, offset = 0, length = 0;
    Status status = NSFFormulaCompile(
          column.isEmpty() ? nullptr : const_cast<char*>(column.constData()),
          static_cast<WORD>(column.size()),
          const_cast<char*>(statement.constData()),
          static_cast<WORD>(statement.size()),
          &handle,
          &formulaLen,
          &compileStatus,
          &line,
          &col,
          &offset,
          &length
          );
    if (status.failure()) { throw status; }
    CompileError compileError { compileStatus, line, col, offset, length };
    if (compileError.failure()) { throw compileError; }
    return handle::make_ptr<Formula>(std::move(handle));
  }
};

} // namespace npp202009::npp

#endif // NPP202009_NPP_FORMULA_HPP
