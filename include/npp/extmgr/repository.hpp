#ifndef NPP202009_NPP_EXTMGR_REPOSITORY_HPP
#define NPP202009_NPP_EXTMGR_REPOSITORY_HPP

#include "extensionmanager.hpp"
#include <QVector>

namespace npp202009::npp::extmgr {

/**
 * @brief エクステンションマネージャハンドルのリポジトリクラス
 */
class Repository
    : public QVector<ExtensionManagerPtr>
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Repository() : QVector<ExtensionManagerPtr>() {}

  /**
   * @brief 仮想デストラクタ
   */
  virtual ~Repository() {
    clear();
  }

  /**
   * @brief エクステンションマネージャ登録
   * @tparam T 特性クラス(T::enabled()が必要)
   *   例えば、HTTPタスク上で有効にしたいフックであれば、T::enabled()で判定する。
   * @param eid フックする関数のID
   * @param regFlags 登録フラグ(EM_REG_BEFORE,EM_REG_AFTER)
   * @param handler コールバック関数ポインタ
   * @param RecursionID 再帰ID
   * @return 有効にした登録であれば真、無効にした登録であれば偽のオブザーバブル
   */
  template <class T>
  observable<bool> install(
      EID eid,
      DWORD regFlags,
      EMHANDLER handler,
      WORD RecursionID
      ) {
    return observable<>::create<bool>(
          [this, eid, regFlags, handler, RecursionID]
          (subscriber<bool> s) {
      if (T::enabled()) {
        HEMREGISTRATION handle = NULLHANDLE;
        STATUS status = EMRegister(
              eid,
              regFlags,
              handler,
              RecursionID,
              &handle
              );
        if (ERR(status) != NOERROR) {
          s.on_error(
                std::make_exception_ptr(
                  std::runtime_error("Could not register extension handler.")
                  )
                );
          return;
        }
        append(handle::make_ptr<ExtensionManager>(std::move(handle)));
        s.on_next(true);
      }
      else {
        s.on_next(false);
      }
      s.on_completed();
    });
  }
};

/**
 * @brief 再帰IDの作成
 * @return 再帰IDのオブザーバブル
 */
inline observable<WORD> createRecursionID() {
  return observable<>::create<WORD>([](subscriber<WORD> s) {
    WORD RecursionID = 0;
    STATUS status = EMCreateRecursionID(&RecursionID);
    if (ERR(status) != NOERROR) {
      s.on_error(
            std::make_exception_ptr(
              std::runtime_error("Can't create recursion ID.")
              )
            );
      return;
    }
    s.on_next(RecursionID);
    s.on_completed();
  });
}

inline WORD createRecursionID_x() noexcept(false) {
  WORD RecursionID = 0;
  STATUS status = EMCreateRecursionID(&RecursionID);
  if (ERR(status) != NOERROR) {
    throw std::runtime_error("Can't create recursion ID.");
  }
  return RecursionID;
}

/**
 * @brief エクステンションマネージャハンドルのリポジトリクラス
 */
class Repository2
    : public QVector<ExtensionManagerPtr>
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Repository2()
    : QVector<ExtensionManagerPtr>()
  {}

  /**
   * @brief 仮想デストラクタ
   */
  virtual ~Repository2() {
    clear();
  }

  observable<WORD> install(
      WORD recursionId,
      EID eid,
      DWORD regFlags,
      EMHANDLER handler
      );
};

/**
 * @brief エクステンションマネージャ登録
 * @tparam T 特性クラス(T::enabled()が必要)
 *   例えば、HTTPタスク上で有効にしたいフックであれば、T::enabled()で判定する。
 * @param handler コールバック関数ポインタ
 * @param RecursionID 再帰ID
 * @return 有効にした登録であれば真、無効にした登録であれば偽のオブザーバブル
 */
inline observable<WORD> Repository2::install(
    WORD recursionId,
    EID eid,
    DWORD regFlags,
    EMHANDLER handler
    ) {
  return observable<>::create<ExtensionManagerPtr>(
        [recursionId, eid, regFlags, handler](
        subscriber<ExtensionManagerPtr> s
        ) {
    HEMREGISTRATION handle = NULLHANDLE;
    STATUS status = EMRegister(
          eid,
          regFlags,
          handler,
          recursionId,
          &handle
          );
    if (ERR(status) != NOERROR) {
      s.on_next(handle::make_ptr<ExtensionManager>(std::move(handle)));
      s.on_completed();
    } else {
      s.on_error(
            std::make_exception_ptr(
              std::runtime_error("Could not register extension handler.")
              )
            );
    }
  }).map([this, recursionId](ExtensionManagerPtr emPtr) {
    this->append(emPtr);
    return recursionId;
  });
}

} // namespace npp202009::npp::extmgr

#endif // NPP202009_NPP_EXTMGR_REPOSITORY_HPP
