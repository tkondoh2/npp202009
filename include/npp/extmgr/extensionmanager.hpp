#ifndef NPP202009_NPP_EXTMGR_REGISTER_HPP
#define NPP202009_NPP_EXTMGR_REGISTER_HPP

#include "../handle/base.hpp"
#include <QList>
#include <rxcpp/rx.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <extmgr.h>
#include <nsferr.h>
#include <bsafeerr.h>

#ifdef NT
#pragma pack(pop)
#endif

using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp::extmgr {

/**
 * @brief エクステンションマネージャ用ハンドル特性
 */
struct ExtensionManagerHandleTraits
{
  /**
   * ハンドルの型
   */
  using handle_type = HEMREGISTRATION;

  /**
   * @brief ヌル判定
   * @param h ハンドル
   * @return ヌルなら真
   */
  static bool is_null(HEMREGISTRATION h) { return h == NULLHANDLE; }

  /**
   * @brief ハンドル解放メソッド
   * @param h ハンドル
   */
#ifdef NT
  static void deleter(HEMREGISTRATION h) {
    EMDeregister(h);
  }
#else
  static void deleter(HEMREGISTRATION) {
    // Linuxではハンドル解放のタイミングが合わず、メッセージが表示されてしまう。
  }
#endif
};

class ExtensionManager;
using ExtensionManagerPtr = QSharedPointer<ExtensionManager>;
using ExtensionManagerHandle = handle::Base<ExtensionManagerHandleTraits>;

/**
 * @brief エクステンションマネージャハンドルのラッパークラス
 */
class ExtensionManager
    : public ExtensionManagerHandle
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  explicit ExtensionManager()
    : ExtensionManagerHandle(NULLHANDLE)
  {}

  /**
   * @brief ムーブコンストラクタ
   * @param handle ハンドル
   */
  explicit ExtensionManager(HEMREGISTRATION &&handle)
    : ExtensionManagerHandle(std::move(handle))
  {}
};

class ExtensionManager2
{
  HEMREGISTRATION handle_;
public:
  ExtensionManager2() : handle_(NULLHANDLE) {}

  virtual ~ExtensionManager2() {
#ifdef NT
    if (handle_ != NULLHANDLE) {
      EMDeregister(handle_);
    }
#else
    // Linuxではハンドル解放のタイミングが合わず、メッセージが表示されてしまう。
#endif
  }

  void install(
      EID eid,
      DWORD regFlags,
      EMHANDLER handler,
      WORD RecursionID
      ) {
    STATUS status = EMRegister(
          eid,
          regFlags,
          handler,
          RecursionID,
          &handle_
          );
    if (ERR(status) != NOERROR) {
      throw std::runtime_error("Could not register extension handler.");
    }
  }
};

} // namespace npp202009::npp::extmgr

#endif // NPP202009_NPP_EXTMGR_REGISTER_HPP
