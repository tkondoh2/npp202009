#ifndef NPP202009_NPP_NUMBERRANGE_HPP
#define NPP202009_NPP_NUMBERRANGE_HPP

#include "./range.hpp"
#include "./number.hpp"
#include "./lmbcslist.hpp"
#include "../utils/utils.hpp"

namespace npp202009::npp {

using NumberRangeBase = Range<NUMBER, NUMBER_PAIR>;

class NumberRange
    : public NumberRangeBase
{
public:
  static const WORD value_type = TYPE_NUMBER_RANGE;

  NumberRange();

  NumberRange(void *ptr);

  NumberRange(NUMBER v);

  observable<LmbcsList> convertToTextList(
      NFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) const;

  static NumberRange fromBytes(const QByteArray &value);

  static std::optional<NumberRange> fromBytes(WORD type, const QByteArray &value);

//  static NumberRange fromItemPtr(void *ptr, DWORD);

//  static NumberRange fromItemPtrEx(WORD type, void *ptr, DWORD len);
};


inline std::optional<Number> Number::fromBytes(WORD type, const QByteArray &value) {
  if (type == NumberRange::value_type) {
    NumberRange range = NumberRange::fromBytes(value);
    return !range.list().isEmpty()
        ? Number(range.list().first())
        : Number();
  }
  return std::nullopt;
}

//inline Number Number::fromItemPtrEx(WORD type, void *ptr, DWORD len) {
//  if (type == NumberRange::value_type) {
//    NumberRange range = NumberRange::fromItemPtr(ptr, len);
//    return !range.list().isEmpty()
//        ? Number(range.list().first())
//        : Number();
//  }
//  return Number();
//}


inline NumberRange::NumberRange()
  : NumberRangeBase()
{}

inline NumberRange::NumberRange(void *ptr)
  : NumberRangeBase(ptr)
{}

inline NumberRange::NumberRange(NUMBER v)
  : NumberRangeBase(v)
{}

inline observable<LmbcsList> NumberRange::convertToTextList(
    NFMT *pTextFormat,
    void *pIntlFormat
    ) const {
  return for_qlist<NUMBER>(list())
  .flat_map([pTextFormat, pIntlFormat](NUMBER number) {
    return Number::convertToText(number, pTextFormat, pIntlFormat);
  })
  .reduce(
        LmbcsList(),
        [](LmbcsList list, Lmbcs item) {
    list.append(item);
    return list;
  });
}

inline NumberRange NumberRange::fromBytes(const QByteArray &value) {
  return NumberRange(reinterpret_cast<void*>(const_cast<char*>(value.constData())));
}

inline std::optional<NumberRange> NumberRange::fromBytes(WORD type, const QByteArray &value) {
  if (type == Number::value_type) {
    Number number = Number::fromBytes(value);
    return NumberRange(number.value());
  }
  return std::nullopt;
}

//inline NumberRange NumberRange::fromItemPtr(void *ptr, DWORD) {
//  return NumberRange(ptr);
//}

//inline NumberRange NumberRange::fromItemPtrEx(WORD type, void *ptr, DWORD len) {
//  if (type == Number::value_type) {
//    Number number = Number::fromItemPtr(ptr, len);
//    return NumberRange(number.value());
//  }
//  return NumberRange();
//}

} // namespace npp202009::npp

#endif // NPP202009_NPP_NUMBERRANGE_HPP
