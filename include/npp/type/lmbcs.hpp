#ifndef NPP202009_NPP_LMBCS_HPP
#define NPP202009_NPP_LMBCS_HPP

#include "../nls/translate.hpp"
#include "../status.hpp"
#include <QString>
#include <optional>
#include <iostream>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfdata.h> // TYPE_TEXT
#include <misc.h> // MAXSPRINTF

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp {

/**
 * @brief LMBCS文字列のラッパー
 */
class Lmbcs
    : public QByteArray
{
public:
  static const WORD value_type = TYPE_TEXT; ///< アイテムデータ型(文字列)

  /**
   * @brief デフォルトコンストラクタ
   */
  Lmbcs() noexcept;

  /**
   * @brief コンストラクタ
   * @param c_str 文字列ポインタ
   * @param size バイトサイズ、デフォルトはヌル終端までのサイズ
   */
  Lmbcs(const char *c_str, int size = -1) noexcept;

  /**
   * @brief コンストラクタ
   * @param size バイトサイズ
   * @param c バイトサイズ分埋める文字
   */
  Lmbcs(int size, char c);

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元オブジェクト
   */
  Lmbcs(const QByteArray &other);

  /**
   * @brief ムーブコピーコンストラクタ
   * @param other コピー元オブジェクト
   */
  Lmbcs(QByteArray &&other);

  /**
   * @brief QByteArrayからLMBCS文字列を作成する。<br>
   * バイト内のヌル文字は改行文字に変換される。
   *
   * @param value QByteArrayデータ
   * @return LMBCS文字列
   */
  static Lmbcs fromBytes(const QByteArray &value);

  /**
   * @brief 別型のQByteArrayからLMBCS文字列ができるか試行する。
   * @param type 文字列以外のデータ型
   * @param value QByteArrayデータ
   * @return LMBCS文字列にできたらそのオプショナル値を返す。
   */
  static std::optional<Lmbcs> fromBytes(WORD type, const QByteArray &value);

  /**
   * @brief プラス演算子
   * @param c 左辺のバイト文字
   * @param rhs 右辺のLMBCS文字列
   * @return 合成文字列
   */
  friend Lmbcs operator +(char c, const Lmbcs &rhs);

  /**
   * @brief プラス演算子
   * @param lhs 左辺のLMBCS文字列
   * @param c 右辺のバイト文字
   * @return 合成文字列
   */
  friend Lmbcs operator +(const Lmbcs &lhs, char c);

  /**
   * @brief プラス演算子
   * @param lhs 左辺のLMBCS文字列
   * @param rhs 右辺のLMBCS文字列
   * @return 合成文字列
   */
  friend Lmbcs operator +(const Lmbcs &lhs, const Lmbcs &rhs);
};

/**
 * @brief QStringからLMBCSに変換する。
 * @param qs QString
 * @return LMBCS
 */
Lmbcs ql(const QString &qs) noexcept;

/**
 * @brief LMBCSからQStringに変換する。
 * @param ls LMBCS
 * @return QString
 */
QString lq(const Lmbcs &ls) noexcept;

/**
 * @brief IDを文字列形式にフォーマットする。
 * @tparam T IDの型
 * @tparam F QByteArray::numberの受け入れ用数値型
 * @param id ID値
 * @return LMBCS文字列
 */
template <typename T, typename F>
Lmbcs formatId(T id);


inline Lmbcs::Lmbcs() noexcept
  : QByteArray()
{}

inline Lmbcs::Lmbcs(const char *c_str, int size) noexcept
  : QByteArray(c_str, size)
{}

inline Lmbcs::Lmbcs(int size, char c)
  : QByteArray(size, c)
{}

inline Lmbcs::Lmbcs(const QByteArray &other)
  : QByteArray(other)
{}

inline Lmbcs::Lmbcs(QByteArray &&other)
  : QByteArray(std::move(other))
{}

inline Lmbcs Lmbcs::fromBytes(const QByteArray &value) {
  Lmbcs s(value);
  s.replace('\0', '\n');
  return s;
}

inline Lmbcs operator +(char c, const Lmbcs &rhs) {
  return Lmbcs(QByteArray(1, c) + QByteArray(rhs));
}

inline Lmbcs operator +(const Lmbcs &lhs, char c) {
  return Lmbcs(QByteArray(lhs) + QByteArray(1, c));
}

inline Lmbcs operator +(const Lmbcs &lhs, const Lmbcs &rhs) {
  return Lmbcs(QByteArray(lhs) + QByteArray(rhs));
}

inline Lmbcs ql(const QString &qs) noexcept {
  // LMBCS化してLmbcsオブジェクトに変換して返す。
  try {
    return Lmbcs(
          nls::unicodeToLmbcs(
            QByteArray::fromRawData(
              reinterpret_cast<const char*>(qs.utf16()),
              qs.size() * sizeof(ushort)
              )));
  }
  catch (nls::Status &status) {
    std::cerr << "\nNLS Error: " << status.what() << std::endl;
  }
  catch (std::exception &ex) {
    std::cerr << "\nException: " << ex.what() << std::endl;
  }
  return Lmbcs();
}

inline QString lq(const Lmbcs &ls) noexcept {
  try {
    auto result = nls::lmbcsToUnicode(ls);
    return QString::fromUtf16(
          reinterpret_cast<const ushort*>(result.constData()),
          result.size() / sizeof(ushort)
          );
  }
  catch (nls::Status &status) {
    std::cerr << "\nNLS Error: " << status.what() << std::endl;
  }
  catch (std::exception &ex) {
    std::cerr << "\nException: " << ex.what() << std::endl;
  }
  return QString();
}

inline const char *Status::what() const noexcept {
  static char buffer[MAXSPRINTF + 1] = "";
  WORD len = OSLoadString(
        NULLHANDLE,
        error(),
        buffer,
        MAXSPRINTF
        );
  buffer[len] = '\0';
  return buffer;
}

inline Lmbcs Status::message() const noexcept {
  return Lmbcs(what());
}

} // namespace npp202009::npp

#endif // NPP202009_NPP_LMBCS_HPP
