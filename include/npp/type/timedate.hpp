#ifndef NPP202009_NPP_TIMEDATE_HPP
#define NPP202009_NPP_TIMEDATE_HPP

#include "./lmbcs.hpp"
#include <QDateTime>
#include <QTimeZone>
#include <rxcpp/rx.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <misc.h> // TFMT,ConvertTIMEDATEToText...
#include <ostime.h> // OSCurrentTIMEDATE...

#ifdef NT
#pragma pack(pop)
#endif

using namespace rxcpp;
using namespace rxcpp::operators;


bool operator ==(const TIMEDATE &lhs, const TIMEDATE &rhs) noexcept;

bool operator !=(const TIMEDATE &lhs, const TIMEDATE &rhs) noexcept;


namespace npp202009::npp {

template <WORD type>
TIMEDATE getConstantTIMEDATE() noexcept;

TIMEDATE currentTIMEDATE() noexcept;

class TimeDate
{
  TIMEDATE value_;

public:
  static const WORD value_type = TYPE_TIME;

  TimeDate();

  TimeDate(const TIMEDATE &value);

  TimeDate(TIMEDATE &&value);

  TIMEDATE value() const;

  observable<Lmbcs> convertToText(
      TFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) const noexcept;

  observable<QDateTime> toDateTime() const;

  static observable<TimeDate> fromDateTime(const QDateTime &dateTime);

  static TimeDate fromBytes(const QByteArray &value);

  static std::optional<TimeDate> fromBytes(WORD type, const QByteArray &value);

//  static TimeDate fromItemPtr(void *ptr, DWORD);

//  static TimeDate fromItemPtrEx(WORD type, void *ptr, DWORD);

  static observable<Lmbcs> convertToText(
      const TIMEDATE &value,
      TFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) noexcept;
};


template <WORD type>
TIMEDATE getConstantTIMEDATE() noexcept {
  TIMEDATE value;
  TimeConstant(type, &value);
  return value;
}

inline TIMEDATE currentTIMEDATE() noexcept {
  TIMEDATE value;
  OSCurrentTIMEDATE(&value);
  return value;
}

inline TimeDate::TimeDate()
  : value_(getConstantTIMEDATE<TIMEDATE_MINIMUM>())
{}

inline TimeDate::TimeDate(const TIMEDATE &value)
  : value_(value)
{}

inline TimeDate::TimeDate(TIMEDATE &&value)
  : value_(std::move(value))
{}

inline TIMEDATE TimeDate::value() const {
  return value_;
}

inline observable<Lmbcs> TimeDate::convertToText(
    TFMT *pTextFormat,
    void *pIntlFormat
    ) const noexcept {
  return convertToText(value_, pTextFormat, pIntlFormat);
}

inline observable<QDateTime> TimeDate::toDateTime() const {
  return observable<>::create<QDateTime>([this](subscriber<QDateTime> s) {
    TIME time;
    time.GM = value_;
    if (TimeGMToLocal(&time) != FALSE) { // 成功時FALSE
      s.on_error(std::make_exception_ptr(std::runtime_error("TimeGMToLocal failed.")));
      return;
    }
    QDate qDate(time.year, time.month, time.day);
    QTime qTime(time.hour, time.minute, time.second, time.hundredth * 10);
    s.on_next(QDateTime(
                qDate,
                qTime,
                Qt::OffsetFromUTC,
                (time.zone - time.dst) * -3600
                ));
    s.on_completed();
  });
}

inline observable<TimeDate> TimeDate::fromDateTime(const QDateTime &dateTime) {
  return observable<>::create<TimeDate>([dateTime](subscriber<TimeDate> s) {
    QDate qDate = dateTime.date();
    QTime qTime = dateTime.time();
    TIME time;
    time.year = qDate.year();
    time.month = qDate.month();
    time.day = qDate.day();
//    time.weekday = qDate.dayOfWeek() == 7 ? 1 : (qDate.dayOfWeek() + 1);
    time.hour = qTime.hour();
    time.minute = qTime.minute();
    time.second = qTime.second();
    time.hundredth = qTime.msec() / 10;
    QTimeZone timeZone = dateTime.timeZone();
    time.dst = dateTime.isDaylightTime() ? 1 : 0;
    time.zone = timeZone.offsetFromUtc(dateTime) / -3600 + time.dst;
    if (TimeLocalToGM(&time) != FALSE) { // 成功時FALSE
      s.on_error(std::make_exception_ptr(std::runtime_error("TimeLocalToGM failed.")));
      return;
    }
    s.on_next(TimeDate(time.GM));
    s.on_completed();
  });
}

inline TimeDate TimeDate::fromBytes(const QByteArray &value) {
  return TimeDate(*reinterpret_cast<TIMEDATE*>(const_cast<char*>(value.constData())));
}

//inline TimeDate TimeDate::fromItemPtr(void *ptr, DWORD) {
//  return TimeDate(*reinterpret_cast<TIMEDATE*>(ptr));
//}

inline observable<Lmbcs> TimeDate::convertToText(
    const TIMEDATE &value,
    TFMT *pTextFormat,
    void *pIntlFormat
    ) noexcept {
  return observable<>::create<Lmbcs>([value, pTextFormat, pIntlFormat](subscriber<Lmbcs> s) {
    char buffer[MAXALPHATIMEDATE] = "";
    WORD len = 0;
    TIMEDATE org = value;
    Status status = ConvertTIMEDATEToText(
          pIntlFormat,
          pTextFormat,
          &org,
          buffer,
          MAXALPHATIMEDATE,
          &len
          );
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(Lmbcs(buffer, len));
    s.on_completed();
  });
}

} // namespace npp202009::npp

inline bool operator ==(const TIMEDATE &lhs, const TIMEDATE &rhs) noexcept
{
  return TimeDateEqual(&lhs, &rhs);
}

inline bool operator !=(const TIMEDATE &lhs, const TIMEDATE &rhs) noexcept
{
  return !operator ==(lhs, rhs);
}

#endif // NPP202009_NPP_TIMEDATE_HPP
