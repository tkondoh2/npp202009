#ifndef NPP202009_NPP_TIMEDATERANGE_HPP
#define NPP202009_NPP_TIMEDATERANGE_HPP

#include "./range.hpp"
#include "./timedate.hpp"
#include "../utils/utils.hpp"

namespace npp202009::npp {

using TimeDateRangeBase = Range<TIMEDATE, TIMEDATE_PAIR>;

class TimeDateRange
    : public TimeDateRangeBase
{
public:
  static const WORD value_type = TYPE_TIME_RANGE;

  TimeDateRange();

  TimeDateRange(void *ptr);

  TimeDateRange(TIMEDATE v);

  observable<LmbcsList> convertToTextList(
      TFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) const;

  static observable<Lmbcs> convertPairToText(
      const TIMEDATE_PAIR &pair,
      TFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      );

  static TimeDateRange fromBytes(const QByteArray &value);

  static std::optional<TimeDateRange> fromBytes(WORD type, const QByteArray &value);

//  static TimeDateRange fromItemPtr(void *ptr, DWORD);

//  static TimeDateRange fromItemPtrEx(WORD type, void *ptr, DWORD len);
};


inline std::optional<TimeDate> TimeDate::fromBytes(WORD type, const QByteArray &value) {
  if (type == TimeDateRange::value_type) {
    TimeDateRange range = TimeDateRange::fromBytes(value);
    return !range.list().isEmpty()
        ? TimeDate(range.list().first())
        : TimeDate();
  }
  return std::nullopt;
}

//inline TimeDate TimeDate::fromItemPtrEx(WORD type, void *ptr, DWORD len) {
//  if (type == TimeDateRange::value_type) {
//    TimeDateRange range = TimeDateRange::fromItemPtr(ptr, len);
//    return !range.list().isEmpty()
//        ? TimeDate(range.list().first())
//        : TimeDate();
//  }
//  return TimeDate();
//}

inline TimeDateRange::TimeDateRange()
  : TimeDateRangeBase()
{}

inline TimeDateRange::TimeDateRange(void *ptr)
  : TimeDateRangeBase(ptr)
{}

inline TimeDateRange::TimeDateRange(TIMEDATE v)
  : TimeDateRangeBase(v)
{}

inline TimeDateRange TimeDateRange::fromBytes(const QByteArray &value) {
  return TimeDateRange(reinterpret_cast<void*>(const_cast<char*>(value.constData())));
}

inline std::optional<TimeDateRange> TimeDateRange::fromBytes(WORD type, const QByteArray &value) {
  if (type == TimeDate::value_type) {
    TimeDate timeDate = TimeDate::fromBytes(value);
    return TimeDateRange(timeDate.value());
  }
  return std::nullopt;
}

//inline TimeDateRange TimeDateRange::fromItemPtr(void *ptr, DWORD) {
//  return TimeDateRange(ptr);
//}

//inline TimeDateRange TimeDateRange::fromItemPtrEx(WORD type, void *ptr, DWORD len) {
//  if (type == TimeDate::value_type) {
//    TimeDate timeDate = TimeDate::fromItemPtr(ptr, len);
//    return TimeDateRange(timeDate.value());
//  }
//  return TimeDateRange();
//}

inline observable<LmbcsList> TimeDateRange::convertToTextList(
    TFMT *pTextFormat,
    void *pIntlFormat
    ) const {

  observable<Lmbcs> singleList = for_qlist<TIMEDATE>(list())
  .flat_map([pTextFormat, pIntlFormat](TIMEDATE td) {
    return TimeDate::convertToText(td, pTextFormat, pIntlFormat);
  });

  observable<Lmbcs> pairList = for_qlist<TIMEDATE_PAIR>(pairs())
      .flat_map([pTextFormat, pIntlFormat](TIMEDATE_PAIR tdp) {
    return convertPairToText(tdp, pTextFormat, pIntlFormat);
  });

  return singleList.concat(pairList)
  .reduce(LmbcsList(), [](LmbcsList list, Lmbcs item) {
    list.append(item);
    return list;
  });
}

inline observable<Lmbcs> TimeDateRange::convertPairToText(
    const TIMEDATE_PAIR &pair,
    TFMT *pTextFormat,
    void *pIntlFormat
    ) {
  return observable<>::create<Lmbcs>(
        [pair, pTextFormat, pIntlFormat](subscriber<Lmbcs> &s) {
    char buffer[MAXALPHATIMEDATEPAIR] = "";
    WORD len = 0;
    TIMEDATE_PAIR org = pair;
    Status status = ConvertTIMEDATEPAIRToText(
          pIntlFormat,
          pTextFormat,
          &org,
          buffer,
          MAXALPHATIMEDATEPAIR,
          &len
          );
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(Lmbcs(buffer, len));
    s.on_completed();
  });
}

} // namespace npp202009::npp

#endif // NPP202009_NPP_TIMEDATERANGE_HPP
