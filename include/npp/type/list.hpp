#ifndef NPP202009_NPP_LIST_HPP
#define NPP202009_NPP_LIST_HPP

#include <QList>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfdata.h>

#ifdef NT
#pragma pack(pop)
#endif

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp {

template <class Single>
class List
    : public QList<Single>
{
public:
  List() : QList<Single>() {}

  List(void *ptr)
    : QList<Single>()
  {
    LIST *pList = reinterpret_cast<LIST*>(ptr);
    USHORT *pLength = reinterpret_cast<USHORT*>(pList + 1);
    char *pStr = reinterpret_cast<char*>(pLength + pList->ListEntries);
    for (USHORT i = 0; i < pList->ListEntries; ++i) {
      this->append(Single(pStr, *pLength));
      pStr += *pLength++;
    }
  }
};

} // namespace npp202009::npp

#endif // NPP202009_NPP_LIST_HPP
