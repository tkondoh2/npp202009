#ifndef NPP202009_NPP_LMBCSLIST_HPP
#define NPP202009_NPP_LMBCSLIST_HPP

#include "./lmbcs.hpp"
#include "./list.hpp"

namespace npp202009::npp {

class LmbcsList
    : public List<Lmbcs>
{
public:
  static const WORD value_type = TYPE_TEXT_LIST;

  LmbcsList();

  LmbcsList(void *ptr);

  Lmbcs join() const;

  Lmbcs join(const Lmbcs &sep) const;

  Lmbcs join(char sep) const;

  QStringList toQStringList() const {
    QStringList result;
    foreach (auto item, *this) {
      result.append(lq(item));
    }
    return result;
  }

  static LmbcsList fromBytes(const QByteArray &value);

  static std::optional<LmbcsList> fromBytes(WORD type, const QByteArray &value);

//  static LmbcsList fromItemPtr(void *ptr, DWORD);

//  static LmbcsList fromItemPtrEx(WORD type, void *ptr, DWORD len);
};


inline std::optional<Lmbcs> Lmbcs::fromBytes(WORD type, const QByteArray &value) {
  if (type == LmbcsList::value_type) {
    LmbcsList list = LmbcsList::fromBytes(value);
    return !list.isEmpty() ? list.first() : Lmbcs();
  }
  return std::nullopt;
}

//inline Lmbcs Lmbcs::fromItemPtrEx(WORD type, void *ptr, DWORD len) {
//  if (type == LmbcsList::value_type) {
//    LmbcsList list = LmbcsList::fromItemPtr(ptr, len);
//    return !list.isEmpty() ? list.first() : Lmbcs();
//  }
//  return Lmbcs();
//}

inline LmbcsList::LmbcsList() : List<Lmbcs>() {}

inline LmbcsList::LmbcsList(void *ptr) : List<Lmbcs>(ptr) {}

inline Lmbcs LmbcsList::join() const {
  Lmbcs result;
  for (int i = 0; i < size(); ++i) {
    result += at(i);
  }
  return result;
}

inline Lmbcs LmbcsList::join(const Lmbcs &sep) const {
  Lmbcs result = (size() > 0) ? at(0) : Lmbcs();
  for (int i = 1; i < size(); ++i) {
    result += sep + at(i);
  }
  return result;
}

inline Lmbcs LmbcsList::join(char sep) const {
  Lmbcs result = (size() > 0) ? at(0) : Lmbcs();
  for (int i = 1; i < size(); ++i) {
    result += sep + at(i);
  }
  return result;
}

inline LmbcsList LmbcsList::fromBytes(const QByteArray &value) {
  return LmbcsList(reinterpret_cast<void*>(const_cast<char*>(value.constData())));
}

inline std::optional<LmbcsList> LmbcsList::fromBytes(WORD type, const QByteArray &value) {
  if (type == Lmbcs::value_type) {
    Lmbcs item = Lmbcs::fromBytes(value);
    LmbcsList list;
    list.append(item);
    return list;
  }
  return std::nullopt;
}

inline LmbcsList convert(const QList<Lmbcs> &list) {
  LmbcsList result;
  for (int i = 0; i < list.length(); ++i) {
    result.append(list.at(0));
  }
  return result;
}

} // namespace npp202009::npp

#endif // NPP202009_NPP_LMBCSLIST_HPP
