#ifndef NPP202009_NPP_DISTINGUISHEDNAME_HPP
#define NPP202009_NPP_DISTINGUISHEDNAME_HPP

#include "./lmbcs.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <names.h>
#include <dname.h>
#include <kfm.h>

#ifdef NT
#pragma pack(pop)
#endif

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp {

class DistinguishedName
    : public Lmbcs
{
public:
  DistinguishedName();

  DistinguishedName(const Lmbcs &name);

  observable<Lmbcs> canonical() const noexcept;

  Lmbcs canonical_x() const noexcept(false) {
    char buffer[MAXUSERNAME] = "";
    WORD len = 0;
    Status status = DNCanonicalize(
          0L,
          nullptr,
          constData(),
          buffer,
          MAXUSERNAME,
          &len
          );
    if (status.failure()) { throw status; }
    return Lmbcs(buffer, len);
  }

  observable<DN_COMPONENTS> getComponents() const noexcept;

  DN_COMPONENTS getComponents_x() const noexcept(false) {
    DN_COMPONENTS dn;
    Status status = DNParse(0, nullptr, constData(), &dn, sizeof(dn));
    if (status.failure()) { throw status; }
    return dn;
  }

  observable<Lmbcs> commonName() const noexcept;

  observable<Lmbcs> orgName() const noexcept;

  Lmbcs orgName_x() const noexcept(false) {
    auto dn = getComponents_x();
    return Lmbcs(dn.O, dn.OLength);
  }

  static observable<DistinguishedName> currentUserName() noexcept;

  static DistinguishedName currentUserName_x() noexcept(false) {
    char buffer[MAXUSERNAME] = "";
    Status status = SECKFMGetUserName(buffer);
    if (status.failure()) { throw status; }
    return DistinguishedName(Lmbcs(buffer));
  }
};

using DName = DistinguishedName;


inline DName::DistinguishedName() : Lmbcs() {}

inline DName::DistinguishedName(const Lmbcs &name) : Lmbcs(name) {}

inline observable<Lmbcs> DName::canonical() const noexcept {
  return observable<>::create<Lmbcs>([this](subscriber<Lmbcs> s) {
    char buffer[MAXUSERNAME] = "";
    WORD len = 0;
    Status status = DNCanonicalize(
          0L,
          nullptr,
          constData(),
          buffer,
          MAXUSERNAME,
          &len
          );
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(Lmbcs(buffer, len));
    s.on_completed();
  });
}

inline observable<DN_COMPONENTS> DName::getComponents() const noexcept {
  return observable<>::create<DN_COMPONENTS>(
        [this](subscriber<DN_COMPONENTS> s
        ) {
    DN_COMPONENTS dn;
    Status status = DNParse(0, nullptr, constData(), &dn, sizeof(dn));
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(dn);
    s.on_completed();
  });
}

inline observable<Lmbcs> DName::commonName() const noexcept {
  return getComponents()
      .map([](DN_COMPONENTS dn) {
    return Lmbcs(dn.CN, dn.CNLength);
  });
}

inline observable<Lmbcs> DName::orgName() const noexcept {
  return getComponents()
      .map([](DN_COMPONENTS dn) {
    return Lmbcs(dn.O, dn.OLength);
  });
}

inline observable<DistinguishedName> DName::currentUserName() noexcept {
  return observable<>::create<DistinguishedName>(
        [](subscriber<DistinguishedName> s) {
    char buffer[MAXUSERNAME] = "";
    Status status = SECKFMGetUserName(buffer);
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(DistinguishedName(Lmbcs(buffer)));
    s.on_completed();
  });
}

} // namespace npp202009::npp

#endif // NPP202009_NPP_DISTINGUISHEDNAME_HPP
