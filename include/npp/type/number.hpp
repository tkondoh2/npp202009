#ifndef NPP202009_NPP_NUMBER_HPP
#define NPP202009_NPP_NUMBER_HPP

#include "./lmbcs.hpp"
#include <rxcpp/rx.hpp>

using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp {

class Number
{
  NUMBER value_;

public:
  static const WORD value_type = TYPE_NUMBER;

  Number();

  Number(NUMBER value);

  NUMBER value() const;

  template <class T>
  T getValue() const;

  observable<Lmbcs> convertToText(
      NFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      ) const;

  static Number fromBytes(const QByteArray &value);

  static std::optional<Number> fromBytes(WORD type, const QByteArray &value);

//  static Number fromItemPtr(void *ptr, DWORD);

//  static Number fromItemPtrEx(WORD type, void *ptr, DWORD);

  static observable<Lmbcs> convertToText(
      NUMBER value,
      NFMT *pTextFormat = nullptr,
      void *pIntlFormat = nullptr
      );
};


inline Number::Number() : value_(0) {}

inline Number::Number(NUMBER value) : value_(value) {}

inline NUMBER Number::value() const { return value_; }

template <class T>
T Number::getValue() const { return static_cast<T>(value_); }

inline observable<Lmbcs> Number::convertToText(
    NFMT *pTextFormat,
    void *pIntlFormat
    ) const {
  return convertToText(value_, pTextFormat, pIntlFormat);
}

inline Number Number::fromBytes(const QByteArray &value) {
  return Number(*reinterpret_cast<NUMBER*>(const_cast<char*>(value.constData())));
}

//inline Number Number::fromItemPtr(void *ptr, DWORD) {
//  return Number(*reinterpret_cast<NUMBER*>(ptr));
//}

inline observable<Lmbcs> Number::convertToText(
    NUMBER value,
    NFMT *pTextFormat,
    void *pIntlFormat
    ) {
  return observable<>::create<Lmbcs>(
        [value, pTextFormat, pIntlFormat](subscriber<Lmbcs> s) {
    char buffer[MAXALPHANUMBER] = "";
    WORD len = 0;
    NUMBER org = value;
    Status status = ConvertFLOATToText(
          pIntlFormat,
          pTextFormat,
          &org,
          buffer,
          MAXALPHANUMBER,
          &len
          );
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(Lmbcs(buffer, len));
    s.on_completed();
  });
}

} // namespace npp202009::npp

#endif // NPP202009_NPP_NUMBER_HPP
