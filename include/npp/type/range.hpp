#ifndef NPP202009_NPP_RANGE_HPP
#define NPP202009_NPP_RANGE_HPP

#include <QList>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfdata.h>

#ifdef NT
#pragma pack(pop)
#endif

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp {

template <class Single, class Pair>
class Range
{
  QList<Single> list_;
  QList<Pair> pairs_;

public:
  Range();

  Range(void *ptr);

  Range(const Single &v);

  const QList<Single> &list() const;

  const QList<Pair> &pairs() const;
};


template <class Single, class Pair>
Range<Single, Pair>::Range(): list_(), pairs_() {}

template <class Single, class Pair>
Range<Single, Pair>::Range(void *ptr)
  : list_()
  , pairs_()
{
  RANGE *pRange = reinterpret_cast<RANGE*>(ptr);
  Single *pListEntry = reinterpret_cast<Single*>(pRange + 1);
  Pair *pPairEntry = reinterpret_cast<Pair*>(
        pListEntry + pRange->ListEntries
        );
  for (USHORT i = 0; i < pRange->ListEntries; ++i) {
    list_.append(pListEntry[i]);
  }
  for (USHORT i = 0; i < pRange->RangeEntries; ++i) {
    pairs_.append(pPairEntry[i]);
  }
}

template <class Single, class Pair>
Range<Single, Pair>::Range(const Single &v)
  : list_()
  , pairs_()
{
  list_.append(v);
}

template <class Single, class Pair>
const QList<Single> &Range<Single, Pair>::list() const { return list_; }

template <class Single, class Pair>
const QList<Pair> &Range<Single, Pair>::pairs() const { return pairs_; }

} // namespace npp202009::npp

#endif // NPP202009_NPP_RANGE_HPP
