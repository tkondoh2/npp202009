#ifndef NPP202009_NPP_HANDLE_OBJECT_HPP
#define NPP202009_NPP_HANDLE_OBJECT_HPP

#include "./lockable.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <osmem.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp::handle {

/**
 * @brief DHANDLE用ハンドルヘルパークラス
 */
struct ObjectHandleTraits
{
  using handle_type = DHANDLE;
  static bool is_null(DHANDLE h) { return h == NULLHANDLE; }
  static void deleter(DHANDLE h) { OSMemFree(h); }
};

class Object;
using ObjectPtr = QSharedPointer<Object>;
using ObjectHandle = Lockable<ObjectHandleTraits>;

/**
 * @brief ハンドルをDHANDLE、デリーターをOSMemFreeで実装したクラス
 */
class Object
    : public ObjectHandle
{
public:
  Object(DHANDLE &&handle) noexcept
    : ObjectHandle(std::move(handle))
  {}
};

} // namespace npp202009::npp::handle

#endif // NPP202009_NPP_HANDLE_OBJECT_HPP
