#ifndef NPP202009_NPP_HANDLE_LOCKABLE_HPP
#define NPP202009_NPP_HANDLE_LOCKABLE_HPP

#include "./base.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <osmem.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp::handle {

/**
 * @brief ロック可能なハンドルテンプレートクラス
 * @tparam T ハンドルヘルパークラス
 */
template <class T>
class Lockable
    : public Base<T>
{
  mutable void *ptr_; /// ロック時のポインタ

public:
  /**
   * @brief コンストラクタ
   * @param handle ハンドル
   */
  Lockable(typename T::handle_type &&handle) noexcept
    : Base<T>(std::move(handle))
    , ptr_(nullptr)
  {}

  /**
   * @brief デストラクタ
   */
  virtual ~Lockable() noexcept {
    unlock();
  }

  /**
   * @brief ハンドルをロックする
   * @tparam P ポインタの型
   * @return ロックしたハンドルのポインタ
   */
  template <class P>
  P *lock() const noexcept {
    if (this->is_null()) return nullptr;
    if (ptr_ == nullptr) {
      ptr_ = OSLockObject(this->handle());
    }
    return reinterpret_cast<P*>(ptr_);
  }

  /**
   * @brief unlock ロックしたハンドルをアンロックする
   */
  void unlock() const noexcept {
    if (this->is_valid() && ptr_ != nullptr) {
      OSUnlockObject(this->handle());
      ptr_ = nullptr;
    }
  }
};

} // namespace npp202009::npp::handle

#endif // NPP202009_NPP_HANDLE_LOCKABLE_HPP
