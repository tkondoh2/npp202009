#ifndef NPP202009_NPP_HANDLE_BASE_HPP
#define NPP202009_NPP_HANDLE_BASE_HPP

#include <utility>
#include <QSharedPointer>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp::handle {

/**
 * @brief Notes/Domino APIで使用するハンドルテンプレートクラス
 * @tparam T ハンドルヘルパークラス
 */
template <class T>
class Base
{
  typename T::handle_type handle_; /// ハンドル値

public:
  using handle_type = typename T::handle_type;

  /**
   * @brief コンストラクタ
   * @param handle ハンドル値
   */
  Base(typename T::handle_type &&handle) noexcept
    : handle_(std::move(handle))
  {}

  /**
   * @brief ハンドル値
   * @return ハンドル値
   */
  typename T::handle_type handle() const noexcept { return handle_; }

  /**
   * @brief ハンドルが空かどうか
   * @return ハンドルが空かどうか
   */
  bool is_null() const noexcept { return T::is_null(handle()); }

  /**
   * @brief 有効なハンドルかどうか
   * @return 有効なハンドルかどうか
   */
  bool is_valid() const noexcept { return !is_null(); }

  /**
   * @brief デストラクタ
   */
  virtual ~Base() noexcept(false) {
    if (is_valid()) T::deleter(handle());
  }
};

/**
 * @brief 指定した型の共有ポインタを任意の引数で生成する。
 * @tparam T ハンドルクラス
 * @tparam Args コンストラクタ用引数パラメータパック型
 * @param handle ハンドル値
 * @param args 引数パラメータパック
 * @return ハンドルインスタンスの共有ポインタ
 */
template <class T, class... Args>
QSharedPointer<T> make_ptr(
    typename T::handle_type &&handle,
    Args... args
    ) {
  return QSharedPointer<T>(new T(std::move(handle), args...));
}

} // namespace npp202009::npp::handle

#endif // NPP202009_NPP_HANDLE_BASE_HPP
