#ifndef NPP202009_NPP_HANDLE_BLOCK_HPP
#define NPP202009_NPP_HANDLE_BLOCK_HPP

#include "./base.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <osmem.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp::handle {

/**
 * @brief BLOCKID用ハンドルヘルパークラス
 */
struct BlockHandleTraits
{
  using handle_type = BLOCKID;
  static bool is_null(BLOCKID h) { return ISNULLBLOCKID(h); }
  static void deleter(BLOCKID) {}
};

class Block;
using BlockPtr = QSharedPointer<Block>;
using BlockHandle = Base<BlockHandleTraits>;

/**
 * @brief BLOCKIDラッパークラス
 */
class Block
    : public BlockHandle
{
  mutable void *ptr_; /// ロック時のポインタ

public:
  /**
   * @brief コンストラクタ
   * @param handle BLOCKIDハンドル
   */
  Block(BLOCKID &&handle) noexcept
    : BlockHandle(std::move(handle))
    , ptr_(nullptr)
  {}

  /**
   * @brief デストラクタ
   */
  virtual ~Block() noexcept {
    unlock();
  }

  /**
   * @brief ハンドルをロックする
   * @tparam P ポインタの型
   * @return ロックしたハンドルのポインタ
   */
  template <typename P>
  P *lock() const noexcept
  {
    if (is_null()) return nullptr;
    if (ptr_ == nullptr) {
      ptr_ = OSLockBlock(P, handle());
    }
    return reinterpret_cast<P*>(ptr_);
  }

  /**
   * @brief unlock ロックしたハンドルをアンロックする
   */
  virtual void unlock() const noexcept
  {
    if (is_valid() && ptr_ != nullptr) {
      OSUnlockBlock(handle());
      ptr_ = nullptr;
    }
  }
};

} // namespace npp202009::npp::handle

#endif // NPP202009_NPP_HANDLE_BLOCK_HPP
