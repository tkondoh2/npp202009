QT -= gui

TEMPLATE = lib
DEFINES += NPP_LIBRARY

include(../../local.pri)
include(../../global.pri)

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    version.cpp

HEADERS += \
    addin/logerror.hpp \
    addin/logmessage.hpp \
    compute.hpp \
    database.hpp \
    env.hpp \
    envstd.hpp \
    extmgr/extensionmanager.hpp \
    extmgr/repository.hpp \
    formula.hpp \
    handle/base.hpp \
    handle/block.hpp \
    handle/lockable.hpp \
    handle/object.hpp \
    http/authenticate.hpp \
    http/authenticateduser.hpp \
    http/authorize.hpp \
    http/context.hpp \
    http/directresponse.hpp \
    http/error.hpp \
    http/exception.hpp \
    http/exec.hpp \
    http/install.hpp \
    http/request.hpp \
    http/usernamelist.hpp \
    id.hpp \
    item.hpp \
    kfm/kfm.hpp \
    mime/codec.hpp \
    mime/decode.hpp \
    mime/quotedprintable.hpp \
    mime/rfc822text.hpp \
    nameslist.hpp \
    nls/info.hpp \
    nls/size.hpp \
    nls/status.hpp \
    nls/translate.hpp \
    note.hpp \
    npp_global.h \
    search.hpp \
    session.hpp \
    status.hpp \
    summary.hpp \
    type/distinguishedname.hpp \
    type/list.hpp \
    type/lmbcs.hpp \
    type/lmbcslist.hpp \
    type/number.hpp \
    type/numberrange.hpp \
    type/range.hpp \
    type/timedate.hpp \
    type/timedaterange.hpp \
    utils/csv/field.hpp \
    utils/csv/fields/datetime.hpp \
    utils/csv/fields/lmbcs.hpp \
    utils/csv/fields/lmbcslist.hpp \
    utils/csv/fields/number.hpp \
    utils/csv/fields/numberrange.hpp \
    utils/csv/fields/rfc822text.hpp \
    utils/csv/fields/string.hpp \
    utils/csv/fields/timedate.hpp \
    utils/csv/fields/timedaterange.hpp \
    utils/csv/file.hpp \
    utils/csv/filebase.hpp \
    utils/csv/fileoptions.hpp \
    utils/csv/record.hpp \
    utils/function.hpp \
    utils/jwt/base64.hpp \
    utils/jwt/bio.hpp \
    utils/jwt/error.hpp \
    utils/jwt/headerjson.hpp \
    utils/jwt/json.hpp \
    utils/jwt/jsonwebtoken.hpp \
    utils/jwt/messagedigest.hpp \
    utils/jwt/payloadjson.hpp \
    utils/jwt/pkey.hpp \
    utils/jwt/rsa.hpp \
    utils/logger/logger.hpp \
    utils/utils.hpp \
    utils/windows/windows.hpp \
    utils/writer/file.hpp \
    utils/writer/filebase.hpp \
    utils/writer/rotator/filerotator.hpp \
    utils/writer/rotator/filescountrotator.hpp \
    utils/writer/rotator/timestamprotator.hpp \
    utils/writer/verifier/dailyverifier.hpp \
    utils/writer/verifier/sizeverifier.hpp \
    version.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
