#ifndef NPP202009_NPP_SESSION_HPP
#define NPP202009_NPP_SESSION_HPP

#include "./type/lmbcs.hpp"
#include <QRunnable>
#include <cstdio>

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp {

template <class Func, class Err>
int start(int argc, char *argv[], Func func, Err efunc) noexcept
{
  STATUS status = NotesInitExtended(argc, argv);
  return (status == NOERROR)
      ? [&]() {
    int retCode = func(argc, argv);
    NotesTerm();
    return retCode;
  }() : [&]() {
    fprintf(stderr, "STATUS Code: %d\n", ERR(status));
    efunc();
    return -1;
  }();
}

class Thread
{
public:
  static void start(QRunnable *pRunnable) {

    Status status = NotesInitThread();
    if (status.success()){
      pRunnable->run();
      if (pRunnable->autoDelete()) {
        delete pRunnable;
      }
      NotesTermThread();
    }
  }
};

class ThreadLocker
{
  Status status_;
public:
  ThreadLocker() : status_(NotesInitThread()) {}
  ~ThreadLocker() {
    if (status_.success()) {
      NotesTermThread();
    }
  }
};

} // namespace npp202009::npp

#endif // NPP202009_NPP_SESSION_HPP
