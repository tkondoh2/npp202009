#ifndef NPP202009_NPP_SUMMARY_HPP
#define NPP202009_NPP_SUMMARY_HPP

#include "item.hpp"
#include <QVector>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfnote.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp {

template <WORD bufferSize>
Lmbcs getSummaryValue(ITEM_TABLE *pSummary, const char *pName) {
  char path[bufferSize] = "";
  if (NSFGetSummaryValue(pSummary, pName, path, sizeof(path)) != 0) {
    return Lmbcs(path);
  }
  return Lmbcs();
}

/**
 * @brief 名前付きサマリーバッファクラス
 */
class NamedItemTable
{
  ITEM_TABLE *pTable_;
  QVector<Lmbcs> names_; ///< 名前リスト

public:
  /**
   * @brief コンストラクタ
   * @param pTable ITEM_TABLE構造体ポインタ
   */
  NamedItemTable(ITEM_TABLE *pTable)
    : pTable_(pTable)
    , names_()
  {
    // 先頭のITEMポインタを取得
    ITEM *pItem = reinterpret_cast<ITEM*>(pTable + 1);

    // 先頭の名前ポインタを取得
    char *pName = reinterpret_cast<char*>(pItem + pTable->Items);

    // アイテム数分の巡回
    for (USHORT i = 0; i < pTable->Items; ++i) {

      // 現在のアイテム名を取得
      Lmbcs name(pName, pItem->NameLength);

      // 名前リストに追加
      names_.append(name);

      // 名前ポインタを次にアイテム名に移動
      pName += pItem->NameLength + pItem->ValueLength;

      // 次のITEMポインタに移動
      pItem += 1;
    }
  }

  /**
   * @brief 名前リストを取得
   * @return
   */
  const QVector<Lmbcs> &names() const { return names_; }

  /**
   * @brief 名前に対応した値をVariantで取得
   * @param pTable テーブルポインタ
   * @param name アイテム名
   * @return アイテム値
   */
  Item::Value value(const Lmbcs &name) const {
    char *pValue = nullptr;
    WORD len = 0, type = 0;
    if (NSFLocateSummaryValue(
          pTable_,
          name.constData(),
          &pValue,
          &len,
          &type
          )) {
      return Item::Value(type, pValue, len);
    }
    return Item::Value();
  }
};

} // namespace npp202009::npp

#endif // NPP202009_NPP_SUMMARY_HPP
