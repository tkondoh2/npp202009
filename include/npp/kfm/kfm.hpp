#ifndef NPP202009_NPP_KFM_KFM_HPP
#define NPP202009_NPP_KFM_KFM_HPP

#include "../type/lmbcs.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <kfm.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp::kfm {

inline observable<Lmbcs> getUserName() {
  return observable<>::create<Lmbcs>([](subscriber<Lmbcs> s) {
    char szUserName[MAXUSERNAME + 1] = "";
    Status status = SECKFMGetUserName(szUserName);
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(Lmbcs(szUserName));
    s.on_completed();
  });
}

} // namespace npp202009::npp::kfm

#endif // NPP202009_NPP_KFM_KFM_HPP
