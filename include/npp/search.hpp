#ifndef NPP202009_NPP_SEARCH_HPP
#define NPP202009_NPP_SEARCH_HPP

#include "./type/lmbcs.hpp"
#include <functional>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfsearc.h>
#include <nsfdb.h>
#include <nsfnote.h>

#ifdef NT
#pragma pack(pop)
#endif

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp {

bool callbackForNoteID(
    const SEARCH_MATCH &match,
    ITEM_TABLE *,
    NOTEID *pNoteId
    );

template <class T = NOTEID>
class Search
{
public:
  using CallbackFunction
  = std::function<bool(const SEARCH_MATCH &, ITEM_TABLE *, T *)>;

  struct Iterator
  {
    CallbackFunction callback_;
    subscriber<T> *pSubscriber_;
    Iterator(CallbackFunction callback, subscriber<T> *pSubscriber);
  };

  struct ListIterator
  {
    CallbackFunction callback_;
    QList<T> *pList_;
    ListIterator(CallbackFunction callback, QList<T> *pList);
  };

  observable<T> operator ()(
      DBHANDLE hDB,
      FORMULAHANDLE hFormula = NULLHANDLE,
      const Lmbcs &viewTitle = "",
      WORD flags = 0,
      WORD noteClassMask = NOTE_CLASS_DOCUMENT,
      CallbackFunction callback = callbackForNoteID,
      TIMEDATE *pSince = nullptr,
      TIMEDATE *retUntil = nullptr
      ) const noexcept;

  QList<T> reduce(
      DBHANDLE hDB,
      FORMULAHANDLE hFormula = NULLHANDLE,
      const Lmbcs &viewTitle = "",
      WORD flags = 0,
      WORD noteClassMask = NOTE_CLASS_DOCUMENT,
      CallbackFunction callback = callbackForNoteID,
      TIMEDATE *pSince = nullptr,
      TIMEDATE *retUntil = nullptr
      ) const noexcept(false);

  static STATUS LNPUBLIC callbackBase(
      void *ptr,
      SEARCH_MATCH *pMatch,
      ITEM_TABLE *pSummaryBuffer
      );

  static STATUS LNPUBLIC callbackReducer(
      void *ptr,
      SEARCH_MATCH *pMatch,
      ITEM_TABLE *pSummaryBuffer
      );
};



inline bool callbackForNoteID(
    const SEARCH_MATCH &match,
    ITEM_TABLE *,
    NOTEID *pNoteId
    ) {
  if (match.SERetFlags == SE_FMATCH) {
    *pNoteId = match.ID.NoteID;
  }
  return true;
}

template <class T>
Search<T>::Iterator::Iterator(
    Search<T>::CallbackFunction callback,
    subscriber<T> *pSubscriber
    )
  : callback_(callback)
  , pSubscriber_(pSubscriber)
{}

template <class T>
Search<T>::ListIterator::ListIterator(
    Search<T>::CallbackFunction callback,
    QList<T> *pList
    )
  : callback_(callback)
  , pList_(pList)
{}

template <class T>
observable<T> Search<T>::operator ()(
      DBHANDLE hDB,
      FORMULAHANDLE hFormula,
      const Lmbcs &viewTitle,
      WORD flags,
      WORD noteClassMask,
      Search<T>::CallbackFunction callback,
      TIMEDATE *pSince,
      TIMEDATE *retUntil
      ) const noexcept {
  return observable<>::create<T>([=](subscriber<T> s) {
      Search<T>::Iterator iterator(callback, &s);
      Status status = NSFSearch(
            hDB,
            hFormula,
            viewTitle.isEmpty()
              ? nullptr
              : const_cast<char*>(viewTitle.constData()),
            flags,
            noteClassMask,
            pSince,
            Search<T>::callbackBase,
            &iterator,
            retUntil
            );
      if (status.failure()) {
        s.on_error(std::make_exception_ptr(status));
        return;
      }
      s.on_completed();
    }
  );
}

template <class T>
QList<T> Search<T>::reduce(
    DBHANDLE hDB,
    FORMULAHANDLE hFormula,
    const Lmbcs &viewTitle,
    WORD flags,
    WORD noteClassMask,
    Search<T>::CallbackFunction callback,
    TIMEDATE *pSince,
    TIMEDATE *retUntil
    ) const noexcept(false) {
  QList<T> list;
  Search<T>::ListIterator iterator(callback, &list);
  Status status = NSFSearch(
        hDB,
        hFormula,
        viewTitle.isEmpty()
        ? nullptr
        : const_cast<char*>(viewTitle.constData()),
        flags,
        noteClassMask,
        pSince,
        Search<T>::callbackReducer,
        &iterator,
        retUntil
        );
  if (status.failure()) { throw status; }
  return list;
}

template <class T>
STATUS LNPUBLIC Search<T>::callbackBase(
        void *ptr,
        SEARCH_MATCH *pMatch,
        ITEM_TABLE *pSummaryBuffer
        ) {
  SEARCH_MATCH match;
  memcpy(&match, pMatch, sizeof(SEARCH_MATCH));
  Search<T>::Iterator *iter
      = reinterpret_cast<Search<T>::Iterator*>(ptr);
  T result;
  if (iter->callback_(match, pSummaryBuffer, &result)) {
    iter->pSubscriber_->on_next(result);
  }
  return NOERROR;
}

template <class T>
STATUS LNPUBLIC Search<T>::callbackReducer(
    void *ptr,
    SEARCH_MATCH *pMatch,
    ITEM_TABLE *pSummaryBuffer
    ) {
  SEARCH_MATCH match;
  memcpy(&match, pMatch, sizeof(SEARCH_MATCH));
  auto iter = reinterpret_cast<Search<T>::ListIterator*>(ptr);
  T result;
  if (iter->callback_(match, pSummaryBuffer, &result)) {
    iter->pList_->append(result);
  }
  return NOERROR;
}


//template <class T>
//class SearchIterator
//{
//  subscriber<T> *pSubscriber_;

//public:
//  using SearchCallbackFunction
//  = std::function<bool(const SEARCH_MATCH &, ITEM_TABLE *, T *)>;

//  SearchCallbackFunction callback;

//  SearchIterator(subscriber<T> *p, SearchCallbackFunction cb);

//  static STATUS LNPUBLIC callbackBase(
//      void *ptr,
//      SEARCH_MATCH *pMatch,
//      ITEM_TABLE *pSummaryBuffer
//      );
//};

//template <class T>
//observable<T> search(
//    DBHANDLE hDB,
//    FORMULAHANDLE hFormula = NULLHANDLE,
//    const Lmbcs &viewTitle = "",
//    WORD flags = 0,
//    WORD noteClassMask = NOTE_CLASS_DOCUMENT,
//    typename SearchIterator<T>::SearchCallbackFunction callback
//    = callbackForNoteID,
//    TIMEDATE *pSince = nullptr,
//    TIMEDATE *retUntil = nullptr
//    ) noexcept;


//template <class T>
//observable<T> search(
//      DBHANDLE hDB,
//      FORMULAHANDLE hFormula,
//      const Lmbcs &viewTitle,
//      WORD flags,
//      WORD noteClassMask,
//      typename SearchIterator<T>::SearchCallbackFunction callback,
//      TIMEDATE *pSince,
//      TIMEDATE *retUntil
//      ) noexcept {
//  return observable<>::create<T>([=](subscriber<T> s) {
//      SearchIterator<T> iter(&s, callback);
//      Status status = NSFSearch(
//            hDB,
//            hFormula,
//            viewTitle.isEmpty()
//              ? nullptr
//              : const_cast<char*>(viewTitle.constData()),
//            flags,
//            noteClassMask,
//            pSince,
//            SearchIterator<T>::callbackBase,
//            &iter,
//            retUntil
//            );
//      if (status.failure()) {
//        s.on_error(std::make_exception_ptr(status));
//      }
//      s.on_completed();
//    }
//  );
//}

//template <class T>
//SearchIterator<T>::SearchIterator(
//    subscriber<T> *p,
//    SearchIterator<T>::SearchCallbackFunction cb
//    )
//  : pSubscriber_(p)
//  , callback(cb)
//{}

//template <class T>
//STATUS LNPUBLIC SearchIterator<T>::callbackBase(
//        void *ptr,
//        SEARCH_MATCH *pMatch,
//        ITEM_TABLE *pSummaryBuffer
//        ) {
//  SEARCH_MATCH match;
//  memcpy(&match, pMatch, sizeof(SEARCH_MATCH));
//  SearchIterator<T> *iter
//      = reinterpret_cast<SearchIterator<T>*>(ptr);
//  T result;
//  if (iter->callback(match, pSummaryBuffer, &result)) {
//    iter->pSubscriber_->on_next(result);
//  }
//  return NOERROR;
//}

} // namespace npp202009::npp

#endif // NPP202009_NPP_SEARCH_HPP
