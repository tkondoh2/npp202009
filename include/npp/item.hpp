#ifndef NPP202009_NPP_ITEM_HPP
#define NPP202009_NPP_ITEM_HPP

#include "./note.hpp"
#include "./handle/block.hpp"
#include "./handle/object.hpp"
#include <optional>

#ifdef NT
#pragma pack(push, 1)
#endif

//#include <nsfnote.h>
//#include <stdnames.h>
#include <ods.h> // ConvertItemToText
#include <nsferr.h> // ERR_ITEM_NOT_FOUND

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp {

class Item
{
  BLOCKID itemId_;
  WORD type_;
  BLOCKID valueId_;
  DWORD valueSize_;

public:
  class Value
  {
    WORD type_;
    QByteArray value_;

  public:
    Value();

    Value(const QByteArray &valueWithType);

    Value(WORD type, QByteArray &&value);

    Value(WORD type, const char *ptr, DWORD size);

    WORD type() const { return type_; }

    const QByteArray &value() const { return value_; }

    Lmbcs typeName() const;

    template <class T>
    std::optional<T> get() const;
  };

  Item();

  Item(const BLOCKID &itemId,
      WORD type,
      const BLOCKID &valueId,
      DWORD valueSize
      );

  BLOCKID itemId() const { return itemId_; }
  WORD type() const { return type_; }
  BLOCKID valueId() const { return valueId_; }
  DWORD valueSize() const { return valueSize_; }

  Value value() const;

  observable<Lmbcs> convertToText(
      const Lmbcs &lineDelimiter,
      WORD charsPerLine,
      bool isStripTabs
      ) const noexcept;

  observable<QByteArray> extractAttachFile(NOTEHANDLE hNote) const;

  QByteArray extractAttachFile_x(NOTEHANDLE hNote) const;

  static STATUS LNPUBLIC extractAttachFileCallback(
      const BYTE *bytes,
      DWORD length,
      void *ptr
      );
};


inline Item::Item()
  : itemId_({NULLHANDLE,NULLBLOCK})
  , type_(TYPE_INVALID_OR_UNKNOWN)
  , valueId_({NULLHANDLE,NULLBLOCK})
  , valueSize_(0)
{}

inline Item::Item(
    const BLOCKID& itemId,
    WORD type,
    const BLOCKID &valueId,
    DWORD valueSize
    )
  : itemId_(itemId)
  , type_(type)
  , valueId_(valueId)
  , valueSize_(valueSize)
{}

inline Item::Value Item::value() const {
  handle::BlockPtr bPtr = handle::make_ptr<handle::Block>(BLOCKID(valueId_));
  if (bPtr->is_valid()) {
    WORD *pType = bPtr->lock<WORD>();
    char *ptr = reinterpret_cast<char*>(pType + 1);
    return Item::Value(*pType, ptr, valueSize_ - sizeof(WORD));
  }
  return Item::Value();
}

inline observable<Lmbcs> Item::convertToText(
    const Lmbcs &lineDelimiter,
    WORD charsPerLine,
    bool isStripTabs
    ) const noexcept {
  return observable<>::create<Lmbcs>([
    this,
    lineDelimiter,
    charsPerLine,
    isStripTabs
    ](subscriber<Lmbcs> s) {
    DHANDLE handle = NULLHANDLE;
    DWORD length = 0;
    Status status = ConvertItemToText(
          valueId_,
          valueSize_,
          lineDelimiter.constData(),
          charsPerLine,
          &handle,
          &length,
          isStripTabs
          );
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
    }
    handle::Object obj(std::move(handle));
    char* ptr = obj.lock<char>();
    s.on_next(Lmbcs(ptr, static_cast<int>(length)));
    s.on_completed();
  });
}

inline observable<QByteArray> Item::extractAttachFile(NOTEHANDLE hNote) const {
  return observable<>::create<QByteArray>(
        [this, hNote](subscriber<QByteArray> s) {
    QByteArray bytes;
    Status status = NSFNoteExtractWithCallback(
          hNote,
          itemId_,
          nullptr,
          0,
          extractAttachFileCallback,
          &bytes
          );
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(bytes);
    s.on_completed();
  });
}

inline QByteArray Item::extractAttachFile_x(NOTEHANDLE hNote) const {
  QByteArray bytes;
  Status status = NSFNoteExtractWithCallback(
        hNote,
        itemId_,
        nullptr,
        0,
        extractAttachFileCallback,
        &bytes
        );
  if (status.failure()) { throw status; }
  return bytes;
}

inline STATUS LNPUBLIC Item::extractAttachFileCallback(
    const BYTE *bytes,
    DWORD length,
    void *ptr
    ) {
  if (length > 0) {
    QByteArray *pBytes = static_cast<QByteArray*>(ptr);
    QByteArray file(
          reinterpret_cast<char *>(const_cast<BYTE*>(bytes)),
          static_cast<int>(length)
          );
    *pBytes += file;
  }
  return NOERROR;
}


inline Item::Value::Value()
  : type_(TYPE_INVALID_OR_UNKNOWN)
  , value_()
{}

inline Item::Value::Value(const QByteArray &valueWithType)
  : type_(TYPE_INVALID_OR_UNKNOWN)
  , value_()
{
  if (valueWithType.size() >= (int)sizeof(WORD)) {
    type_ = *reinterpret_cast<const WORD*>(valueWithType.constData());
    value_ = valueWithType.right(valueWithType.size() - sizeof(WORD));
  }
}

inline Item::Value::Value(WORD type, QByteArray &&value)
  : type_(type)
  , value_(std::move(value))
{}

inline Item::Value::Value(WORD type, const char *ptr, DWORD size)
  : type_(type)
  , value_(ptr, static_cast<int>(size))
{}

template <class T>
std::optional<T> Item::Value::get() const {
  if (type_ == T::value_type) {
    return T::fromBytes(value_);
  }
  return T::fromBytes(type_, value_);
}

inline Lmbcs Item::Value::typeName() const {
  switch (type_)
  {
  case TYPE_TEXT: return Lmbcs("Text");
  case TYPE_TEXT_LIST: return Lmbcs("TextList");
  case TYPE_NUMBER: return Lmbcs("Number");
  case TYPE_NUMBER_RANGE: return Lmbcs("NumberRange");
  case TYPE_TIME: return Lmbcs("TimeDate");
  case TYPE_TIME_RANGE: return Lmbcs("TimeDateRange");
  case TYPE_OBJECT: return Lmbcs("Object");
  case TYPE_RFC822_TEXT: return Lmbcs("RFC822Text");
  }
  return ql(QString("Invalid or Unknown(%1)").arg(type_));
}


inline bool Note::hasItem(NOTEHANDLE hNote, const Lmbcs &name) {
  return NSFItemIsPresent(
        hNote,
        name.constData(),
        static_cast<WORD>(name.size())
        );
}

inline observable<Item> Note::getItem(NOTEHANDLE hNote, const Lmbcs &name) {
  return observable<>::create<Item>(
        [hNote, name](subscriber<Item> s) {
    BLOCKID itemId, valueId, prevId;
    WORD dataType = 0;
    DWORD valueSize = 0;
    Status status = NSFItemInfo(
          hNote,
          name.constData(),
          static_cast<WORD>(name.size()),
          &itemId,
          &dataType,
          &valueId,
          &valueSize
          );
    int count = 0;
    while (status.error() != ERR_ITEM_NOT_FOUND) {
      if (status.failure()) {
        s.on_error(std::make_exception_ptr(status));
        return;
      }
      ++count;
      s.on_next(Item(itemId, dataType, valueId, valueSize));
      prevId = itemId;
      status = NSFItemInfoNext(
            hNote,
            prevId,
            name.constData(),
            static_cast<WORD>(name.size()),
            &itemId,
            &dataType,
            &valueId,
            &valueSize
            );
    }
    if (count < 1) {
      s.on_next(Item());
    }
    s.on_completed();
  });
}

inline QVector<Item> Note::getItem_x(NOTEHANDLE hNote, const Lmbcs &name) {
  QVector<Item> list;
  BLOCKID itemId, valueId, prevId;
  WORD dataType = 0;
  DWORD valueSize = 0;
  Status status = NSFItemInfo(
        hNote,
        name.constData(),
        static_cast<WORD>(name.size()),
        &itemId,
        &dataType,
        &valueId,
        &valueSize
        );
  while (status.error() != ERR_ITEM_NOT_FOUND) {
    if (status.failure()) { throw status; }
    list << Item(itemId, dataType, valueId, valueSize);
    prevId = itemId;
    status = NSFItemInfoNext(
          hNote,
          prevId,
          name.constData(),
          static_cast<WORD>(name.size()),
          &itemId,
          &dataType,
          &valueId,
          &valueSize
          );
  }
  return list;
}

inline observable<AttachFileItem> Note::attachFileNames(NOTEHANDLE hNote) {
  return getItem(hNote, ITEM_NAME_ATTACHMENT)
      .map([](Item item) {
    handle::Block value(item.valueId());
    if (value.is_valid()) {
      char *ptr = value.lock<char>() + sizeof(WORD);
#ifdef NT
      OBJECT_DESCRIPTOR *pObjDesc = reinterpret_cast<OBJECT_DESCRIPTOR*>(ptr);

      if (pObjDesc->ObjectType == OBJECT_FILE) {
        FILEOBJECT *pFileObj = reinterpret_cast<FILEOBJECT*>(pObjDesc);
        Lmbcs fileName(ptr + sizeof(FILEOBJECT), pFileObj->FileNameLength);
#else
      char *pObjDesc = ptr;
      OBJECT_DESCRIPTOR objDesc;
      ODSReadMemory(&pObjDesc, _OBJECT_DESCRIPTOR, &objDesc, 1);

      if (objDesc.ObjectType == OBJECT_FILE) {
        FILEOBJECT fileObj;
        ODSReadMemory(&ptr, _FILEOBJECT, &fileObj, 1);
        Lmbcs fileName(ptr, fileObj.FileNameLength);
#endif
        return std::make_tuple(fileName, item);
      }
    }
    return std::make_tuple(Lmbcs(), item);
  });
}

inline QVector<AttachFileItem> Note::attachFileNames_x(NOTEHANDLE hNote) {
  QVector<AttachFileItem> list;
  auto itemList = getItem_x(hNote, ITEM_NAME_ATTACHMENT);
  for (int i = 0; i < itemList.size(); ++i) {
    auto item = itemList.at(i);
    handle::Block value(item.valueId());
    if (value.is_valid()) {
      char *ptr = value.lock<char>() + sizeof(WORD);
#ifdef NT
      OBJECT_DESCRIPTOR *pObjDesc = reinterpret_cast<OBJECT_DESCRIPTOR*>(ptr);

      if (pObjDesc->ObjectType == OBJECT_FILE) {
        FILEOBJECT *pFileObj = reinterpret_cast<FILEOBJECT*>(pObjDesc);
        Lmbcs fileName(ptr + sizeof(FILEOBJECT), pFileObj->FileNameLength);
#else
      char *pObjDesc = ptr;
      OBJECT_DESCRIPTOR objDesc;
      ODSReadMemory(&pObjDesc, _OBJECT_DESCRIPTOR, &objDesc, 1);

      if (objDesc.ObjectType == OBJECT_FILE) {
        FILEOBJECT fileObj;
        ODSReadMemory(&ptr, _FILEOBJECT, &fileObj, 1);
        Lmbcs fileName(ptr, fileObj.FileNameLength);
#endif
        list.append(std::make_tuple(fileName, item));
        continue;
      }
    }
    list.append(std::make_tuple(Lmbcs(), item));
  }
  return list;
}

} // namespace npp202009::npp

#endif // NPP202009_NPP_ITEM_HPP
