#ifndef NPP202009_NPP_MIME_QUOTEDPRINTABLE_HPP
#define NPP202009_NPP_MIME_QUOTEDPRINTABLE_HPP

#include <QByteArray>

namespace npp202009::npp::mime {

class QuotedPrintable
{
public:

  static QByteArray encode(const QByteArray &latin1) noexcept {
    QByteArray output;
    char byte;
    const char hex[] = {
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
      'A', 'B', 'C', 'D', 'E', 'F'
    };

    for (int i = 0; i < latin1.length() ; ++i) {
      byte = latin1[i];
      if ((byte == ' ') || ((byte >= 33) && (byte <= 126)  && (byte != '='))) {
        output.append(byte);
      }
      else {
        output.append('=');
        output.append(hex[((byte >> 4) & 0x0F)]);
        output.append(hex[(byte & 0x0F)]);
      }
    }
    return output;
  }

  static QByteArray decode(const QByteArray &org_input) noexcept {
    QByteArray input = org_input;
    input.replace("=\r\n", "");
    QByteArray output;
    for (int i = 0; i < input.length(); ++i) {
      if (input.at(i) == '=' && (i + 2) < input.length()) {
        QString strValue = input.mid((++i)++, 2);
        bool converted;
        char character = static_cast<char>(strValue.toUShort(&converted, 16));
        if (converted) {
          output.append(character);
        }
        else {
          output.append("=" + strValue);
        }
      }
      else {
        output.append(input.at(i));
      }
    }
    return output;
  }

private:
  QuotedPrintable();
};

} // namespace npp202009::npp::mime

#endif // NPP202009_NPP_MIME_QUOTEDPRINTABLE_HPP
