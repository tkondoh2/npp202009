#ifndef NPP202009_NPP_DECODE_HPP
#define NPP202009_NPP_DECODE_HPP

#include "./codec.hpp"
#include "./quotedprintable.hpp"
#include <QTextStream>
#include <rxcpp/rx.hpp>

using namespace rxcpp;
using namespace rxcpp::operators;

using QOptionalByteArray = std::optional<QByteArray>;

namespace npp202009::npp::mime {

observable<QString> decodeMimeHeader(const QString &source) noexcept;

QString decodeEncodingPart(
    const QString &charSet,
    const QString &encoding,
    const QString &source
    ) noexcept(false);

template <typename T>
QOptionalByteArray decodeByteArray(
    const QString &pattern,
    const QByteArray &from
    );

struct EncodeBase64;
struct EncodeQuoted;

inline QString decodeMimeHeaderType1(const QString &source) {
  // デリミターで分割
  auto list = source.split(QRegExp(R"(\r\n\s+)"));

  // 分割単位でデコード処理し、一つにまとめる
  auto buffer = QString();
  QTextStream cout(&buffer, QIODevice::WriteOnly);
  foreach (auto s2, list) {
    int pos = 0;
    while (pos < s2.length()) {
      auto rx = QRegExp(R"rx(=\?(\S+)\?(B|b|Q|q)\?(\S+)\?=)rx");
      auto offset = rx.indexIn(s2, pos);
      if (offset >= pos) {
        if (offset > pos) {
          cout << s2.mid(pos, offset - pos);
        }
        cout << decodeEncodingPart(rx.cap(1), rx.cap(2), rx.cap(3));
        pos = offset + rx.matchedLength();
      }
      else {
        if (s2.length() > pos) {
          cout << s2.mid(pos, s2.length() - pos);
        }
        break;
      }
    }
  }
  cout.flush();
  return buffer;
}

inline QString decodeMimeHeaderType2(const QString &source) {
  auto buffer = QString();
  QTextStream cout(&buffer, QIODevice::WriteOnly);

  int pos = 0;
  while (pos < source.length()) {
    int offset1 = source.indexOf("=?", pos);
    if (offset1 >= pos) {
      if (offset1 > pos) {
        cout << source.mid(pos, offset1 - pos);
      }
      int offset2 = source.indexOf("?=", offset1 + 2);
      auto part = source.mid(offset1, offset2 + 2 - offset1);

      auto rx = QRegExp(R"rx(=\?(\S+)\?(B|b|Q|q)\?(\S+)\?=)rx");
      if (rx.indexIn(part) == 0) {
        part = decodeEncodingPart(rx.cap(1), rx.cap(2), rx.cap(3));
      }
      cout << part;

      pos = offset2 + 2;
    }
    else {
      if (pos < source.length()) {
        cout << source.right(source.length() - pos);
      }
      break;
    }
  }
  cout.flush();
  return buffer;
}

inline observable<QString> decodeMimeHeader(
    const QString &source
    ) noexcept {
  return observable<>::create<QString>([source](subscriber<QString> s) {
    std::exception_ptr ep;
    try {
      s.on_next(decodeMimeHeaderType1(source));
      s.on_completed();
      return;
    }
    catch (...) {
      ep = std::current_exception();
    }
    try {
      s.on_next(decodeMimeHeaderType2(source));
      s.on_completed();
      return;
    }
    catch (...) {
      ep = std::current_exception();
    }
    s.on_error(ep);
  });
}

inline QString decodeEncodingPart(
    const QString &charSet,
    const QString &encoding,
    const QString &source
    ) noexcept(false) {

  if (source.isEmpty()) return QString();

  QByteArray bytes1 = source.toLatin1();
  auto bytes2 = decodeByteArray<EncodeBase64>(encoding, bytes1);
  bytes2 = bytes2 ? bytes2 : decodeByteArray<EncodeQuoted>(encoding, bytes1);
  if (!bytes2) {
    throw std::runtime_error(
          QString("Unknown encoding pattern(%1).").arg(encoding)
          .toStdString()
          );
  }

  Codec codec(charSet);
  auto dest = codec.to<Codec::UTF8>(bytes2.value());
  dest = dest ? dest : codec.to<Codec::JIS>(bytes2.value());
  dest = dest ? dest : codec.to<Codec::ShiftJIS>(bytes2.value());
  dest = dest ? dest : codec(bytes2.value());
  dest = dest ? dest : codec.to<Codec::US_ASCII>(bytes2.value());
  if (!dest) {
    throw std::runtime_error(
          QString("Unknown character set(%1).").arg(charSet)
          .toStdString()
          );
  }
  return dest.value();
}

template <class T>
QOptionalByteArray decodeByteArray(
    const QString &pattern,
    const QByteArray &from
    ) {
  if (pattern.toUpper().at(0).toLatin1() == T::pattern) {
    return T::decode(from);
  }
  else {
    return std::nullopt;
  }
}

struct EncodeBase64
{
  static const char pattern = 'B';
  static QByteArray decode(const QByteArray &bytes) {
    return QByteArray::fromBase64(bytes);
  }
};

struct EncodeQuoted
{
  static const char pattern = 'Q';
  static QByteArray decode(const QByteArray &bytes) {
    return QuotedPrintable::decode(bytes);
  }
};

} // namespace npp202009::npp::mime

#endif // NPP202009_NPP_DECODE_HPP
