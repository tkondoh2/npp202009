#ifndef NPP202009_NPP_MIME_RFC822TEXT_HPP
#define NPP202009_NPP_MIME_RFC822TEXT_HPP

#include "./decode.hpp"
#include "../type/lmbcs.hpp"
#include "../type/timedate.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <mimeods.h> // RFC822ITEMDESC
#include <ods.h> // _RFC822ITEMDESC

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp::mime {

class Rfc822Text
{
  RFC822ITEMDESC desc_;
  QByteArray native_;
  QByteArray name_;
  QByteArray delim_;
  QByteArray body_;

public:
  static const WORD value_type = TYPE_RFC822_TEXT;

  Rfc822Text() : desc_(), native_(), delim_(), body_() {}

  WORD version() const { return desc_.wVersion; }
  DWORD flags() const { return desc_.dwFlags; }
  DWORD type() const { return flags() & RFC822_ITEM_FORMAT_MASK; }

  WORD nativeLen() const { return desc_.wNotesNativeLen; }
  WORD nameLen() const { return desc_.w822NameLen; }
  WORD delimLen() const { return desc_.w822DelimLen; }
  WORD bodyLen() const { return desc_.w822BodyLen; }

  const QByteArray &native() const { return native_; }
  const QByteArray &name() const { return name_; }
  const QByteArray &delim() const { return delim_; }
  const QByteArray &body() const { return body_; }

  bool isAddress() const { return type() == RFC822_ITEM_FORMAT_ADDR; }
  bool isDate() const { return type() == RFC822_ITEM_FORMAT_DATE; }
  bool isText() const { return type() == RFC822_ITEM_FORMAT_TEXT; }
  bool isStorageStrict() const { return flags() & RFC822_ITEM_STORAGE_STRICT; }
  bool isTextList() const { return flags() & RFC822_ITEM_TEXT_LIST; }
  bool isUnused() const { return flags() & RFC822_TEXT_UNUSED; }

  observable<Lmbcs> text() const {
    if (nativeLen() > 0) {
      return observable<>::just(Lmbcs(native_));
    }
    else if (bodyLen() > 0) {
      return decodeMimeHeader(QString::fromLatin1(body_))
          .map([](QString str) {return ql(str);});
    }
    else {
      return observable<>::just(Lmbcs());
    }
  }

  observable<TimeDate> date() const {
    if (nativeLen() > 0) {
      return observable<>::just(TimeDate::fromBytes(native_));
    }
    if (bodyLen() > 0) {
      return TimeDate::fromDateTime(
            QDateTime::fromString(
              QString::fromLocal8Bit(body_),
              Qt::RFC2822Date
              )
            );
    }
    return observable<>::just(TimeDate());
  }

  observable<LmbcsList> textList() const {
    LmbcsList list;
    if (isTextList()) {
      list = LmbcsList::fromBytes(native_);
//      list = LmbcsList::fromItemPtr(
//            reinterpret_cast<void*>(const_cast<char*>(native_.constData())),
//            static_cast<DWORD>(nativeLen())
//            );
    }
    return observable<>::just(list);
  }

  static Rfc822Text fromBytes(const QByteArray &value);

  static std::optional<Rfc822Text> fromBytes(WORD type, const QByteArray &value);

  static Rfc822Text fromItemPtr(void *ptr, DWORD len);

  static Rfc822Text fromItemPtrEx(WORD type, void *ptr, DWORD len);

private:

  static void *copyODStoRFC822ITEMDESC(
      void *ppSrc,
      RFC822ITEMDESC *pDest,
      WORD iterations
      );

  static QByteArray trimLastCRLF(const QByteArray &bytes);

#ifndef NT
  template <typename T>
  static char *_copy(char* from, void *to);
#endif
};

inline Rfc822Text Rfc822Text::fromBytes(const QByteArray &value) {
  Rfc822Text result;
  char *p = const_cast<char*>(value.constData());
  copyODStoRFC822ITEMDESC(&p, &result.desc_, 1);
  int index = ODSLength(_RFC822ITEMDESC);

  result.native_ = value.mid(index, result.nativeLen());
  index += result.nativeLen();

  result.name_ = value.mid(index, result.nameLen());
  index += result.nameLen();

  result.delim_ = value.mid(index, result.delimLen());
  index += result.delimLen();

  result.body_ = trimLastCRLF(value.mid(index, result.bodyLen()));
  return result;
}

inline std::optional<Rfc822Text> Rfc822Text::fromBytes(WORD, const QByteArray &) {
  return Rfc822Text();
}

inline void *Rfc822Text::copyODStoRFC822ITEMDESC(
    void *ppSrc,
    RFC822ITEMDESC *pDest,
    WORD iterations
    ) {
  char** pRef = reinterpret_cast<char**>(ppSrc);
  for (WORD i = 0; i < iterations; ++i) {
#ifdef NT
    static const auto size = ODSLength(_RFC822ITEMDESC);
    memcpy_s(pDest, size, *pRef, size);
    *pRef += size;
#else
    *pRef = _copy<WORD>(*pRef, &pDest->wVersion);
    *pRef = _copy<DWORD>(*pRef, &pDest->dwFlags);
    *pRef = _copy<WORD>(*pRef, &pDest->wNotesNativeLen);
    *pRef = _copy<WORD>(*pRef, &pDest->w822NameLen);
    *pRef = _copy<WORD>(*pRef, &pDest->w822DelimLen);
    *pRef = _copy<WORD>(*pRef, &pDest->w822BodyLen);
#endif
  }
  return pRef;
}

inline QByteArray Rfc822Text::trimLastCRLF(const QByteArray &bytes) {
  if (bytes.right(2) == QByteArray("\x0d\x0a")) {
    return bytes.left(bytes.length() - 2);
  }
  return bytes;
}

#ifndef NT
template <typename T>
char *Rfc822Text::_copy(char* from, void *to) {
  memcpy(to, from, sizeof(T));
  return from + sizeof(T);
}
#endif

} // namespace npp202009::npp::mime

#endif // NPP202009_NPP_MIME_RFC822TEXT_HPP
