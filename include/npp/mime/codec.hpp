#ifndef NPP202009_NPP_MIME_CODEC_HPP
#define NPP202009_NPP_MIME_CODEC_HPP

#include <QString>
#include <QByteArray>
#include <QTextCodec>
#include <optional>

using QOptionalString = std::optional<QString>;

namespace npp202009::npp::mime {

class Codec
{
  QString pattern_;

public:
  Codec(const QString &pattern);

  QOptionalString operator ()(const QByteArray &from) const;

  template <typename T>
  QOptionalString to(const QByteArray &from) const;

  static bool equals(const QString &lhs, const QString &rhs);

  static QString standardize(const QString &s);

  struct US_ASCII;

  struct UTF8;

  struct ShiftJIS;

  struct JIS;
};

inline Codec::Codec(const QString &pattern) : pattern_(standardize(pattern)) {}

inline QOptionalString Codec::operator ()(const QByteArray &from) const {
  static const QByteArrayList keyNames = QTextCodec::availableCodecs();

  foreach (auto keyName, keyNames) {
    QString key = standardize(QString::fromLatin1(keyName));
    if (pattern_.compare(key, Qt::CaseInsensitive) == 0) {
      QTextCodec *codec = QTextCodec::codecForName(keyName);
      return codec->toUnicode(from);
    }
  }
  return std::nullopt;
}

template <typename T>
QOptionalString Codec::to(const QByteArray &from) const {
  if (T::compare(pattern_)) {
    return T::codec(from);
  }
  else {
    return std::nullopt;
  }
//  return T::compare(pattern_) ? T::codec(from) : std::nullopt;
}

struct Codec::US_ASCII
{
  static bool compare(const QString &pattern) {
    return equals(pattern, "us-ascii");
  }

  static QString codec(const QByteArray &from) {
    return QString::fromLatin1(from);
  }
};

struct Codec::UTF8
{
  static bool compare(const QString &pattern) {
    return equals(pattern, "utf-8");
  }

  static QString codec(const QByteArray &from) {
    return QString::fromUtf8(from);
  }
};

struct Codec::ShiftJIS
{
  static bool compare(const QString &pattern) {
    return equals(pattern, "Shift-JIS")
        || equals(pattern, "SJIS")
        || equals(pattern, "x-sjis")
        || equals(pattern, "Windows-31J")
        || equals(pattern, "MS_Kanji")
        || equals(pattern, "cp932")
        || equals(pattern, "MS932");
  }

  static QString codec(const QByteArray &from) {
    QTextCodec *codec = QTextCodec::codecForName("Shift-JIS");
    return codec->toUnicode(from);
  }
};

struct Codec::JIS
{
  static bool compare(const QString &pattern) {
    return equals(pattern, "JIS") || equals(pattern, "ISO 2022-JP");
  }

  static QString codec(const QByteArray &from) {
    QTextCodec *codec = QTextCodec::codecForName("ISO 2022-JP");
    return codec->toUnicode(from);
  }
};

inline bool Codec::equals(const QString &lhs, const QString &rhs) {
  auto l = standardize(lhs);
  auto r = standardize(rhs);
  return l.compare(r, Qt::CaseInsensitive) == 0;
}

inline QString Codec::standardize(const QString &s) {
  static const auto rx = QRegExp(R"rx([^a-zA-Z0-9])rx");
  auto r = s;
  r.replace(rx, QChar('-'));
  return r;
}

} // namespace npp202009::npp::mime

#endif // NPP202009_NPP_MIME_CODEC_HPP
