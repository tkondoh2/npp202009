#ifndef NPP202009_NPP_STATUS_HPP
#define NPP202009_NPP_STATUS_HPP

#include <exception>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp {

class Lmbcs;

/**
 * @brief ステータス値のラッパー<br>
 * Notes C API関数の大部分が返す「STATUS」型のラッパークラス
 */
class Status
    : public std::exception
{
  STATUS status_; ///< ステータス値

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Status() noexcept : std::exception(), status_(NOERROR) {}

  /**
   * @brief コンストラクタ
   * @param status 初期ステータス値
   */
  Status(STATUS status) noexcept : std::exception(), status_(status) {}

  /**
   * @brief エラー値を返す。
   * @return エラー値
   */
  STATUS error() const noexcept { return ERR(status_); }

  /**
   * @brief エラーが発生しなければ真を返す。
   * @return エラーが発生しなければ真
   */
  bool success() const noexcept { return error() == NOERROR; }

  /**
   * @brief エラーが発生していれば真を返す。
   * @return エラーが発生していれば真
   */
  bool failure() const noexcept { return !success(); }

  /**
   * @brief エラーの説明を返す。
   * @return エラー説明
   */
  virtual const char *what() const noexcept override;

  /**
   * @brief エラーメッセージをLMBCS形式で返す。
   * @return エラーメッセージ
   */
  Lmbcs message() const noexcept;

  /**
   * @brief 表示済みかどうか
   * @return 表示済みなら真
   */
  bool isDisplayed() const noexcept { return ERROR_DISPLAYED(status_); }

  /**
   * @brief リモート先のエラーかどうか
   * @return リモート先のエラーなら真
   */
  bool isRemote() const noexcept { return ERROR_REMOTE(status_); }

  /**
   * @brief エラーが発生したパッケージ値を返す。
   * @return パッケージ値
   */
  STATUS package() const noexcept { return PKG(status_); }

  /**
   * @brief エラー番号を返す。
   * @return エラー番号
   */
  STATUS errnum() const noexcept { return ERRNUM(status_); }
};

} // namespace npp202009::npp

#endif // NPP202009_NPP_STATUS_HPP
