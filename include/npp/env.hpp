#ifndef NPP202009_NPP_ENV_HPP
#define NPP202009_NPP_ENV_HPP

#include "./type/lmbcs.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osenv.h>
#include <osfile.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp202009::npp::env {

Lmbcs getProgramDirectory() noexcept;
Lmbcs getDataDirectory() noexcept;

bool has(const Lmbcs &key) noexcept;

Lmbcs getString(
    const Lmbcs &key,
    const Lmbcs &defaultValue = ""
    ) noexcept;

void setString(const Lmbcs &key, const Lmbcs &value) noexcept;

template <class T>
T getNumber(const Lmbcs &key, T defaultValue = 0) noexcept;

template <class T = int>
void setInteger(const Lmbcs &key, T value) noexcept;

inline Lmbcs getProgramDirectory() noexcept {
  char path[MAXPATH] = "";
  OSGetExecutableDirectory(path);
  return Lmbcs(path);
}

inline Lmbcs getDataDirectory() noexcept {
  char path[MAXPATH] = "";
  WORD len = OSGetDataDirectory(path);
  return Lmbcs(path, static_cast<int>(len));
}

inline bool has(const Lmbcs &key) noexcept {
  return !getString(key).isEmpty();
}

inline Lmbcs getString(
    const Lmbcs &key,
    const Lmbcs &defaultValue
    ) noexcept {
  char buffer[MAXENVVALUE + 1];
  return OSGetEnvironmentString(key.constData(), buffer, MAXENVVALUE)
      ? Lmbcs(buffer)
      : defaultValue;
}

inline void setString(const Lmbcs &key, const Lmbcs &value) noexcept {
  OSSetEnvironmentVariable(key.constData(), value.constData());
}

template <class T>
T getNumber(const Lmbcs &key, T defaultValue) noexcept {
  return has(key)
      ? static_cast<T>(OSGetEnvironmentLong(key.constData()))
      : defaultValue;
}

template <class T>
void setInteger(const Lmbcs &key, T value) noexcept {
  OSSetEnvironmentInt(key.constData(), static_cast<int>(value));
}


} // namespace npp202009::npp::env

#endif // NPP202009_NPP_ENV_HPP
