#ifndef NPP202009_NPP_ID_HPP
#define NPP202009_NPP_ID_HPP

#include "./type/timedate.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfdata.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp {

template <typename T, typename F>
Lmbcs formatId(T id) {
  size_t w = sizeof(T) * 2; // 1バイト当たり2文字計算
  QByteArray s = QByteArray("0").repeated(w - 1);
  s += QByteArray::number(static_cast<F>(id), 16);
  s = s.right(w).toUpper();
  return Lmbcs(s.constData(), s.size());
}

inline TIMEDATE nullTimeDate() {
  return getConstantTIMEDATE<TIMEDATE_MINIMUM>();
}

class ReplicaId
{
  DBID value_;

public:
  ReplicaId(): value_(nullTimeDate()) {}

  ReplicaId(DBID &&td) : value_(std::move(td)) {}

  ReplicaId(const DBID &td) : value_(td) {}

  operator DBID() const { return value_; }

  Lmbcs toString() const {
    return formatId<DWORD,uint>(value_.Innards[1])
        + formatId<DWORD,uint>(value_.Innards[0]);
  }
};

class UniversalNoteId
{
  UNID value_;

public:
  UniversalNoteId(): value_({nullTimeDate(), nullTimeDate()}) {}

  UniversalNoteId(UNID &&unid): value_(std::move(unid)) {}

  UniversalNoteId(const UNID &unid): value_(unid) {}

  operator UNID() const { return value_; }

  Lmbcs toString() const {
    return formatId<DWORD,uint>(value_.File.Innards[1])
        + formatId<DWORD,uint>(value_.File.Innards[0])
        + formatId<DWORD,uint>(value_.Note.Innards[1])
        + formatId<DWORD,uint>(value_.Note.Innards[0]);
  }
};

using Unid = UniversalNoteId;

} // namespace npp202009::npp

#endif // NPP202009_NPP_ID_HPP
