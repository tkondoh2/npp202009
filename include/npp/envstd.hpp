#ifndef NPP202009_NPP_ENVSTD_HPP
#define NPP202009_NPP_ENVSTD_HPP

#include <string>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <osenv.h>
#include <osfile.h>
#include <names.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp202009::npp::envstd {

std::string getProgramDirectory() noexcept;
std::string getDataDirectory() noexcept;

bool has(const char *key) noexcept;

std::string getString(
    const char *key,
    const std::string &defaultValue = ""
    ) noexcept;

void setString(const char *key, const std::string &value) noexcept;

template <class T>
T getNumber(const char *key, T defaultValue = 0) noexcept;

template <class T = int>
void setInteger(const char *key, T value) noexcept;

inline std::string getProgramDirectory() noexcept {
  char path[MAXPATH] = "";
  OSGetExecutableDirectory(path);
  return std::string(path);
}

inline std::string getDataDirectory() noexcept {
  char path[MAXPATH] = "";
  WORD len = OSGetDataDirectory(path);
  return std::string(path, static_cast<int>(len));
}

inline bool has(const char *key) noexcept {
  return !getString(key).empty();
}

inline std::string getString(
    const char *key,
    const std::string &defaultValue
    ) noexcept {
  char buffer[MAXENVVALUE + 1];
  return OSGetEnvironmentString(key, buffer, MAXENVVALUE)
      ? std::string(buffer)
      : defaultValue;
}

inline bool hasStd(const char *key) noexcept {
  return !getString(key).empty();
}

inline void setString(const char *key, const std::string &value) noexcept {
  OSSetEnvironmentVariable(key, value.c_str());
}

template <class T>
T getNumber(const char *key, T defaultValue) noexcept {
  return hasStd(key)
      ? static_cast<T>(OSGetEnvironmentLong(key))
      : defaultValue;
}

template <class T>
void setInteger(const char *key, T value) noexcept {
  OSSetEnvironmentInt(key, static_cast<int>(value));
}

} // namespace npp202009::npp::envstd

#endif // NPP202009_NPP_ENVSTD_HPP
