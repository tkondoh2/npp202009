#ifndef NPP202009_NPP_NOTE_HPP
#define NPP202009_NPP_NOTE_HPP

#include "./type/lmbcs.hpp"
#include "./handle/base.hpp"
#include "./id.hpp"
#include <functional>
#include <QByteArray>
#include <rxcpp/rx.hpp>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfnote.h>
#include <stdnames.h>

#ifdef NT
#pragma pack(pop)
#endif

using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp {

class Item;
using AttachFileItem = std::tuple<Lmbcs,Item>;

class Database;
using DatabasePtr = QSharedPointer<Database>;

struct NoteHandleTraits
{
  using handle_type = NOTEHANDLE;
  static bool is_null(NOTEHANDLE h) { return h == NULLHANDLE; }
  static void deleter(NOTEHANDLE h) { NSFNoteClose(h); }
};

class Note;
using NotePtr = QSharedPointer<Note>;
using NoteHandle = handle::Base<NoteHandleTraits>;

class Note
    : public NoteHandle
{
public:
  Note(NOTEHANDLE &&hNote)
    : NoteHandle(std::move(hNote))
  {}

  operator NOTEHANDLE() const noexcept { return handle(); }

  bool hasItem(const Lmbcs &name) {
    return Note::hasItem(handle(), name);
  }

  observable<Item> getItem(const Lmbcs &name) {
    return Note::getItem(handle(), name);
  }

  QVector<Item> getItem_x(const Lmbcs &name) {
    return Note::getItem_x(handle(), name);
  }

  QVector<AttachFileItem> attachFileNames_x() {
    return Note::attachFileNames_x(handle());
  }

  Unid getUnid() const noexcept {
    return getUnid(handle());
  }

  WORD getNoteClass() const noexcept {
    return Note::getNoteClass(handle());
  }

  observable<AttachFileItem> attachFileNames() {
    return Note::attachFileNames(handle());
  }

  Lmbcs getTextItem(const Lmbcs &name) noexcept(false) {
    return Note::getTextItem(handle(), name);
  }

  void setTextItem(
      const Lmbcs &name,
      const Lmbcs &text,
      BOOL isSummary = TRUE
      ) noexcept(false) {
    Note::setTextItem(handle(), name, text, isSummary);
  }

  void update(WORD flags = 0) noexcept(false) {
    Note::update(handle(), flags);
  }

  template <WORD Type, class T>
  static T getInfo(NOTEHANDLE hNote);

  static Unid oidToUnid(const OID &oid) noexcept;

  static Unid getUnid(NOTEHANDLE hNote) noexcept;

//  static Lmbcs unidToText(const UNID &unid) noexcept;

//  static Lmbcs getUnidText(NOTEHANDLE hNote) noexcept;

  static WORD getNoteClass(NOTEHANDLE hNote) noexcept;

  static observable<NotePtr> open(
      DBHANDLE hDb,
      NOTEID noteId,
      WORD flags
      ) noexcept;

  static NotePtr open_x(
      DBHANDLE hDb,
      NOTEID noteId,
      WORD flags
      ) noexcept(false);

  static NotePtr create(DBHANDLE hDb) noexcept(false) {
    NOTEHANDLE handle = NULLHANDLE;
    Status status = NSFNoteCreate(hDb, &handle);
    if (status.failure()) { throw status; }
    return handle::make_ptr<Note>(std::move(handle));
  }

  static STATUS LNPUBLIC getFieldNamesCallback(
      WORD,
      WORD /*ItemFlags*/,
      char *Name,
      WORD NameLength,
      void * /*Value*/,
      DWORD /*ValueLength*/,
      void *pParam
      );

  static observable<Lmbcs> getFieldNames(NOTEHANDLE hNote);

  using DefaultScanData = std::tuple<WORD,Lmbcs,QByteArray>;

  static STATUS LNPUBLIC defaultScanItemsCallback(
      WORD,
      WORD ItemFlags,
      char *Name,
      WORD NameLength,
      void * Value,
      DWORD ValueLength,
      void *pParam
      );

  template <class T = DefaultScanData>
  static observable<T> scanItems(
      NOTEHANDLE hNote,
      NSFITEMSCANPROC cb = Note::defaultScanItemsCallback
      );

  static bool hasItem(NOTEHANDLE hNote, const Lmbcs &name);

  static observable<Item> getItem(NOTEHANDLE hNote, const Lmbcs &name);
  static QVector<Item> getItem_x(NOTEHANDLE hNote, const Lmbcs &name);

  static observable<AttachFileItem> attachFileNames(NOTEHANDLE hNote);
  static QVector<AttachFileItem> attachFileNames_x(NOTEHANDLE hNote);

  static Lmbcs getTextItem(NOTEHANDLE hNote, const Lmbcs &name) noexcept(false) {
    char text[256] = "";
    WORD len = NSFItemGetText(hNote, name.constData(), text, sizeof(text));
    std::replace(text, text + len, '\0', '\n');
    return Lmbcs(text, len);
  }

  static void setTextItem(
      NOTEHANDLE hNote,
      const Lmbcs &name,
      const Lmbcs &text,
      BOOL isSummary = TRUE
      ) noexcept(false) {
    Status status = NSFItemSetTextSummary(
          hNote,
          const_cast<char*>(name.constData()),
          const_cast<char*>(text.constData()),
          static_cast<WORD>(text.length()),
          isSummary
          );
    if (status.failure()) throw status;
  }

  static void update(NOTEHANDLE hNote, WORD flags = 0) noexcept(false) {
    Status status = NSFNoteUpdate(hNote, flags);
    if (status.failure()) throw status;
  }
};


template <WORD Type, class T>
T Note::getInfo(NOTEHANDLE hNote) {
  T value;
  NSFNoteGetInfo(hNote, Type, &value);
  return value;
}

inline Unid Note::oidToUnid(const OID &oid) noexcept {
  UNID unid {oid.File, oid.Note};
//  unid.File = oid.File;
//  unid.Note = oid.Note;
  return Unid(unid);
}

inline Unid Note::getUnid(NOTEHANDLE hNote) noexcept {
  return oidToUnid(getInfo<_NOTE_OID, OID>(hNote));
}

//inline Lmbcs Note::unidToText(const UNID &unid) noexcept {
//  return formatId<DWORD, uint>(unid.File.Innards[1])
//      + formatId<DWORD, uint>(unid.File.Innards[0])
//      + formatId<DWORD, uint>(unid.Note.Innards[1])
//      + formatId<DWORD, uint>(unid.Note.Innards[0])
//      ;
//}

//inline Lmbcs Note::getUnidText(NOTEHANDLE hNote) noexcept {
//  UNID unid = getUnid(hNote);
//  return unidToText(unid);
//}

inline WORD Note::getNoteClass(NOTEHANDLE hNote) noexcept {
  return getInfo<_NOTE_CLASS, WORD>(hNote);
}

inline observable<NotePtr> Note::open(
    DBHANDLE hDb,
    NOTEID noteId,
    WORD flags
    ) noexcept {
  return observable<>::create<NotePtr>([=](subscriber<NotePtr> s) {
    NOTEHANDLE handle = NULLHANDLE;
    Status status = NSFNoteOpen(hDb, noteId, flags, &handle);
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(handle::make_ptr<Note>(std::move(handle)));
    s.on_completed();
  });
}

inline NotePtr Note::open_x(
    DBHANDLE hDb,
    NOTEID noteId,
    WORD flags
    ) noexcept(false) {
  NOTEHANDLE handle = NULLHANDLE;
  Status status = NSFNoteOpen(hDb, noteId, flags, &handle);
  if (status.failure()) { throw status; }
  return handle::make_ptr<Note>(std::move(handle));
}

inline STATUS LNPUBLIC Note::getFieldNamesCallback(
    WORD,
    WORD /*ItemFlags*/,
    char *Name,
    WORD NameLength,
    void * /*Value*/,
    DWORD /*ValueLength*/,
    void *pParam
    ) {
  subscriber<Lmbcs> *s = reinterpret_cast<subscriber<Lmbcs>*>(pParam);
  s->on_next(Lmbcs(Name, NameLength));
  return NOERROR;
}

inline observable<Lmbcs> Note::getFieldNames(NOTEHANDLE hNote) {
  return scanItems<Lmbcs>(hNote, getFieldNamesCallback);
}

inline STATUS LNPUBLIC Note::defaultScanItemsCallback(
      WORD,
      WORD ItemFlags,
      char *Name,
      WORD NameLength,
      void * Value,
      DWORD ValueLength,
      void *pParam
    ) {
  auto s = reinterpret_cast<subscriber<Note::DefaultScanData>*>(pParam);
  auto v = std::make_tuple(
        ItemFlags,
        Lmbcs(Name, NameLength),
        QByteArray(static_cast<char*>(Value), static_cast<int>(ValueLength))
        );
  s->on_next(v);
  return NOERROR;
}

template <class T>
inline observable<T> Note::scanItems(
    NOTEHANDLE hNote,
    NSFITEMSCANPROC cb
    ) {
  return observable<>::create<T>([hNote,cb](subscriber<T> s) {
    Status status = NSFItemScan(hNote, cb, &s);
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_completed();
  });
}

} // namespace npp202009::npp

#endif // NPP202009_NPP_NOTE_HPP
