#ifndef NPP202009_NPP_HTTP_ERROR_HPP
#define NPP202009_NPP_HTTP_ERROR_HPP

#include <QtGlobal>
#include <dsapi.h>
#include <exception>

namespace npp202009::npp::http {

class Error
    : public std::exception
{
  uint id_;

public:
  Error(uint id)
    : std::exception()
    , id_(id)
  {}

  uint id() const { return id_; }

  virtual const char *what() const noexcept override {
    switch (id_) {
    case DSAPI_BUFFER_TOO_SMALL: return "[npp::http] Buffer too small";
    case DSAPI_INTERNAL_ERROR: return "[npp::http] Internal error";
    case DSAPI_INVALID_ARGUMENT: return "[npp::http] Invalid argument";
    case DSAPI_MEMORY_ERROR: return "[npp::http] Memory error";
    case DSAPI_REQUEST_ALREADY_OWNED: return "[npp::http] Request already owned";
    case 0: return "[npp::http] No error";
    default: return "[npp::http] Unknown error";
    }
  }
};

} // namespace npp202009::npp::http

#endif // NPP202009_NPP_HTTP_ERROR_HPP
