#ifndef NPP202009_NPP_HTTP_AUTHENTICATE_HPP
#define NPP202009_NPP_HTTP_AUTHENTICATE_HPP

#include "./context.hpp"

namespace npp202009::npp::http {

class Authenticate
{
  FilterAuthenticate *ptr_;

public:
  Authenticate(const Context &ctx)
    : ptr_(reinterpret_cast<FilterAuthenticate*>(ctx.ptr()))
  {}

  Lmbcs userName() const {
    return Lmbcs(reinterpret_cast<const char*>(ptr_->userName));
  }

  bool foundInCache() const {
    return ptr_->foundInCache != 0;
  }

  static char *setAuthName(const Authenticate &a, const Lmbcs &authName) {
    return qstrncpy(
          reinterpret_cast<char*>(a.ptr_->authName),
          authName.constData(),
          a.ptr_->authNameSize
          );
  }

  static void setAuthType(const Authenticate &a, FilterAuthenticationTypes type) {
    a.ptr_->authType = type;
  }

  template <int BufferSize = 1024>
  static observable<Lmbcs> getUserNameList(
      FilterContext *ctx,
      const Authenticate &auth
      ) {
    return observable<>::create<Lmbcs>([ctx, auth](subscriber<Lmbcs> s) {
      char buffer[BufferSize];
      uint count = 0, ErrID = 0;
      int size = auth.ptr_->GetUserNameList(
            ctx,
            reinterpret_cast<LMBCS*>(buffer),
            static_cast<uint>(BufferSize),
            &count,
            0,
            &ErrID
            );
      int p = 0, i = 0;
      while (((size >= 0 && p < size) || (size < 0 && p < BufferSize))
             && i++ < static_cast<int>(count)
             ) {
        s.on_next(Lmbcs(buffer + p));
        p += qstrlen(buffer + p) + 1;
      }
      s.on_completed();
    });
  }

  template <int BufferSize = 1024>
  static QList<Lmbcs> getUserNameList_x(
      FilterContext *ctx,
      const Authenticate &auth
      ) {
    QList<Lmbcs> list;
    char buffer[BufferSize];
    uint count = 0, ErrID = 0;
    int size = auth.ptr_->GetUserNameList(
          ctx,
          reinterpret_cast<LMBCS*>(buffer),
          static_cast<uint>(BufferSize),
          &count,
          0,
          &ErrID
          );
    int p = 0, i = 0;
    while (((size >= 0 && p < size) || (size < 0 && p < BufferSize))
           && i++ < static_cast<int>(count)
           ) {
      list.append(Lmbcs(buffer + p));
      p += qstrlen(buffer + p) + 1;
    }
    return list;
  }

  template <uint BufferSize = 1024>
  static observable<Lmbcs> getHeader(
      FilterContext *pCtx,
      const Authenticate &a,
      const Lmbcs &name
      ) {
    return observable<>::create<Lmbcs>([pCtx, a, name](subscriber<Lmbcs> s) {
      char buffer[BufferSize] = "";
      uint ErrID = 0;
      int bytes = a.ptr_->GetHeader(
            pCtx,
            const_cast<char*>(name.constData()),
            buffer,
            BufferSize,
            &ErrID
            );
      if (ErrID != 0) {
        s.on_error(std::make_exception_ptr(Error(ErrID)));
        return;
      }
      else if (bytes < 1) {
        s.on_next(Lmbcs());
      }
      else  {
        s.on_next(Lmbcs(buffer, bytes - 1));
      }
      s.on_completed();
    });
  }

  template <uint BufferSize = 1024>
  static Lmbcs getHeader_x(
      FilterContext *pCtx,
      const Authenticate &a,
      const Lmbcs &name
      ) {
    char buffer[BufferSize] = "";
    uint ErrID = 0;
    int bytes = a.ptr_->GetHeader(
          pCtx,
          const_cast<char*>(name.constData()),
          buffer,
          BufferSize,
          &ErrID
          );
    if (ErrID != 0) { throw Error(ErrID); }
    else if (bytes < 1) { return Lmbcs(); }
    return Lmbcs(buffer, bytes - 1);
  }
};

} // namespace npp202009::npp::http {

#endif // NPP202009_NPP_HTTP_AUTHENTICATE_HPP
