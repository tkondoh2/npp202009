#ifndef NPP202009_NPP_HTTP_DIRECTRESPONSE_HPP
#define NPP202009_NPP_HTTP_DIRECTRESPONSE_HPP

#include "./error.hpp"
#include "./context.hpp"
#include "./request.hpp"
#include "../utils/utils.hpp"
#include <QByteArray>
#include <QVariantMap>
#include <QTextStream>
#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp::http {

class DirectResponse
    : public QVariantMap
{
  uint code_;
  QString reasonText_;
  QString body_;

public:
  DirectResponse()
    : QVariantMap()
    , code_(200)
    , reasonText_("OK")
    , body_()
  {}

  DirectResponse(QString &&body)
    : QVariantMap()
    , code_(200)
    , reasonText_("OK")
    , body_(std::move(body))
  {}

  DirectResponse(uint code, QString &&reasonText, QString &&body)
    : QVariantMap()
    , code_(code)
    , reasonText_(std::move(reasonText))
    , body_(std::move(body))
  {}

  uint code() const { return code_; }
  const QString &reasonText() const { return reasonText_; }
  const QString &body() const { return body_; }
  void setContentType(const QString &value) {
    insert("Content-Type", value);
  }

  static Context response(const Context &ctx, DirectResponse &&res) {
    Context result = ctx;
    getRequest(ctx)
    .flat_map([&res](Request req) {
      return DirectResponse::render(req, res);
    })
    .flat_map([&ctx](QByteArray bytes) {
      return writeClient(ctx, bytes);
    })
    .subscribe([&result](bool) {
      result.setResult(kFilterHandledRequest);
      result.setProcessed(true);
    });
    return result;
  }

  static observable<QByteArray> render(
      const Request &req,
      const DirectResponse &res
      ) {
    return observable<>::create<QByteArray>(
          [req, res](subscriber<QByteArray> s) {
      QString buffer;
      QTextStream stream(&buffer, QIODevice::WriteOnly);
      stream << QString("%1 %2 %3")
                .arg(req.version())
                .arg(res.code())
                .arg(res.reasonText())
             << endl;

      foreach (QString key, res.keys()) {
        stream << QString("%1: %2")
                  .arg(key, res.value(key).toString())
               << endl;
      }
      if (!res.body().isEmpty()) {
        stream << endl << res.body();
      }
      stream.flush();
      s.on_next(buffer.toUtf8());
      s.on_completed();
    });
  }

  static QByteArray render_x(
      const Request &req,
      const DirectResponse &res
      ) {
    QString buffer;
    QTextStream stream(&buffer, QIODevice::WriteOnly);
    stream << QString("%1 %2 %3")
              .arg(req.version())
              .arg(res.code())
              .arg(res.reasonText())
           << endl;

    foreach (QString key, res.keys()) {
      stream << QString("%1: %2")
                .arg(key, res.value(key).toString())
             << endl;
    }
    stream << endl;
    if (!res.body().isEmpty()) {
      stream << res.body();
    }
    stream << endl;
    stream.flush();
    return buffer.toUtf8();
  }
};

} // namespace npp202009::npp::http

#endif // NPP202009_NPP_HTTP_DIRECTRESPONSE_HPP
