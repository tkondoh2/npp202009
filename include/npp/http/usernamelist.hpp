#ifndef NPP202009_NPP_HTTP_USERNAMELIST_HPP
#define NPP202009_NPP_HTTP_USERNAMELIST_HPP

#include "./context.hpp"

namespace npp202009::npp::http {

class UserNameList
{
  FilterUserNameList *ptr_;

public:
  UserNameList(const Context &ctx)
    : ptr_(reinterpret_cast<FilterUserNameList*>(ctx.ptr()))
  {}

  template <class T>
  T userName() const {
    return T(reinterpret_cast<const char*>(ptr_->userName));
  }
};

} // namespace npp202009::npp::http

#endif // NPP202009_NPP_HTTP_USERNAMELIST_HPP

