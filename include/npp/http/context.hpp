#ifndef NPP202009_NPP_HTTP_CONTEXT_HPP
#define NPP202009_NPP_HTTP_CONTEXT_HPP

#include "../type/lmbcs.hpp"
#include "./error.hpp"
#include "./request.hpp"
#include "./authenticateduser.hpp"
#include <QJsonDocument>
#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp::http {

class Context
{
  FilterContext *ctx_; /// コンテキストへのポインタ
  uint eventType_; /// イベント種別
  void *ptr_; /// イベント別データへのポインタ
  uint result_; /// HttpFilterProcへの戻り値
  bool processed_; /// 処理済みの場合は真

public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Context()
    : ctx_(nullptr)
    , eventType_(0)
    , ptr_(nullptr)
    , result_(0)
    , processed_(false)
  {}

  /**
   * @brief コンストラクタ
   * @param ctx コンテキスト
   * @param eventType イベント種別
   * @param ptr イベント別データへのポインタ
   */
  Context(FilterContext *ctx, uint eventType, void *ptr)
    : ctx_(ctx)
    , eventType_(eventType)
    , ptr_(ptr)
    , result_(kFilterNotHandled)
    , processed_(false)
  {}

  FilterContext *data() const { return ctx_; }

  /**
   * @brief コンテキスト
   * @return コンテキスト
   */
  FilterContext *operator ->() const { return ctx_; }

  /**
   * @brief イベント種別
   * @return イベント種別
   */
  uint eventType() const { return eventType_; }

  /**
   * @brief イベント別データへのポインタ
   * @return イベント別データへのポインタ
   */
  void *ptr() const { return ptr_; }

  /**
   * @brief HttpFilterProcへの戻り値
   * @return HttpFilterProcへの戻り値
   */
  uint result() const { return result_; }

  void setResult(uint value) { result_ = value; }

  /**
   * @brief 処理済みかどうか
   * @return 処理済みかどうか
   */
  bool processed() const { return processed_; }

  void setProcessed(bool value) { processed_ = value; }

  /**
   * @brief プライベートコンテキスト
   * @return プライベートコンテキスト
   */
  void *privateContext() const { return ctx_->privateContext; }

  void setPrivateContext(void *p) { ctx_->privateContext = p; }
};

/**
 * @brief リクエストデータの取得
 * @param ctx コンテキスト
 * @return リクエストのオブザーバブル
 */
inline observable<Request> getRequest(const Context &ctx) {
  return observable<>::create<Request>(
        [ctx](subscriber<Request> s) {
    FilterRequest request;
    uint ErrID = 0;
    bool result = ctx->GetRequest(ctx.data(), &request, &ErrID);
    if (!result || ErrID != 0) {
      s.on_error(std::make_exception_ptr(Error(ErrID)));
    }
    else {
      s.on_next(Request(request));
      s.on_completed();
    }
  });
}

/**
 * @brief 未加工ボディの取得
 * @param ctx コンテキスト
 * @return 未加工ボディのオブザーバブル
 */
inline observable<QByteArray> getRawBody(const Context &ctx) {
  return observable<>::create<QByteArray>(
        [ctx](subscriber<QByteArray> s) {
    char* pBody;
    uint ErrID = 0;
    int size = ctx->GetRequestContents(ctx.data(), &pBody, &ErrID);
    if (size < 0 || ErrID != 0) {
      s.on_error(std::make_exception_ptr(Error(ErrID)));
    }
    else {
      s.on_next(QByteArray(pBody, size));
      s.on_completed();
    }
  });
}

/**
 * @brief ボディデータをクエリ文字列形式でパース(フォームデータなど)
 * @param body ボディデータ
 * @return マップコンテナ
 */
inline QVariantMap urlEncodedToMap(const QString &body) {
  QUrlQuery q(body);
  return pairsToMap(q.queryItems(QUrl::FullyDecoded));
}

/**
 * @brief ボディデータをJSON形式でパース(APIデータなど)
 * @param raw ボディデータ
 * @return JSON文書のオブザーバブル
 */
inline observable<QJsonDocument> json(const QByteArray &raw) {
  return observable<>::create<QJsonDocument>(
        [raw](subscriber<QJsonDocument> s) {
    QJsonParseError parseError;
    QJsonDocument json = QJsonDocument::fromJson(raw, &parseError);
    if (parseError.error != QJsonParseError::NoError) {
      s.on_error(
            std::make_exception_ptr(
              std::runtime_error(parseError.errorString().toStdString())
              )
            );
      return;
    }
    s.on_next(json);
    s.on_completed();
  });
}

/**
 * @brief レスポンスデータを書き込む
 * @param ctx コンテキスト
 * @param bytes 書き込みバイト列
 * @return エラーなく処理できれば真のオブザーバブル
 */
inline observable<bool> writeClient(
    const Context &ctx,
    const QByteArray &bytes
    ) {
  return observable<>::create<bool>([ctx, bytes](subscriber<bool> s) {
    uint errId = 0;
    if (!ctx->WriteClient(
          ctx.data(),
          const_cast<char*>(bytes.constData()),
          static_cast<uint>(bytes.size()),
          0,
          &errId
          )
        || errId != 0) {
      s.on_error(std::make_exception_ptr(Error(errId)));
      return;
    }
    s.on_next(true);
    s.on_completed();
  });
}

inline bool writeClient_x(
    const Context &ctx,
    const QByteArray &bytes
    ) {
  uint errId = 0;
  if (!ctx->WriteClient(
        ctx.data(),
        const_cast<char*>(bytes.constData()),
        static_cast<uint>(bytes.size()),
        0,
        &errId
        )
      || errId != 0) {
    throw Error(errId);
  }
  return true;
}

/**
 * @brief DSAPIが後始末するメモリ割り当ての取得
 * @param ctx コンテキスト
 * @param size 割り当てサイズ
 * @return 割り当てたメモリへのポインタのオブザーバブル
 */
inline observable<void*> allocMem(
    const Context &ctx,
    uint size
    ) {
  return observable<>::create<void*>(
        [ctx, size](subscriber<void*> s) {
    uint ErrID = 0;
    void *p = ctx->AllocMem(ctx.data(), size, 0, &ErrID);
    if (ErrID != 0) {
      s.on_error(std::make_exception_ptr(Error(ErrID)));
    }
    else {
      s.on_next(p);
      s.on_completed();
    }
  });
}

/**
 * @brief サーバサポートメソッドの実行
 * @tparam FuncType 実行タイプ
 * @tparam T 戻り値の型
 * @tparam U 引数1の型
 * @param ctx コンテキスト
 * @param data1 引数1
 * @return T型のインスタンスのオブザーバブル
 */
template <uint FuncType, class T, class U>
observable<T> serverSupport(const Context &ctx, const U &data1) {
  return observable<>::create<T>(
        [ctx, data1](subscriber<T> s) {
    U _data1 = data1;
    uint ErrID = 0;
    bool result = ctx->ServerSupport(
          ctx.data(),
          FuncType,
          &_data1,
          nullptr,
          0,
          &ErrID
          );
    if (!result || ErrID != 0) {
      s.on_error(std::make_exception_ptr(Error(ErrID)));
    }
    else {
      s.on_next(T(_data1));
      s.on_completed();
    }
  });
}

template <uint FuncType, class T, class U>
T serverSupport_x(const Context &ctx, const U &data1) {
  U _data1 = data1;
  uint ErrID = 0;
  bool result = ctx->ServerSupport(
        ctx.data(),
        FuncType,
        &_data1,
        nullptr,
        0,
        &ErrID
        );
  if (!result || ErrID != 0) { throw Error(ErrID); }
  return T(_data1);
}

/**
 * @brief 認証済みユーザの取得
 * @param ctx コンテキスト
 * @param fieldFlags 取得したいフィールドのフラグ
 * @return 認証済みユーザ構造体のオブザーバブル
 */
inline observable<AuthenticatedUser> getAuthenticatedUser(
    const Context &ctx,
    uint fieldFlags
    ) {
  AuthenticatedUser authUser(fieldFlags);
  return serverSupport<
      kGetAuthenticatedUserInfo,
      AuthenticatedUser
      >(ctx, authUser.constData());
}

inline AuthenticatedUser getAuthenticatedUser_x(
    const Context &ctx,
    uint fieldFlags
    ) {
  AuthenticatedUser authUser(fieldFlags);
  return serverSupport_x<
      kGetAuthenticatedUserInfo,
      AuthenticatedUser
      >(ctx, authUser.constData());
}

} // namespace npp202009::npp::http

#endif // NPP202009_NPP_HTTP_CONTEXT_HPP
