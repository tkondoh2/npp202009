#ifndef NPP202009_NPP_HTTP_INSTALL_HPP
#define NPP202009_NPP_HTTP_INSTALL_HPP

#include "./error.hpp"
#include <QByteArray>

namespace npp202009::npp::http {

/**
 * @brief DSAPIのeventFlagsのOR演算子で合成する。
 * @tparam T eventFlagsの型(通常uint)
 * @tparam Head eventFlags静的メンバ定数を持つ先頭のクラス
 * @tparam Tails eventFlags静的メンバ定数を持つ2番目以降のクラス
 * @return 合成されたeventFlags値
 */
template <class T, class Head, class... Tails>
T eventFlagsOr() {
  return Head::eventFlags | eventFlagsOr<T, Tails...>();
}

/**
 * @brief DSAPIのeventFlags合成で終端値を返す。
 * 不定数のクラスを処理するためのテンプレート関数
 * @tparam T eventFlagsの型(通常uint)
 * @return 0
 */
template <class T>
T eventFlagsOr() {
  return 0;
}

/**
 * @brief DSAPI処理モジュールをインストールする。
 * @tparam Args eventFlags静的メンバ定数を持つ1つ以上のクラス
 * @param pInitData DSAPI初期化オブジェクトへのポインタ
 * @param title DSAPIアドイン登録名
 */
template <class... Args>
void install(
    FilterInitData *pInitData,
    const char *title
    )
{
  pInitData->appFilterVersion = kInterfaceVersion;
  pInitData->eventFlags = eventFlagsOr<uint, Args...>();
  qstrcpy(pInitData->filterDesc, title);
}

inline void install_x(
    FilterInitData *pInitData,
    const char *title,
    uint eventFlags
    ) {
  pInitData->appFilterVersion = kInterfaceVersion;
  pInitData->eventFlags = eventFlags;
  qstrcpy(pInitData->filterDesc, title);
}

} // namespace npp202009::npp::http

#endif // NPP202009_NPP_HTTP_INSTALL_HPP
