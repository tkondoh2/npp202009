#ifndef NPP202009_NPP_HTTP_EXCEPTION_HPP
#define NPP202009_NPP_HTTP_EXCEPTION_HPP

#include "./context.hpp"
#include "./directresponse.hpp"
//#include <QString>
//#include <QVariantMap>
//#include <exception>

namespace npp202009::npp::http {

class Exception
    : public std::exception
{
  QVariantMap headers_;
  uint responseCode_;
  QString reasonText_;
  QString errorCode_;
  QString description_;
  QVariantMap options_;

  Exception(
      QVariantMap &&headers,
      uint responseCode,
      QString &&reasonText,
      QString &&errorCode,
      QString &&description,
      QVariantMap &&options = QVariantMap()
      )
    : std::exception()
    , headers_(std::move(headers))
    , responseCode_(responseCode)
    , reasonText_(std::move(reasonText))
    , errorCode_(std::move(errorCode))
    , description_(std::move(description))
    , options_(std::move(options))
  {}

public:
  Exception(const Exception &other) = default;

  virtual const char *what() const noexcept override {
    return QString("%1: %2")
        .arg(errorCode_)
        .arg(description_)
        .toUtf8()
        .constData();
  }

  const QVariantMap &headers() const { return headers_; }
  uint responseCode() const { return responseCode_; }
  const QString &reasonText() const { return reasonText_; }
  const QString &errorCode() const { return errorCode_; }
  const QString &description() const { return description_; }
  const QVariantMap &options() const { return options_; }

  bool isHtml() const {
    return (headers_.value("Content-Type").toString()
        == "text/html; charset=utf-8")
        && !headers_.contains("Location");
  }

  bool isJson() const {
    return headers_.value("Content-Type").toString()
        == "application/json";
  }

  bool isRedirect() const {
    return (headers_.value("Content-Type").toString()
        == "text/html; charset=utf-8")
        && headers_.contains("Location");
  }

  static Exception makeHtml(
      uint responseCode,
      QString &&reasonText,
      QString &&errorCode,
      QString &&description,
      QVariantMap &&options = QVariantMap(),
      const QVariantMap &headers = QVariantMap()
      ) {
    QVariantMap h(headers);
    if (!h.contains("Content-Type")) {
      h.insert("Content-Type", "text/html; charset=utf-8");
    }
    return Exception(
          std::move(h),
          responseCode,
          std::move(reasonText),
          std::move(errorCode),
          std::move(description),
          std::move(options)
          );
  }

  static Exception makeRedirect(
      QString &&redirectUri,
      QString &&errorCode,
      QString &&description,
      QVariantMap &&options = QVariantMap(),
      const QVariantMap &headers = QVariantMap()
      ) {
    QVariantMap h(headers);
    if (!h.contains("Content-Type")) {
      h.insert("Content-Type", "text/html; charset=utf-8");
    }
    if (!h.contains("Location")) {
      h.insert("Location", std::move(redirectUri));
    }
    return Exception(
          std::move(h),
          302,
          "Found",
          std::move(errorCode),
          std::move(description),
          std::move(options)
          );
  }

  static Exception makeJson(
      uint responseCode,
      QString &&reasonText,
      QString &&errorCode,
      QString &&description,
      QVariantMap &&options = QVariantMap(),
      const QVariantMap &headers = QVariantMap()
      ) {
    QVariantMap h(headers);
    if (!h.contains("Content-Type")) {
      h.insert("Content-Type", "application/json");
    }
    return Exception(
          std::move(h),
          responseCode,
          std::move(reasonText),
          std::move(errorCode),
          std::move(description),
          std::move(options)
          );
  }

  static Context responseHtmlError(
      const Context &ctx,
      const Exception &ex,
      QString &&page
      ) {
    if (page.isEmpty()) {
      page = R"(<!DOCTYPE html>
<html><head><title>${response_code} ${reason_text}</title></head>
<body>
<h1>${response_code} ${reason_text}</h1>
<p>code: ${error_code}</p>
<p>${error_description}</p>
${options}
</body> </html>)";
    }
    page.replace("${response_code}", QString::number(ex.responseCode()));
    page.replace("${reason_text}", ex.reasonText());
    page.replace("${error_code}", ex.errorCode());
    page.replace("${error_description}", ex.description());

    QStringList optList;
    foreach (QString key, ex.options().keys()) {
      QString value = ex.options().value(key).toString();
      optList.append(QString("<dt>%1</dt><dd>%2</dd>").arg(key).arg(value));
    }
    if (!optList.isEmpty()) {
      page.replace("${options}", QString("<dl>%1</dl>").arg(optList.join("")));
    }

    return DirectResponse::response(
          ctx,
          DirectResponse(
            ex.responseCode(),
            QString(ex.reasonText()),
            std::move(page)
            )
          );
  }

  static Context responseRedirectError(
      const Context &ctx,
      const Exception &ex
      ) {
    DirectResponse res(ex.responseCode(), QString(ex.reasonText()), "");
    res.unite(ex.headers());
    QUrl url(res.take("Location").toString());
    QUrlQuery query(url);
    query.addQueryItem("error", encodeForQueryItem(ex.errorCode()));
    query.addQueryItem("error_description", encodeForQueryItem(ex.description()));

    foreach (QString key, ex.options().keys()) {
      query.addQueryItem(
            key,
            encodeForQueryItem(ex.options().value(key).toString())
            );
    }

    url.setQuery(query);
    res.insert("Location", url.url(QUrl::FullyEncoded));
    return DirectResponse::response(ctx, std::move(res));
  }

  static Context responseJsonError(
      const Context &ctx,
      const Exception &ex
      ) {
    QVariantMap data {
      { "error", ex.errorCode() },
      { "error_description", ex.description() }
    };

    foreach (QString key, ex.options().keys()) {
      QString value = ex.options().value(key).toString();
      data.insert(key, value);
    }

    QJsonDocument json = QJsonDocument::fromVariant(data);
    DirectResponse res(
      ex.responseCode(),
      QString(ex.reasonText()),
      QString::fromUtf8(json.toJson(QJsonDocument::Compact))
      );
    res.unite(ex.headers());
    return DirectResponse::response(ctx, std::move(res));
  }
};

} // namespace npp202009::npp::http

#endif // NPP202009_NPP_HTTP_EXCEPTION_HPP
