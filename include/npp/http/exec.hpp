#ifndef NPP202009_NPP_HTTP_EXEC_HPP
#define NPP202009_NPP_HTTP_EXEC_HPP

#include "./context.hpp"

namespace npp202009::npp::http {

/**
 * @brief プロセスの実行
 * @tparam T ヘルパークラス
 * @param ctx コンテキスト
 * @return 処理結果コンテキストのオブザーバブル
 */
template <class T>
observable<Context> exec(Context ctx) {
  return (!ctx.processed() && T::matchEventType(ctx.eventType()))
      ? T::exec(ctx)
      : observable<>::just(ctx);
}

template <class T>
Context exec_x(const Context &ctx) {
  return (!ctx.processed() && T::matchEventType(ctx.eventType()))
      ? T(ctx)()
      : ctx;
}

class Filter
{
  Context ctx_;

public:
  Filter(const Context &ctx) : ctx_(ctx) {}

  const Context &context() const { return ctx_; }
  void setContext(const Context &ctx) { ctx_ = ctx; }

  virtual Context operator ()() = 0;

  /**
   * @brief リクエストデータの取得
   * @param ctx コンテキスト
   * @return リクエストのオブザーバブル
   */
  static Request getRequest(const Context &ctx) {
    FilterRequest request;
    uint ErrID = 0;
    auto result = ctx->GetRequest(ctx.data(), &request, &ErrID);
    if (!result || ErrID != 0) {
      throw Error(ErrID);
    }
    return Request(request);
  }

  /**
   * @brief 未加工ボディの取得
   * @param ctx コンテキスト
   * @return 未加工ボディのオブザーバブル
   */
  static QByteArray getRawBody(const Context &ctx) {
    char* pBody;
    uint ErrID = 0;
    int size = ctx->GetRequestContents(ctx.data(), &pBody, &ErrID);
    if (size < 0 || ErrID != 0) {
      throw Error(ErrID);
    }
    return QByteArray(pBody, size);
  }
};

/**
 * @brief プロセス順次実行の終端処理
 * @tparam T コンテキストの型(通常Context)
 * @param ctx コンテキスト
 * @return 処理結果コンテキストのオブザーバブル
 */
template <class T>
observable<T> _execute(T ctx) {
  return observable<>::just(ctx);
}

/**
 * @brief プロセスを順次実行するインナー関数
 * @tparam T コンテキストの型(通常Context)
 * @tparam Head 最初のヘルパークラス
 * @tparam Tails 2番目以降のヘルパークラス
 * @param ctx コンテキスト
 * @return 処理結果コンテキストのオブザーバブル
 */
template <class T, class Head, class... Tails>
observable<T> _execute(T ctx) {
  return exec<Head>(ctx)
      .flat_map([](T ctx) {
    return _execute<T, Tails...>(ctx);
  });
}

/**
 * @brief プロセスを順次実行
 * @tparam Args ヘルパークラスリスト
 * @param ctx コンテキスト
 * @return 処理結果コンテキストのオブザーバブル
 */
template <class... Args>
observable<Context> execute(Context ctx) {
  return _execute<Context, Args...>(ctx);
}

} // namespace npp202009::npp::http

#endif // NPP202009_NPP_HTTP_EXEC_HPP
