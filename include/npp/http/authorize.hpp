#ifndef NPP202009_NPP_HTTP_AUTHORIZE_HPP
#define NPP202009_NPP_HTTP_AUTHORIZE_HPP

#include "./context.hpp"

namespace npp202009::npp::http {

class Authorize
{
  FilterAuthorize *ptr_;

public:
  Authorize(const Context &ctx)
    : ptr_(reinterpret_cast<FilterAuthorize*>(ctx.ptr()))
  {}

  uint isAuthorized() const { return ptr_->isAuthorized; }
};

} // namespace npp202009::npp::http

#endif // NPP202009_NPP_HTTP_AUTHORIZE_HPP
