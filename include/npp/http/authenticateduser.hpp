#ifndef NPP202009_NPP_HTTP_AUTHENTICATEDUSER_HPP
#define NPP202009_NPP_HTTP_AUTHENTICATEDUSER_HPP

#include "./error.hpp"
#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp::http {

class AuthenticatedUser
{
  FilterAuthenticatedUser value_;

public:
  AuthenticatedUser(uint flags)
    : value_()
  {
    value_.fieldFlags = static_cast<FilterAuthenticatedUserFields>(flags);
  }

  AuthenticatedUser(const FilterAuthenticatedUser &value) : value_(value) {}

  FilterAuthenticatedUser &constData() { return value_; }

  template <class T>
  T userCanonicalName() const {
    return T(value_.pUserCannonicalName);
  }

  template <class T>
  T webUserName() const {
    return T(value_.pWebUserName);
  }

  template <class T>
  T userPassword() const {
    return T(value_.pUserPassword);
  }

  template <class T>
  T userGroupList() const {
    return T(value_.pUserGroupList);
  }
};

} // namespace npp202009::npp::http

#endif // NPP202009_NPP_HTTP_AUTHENTICATEDUSER_HPP
