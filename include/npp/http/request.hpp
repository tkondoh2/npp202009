#ifndef NPP202009_NPP_HTTP_REQUEST_HPP
#define NPP202009_NPP_HTTP_REQUEST_HPP

#include "./error.hpp"
#include "../utils/utils.hpp"
#include <QUrl>
#include <QUrlQuery>
#include <QVariantMap>
#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

namespace npp202009::npp::http {

class Request
{
  FilterRequest value_;

public:
  Request(FilterRequest value) : value_(value) {}

  char *version() const { return value_.version; }

  uint method() const { return value_.method; }

  QUrl url(const QString &dummyHost = "http://dummy") const {
    return QUrl(QString("%1%2").arg(dummyHost, value_.URL));
  }

  QString path(
      QUrl::ComponentFormattingOption options = QUrl::FullyDecoded
      ) const {
    return url().path(options);
  }

  bool matchUrl(
      const QString &path_,
      QUrl::ComponentFormattingOption options = QUrl::FullyDecoded
      ) const {
    return path(options) == path_;
  }

  bool matchUrl(
      const QRegExp &reg,
      QUrl::ComponentFormattingOption options = QUrl::FullyDecoded
      ) const {
    return reg.exactMatch(path(options));
  }

  QStringList parsePathParams(
      QUrl::ComponentFormattingOption options = QUrl::FullyDecoded
      ) const {
    return path(options).split("/");
  }

  QVariantMap queryMap(
      QUrl::ComponentFormattingOption options = QUrl::FullyDecoded
      ) const {
    QUrlQuery q(url());
    return pairsToMap(q.queryItems(options));
  }

  QByteArray rawBody() const {
    return QByteArray(
          value_.contentRead,
          static_cast<int>(value_.contentReadLen)
          );
  }
};

} // namespace npp202009::npp::http

#endif // NPP202009_NPP_HTTP_REQUEST_HPP
