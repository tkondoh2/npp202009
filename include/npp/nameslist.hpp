#ifndef NPP202009_NPP_NAMESLIST_HPP
#define NPP202009_NPP_NAMESLIST_HPP

#include "./handle/lockable.hpp"
#include "./type/lmbcs.hpp"

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfdb.h> // NSFBuildNamesList
#include <acl.h> // NAMES_LIST

#ifdef NT
#pragma pack(pop)
#endif

namespace npp202009::npp {

#if defined(UNIX) || defined(OS2_2x) || defined(W32)
using AuthenticatedType = DWORD;		/*	Authentication flags */
#else
using AuthenticatedType = WORD;		/*	Authentication flags */
#endif


struct NamesListHandleTraits
{
  using handle_type = DHANDLE;
  static bool is_null(DHANDLE h) { return h == NULLHANDLE; }
  static void deleter(DHANDLE h) { OSMemFree(h); }
};

class NamesList;
using NamesListPtr = QSharedPointer<NamesList>;
using NamesListHandle = handle::Lockable<NamesListHandleTraits>;

class NamesList
    : public NamesListHandle
{
public:
  NamesList() : NamesListHandle(NULLHANDLE) {}

  NamesList(DHANDLE &&handle) : NamesListHandle(std::move(handle)) {}

  virtual ~NamesList() = default;

  void setAuthenticated(AuthenticatedType authType);

  static observable<NamesListPtr> build(const Lmbcs &name);
};

inline void NamesList::setAuthenticated(AuthenticatedType authType) {
  NAMES_LIST *pList = lock<NAMES_LIST>();
  pList->Authenticated = authType;
  unlock();
}

inline observable<NamesListPtr> NamesList::build(const Lmbcs &name) {
  return observable<>::create<NamesListPtr>([name](subscriber<NamesListPtr> s) {
    DHANDLE handle = NULLHANDLE;
    Status status = NSFBuildNamesList(
          const_cast<char*>(name.constData()),
          0,
          &handle
          );
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(handle::make_ptr<NamesList>(std::move(handle)));
    s.on_completed();
  });
}

} // namespace npp202009::npp

#endif // NPP202009_NPP_NAMESLIST_HPP
