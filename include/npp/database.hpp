#ifndef NPP202009_NPP_DATABASE_HPP
#define NPP202009_NPP_DATABASE_HPP

#include "nameslist.hpp"
#include "type/lmbcslist.hpp"
#include "id.hpp"
#include <QtDebug>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfdb.h>
#include <osfile.h> // OSPathNetConstruct
#include <nsfobjec.h> // NSFDbGetObjectSize

#ifdef NT
#pragma pack(pop)
#endif

#include <QTextStream>

namespace npp202009::npp {

struct DatabaseHandleTraits
{
  using handle_type = DBHANDLE;
  static bool is_null(DBHANDLE h) { return h == NULLHANDLE; }
  static void deleter(DBHANDLE h) {
    STATUS status = NSFDbClose(h);
    if (ERR(status) != NOERROR) {
      fprintf(stderr, "NSFDbClose Error(code=%u).", ERR(status));
    }
  }
};

class Database;
using DatabasePtr = QSharedPointer<Database>;
using DatabaseHandle = handle::Base<DatabaseHandleTraits>;

class Database
    : public DatabaseHandle
{
public:
  class Path;

  class Paths;

  class NoteInfo;

  using ObjectSize = std::tuple<DWORD,WORD,WORD>;

  Database() : DatabaseHandle(NULLHANDLE) {}

  Database(DBHANDLE &&handle) : DatabaseHandle(std::move(handle)) {}

  virtual ~Database() = default;

  operator DBHANDLE() const noexcept { return handle(); }

  observable<ReplicaId> getReplicaId() noexcept {
    return Database::getReplicaId(handle());
  }

  ReplicaId getReplicaId_x() noexcept(false) {
    return Database::getReplicaId_x(handle());
  }

  template <WORD what>
  Lmbcs getInfo_x() noexcept(false) {
    return getInfo_x<what>(handle());
  }

//  observable<Lmbcs> getReplicaIdText() noexcept {
//    return Database::getReplicaIdText(handle());
//  }

  template <class T = Database>
  static observable<QSharedPointer<T>> open(const Path &netpath) noexcept;

  template <class T = Database>
  static QSharedPointer<T> open_x(const Path &netpath) noexcept(false);

  template <class T = Database>
  static QSharedPointer<T> reopen_x(DBHANDLE hDB) noexcept(false);

  template <class T = Database>
  static observable<QSharedPointer<T>> openEx(
      const Path &netpath,
      const NamesListPtr &pNamesList
      ) noexcept;

//  static Lmbcs getIdText(TIMEDATE id) noexcept;

  static observable<DBREPLICAINFO> getReplicaInfo(DBHANDLE handle) noexcept;

  static observable<ReplicaId> getReplicaId(DBHANDLE handle) noexcept;

  static DBREPLICAINFO getReplicaInfo_x(DBHANDLE handle) noexcept(false);

  static ReplicaId getReplicaId_x(DBHANDLE handle) noexcept(false);

//  static observable<Lmbcs> getReplicaIdText(DBHANDLE handle) noexcept;

  static observable<Lmbcs> getAccessingUserName(DBHANDLE handle) noexcept;
  static Lmbcs getAccessingUserName_x(DBHANDLE handle) noexcept(false);

  static observable<Paths> getPaths(DBHANDLE hDB) noexcept;
  static Paths getPaths_x(DBHANDLE hDB) noexcept(false);

  template <WORD what>
  static observable<Lmbcs> getInfo(DBHANDLE hDB) noexcept;

  template <WORD what>
  static Lmbcs getInfo_x(DBHANDLE hDB) noexcept(false);

  static observable<NoteInfo> getNoteInfo(DBHANDLE hDB, NOTEID noteId) noexcept;

  static observable<DBID> getId(DBHANDLE hDB) noexcept;

//  static observable<Lmbcs> getIdText(DBHANDLE hDB) noexcept;

  static observable<ObjectSize> getObjectSize(DBHANDLE hDB, DWORD objectId, WORD objectType) noexcept;
};

observable<Lmbcs> make_netpath(const Database::Path &dbPath) noexcept;

Lmbcs makeNetpath_x(const Database::Path &dbPath) noexcept(false);

observable<Database::Path> parse_netpath(const Lmbcs &netpath) noexcept;

class Database::Path
{
  Lmbcs server_;
  Lmbcs path_;
  Lmbcs port_;

public:
  Path() noexcept : server_(), path_(), port_() {}

  Path(
      const Lmbcs &path,
      const Lmbcs &server,
      const Lmbcs &port = Lmbcs()
      ) noexcept
    : server_(server)
    , path_(path)
    , port_(port)
  {}

  const Lmbcs &server() const noexcept { return server_; }

  const Lmbcs &path() const noexcept { return path_; }

  const Lmbcs &port() const noexcept { return port_; }
};

class Database::Paths
{
  Lmbcs canonical_;
  Lmbcs expanded_;

public:
  Paths() : canonical_(), expanded_() {}

  Paths(Lmbcs &&canonical, Lmbcs &&expanded)
    : canonical_(std::move(canonical))
    , expanded_(std::move(expanded))
  {}

  const Lmbcs &canonical() const { return canonical_; }
  const Lmbcs &expanded() const { return expanded_; }

  friend class Database;
};

class Database::NoteInfo
{
  OID oid_;
  TIMEDATE modified_;
  WORD noteClass_;

public:
  NoteInfo() : oid_(), modified_(), noteClass_(0) {}

  const OID &oid() const { return oid_; }
  const TIMEDATE &modified() const { return modified_; }
  WORD noteClass() const { return noteClass_; }

  friend class Database;
};

inline observable<Lmbcs> make_netpath(const Database::Path &dbPath) noexcept {
  return observable<>::create<Lmbcs>([dbPath](subscriber<Lmbcs> s) {
    char netPath[MAXPATH] = "";
    Status status = OSPathNetConstruct(
          dbPath.port().isEmpty() ? nullptr : dbPath.port().constData(),
          dbPath.server().constData(),
          dbPath.path().constData(),
          netPath
          );
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(Lmbcs(netPath));
    s.on_completed();
  });
}

inline Lmbcs makeNetpath_x(const Database::Path &dbPath) noexcept(false) {
  char netPath[MAXPATH] = "";
  Status status = OSPathNetConstruct(
        dbPath.port().isEmpty() ? nullptr : dbPath.port().constData(),
        dbPath.server().constData(),
        dbPath.path().constData(),
        netPath
        );
  if (status.failure()) { throw status; }
  return Lmbcs(netPath);
}

inline observable<Database::Path> parse_netpath(const Lmbcs &netpath) noexcept {
  return observable<>::create<Database::Path>(
        [netpath](subscriber<Database::Path> s
        ) {
    char port[MAXPATH], server[MAXPATH], file[MAXPATH];
    Status status = OSPathNetParse(netpath.constData(), port, server, file);
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(Database::Path(Lmbcs(file), Lmbcs(server), Lmbcs(port)));
    s.on_completed();
  });
}

template <class T>
observable<QSharedPointer<T>> Database::open(
    const Database::Path &path
    ) noexcept {
  return make_netpath(path)
      .flat_map([](Lmbcs netpath) {
    DBHANDLE handle = NULLHANDLE;
    Status status = NSFDbOpen(netpath.constData(), &handle);
    observable<QSharedPointer<T>> result;
    if (status.failure()) {
      result = observable<>::error<QSharedPointer<T>>(status);
    }
    else {
      result = observable<>::just(handle::make_ptr<T>(std::move(handle)));
    }
    return result;
  });
}

template <class T>
QSharedPointer<T> Database::open_x(
    const Database::Path &path
    ) noexcept(false) {
  auto netpath = makeNetpath_x(path);
  DBHANDLE handle = NULLHANDLE;
  Status status = NSFDbOpen(netpath.constData(), &handle);
  if (status.failure()) { throw status; }
  return handle::make_ptr<T>(std::move(handle));
}

template <class T>
QSharedPointer<T> Database::reopen_x(DBHANDLE hDB) {
  DBHANDLE handle = NULLHANDLE;
  Status status = NSFDbReopen(hDB, &handle);
  if (status.failure()) { throw status; }
  return handle::make_ptr<T>(std::move(handle));
}

template <class T>
observable<QSharedPointer<T>> Database::openEx(
    const Database::Path &netpath,
    const NamesListPtr &pNameList
    ) noexcept {
  return make_netpath(netpath)
      .flat_map([pNameList](Lmbcs path) {
    DBHANDLE handle = NULLHANDLE;
    Status status = NSFDbOpenExtended(
          path.constData(),
          0,
          pNameList->handle(),
          nullptr,
          &handle,
          nullptr,
          nullptr
          );
    observable<QSharedPointer<T>> result;
    if (status.failure()) {
      result = observable<>::error<QSharedPointer<T>>(status);
    }
    else {
      result = observable<>::just(handle::make_ptr<T>(std::move(handle)));
    }
    return result;
  });
}

//inline Lmbcs Database::getIdText(TIMEDATE id) noexcept {
//  return formatId<DWORD,uint>(id.Innards[1])
//      + formatId<DWORD,uint>(id.Innards[0]);
//}

inline observable<DBREPLICAINFO> Database::getReplicaInfo(
    DBHANDLE handle
    ) noexcept {
  return observable<>::create<DBREPLICAINFO>(
        [handle](subscriber<DBREPLICAINFO> s) {
    DBREPLICAINFO info;
    Status status = NSFDbReplicaInfoGet(handle, &info);
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(info);
    s.on_completed();
  });
}

inline observable<ReplicaId> Database::getReplicaId(DBHANDLE handle) noexcept {
  return getReplicaInfo(handle)
      .map([](DBREPLICAINFO info) {
    return ReplicaId(info.ID);
  });
}

inline DBREPLICAINFO Database::getReplicaInfo_x(
    DBHANDLE handle
    ) noexcept(false) {
  DBREPLICAINFO info;
  Status status = NSFDbReplicaInfoGet(handle, &info);
  if (status.failure()) { throw status; }
  return info;
}

inline ReplicaId Database::getReplicaId_x(DBHANDLE handle) noexcept(false) {
  auto info = getReplicaInfo_x(handle);
  return ReplicaId(info.ID);
}

//inline observable<Lmbcs> Database::getReplicaIdText(DBHANDLE handle) noexcept {
//  return getReplicaId(handle).map([](TIMEDATE id) { return getIdText(id); });
//}

inline observable<Lmbcs> Database::getAccessingUserName(
    DBHANDLE handle
    ) noexcept {
  return observable<>::create<Lmbcs>([handle](subscriber<Lmbcs> s) {
    char user[MAXUSERNAME + 1] = "";
    Status status = NSFDbUserNameGet(handle, user, MAXUSERNAME);
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(Lmbcs(user));
    s.on_completed();
  });
}

inline Lmbcs Database::getAccessingUserName_x(DBHANDLE handle) noexcept(false) {
  char user[MAXUSERNAME + 1] = "";
  Status status = NSFDbUserNameGet(handle, user, MAXUSERNAME);
  if (status.failure()) { throw status; }
  return Lmbcs(user);
}

inline observable<Database::Paths> Database::getPaths(
    DBHANDLE hDB
    ) noexcept {
  return observable<>::create<Database::Paths> (
        [hDB](subscriber<Database::Paths> s
        ) {
    char canonical[MAXPATH] = "";
    char expanded[MAXPATH] = "";
    Status status = NSFDbPathGet(hDB, canonical, expanded);
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(Database::Paths(Lmbcs(canonical), Lmbcs(expanded)));
    s.on_completed();
  });
}

inline Database::Paths Database::getPaths_x(
    DBHANDLE hDB
    ) noexcept(false) {
  char canonical[MAXPATH] = "";
  char expanded[MAXPATH] = "";
  Status status = NSFDbPathGet(hDB, canonical, expanded);
  if (status.failure()) { throw status; }
  return Database::Paths(Lmbcs(canonical), Lmbcs(expanded));
}

template <WORD what>
observable<Lmbcs> Database::getInfo(DBHANDLE hDB) noexcept {
  return observable<>::create<Lmbcs>([hDB](subscriber<Lmbcs> s) {
    char buffer[NSF_INFO_SIZE] = "";
    Status status = NSFDbInfoGet(hDB, buffer);
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    buffer[NSF_INFO_SIZE - 1] = '\0';
    char buf[NSF_INFO_SIZE] = "";
    NSFDbInfoParse(buffer, what, buf, NSF_INFO_SIZE - 1);
    buf[NSF_INFO_SIZE - 1] = '\0';
    s.on_next(Lmbcs(buf));
    s.on_completed();
  });
}

template <WORD what>
Lmbcs Database::getInfo_x(DBHANDLE hDB) noexcept(false) {
  char buffer[NSF_INFO_SIZE] = "";
  Status status = NSFDbInfoGet(hDB, buffer);
  if (status.failure()) { throw status; }
  buffer[NSF_INFO_SIZE - 1] = '\0';
  char buf[NSF_INFO_SIZE] = "";
  NSFDbInfoParse(buffer, what, buf, NSF_INFO_SIZE - 1);
  buf[NSF_INFO_SIZE - 1] = '\0';
  return Lmbcs(buf);
}

inline observable<Database::NoteInfo> Database::getNoteInfo(
    DBHANDLE hDB,
    NOTEID noteId
    ) noexcept {
  return observable<>::create<Database::NoteInfo>(
        [hDB, noteId](subscriber<Database::NoteInfo> s) {
    Database::NoteInfo noteInfo;
    Status status = NSFDbGetNoteInfo(
          hDB,
          noteId,
          &noteInfo.oid_,
          &noteInfo.modified_,
          &noteInfo.noteClass_
          );
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(noteInfo);
    s.on_completed();
  });
}

inline observable<DBID> Database::getId(DBHANDLE hDB) noexcept {
  return observable<>::create<DBID>([hDB](subscriber<DBID> s) {
    DBID dbId;
    Status status = NSFDbIDGet(hDB, &dbId);
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(dbId);
    s.on_completed();
  });
}

//inline observable<Lmbcs> Database::getIdText(DBHANDLE hDB) noexcept {
//  return getId(hDB).map([](DBID dbId) { return getIdText(dbId); });
//}

inline observable<Database::ObjectSize> Database::getObjectSize(
    DBHANDLE hDB,
    DWORD objectId,
    WORD objectType
    ) noexcept {
  return observable<>::create<Database::ObjectSize>(
        [hDB, objectId, objectType](subscriber<Database::ObjectSize> s) {
    DWORD retSize = 0;
    WORD retClass = 0, retPrivileges = 0;
    Status status = NSFDbGetObjectSize(
          hDB,
          objectId,
          objectType,
          &retSize,
          &retClass,
          &retPrivileges
          );
    if (status.failure()) {
      s.on_error(std::make_exception_ptr(status));
      return;
    }
    s.on_next(std::make_tuple(retSize, retClass, retPrivileges));
    s.on_completed();
  });
}

} // namespace npp202009::npp

#endif // NPP202009_NPP_DATABASE_HPP
