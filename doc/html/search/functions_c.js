var searchData=
[
  ['length_951',['length',['../classnpp202009_1_1npp_1_1_formula_1_1_compile_error.html#ab3193bf2cf2b906ccfb8cc2d3b998879',1,'npp202009::npp::Formula::CompileError']]],
  ['line_952',['line',['../classnpp202009_1_1npp_1_1_formula_1_1_compile_error.html#aaccf7ea935a0242c2c65c8d03958dd29',1,'npp202009::npp::Formula::CompileError']]],
  ['list_953',['List',['../classnpp202009_1_1npp_1_1_list.html#a1e5d7c7bb653439f813743b564e96f15',1,'npp202009::npp::List::List()'],['../classnpp202009_1_1npp_1_1_list.html#af03551f0e909d33345d7827c976ec702',1,'npp202009::npp::List::List(void *ptr)'],['../classnpp202009_1_1npp_1_1_range.html#ab84d8843cf247db8f6eab09cfaf84fc5',1,'npp202009::npp::Range::list()']]],
  ['lmbcs_954',['Lmbcs',['../classnpp202009_1_1npp_1_1_lmbcs.html#aa1064927bbbd4469049026e3d0363b14',1,'npp202009::npp::Lmbcs::Lmbcs() noexcept'],['../classnpp202009_1_1npp_1_1_lmbcs.html#a4a3076833ee427c668e927531f21ac62',1,'npp202009::npp::Lmbcs::Lmbcs(const char *c_str, int size=-1) noexcept'],['../classnpp202009_1_1npp_1_1_lmbcs.html#aff5437723f8a713f334d3eabd3023d93',1,'npp202009::npp::Lmbcs::Lmbcs(int size, char c)'],['../classnpp202009_1_1npp_1_1_lmbcs.html#adc1dd039a6cfe920bc8165f9d32c2da2',1,'npp202009::npp::Lmbcs::Lmbcs(const QByteArray &amp;other)'],['../classnpp202009_1_1npp_1_1_lmbcs.html#ab6c94155948d0a5da3f526383cdf1bbb',1,'npp202009::npp::Lmbcs::Lmbcs(QByteArray &amp;&amp;other)']]],
  ['lmbcsinfo_955',['LmbcsInfo',['../classnpp202009_1_1npp_1_1nls_1_1_lmbcs_info.html#ab93a60be3891bf3796ee37667eaeb5b3',1,'npp202009::npp::nls::LmbcsInfo']]],
  ['lmbcslist_956',['LmbcsList',['../classnpp202009_1_1npp_1_1_lmbcs_list.html#ad14676c5a582e3e12a44b09161ca73d6',1,'npp202009::npp::LmbcsList::LmbcsList()'],['../classnpp202009_1_1npp_1_1_lmbcs_list.html#a1898af01a519683d228a9d2b379c9899',1,'npp202009::npp::LmbcsList::LmbcsList(void *ptr)']]],
  ['lmbcstounicode_957',['lmbcsToUnicode',['../namespacenpp202009_1_1npp_1_1nls.html#ac12bc8c191f2d45379c04a501d350e8f',1,'npp202009::npp::nls']]],
  ['loadinginfo_958',['LoadingInfo',['../classnpp202009_1_1npp_1_1nls_1_1_loading_info.html#a74ff4ba64678feddbc23a4e4af69a9c3',1,'npp202009::npp::nls::LoadingInfo']]],
  ['lock_959',['lock',['../classnpp202009_1_1npp_1_1handle_1_1_block.html#a30896a8cfc796b0babbd83550ffbf057',1,'npp202009::npp::handle::Block::lock()'],['../classnpp202009_1_1npp_1_1handle_1_1_lockable.html#a269ae16ff956af1a871fb68703561915',1,'npp202009::npp::handle::Lockable::lock()']]],
  ['lockable_960',['Lockable',['../classnpp202009_1_1npp_1_1handle_1_1_lockable.html#a0c7c41849a25a9743855d1f067103576',1,'npp202009::npp::handle::Lockable']]],
  ['logerror_961',['logError',['../namespacenpp202009_1_1npp_1_1addin.html#a5ac5b30f2d9ca0c0ec3d79a66550fc20',1,'npp202009::npp::addin']]],
  ['logerrortext_962',['logErrorText',['../namespacenpp202009_1_1npp_1_1addin.html#a1334f8491fe4419e288ac55d8a7f0ead',1,'npp202009::npp::addin']]],
  ['logger_963',['Logger',['../classnpp202009_1_1npp_1_1logger_1_1_logger.html#aa0c105d3283cb797c52611d25d6177aa',1,'npp202009::npp::logger::Logger']]],
  ['logmessage_964',['logMessage',['../namespacenpp202009_1_1npp_1_1addin.html#a589d632e62514314cf814dc1d357abba',1,'npp202009::npp::addin']]],
  ['logmessagetext_965',['logMessageText',['../namespacenpp202009_1_1npp_1_1addin.html#a71e67a1038390ed44f6e267dc7501c3d',1,'npp202009::npp::addin']]],
  ['logmsg_966',['logMsg',['../namespacenpp202009_1_1npp_1_1addin.html#a115e85bb7983f63dbb8b092ba2089b19',1,'npp202009::npp::addin']]],
  ['lq_967',['lq',['../namespacenpp202009_1_1npp.html#aa2d749f0988ea281c748af8c542eb095',1,'npp202009::npp']]]
];
