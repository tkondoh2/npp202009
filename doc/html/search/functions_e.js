var searchData=
[
  ['name_980',['name',['../classnpp202009_1_1npp_1_1mime_1_1_rfc822_text.html#ab23fdad1ce9c38920c6009fabbfe86eb',1,'npp202009::npp::mime::Rfc822Text']]],
  ['namelen_981',['nameLen',['../classnpp202009_1_1npp_1_1mime_1_1_rfc822_text.html#aa6c8a3c20b2b99d7e98e40f36c677963',1,'npp202009::npp::mime::Rfc822Text']]],
  ['names_982',['names',['../classnpp202009_1_1npp_1_1logger_1_1_level.html#a4d4c4aac90d1188608abdc8242b049e4',1,'npp202009::npp::logger::Level']]],
  ['nameslist_983',['NamesList',['../classnpp202009_1_1npp_1_1_names_list.html#a2828a9c10d81e23822dccf2c18c62e0d',1,'npp202009::npp::NamesList::NamesList()'],['../classnpp202009_1_1npp_1_1_names_list.html#a487551dba4248b5aeaddf24e1f927f5b',1,'npp202009::npp::NamesList::NamesList(DHANDLE &amp;&amp;handle)']]],
  ['native_984',['native',['../classnpp202009_1_1npp_1_1mime_1_1_rfc822_text.html#a5ba6da42e39a784a0f4c2c725259814c',1,'npp202009::npp::mime::Rfc822Text']]],
  ['nativelen_985',['nativeLen',['../classnpp202009_1_1npp_1_1mime_1_1_rfc822_text.html#a1933bc16e7430b8da2106740f84c4d0b',1,'npp202009::npp::mime::Rfc822Text']]],
  ['note_986',['Note',['../classnpp202009_1_1npp_1_1_note.html#a362afdc30701fb527a8ca3416edcfcb3',1,'npp202009::npp::Note']]],
  ['noteclass_987',['noteClass',['../classnpp202009_1_1npp_1_1_database_1_1_note_info.html#aa4f7b8dd58b7b4c4d24dc7dc49b40fd4',1,'npp202009::npp::Database::NoteInfo']]],
  ['noteinfo_988',['NoteInfo',['../classnpp202009_1_1npp_1_1_database_1_1_note_info.html#ad0a2c9cf2971ea0b20fb91e5ef6d17d7',1,'npp202009::npp::Database::NoteInfo']]],
  ['number_989',['Number',['../classnpp202009_1_1npp_1_1_number.html#abc8e86ab43be32fa052bfa5450e71b49',1,'npp202009::npp::Number::Number()'],['../classnpp202009_1_1npp_1_1_number.html#a6f6ac7b85520b02f4e18f14e1d325fbd',1,'npp202009::npp::Number::Number(NUMBER value)']]],
  ['numberrange_990',['NumberRange',['../classnpp202009_1_1npp_1_1_number_range.html#ab7e46359b2a8aba001f81ffb6fb81fe1',1,'npp202009::npp::NumberRange::NumberRange()'],['../classnpp202009_1_1npp_1_1_number_range.html#a9b1c2e2c1d87a6a6cc2b6fa3651fdff5',1,'npp202009::npp::NumberRange::NumberRange(void *ptr)'],['../classnpp202009_1_1npp_1_1_number_range.html#a096c282b9ec82b7b1ccbe8c385089045',1,'npp202009::npp::NumberRange::NumberRange(NUMBER v)']]]
];
