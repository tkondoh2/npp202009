var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvw~",
  1: "abcdefhijlmnopqrstuv",
  2: "n",
  3: "abcdefhijklmnopqrstuvw",
  4: "_abcdefghijklmnopqrstuvw~",
  5: "bcdefhilmnoprstvw",
  6: "abcdefhmnopqrt",
  7: "l",
  8: "cdeimotw",
  9: "do",
  10: "n"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines"
};

var indexSectionLabels =
{
  0: "全て",
  1: "クラス",
  2: "名前空間",
  3: "ファイル",
  4: "関数",
  5: "変数",
  6: "型定義",
  7: "列挙型",
  8: "列挙値",
  9: "フレンド",
  10: "マクロ定義"
};

