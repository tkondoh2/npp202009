TEMPLATE = subdirs

DISTFILES += \
  CHANGELOG.md \
  .gitignore \
  global.pri \
  local.pri

SUBDIRS += \
  include/npp \
  include/npp2
