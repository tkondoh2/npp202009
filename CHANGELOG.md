# NPP 2020.09 Project

# 2.1.0.0-2024-06-25

MSVC2019 でコンパイルした時の以下のエラーを修正

- `std::unique_ptr` 用にヘッダーファイル読み込み `#include <memory>` を追加
  - modified: include/npp2/converter/intlformat.hpp
- `std::runtime_error` 用にヘッダーファイル読み込み `#include <stdexcept>` を追加
  - modified: src/npp2_data_timedate.cpp
  - modified: src/npp2_mime_decode.cpp
- `endl` だけでは解決できないので、 `Qt::endl` とする。
  - modified: src/npp2_logger_index.cpp
- `strValue` の型を `QString` から `auto(QByteArray)` にする。
  - modified: src/npp2_mime_quotedprintable.cpp

# 2.1.0-dev9-2024-06-25

- build.bat は環境依存ファイルなので、追跡対象外にする。
  - deleted: build.bat
  - modified: .gitignore

# 2.1.0-dev8-2024-06-11

- `npp2::logger::Logger::createFile` の第 3 引数に、監査用関数 `std::function<void(const QString&)>` を挿入
  - modified: include/npp2/logger/index.hpp
  - modified: src/npp2_logger_index.cpp
- `npp2::utils::Auditor` を定義
  - new file: include/npp2/utils/audit.hpp
- `npp2::utils::iif<T>` を定義
- `npp2::utils::filter<T>` を定義
  - modified: include/npp2/utils/index.hpp
- `npp2::_FileWriter<T>` に `npp2::utils::Auditor` を組み込む。
  - modified: include/npp2/utils/writer/filewriter.hpp
- `npp2::_Writer<T>::pStream` を追加
  - modified: include/npp2/utils/writer/index.hpp
- `npp2::QDataFileWriter::QDataFileWriter` の第 2 引数に、監査用関数 `std::function<void(const QString&)>` を追加
  - modified: include/npp2/utils/writer/qdatafilewriter.hpp
  - modified: src/npp2_utils_writer_qdatafilewriter.cpp
- `npp2::_QFileWriter<T>::_QFileWriter` の第 2 引数に、監査用関数 `std::function<void(const QString&)>` を追加
  - modified: include/npp2/utils/writer/qfilewriter.hpp
- `npp2::QTextFileWriter::QTextFileWriter` の第 2 引数に、監査用関数 `std::function<void(const QString&)>` を追加
- `npp2::QTextFileWriter::codecName` を追加
- `npp2::QTextFileWriter::bom` を追加
  - modified: include/npp2/utils/writer/qtextfilewriter.hpp
  - modified: src/npp2_utils_writer_qtextfilewriter.cpp
- `npp2::StdFileWriter::StdFileWriter` の第 2 引数に、監査用関数 `std::function<void(const QString&)>` を追加
  - modified: include/npp2/utils/writer/stdfilewriter.hpp
  - modified: src/npp2_utils_writer_stdfilewriter.cpp
- `npp2::AttachmentItem::AttachmentItem` でブロックデータから `header_` に内容をコピーしていない不具合を修正
  - modified: src/npp2_attachmentitem.cpp

# 2.1.0-dev7-2024-05-16

- `AclEntry` クラスを定義
- `_AclEntry` クラスを定義
  - new file: include\npp2\acl.h
  - new file: src\npp2_acl.cpp

# 2.1.0-dev6-2024-04-05

- ファイル追加
  - modified: qmake.pro
  - modified: include/npp2/extmgr.hpp
  - new file: src/npp2_extmgr.cpp
- `extmgr::createRecursionID` 追加
- `extmgr::filter` 追加
  - modified: include/npp2/extmgr.hpp
  - new file: src/npp2_extmgr.cpp
- `npp2::utils::some` 追加
- `npp2::utils::every` 追加
  - modified: include/npp2/utils/index.hpp
- `npp2::_FileWriter<T>::filePath` 追加
  - modified: include/npp2/utils/writer/filewriter.hpp
- `npp2::_QFileWriter<T>::size` のサイズ算出を `QFileInfo` 経由に変更
  - modified: include/npp2/utils/writer/qfilewriter.hpp

# 2.1.0-dev5-2024-02-20

- 定義 (h/hpp) と実装 (cpp) を分ける。
  - modified: qmake.pro
  - modified: include/npp2/collection.hpp
    - new file: src/npp2_collection.cpp
  - modified: include/npp2/compute.hpp
    - new file: src/npp2_compute.cpp
  - modified: include/npp2/database.hpp
    - new file: src/npp2_database.cpp
  - modified: include/npp2/dname.hpp
    - new file: src/npp2_dname.cpp
  - modified: include/npp2/env.hpp
    - new file: src/npp2_env.cpp
  - modified: include/npp2/formula.hpp
    - new file: src/npp2_formula.cpp
  - modified: include/npp2/idtable.hpp
    - new file: src/npp2_idtable.cpp
  - modified: include/npp2/item.hpp
    - new file: src/npp2_item.cpp
  - modified: include/npp2/itemtable.hpp
    - new file: src/npp2_itemtable.cpp
  - modified: include/npp2/namelookup.hpp
    - new file: src/npp2_namelookup.cpp
  - modified: include/npp2/nameslist.hpp
    - new file: src/npp2_nameslist.cpp
  - modified: include/npp2/note.hpp
    - new file: src/npp2_note.cpp
  - modified: include/npp2/search.hpp
    - new file: src/npp2_search.cpp
  - modified: include/npp2/thread.hpp
    - new file: src/npp2_thread.cpp
  - modified: include/npp2/unreadidtable.hpp
    - new file: src/npp2_unreadidtable.cpp
  - new file: include/npp2/attachmentitem.hpp
  - new file: src/npp2_attachmentitem.cpp

# 2.1.0-dev4-2024-02-11

- 定義 (h/hpp) と実装 (cpp) を分ける。
  - modified: qmake.pro
  - modified: include/npp2/format/number.hpp
    - new file: src/npp2_format_number.cpp
  - modified: include/npp2/format/time.hpp
    - new file: src/npp2_format_time.cpp
  - modified: include/npp2/logger/formatter.hpp
    - new file: include/npp2/logger/defaultformatter.hpp
    - new file: src/npp2_logger_defaultformatter.cpp
  - modified: include/npp2/logger/level.hpp
    - new file: src/npp2_logger_level.cpp
  - deleted: include/npp2/logger/logger.hpp
    - new file: include/npp2/logger/index.hpp
    - new file: src/npp2_logger_index.cpp
  - modified: include/npp2/mime/codec.hpp
    - new file: src/npp2_mime_codec.cpp
  - modified: include/npp2/mime/decode.hpp
    - new file: src/npp2_mime_decode.cpp
  - modified: include/npp2/mime/quotedprintable.hpp
    - new file: src/npp2_mime_quotedprintable.cpp

# 2.1.0-dev3-2024-02-09

- 定義 (h/hpp) と実装 (cpp) を分ける。
  - modified: qmake.pro
  - deleted: include/npp2/utils.hpp
    - new file: include/npp2/utils/index.hpp
    - new file: src/npp2_utils_index.cpp
  - modified: include/npp2/utils/data.hpp
  - modified: include/npp2/utils/os.hpp
  - modified: include/npp2/utils/process.hpp
    - new file: src/npp2_utils_process.cpp
  - modified: include/npp2/utils/replicaid.hpp
    - new file: src/npp2_utils_replicaid.cpp
  - modified: include/npp2/utils/thread.hpp
    - new file: src/npp2_utils_thread.cpp
  - modified: include/npp2/utils/time.hpp
    - new file: src/npp2_utils_time.cpp
  - modified: include/npp2/utils/unid.hpp
    - new file: src/npp2_utils_unid.cpp
  - deleted: include/npp2/utils/writer.hpp
    - new file: include/npp2/utils/writer/index.hpp
  - deleted: include/npp2/utils/filewriter.hpp
    - new file: include/npp2/utils/writer/filewriter.hpp
  - deleted: include/npp2/utils/qfilewriter.hpp
    - new file: include/npp2/utils/writer/qfilewriter.hpp
  - deleted: include/npp2/utils/qdatafilewriter.hpp
    - new file: include/npp2/utils/writer/qdatafilewriter.hpp
    - new file: src/npp2_utils_writer_qdatafilewriter.cpp
  - deleted: include/npp2/utils/qtextfilewriter.hpp
    - new file: include/npp2/utils/writer/qtextfilewriter.hpp
    - new file: src/npp2_utils_writer_qtextfilewriter.cpp
  - deleted: include/npp2/utils/stdfilewriter.hpp
    - new file: include/npp2/utils/writer/stdfilewriter.hpp
    - new file: src/npp2_utils_writer_stdfilewriter.cpp
  - deleted: include/npp2/utils/renamer.hpp
    - new file: include/npp2/utils/renamer/index.hpp
    - new file: src/npp2_utils_renamer_index.cpp
    - new file: include/npp2/utils/renamer/count.hpp
    - new file: src/npp2_utils_renamer_count.cpp
    - new file: include/npp2/utils/renamer/timestamp.hpp
    - new file: src/npp2_utils_renamer_timestamp.cpp
  - deleted: include/npp2/utils/rotator.hpp
    - new file: include/npp2/utils/rotator/index.hpp
    - new file: src/npp2_utils_rotator_index.cpp
    - new file: include/npp2/utils/rotator/daily.hpp
    - new file: src/npp2_utils_rotator_daily.cpp
    - new file: include/npp2/utils/rotator/size.hpp
    - new file: src/npp2_utils_rotator_size.cpp

# 2.1.0-dev2-2024-02-08

- 定義 (h/hpp) と実装 (cpp) を分ける。
  - modified: include/npp2/\_private/handleobject.hpp
  - modified: include/npp2/data/any.hpp
  - modified: include/npp2/data/lmbcs.hpp
  - modified: include/npp2/data/lmbcslist.hpp
  - modified: include/npp2/data/number.hpp
  - modified: include/npp2/data/numberrange.hpp
  - modified: include/npp2/data/range.hpp
  - modified: include/npp2/data/rfc822text.hpp
  - modified: include/npp2/data/textlist.hpp
  - modified: include/npp2/data/time.hpp
  - modified: include/npp2/data/timedate.hpp
  - modified: include/npp2/data/timedaterange.hpp
  - modified: include/npp2/data/variant.hpp
  - modified: include/npp2/nls/info.hpp
  - modified: include/npp2/nls/size.hpp
  - modified: include/npp2/nls/status.hpp
  - modified: include/npp2/nls/translate.hpp
  - modified: qmake.pro
  - modified: src/npp2_converter_number.cpp
  - modified: src/npp2_converter_time.cpp
  - modified: src/npp2_converter_timepair.cpp
  - new file: src/npp2_data_any.cpp
  - new file: src/npp2_data_lmbcs.cpp
  - new file: src/npp2_data_lmbcslist.cpp
  - new file: src/npp2_data_number.cpp
  - new file: src/npp2_data_numberrange.cpp
  - new file: src/npp2_data_rfc822text.cpp
  - new file: src/npp2_data_textlist.cpp
  - new file: src/npp2_data_time.cpp
  - new file: src/npp2_data_timedate.cpp
  - new file: src/npp2_data_timedaterange.cpp
  - new file: src/npp2_data_variant.cpp
  - new file: src/npp2_nls_info.cpp
  - new file: src/npp2_nls_size.cpp
  - new file: src/npp2_nls_status.cpp
  - new file: src/npp2_nls_translate.cpp

# 2.1.0-dev1-2024-02-06

- 定義 (h/hpp) と実装 (cpp) を分ける。
  - modified: qmake.pro
  - modified: include/npp2/\_private/block.hpp
  - modified: include/npp2/\_private/handleobject.hpp
  - modified: include/npp2/\_private/range.hpp
  - modified: include/npp2/converter/base.hpp
  - modified: include/npp2/converter/intlformat.hpp
  - modified: include/npp2/converter/number.hpp
  - modified: include/npp2/converter/numberpair.hpp
  - modified: include/npp2/converter/time.hpp
  - modified: include/npp2/converter/timepair.hpp
  - modified: include/npp2/data/textlist.hpp
  - new file: src/npp2_converter_intlformat.cpp
  - new file: src/npp2_converter_number.cpp
  - new file: src/npp2_converter_numberpair.cpp
  - new file: src/npp2_converter_time.cpp
  - new file: src/npp2_converter_timepair.cpp
  - new file: src/npp2_private_block.cpp
  - new file: src/npp2_private_handleobject.cpp
  - new file: src/npp2_private_range.cpp

# [dev] v2.1.0.0-2024-01-22

- `build` フォルダを対象外に追加する。
  - modified: .gitignore
- ビルド用バッチファイルを作成する。
  - new file: build.bat
- RxCpp 関連項目を削除する。
- OpenSSL 関連項目を削除する。
  - modified: global.pri
- qmake プロジェクトファイルを全面的に刷新する。
  - renamed: include/npp2/npp2.pro -> qmake.pro
- バージョン埋め込み用 pri ファイルを追加する。
  - new file: version.pri
- `npp2::Status` を DLL 化する。
  - modified: include/npp2/status.hpp
  - new file: src/npp2_status.cpp
- 未ロード状態になった misc.h(notes api ヘッダ) をロードする。
  - modified: include/npp2/data/number.hpp

# [dev] v2.0.28.0-2023-08-10

- include\npp2\data\rfc822text.hpp
  - バグとなる余計なポイント移動を取り除く。

# [dev] v2.0.27.0-2023-02-06

- include\npp2\utils\replicaid.hpp
  - レプリカ ID 取得時に `DBID` ではなく、 `DBREPLICAINFO.ID` から取得する(バグ)。
- include\npp2\database.hpp
  - `Database::getReplicaInfo(...)` 追加

# [dev] v2.0.26.0-2022-12-30

- include\npp2\compute.hpp
  - じかに `OSMemFree` したハンドルを、 `LockableObject3` の管理下に置く。
- include\npp2\namelookup.hpp
  - `class NameLookup` 追加
- include\npp2\utils.hpp
  - `mapContainsAnd<T>(T, QStringList)` → `mapContainsAnd<K,V>(QMap<K,V>,QList<K>)`
- include\npp2\sec\verifypassword.hpp
  - `sec::verifyPassword(...)` 追加

# [dev] v2.0.25.0-2022-12-23

- include\npp2\compute.hpp
  - `evaluate(...)` で結果ハンドル(`BLK_FORMULARES`)の未解放バグを修正。

# [dev] v2.0.24.0-2022-12-20

- include\npp2\extmgr.hpp
  - `isEnabled<>(...)` 引数に `WORD` 値を追加
- include\npp2\http1\authenticate.hpp
  - `Authenticate::setAuthName(...)` 追加
  - `Authenticate::setAuthType(...)` 追加

# [dev] v2.0.23.0-2022-12-15

- include\npp2\http1\authenticate.hpp
  - `Authenticate::getHeader` 追加

# [dev] v2.0.22.0-2022-12-14

- include\npp2\utils\pairlisttomap.hpp → 削除
- include\npp2\utils\container.hpp → 削除
- include\npp2\utils.hpp → 追加
  - `pairListToMap` utils\pairlisttomap.hpp から移転
  - `map` utils\container.hpp から移転
  - `mapContainsAnd` 追加
  - `addQueryItem` 追加
- include\npp2\http1\authenticate.hpp
  - `class Authenticate` 追加
- include\npp2\http1\authenticateduser.hpp
  - `class AuthenticatedUser` 追加
- include\npp2\http1\directresponse.hpp
  - `class DirectResponse` 追加
- include\npp2\http1\error.hpp
  - `class Error` 追加
- include\npp2\http1\exception.hpp
  - `class Exception` 追加
- include\npp2\http1\exec.hpp
  - `exec` 関数追加
- include\npp2\http1\filter.hpp
  - `class Filter` 追加
- include\npp2\http1\request.hpp
  - `strMapToVarMap` 関数追加
  - `class _Request` 追加
  - `using Request(std::unique_ptr<_Request>)` 追加
- include\npp2\jwt\payload.hpp
  - `setIssueAndExpires` 引数 `expires_in` 追加、 `true` の場合、 `expires_in` /_! _/を追加するように変更

# [dev] v2.0.21.0-2022-11-16

- include\npp2\dname.hpp
  - `DistinguishedName::abbreviated` 追加
- include\npp2\database.hpp
  - `_Database::PathNet` コンストラクタを public 化
  - `_Database::PathNet::parse` 追加

# [dev] v2.0.20.0-2022-11-06

- include/npp2/\_private/handleobject.hpp
  - `LockableObject` と `LockableObject2` を統合し、 `LockableObject2` とする。
  - `LockableObject2` のテンプレートパラメータを `<T>` から `<T,Deleter>` に変更
  - `LockableObject2::free` のハンドル解放を `Deleter` 関数オブジェクトによる実行に変更
  - `OSMemFreeDeleter` 関数オブジェクト追加
  - `class LockableObject3<T> : LockableObject2<T, OSMemFreeDeleter>` 追加
- include/npp2/collection.hpp
  - `Entries` クラスの親を `LockableObject<DHANDLE>` から `LockableObject3<DHANDLE>` に変更
  - `Collection` クラスの名前を `_Collection` に変更
  - `CollectionPtr` 別名クラスの名前を `Collection` に変更
  - `Collection` 別名クラスの定義元を `QSharedPointer<Collection>` から `std::unique_ptr<_Collection>` に変更
  - `NIFCloseCollectionDeleter` 関数オブジェクト追加
  - `_Collection` クラスの親を `HandleObject<HCOLLECTION>` から `LockableObject2<HCOLLECTION,NIFCloseCollectionDeleter>` に変更
- include/npp2/compute.hpp
  - `compute` のシグネチャを `void(FormulaPtr,std::function<void(HCOMPUTE hCompute)>)` から `void(FORMULAHANDLE,std::function<void(HCOMPUTE hCompute)>)` に変更
- include/npp2/converter/base.hpp
  - `converter::Base<T>` の `formatPtr_` を `QSharedPointer<T>` から `std::unique_ptr<T>` に変更
- include/npp2/converter/intlformat.hpp
  - `IntlFormatPtr` の定義元を `QSharedPointer<IntlFormat>` から `std::unique_ptr<IntlFormat>` に変更
- include/npp2/converter/range.hpp
  - `RangeToTextConverter` の引数 `tPtr` を `QSharedPointer<Format>` から `std::unique_ptr<Format>` に変更
- include/npp2/data/any.hpp
  - `data::Any::_rangeToString<S,P,T>(const T&)` 内で使用する `data::TextList` を `data::LmbcsList` に変更
- include/npp2/data/list.hpp
  - `LmbcsList` 定義を lmbcslist.hpp に移動
  - `TextList` 定義を textlist.hpp に移動
  - list.hpp に廃止
- include/npp2/data/range.hpp
  - `Range` クラスの親を `LockableObject<DHANDLE>` から `LockableObject3<DHANDLE>` に変更
- include/npp2/data/textlist.hpp
  - `TextList` クラスの名前を `_TextList` に変更
  - `TextListPtr` 別名クラスの名前を `TextList` に変更
  - `TextList` 別名クラスの定義元を `QSharedPointer<TextList>` から `std::unique_ptr<_TextList>` に変更
  - `_TextList` クラスの親を `LockableObject<DHANDLE>` から `LockableObject3<DHANDLE>` に変更
- include/npp2/data/time.hpp
  - `TimePtr` の定義元を `QSharedPointer<Time>` から `std::unique_ptr<Time>` に変更
- include/npp2/data/timedate.hpp
  - `TimeDatePtr` の定義元を `QSharedPointer<TimeDate>` から `std::unique_ptr<TimeDate>` に変更
- include/npp2/database.hpp
  - `Database` クラスの名前を `_Database` に変更
  - `DbPtr` 別名クラスの名前を `Database` に変更
  - `Database` 別名クラスの定義元を `QSharedPointer<Database>` から `std::unique_ptr<_Database>` に変更
  - `NSFDbCloseDeleter` 関数オブジェクト追加
  - `_Database` クラスの親を `HandleObject<DBHANDLE>` から `LockableObject2<DBHANDLE,NSFDbCloseDeleter>` に変更
- include/npp2/format/number.hpp
  - `NumberPtr` の定義元を `QSharedPointer<Number>` から `std::unique_ptr<Number>` に変更
- include/npp2/format/time.hpp
  - `TimePtr` の定義元を `QSharedPointer<Time>` から `std::unique_ptr<Time>` に変更
- include/npp2/formula.hpp
  - `Formula` クラスの名前を `_Formula` に変更
  - `FormulaPtr` 別名クラスの名前を `Formula` に変更
  - `Formula` 別名クラスの定義元を `QSharedPointer<Formula>` から `std::unique_ptr<_Formula>` に変更
  - `_Formula` クラスの親を `LockableObject<FORMULAHANDLE>` から `LockableObject3<FORMULAHANDLE>` に変更
- include/npp2/idtable.hpp
  - `IdTablePtr` 別名クラスの定義元を `QSharedPointer<IdTable>` から `std::unique_ptr<IdTable>` に変更
  - `IDDestroyTableDeleter` 追加
  - `_Formula` クラスの親を `LockableObject<DHANDLE>` から `LockableObject2<DHANDLE, IDDestroyTableDeleter>` に変更
- include/npp2/item.hpp
  - `convertToText` 内で使用する `buffer` 変数を `LockableObject2<DHANDLE>` から `LockableObject3<DHANDLE>` に変更
- include/npp2/nameslist.hpp
  - `NamesListPtr` 別名クラスの定義元を `QSharedPointer<NamesList>` から `std::unique_ptr<NamesList>` に変更
  - `NamesList` クラスの親を `LockableObject<DHANDLE>` から `LockableObject3<DHANDLE>` に変更
- include/npp2/note.hpp
  - `Note` クラスの名前を `_Note` に変更
  - `NotePtr` 別名クラスの名前を `Note` に変更
  - `Note` 別名クラスの定義元を `QSharedPointer<Note>` から `std::unique_ptr<_Note>` に変更
  - `NSFNoteCloseDeleter` 関数オブジェクト追加
  - `_Note` クラスの親を `HandleObject<NOTEHANDLE>` から `LockableObject2<NOTEHANDLE,NSFNoteCloseDeleter>` に変更
- include/npp2/search.hpp
  - `Search` のメンバから `dbPtr_` を削除
  - `Search` のコンストラクタの引数を `(const DbPtr &)` から `()` に変更
  - `Search::operator()` の引数を `const FormulaPtr &, const data::Lmbcs &, WORD, WORD, SearchCallback, TIMEDATE *` から `DBHANDLE, FORMULAHANDLE, const data::Lmbcs &, WORD, WORD, SearchCallback, TIMEDATE *` に変更
  - `Search::operator()` の引数を `const data::Lmbcs &, const data::Lmbcs &, WORD, WORD, SearchCallback, TIMEDATE *` から `DBHANDLE, const data::Lmbcs &, const data::Lmbcs &, WORD, WORD, SearchCallback, TIMEDATE *` に変更

# [dev] v2.0.19.1-2022-10-29

- include\npp2\nls\translate.hpp
  - バッファサイズ(`MAX_TRANSLATE_BUFFER_SIZE`)を 64KiB から 60KiB に変更。 `ushort` の際を超えてクラッシュしてしまうため。
  - `translateMax64kib` を `translateMax60kib` に変更。
- include\npp2\data\list.hpp
  - `LmbcsList::join` のロジックの不具合を修正。
- include\npp2\item.hpp
  - `AttachmentItem` コンストラクタの見直し。

# [dev] v2.0.18.1-2022-10-25

- include\npp2\data\list.hpp
  - `_TextList::toLmbcsList()` でテキストリストから`LmbcsList` を作成するロジックで、最初のアイテムを飛ばすバグを修正。

# [dev] v2.0.18-2022-10-25

Linux/GCC 適用

- include/npp2/\_private/handleobject.hpp

```cpp
    if (!isNull()) {
      OSMemFree(rawHandle());
      setHandle(NULLHANDLE);
```

```cpp
    if (!this->isNull()) {
      OSMemFree(this->rawHandle());
      this->setHandle(NULLHANDLE);
```

- include/npp2/data/any.hpp

```cpp
    auto slist = range.reduceItems<TextList>([](TextList acc, typename S::type v) {
      acc->append(S(v).toString());
      return acc;
    }, TextList::create());
    slist = range.reducePairs<TextList>([](TextList acc, typename P::type v) {
```

```cpp
    auto slist = range.template reduceItems<TextList>([](TextList acc, typename S::type v) {
      acc->append(S(v).toString());
      return acc;
    }, TextList::create());
    slist = range.template reducePairs<TextList>([](TextList acc, typename P::type v) {
```

- include/npp2/data/lmbcs.hpp

```cpp
  virtual ~Lmbcs() {}
```

- include/npp2/utils/filewriter.hpp

```cpp
    return *pStream_;
```

```cpp
    return *this->pStream_;
```

- include/npp2/utils/qfilewriter.hpp

```cpp
    pFile_.reset(new QFile(filePath_));
    if (pFile_->open(QFile::Text | QFile::WriteOnly | QFile::Append)) {
      pStream_.reset(new T(pFile_.data()));
// ---------------------------------------------------------------------
      pStream_.reset();
```

```cpp
    pFile_.reset(new QFile(this->filePath_));
    if (pFile_->open(QFile::Text | QFile::WriteOnly | QFile::Append)) {
      this->pStream_.reset(new T(pFile_.data()));
// ---------------------------------------------------------------------
      this->pStream_.reset();
```

- include/npp2/utils/stdfilewriter.hpp

```cpp
/**
 * @brief 標準ファイル出力ストリームを使ったライタークラス
 */
class StdFileWriter
  : public _FileWriter<std::ofstream>
{
public:
  /**
   * @brief コンストラクタ
   * @param filePath ファイルパス
   */
  StdFileWriter(const QString &filePath)
    : _FileWriter<std::ofstream>(filePath)
  {}

  /**
   * @brief ファイルを開く。
   * @return true 成功
   * @return false 失敗
   */
  virtual bool open() override {
    pStream_.reset(
      new std::ofstream(
        filePath_.toStdWString(),
        std::ios_base::out | std::ios_base::app
      )
    );
    return isOpen();
  }
```

```cpp
using StdStream = std::ofstream;

/**
 * @brief 標準ファイル出力ストリームを使ったライタークラス
 */
class StdFileWriter
  : public _FileWriter<StdStream>
{
public:
  /**
   * @brief コンストラクタ
   * @param filePath ファイルパス
   */
  StdFileWriter(const QString &filePath)
    : _FileWriter<StdStream>(filePath)
  {}

  /**
   * @brief ファイルを開く。
   * @return true 成功
   * @return false 失敗
   */
  virtual bool open() override {
#ifndef NT
    auto path = filePath_.toUtf8().toStdString();
#else
    auto path = filePath_.toStdWString();
#endif
    auto mode = std::ios_base::out | std::ios_base::app;
    auto p = new StdStream(path, mode);
    pStream_.reset(p);
    return isOpen();
  }
```

# [dev] v2.0.17-2022-10-17

- include\npp2_private\handleobject.hpp
  - `LockableObject2::free` を追加
- include\npp2\data\list.hpp => include\npp2\data\list_v1.hpp
- include\npp2\data\list.hpp (new)
  - `class LmbcsList : public QList<Lmbcs>` 追加
  - `template<WORD> class List` を `class _TextList` に変更
  - `LockableObject2<DHANDLE>` に継承元を変更
  - `bool allocated_` 追加し、空の `_TextList` を作成してもすぐにリストハンドルを作成しないようにする。
  - `_TextList` のコピーコンストラクタ(ムーブ含む) 廃止
  - `_TextList` の代入演算子(ムーブ含む) 廃止
  - `_TextList::~_TextList` 仮想デストラクタ追加
  - `virtual void _TextList::free()` オーバーライド
  - `void _TextList::duplicate(const LIST *from, BOOL fPrefix)` 追加
  - `LmbcsList _TextList::toLmbcsList() const` 追加
  - `bool _TextList::allocate(WORD count = 0, WORD quota)` で `allocated_` を考慮したロジックに変更
  - `TextList` を `List<TYPE_TEXT_LIST>` から `QSharedPointer<_TextList>` に変更
  - `fromLmbcsList` を `fromTextList` に変更
  - `toLmbcsList` を `toTextList` に変更
  - `npp2::data::TextList lmbcsToTextList(const npp2::data::LmbcsList &lmbcsList)` を追加
- include\npp2\converter\range.hpp
  - `TextList` の扱いを変更
- include\npp2\data\rfc822text.hpp
  - `TextList` の扱いを変更
- include\npp2\data\any.hpp
  - `TextList` の扱いを変更
- include\npp2\note.hpp
  - `static bool Note::hasItem(NOTEHANDLE hNote, const data::Lmbcs &field)` 追加
  - `bool Note::hasItem(const data::Lmbcs &field)` 追加

# [dev] v2.0.16-2022-10-12

- include\npp2_private\handleobject.hpp
  - `LockableObject2` 追加(ハンドルが有効であれば、デストラクタでハンドルを解放(`OSMemFree`)する)
- include\npp2\note.hpp
  - `data::Lmbcs Note::convertToText(const data::Lmbcs &name, char separator) const` 追加
  - `static data::Lmbcs Note::convertToText(NOTEHANDLE hNote, const data::Lmbcs &name, char separator)` 追加
- include\npp2\item.hpp
  - `data::Lmbcs convertToText( const data::Lmbcs &lineDelimiter, WORD charsPerLine, BOOL fStripTabs) const` 追加
  - `data::Lmbcs convertToText(WORD valueType, char separator) const` 追加

# [dev] v2.0.15-2022-10-05

- include\npp2\utils\filewriter.hpp
  - `void _FileWriter<T>::afterOpenFile(bool emptyFile)` 引数を追加
  - `T &_FileWriter<T>::operator()()` ファイルが空の状態を検知し、 `afterOpenFile` 関数に渡す。
- include\npp2\utils\qtextfilewriter.hpp
  - `void afterOpenFile(bool emptyFile)` 引数を追加し、 `pStream_->setGenerateByteOrderMark` に渡すブール値を `bom_` との論理積とする(BOM の途中挿入の防止のため)

# [dev] v2.0.14-2022-10-01

- include/npp2/data/any.hpp
  - `class Any`
    - `static Any fromItem(const Item &item)` 削除
    - `template <class S, class P, class T> static Lmbcs _rangeToString(const T &range)` 追加
    - `Lmbcs toString() const` 追加
- include/npp2/data/list.hpp
  - `inline npp2::data::TextList createTextList(const std::initializer_list<npp2::data::Lmbcs> &list)` 追加
- include/npp2/data/range.hpp
  - `template<Item,Pair> class Range`
    - `WORD pairSize() const` ハンドル型の指定ミスを修正
- include/npp2/item.hpp
  - `class Item`
    - `data::Any value() const` 追加
- include/npp2/note.hpp
  - `ScanItemsFn` 追加
  - `class Note`
    - `static STATUS LNPUBLIC scanItemsCallback(WORD, WORD itemFlags, char *name, WORD nameLength, void *pValue, DWORD valueLength, void *routineParameter)` 追加
    - `static void scanItems(NOTEHANDLE hNote, ScanItemsFn fn)` 追加
    - `void scanItems(const ScanItemsFn &fn) const` 追加
- include/npp2/utils/stdfilewriter.hpp
  - `class StdFileWriter`
    - `bool open()` 追記指定ミスを修正

# [dev] v2.0.13-2022-09-23

- include\npp2\note.hpp
  - `NoteClass::raw` 追加
- include\npp2\data\list.hpp
  - `List<DATATYPE>::clear` 追加
  - `bool contains(...)` 追加
  - `bool append(...)` 追加
  - `bool split(...)` 追加
- include\npp2\utils\os.hpp
  - `npp2::os::unifySep` テンプレート化
- include\npp2\utils\stdfilewriter.hpp
  - `StdFileWriter::open` バグフィックス

# [dev] v2.0.12-2022-09-16

- include\npp2\collection.hpp
  - `Entries` を追加
  - `ReadEntriesResult` を追加
  - `Collection` を追加
  - `CollectionPtr` を追加
- include\npp2\database.hpp
  - `Database::pUnreadIdTable_` を追加
  - `Database::openEx` を追加
  - `Database::findDesign` を追加
  - `Database::getUnreadIdTable` を追加
  - `Database::generateOid` を追加
  - `Database::createFolder` を追加
  - `Database::addToFolder` を追加
- include\npp2\idtable.hpp
  - `IdTable` を追加
  - `IdTablePtr` を追加
- include\npp2\nameslist.hpp
  - `NamesList` を追加
  - `NamesListPtr` を追加
- include\npp2\note.hpp
  - `Note::getNoteId` を変更
  - `Note::getOID` を変更
  - `Note::setNoteInfo` を追加
  - `Note::isUnread` を追加
  - `Note::setUnread` を追加
  - `Note::copy` を追加
  - `Note::update` を追加
  - `Note::updateEx` を追加
  - `Note::setText` を追加
- include\npp2\session.hpp
  - `SessionLocker` を追加
- include\npp2\unreadidtable.hpp
  - `UnreadIdTable` を追加

# [dev] v2.0.11-2022-09-12

- include\npp2\nls\info.hpp
  - `NativeInfo` を追加
- include\npp2\nls\translate.hpp
  - `nativeToLmbcs` を追加
  - `lmbcsToNative` を追加
- include\npp2\data\lmbcs.hpp
  - `nativeTo` 関数を追加
  - `toNative` 関数を追加
- include\npp2\utils\renamer.hpp
  - `Renamer::newPath` => `Renamer::renamePath` に変更
  - `CountRenamer::formatter_` => `CountRenamer::fn_` に変更
  - `TimestampRenamer` コンストラクタの `fn` 式のデフォルトを修正
- include\npp2\utils\rotator.hpp
  - `SizeRotator` のデフォルトコンストラクタを廃止
  - `SizeRotator::limit_` の値をコンストラクタで設定
  - `SizeRotator::setLimit` を廃止(作成時以降は変更不可)
  - `SizeRotator::limit` を追加
- include\npp2\utils\writer.hpp
  - `Writer` のストリームをテンプレート化
  - `createStdoutStream` を廃止
- include\npp2\utils\filewriter.hpp
  - `FileWriter` を `_FileWriter<T>` テンプレートクラスに変更
  - `_FileWriter<T>::codecName_` を廃止(QTextFileWriter に移行)
  - `_FileWriter<T>::bom_` を廃止(QTextFileWriter に移行)
  - `_FileWriter<T>::pFile_` を廃止(QFileWriter に移行)
  - `_FileWriter<T>::open` を追加(純粋仮想)
  - `_FileWriter<T>::afterOpenFile` を追加(仮想)
  - `_FileWriter<T>::isOpen` を純粋仮想に変更
  - `_FileWriter<T>::size` を純粋仮想に変更
  - `_FileWriter<T>::close` を純粋仮想に変更
- include\npp2\utils\stdfilewriter.hpp
  - `StdFileWriter` を `_FileWriter<std::ofstream>` ベースで追加
- include\npp2\utils\qfilewriter.hpp
  - `_QFileWriter<T>` を `_FileWriter<T>` ベースで追加
  - `_QFileWriter<T>::pFile_` を追加
  - `_QFileWriter<T>::open` を実装
  - `_QFileWriter<T>::isOpen` を実装
  - `_QFileWriter<T>::size` を実装
  - `_QFileWriter<T>::close` を実装
- include\npp2\utils\qdatafilewriter.hpp
  - `QDataFileWriter` を `_QFileWriter<QTextStream>` ベースで追加
- include\npp2\utils\qtextfilewriter.hpp
  - `QTextFileWriter` を `_QFileWriter<QDataStream>` ベースで追加
  - `QTextFileWriter::codecName_` を追加
  - `QTextFileWriter::bom_` を追加
  - `QTextFileWriter::afterOpenFile` を実装、ストリームにコーデックや BOM で調整
- include\npp2\logger\logger.hpp
  - `Writer` 仕様変更に対応
  - `createFile` メソッドに `codecName` `bom` を追加
  - インライン関数 `critical` `error` `warning` `info` `debug` `trace` を廃止

# [dev] v2.0.10-2022-08-26

- include\npp2\utils\filewriter.hpp
  - BOM の有無を管理する `bom_` を追加。
  - `codecPtr()` メソッドで、 `codecName_` が空の時はヌルポインタを返すようにする。

# [dev] v2.0-9-2022-08-05

- include\npp2\note.hpp
  - `NoteClass` 追加。
- include\npp2\utils\filewriter.hpp
- include\npp2\utils\rotator.hpp
- include\npp2\utils\renamer.hpp
  - `RenamerPtr` と `RotatorPtr` の定義と適用。
- include\npp2\utils\os.hpp
  - `npp2::os::unifySep` 追加。

# [dev] v2.0-8-2022-07-29

- include\npp2\data\list.hpp
  - `List<DATATYPE>::getAt(WORD,LIST*,BOOL)` を `List<DATATYPE>::getAt(WORD,LIST*,BOOL,bool)` に修正し、テキストアイテムにヌル文字が含まれていたら改行文字に置き換えられるようにする。
  - `getAt` の修正に伴う関連メソッドの修正。

# [dev] v2.0-7-2022-07-21

- Linux 対応
- include/npp2/extmgr.hpp
  - `EMRECORD` の対象処理判定を Windows x64 用と Linux 用に分ける。
- include/npp2/item.hpp
  - 不足ヘッダーを追加。
- include/npp2/npp2_global.h
  - `WINAPI` など Linux では使えないので、別途 `NPP2CALLBACK` を立てて、Linux では `LNPUBLIC` を使うようにする。
- include/npp2/search.hpp
  - `WINAPI` => `NPP2CALLBACK`
- include/npp2/\_private/range.hpp
  - 不足ヘッダーを追加。
  - `Hoge::type` のようなシンボルが型を表すときは `typename` を頭に入れる。
- include/npp2/data/any.hpp
  - ローカルヘッダーの位置を正確に指定。
- include/npp2/data/range.hpp
  - `typename` が不要な指定は外す。
  - `this` が必要なメンバーに `this->` を指定する。
  - `at` が通らない場所を `getItemAt` に変更する。
- include/npp2/logger/formatter.hpp
  - 仮想デストラクタがないクラスに仮想デストラクタを設置する。
- include/npp2/nls/status.hpp
  - `noexcept` が不足しているメソッドに追加。
- include/npp2/utils/filewriter.hpp
  - `override` が不足しているメソッドに追加。
- include/npp2/utils/renamer.hpp
  - 仮想デストラクタがないクラスに仮想デストラクタを設置する。
- include/npp2/utils/rotator.hpp
  - 仮想デストラクタがないクラスに仮想デストラクタを設置する。
  - `override` が不足しているメソッドに追加。
- include/npp2/utils/writer.hpp
  - 仮想デストラクタがないクラスに仮想デストラクタを設置する。

# [dev] v2.0-6-2022-07-03

- include\npp2\extmgr.hpp
  - `EMRECORD::EId` と `EMRECORD::NotificationType` の間に境界違いがあり、正しい値を示していないことを確認したので、正しい値で比較できるように調整。

# [dev] v2.0-5-2022-06-29

- include\npp2\nls\translate.hpp
  - 1 バイト文字が変換できないバグを修正。
- include\npp2\note.hpp
  - `npp2::Note::text256` を Static 版とメンバー版に分割。

# [dev] v2.0-4-2022-06-17

- include\npp2\utils\renamer.hpp
  - カウンターリネームで最終更新番号の次に 0 に戻らない不具合を修正

# [dev] v2.0-3-2022-06-16

- include\npp2\data\rfc822text.hpp
- include\npp2\mime\codec.hpp
- include\npp2\mime\decode.hpp
- include\npp2\mime\quotedprintable.hpp
  - 追加
- include\npp2\data\any.hpp
  - `npp2::data::Any::toRfc822Text` 追加
- include\npp2\data\timedate.hpp
  - `npp2::data::TimeDate::fromQDateTime` 追加

# [dev] v2.0-2-2022-06-05

- include\npp\database.hpp
  - `npp::Database::getReplicaId_x` 追加
  - `npp::Database::getReplicaInfo_x` 追加
- include\npp\utils\csv\fields\datetime.hpp

  - `DateTimeISO8601` 追加

- include\npp2\env.hpp
  - `npp2::env::getString` 引数変更
  - `npp2::env::getNumber` 引数変更
- include\npp2\data\lmbcs.hpp
  - `npp2::data::Variant` 仕様変更に対応
  - `npp2::data::Lmbcs::data` 追加
- include\npp2\data\variant.hpp
  - `npp2::data::Variant::adjust` 削除(`npp2::dataptr` に委譲)
- include\npp2\http\install.hpp

  - `npp2::http::filterEventSummary<T,Head,Tails>` 追加
  - `npp2::http::filterEventSummary<T>` 追加

- include\npp2\compute.hpp
- include\npp2\database.hpp
- include\npp2\dname.hpp
- include\npp2\extmgr.hpp
- include\npp2\formula.hpp
- include\npp2\item.hpp
- include\npp2\itemtable.hpp
- include\npp2\note.hpp
- include\npp2\search.hpp
- include\npp2\thread.hpp
- include\npp2_private\block.hpp
- include\npp2_private\handleobject.hpp
- include\npp2_private\range.hpp
- include\npp2\converter\base.hpp
- include\npp2\converter\extintlformat.hpp
- include\npp2\converter\intlformat.hpp
- include\npp2\converter\number.hpp
- include\npp2\converter\numberpair.hpp
- include\npp2\converter\range.hpp
- include\npp2\converter\time.hpp
- include\npp2\converter\timepair.hpp
- include\npp2\data\any.hpp
- include\npp2\data\list.hpp
- include\npp2\data\lmbcs.hpp
- include\npp2\data\number.hpp
- include\npp2\data\numberrange.hpp
- include\npp2\data\range.hpp
- include\npp2\data\time.hpp
- include\npp2\data\timedate.hpp
- include\npp2\data\timedaterange.hpp
- include\npp2\data\variant.hpp
- include\npp2\format\number.hpp
- include\npp2\format\time.hpp
- include\npp2\http\authenticate.hpp
- include\npp2\http\exception.hpp
- include\npp2\http\exceptionsender.hpp
- include\npp2\http\Filter.hpp
- include\npp2\http\request.hpp
- include\npp2\http\response.hpp
- include\npp2\http\responsestatus.hpp
- include\npp2\http\charset\utf8.hpp
- include\npp2\http\ctxext_ctxext.hpp
- include\npp2\http\ctxext\authenticate.hpp
- include\npp2\jwt\algorithm.hpp
- include\npp2\jwt\base64.hpp
- include\npp2\jwt\bio.hpp
- include\npp2\jwt\ec.hpp
- include\npp2\jwt\evp.hpp
- include\npp2\jwt\header.hpp
- include\npp2\jwt\hmac.hpp
- include\npp2\jwt\object.hpp
- include\npp2\jwt\payload.hpp
- include\npp2\jwt\rsa.hpp
- include\npp2\jwt\sha.hpp
- include\npp2\jwt\sign.hpp
- include\npp2\jwt\verify.hpp
- include\npp2\logger\formatter.hpp
- include\npp2\logger\level.hpp
- include\npp2\logger\logger.hpp
- include\npp2\utils\container.hpp
- include\npp2\utils\data.hpp
- include\npp2\utils\filewriter.hpp
- include\npp2\utils\function.hpp
- include\npp2\utils\hexint.hpp
- include\npp2\utils\pairlisttomap.hpp
- include\npp2\utils\process.hpp
- include\npp2\utils\renamer.hpp
- include\npp2\utils\replicaid.hpp
- include\npp2\utils\rotator.hpp
- include\npp2\utils\thread.hpp
- include\npp2\utils\time.hpp
- include\npp2\utils\unid.hpp
- include\npp2\utils\writer.hpp
- installer\makeDominoAddinBasename.js
  - 新設

# [dev] v2.0-1-2022-03-06

- npp202009.pro
  - include/npp2 サブプロジェクトを追加
- include\npp2\env.hpp
  - `npp2::env::getString` 追加
  - `npp2::env::setString` 追加
  - `npp2::env::has` 追加
  - `npp2::env::getNumber` 追加
  - `npp2::env::setNumber` 追加
  - `npp2::env::getDataDirectory` 追加
  - `npp2::env::getProgramDirectory` 追加
- include/npp2/status.hpp
  - `npp2::Status` 追加
- include\npp2\addin\logmessage.hpp
  - `npp2::addin::logMessageText` 追加
- include\npp2\data\lmbcs.hpp
  - `npp2::data::Lmbcs` 追加
  - `npp2::fromStatus` 追加
  - `toLmbcs` 追加
  - `fromLmbcs` 追加
- include\npp2\data\variant.hpp
  - `npp2::data::Variant` 追加
- include\npp2\http\error.hpp
  - `npp2::http::Error` 追加
- include\npp2\http\install.hpp
  - `npp2::http::install` 追加
- include\npp2\nls\info.hpp
  - `npp2::nls::Info` 追加
  - `npp2::nls::LoadedInfo` 追加
  - `npp2::nls::LmbcsInfo` 追加
- include\npp2\nls\size.hpp
  - `npp2::nls::getStringBytes` 追加
  - `npp2::nls::getStringChars` 追加
  - `npp2::nls::adjustByteSize` 追加
- include\npp2\nls\status.hpp
  - `npp2::nls::Status` 追加
- include\npp2\nls\translate.hpp
  - `npp2::nls::KiBytesToBytes` 追加
  - `npp2::nls::MAX_TRANSLATE_BUFFER_SIZE` 追加
  - `npp2::nls::TranslateResult` 追加
  - `npp2::nls::translateMax64kib` 追加
  - `npp2::nls::translate` 追加
  - `npp2::nls::unicodeToLmbcs` 追加
  - `npp2::nls::lmbcsToUnicode` 追加

# 1.0.0.7-20220305

- installer/libs.js
  - このファイルを廃止し、定義内容を以下のファイルに割り振る
    - installer\BitWidth.js
    - installer\CopyFiles.js
    - installer\makeLibName.js
    - installer\makeXml.js
    - installer\OpenSSL.js
    - installer\os.js
    - installer\Package.js
    - installer\QtCompiler.js
    - installer\QtInstallerFramework.js
    - installer\QtProject.js
    - installer\QtRuntime.js
    - installer\removeDirRecursive.js
    - installer\UniversalCRuntime.js
    - installer\VisualCpp2017.js

# 1.0.0.6-20220303

- include/npp/http/error.hpp
- include/npp/http/exception.hpp
  - GCC に適合するように `Error::what` のシンボルを調整
- installer/libs.js
  - Linux 版用に以下を調整
    - `os.libName`
    - `QtCompiler.libName`
    - `QtRuntime.sharedLibFile`
    - `QtPluginsLibBuild.files`
    - `OpenSSLBuild.buildPath`
    - `OpenSSLBuild.files`

# 1.0.0.5-20220302

- installer\libs.js
  - `OpenSSL`,`OpenSSLBuild` クラスを追加
- include\npp\http\directresponse.hpp
  - `DirectResponse::render_x` で本体がなくても改行を 2 つ出力するようにする(Chrome の HTTPS/自己認証証明書対策として)

# 1.0.0.4-20220301

- installer\libs.js
  - `Package.createSubdir` 追加
  - `Package.clearData` 内部で呼び出す `clearDir` を `await` コールにする
  - `dllName` 関数追加
  - `VisualCpp2017_14_0.constructor` debug 値の追加
  - `VisualCpp2017_14_0.dir` => `VisualCpp2017_14_0.buildPath`
  - `VisualCpp2017_14_0.createLibBuild` 追加
  - `VisualCpp2017_14_1.constructor` debug 値の追加
  - `VisualCpp2017_14_1.dir` => `VisualCpp2017_14_1.buildPath`
  - `VisualCppBuild` クラスの追加
  - `createVisualCpp` debug 値の追加
  - `UniversalCRuntime` クラスの追加
  - `UniversalCRTBuild` クラスの追加
  - `QtCompiler.releaseOrDebug` 追加
  - `QtCompiler.libName` 修正
  - `QtRuntime.rootPath` 追加
  - `QtRuntime.libPath` => `QtRuntime.libDir`
  - `QtRuntime.pluginsDir` 追加
  - `QtRuntime.sharedLib` 削除
  - `QtRuntime.sharedLibFile` 追加
  - `QtRuntime.icuLib` 削除
  - `QtRuntime.icuLibFile` 追加
  - `QtRuntime.createLibBuild` 追加
  - `QtRuntime.createICULibBuild` 追加
  - `QtRuntime.createPluginsLibBuild` 追加
  - `QtRuntimeLibBuild` クラス追加
  - `QtPluginsLibBuild` クラス追加
  - `QtICULibBuild` クラス追加
  - `QtMainBuild.createSubBuild` files 値の追加
  - `QtMainBuild.rootPath` 修正
  - `CopyFiles.copy` 修正
  - `QtInstallerFramework` クラス追加

# 1.0.0.3-20220226

- include\npp\utils\csv\file.hpp
- include\npp\utils\csv\filebase.hpp
  - `writeRecord` で `flush` 制御しなくなったので、別途 `flush` メソッドを追加

# 1.0.0.2-20220225

- include\npp\database.hpp
  - `Database::getInfo_x` を追加
  - `static Database::reopen_x` を追加
- include\npp\note.hpp
  - `Note::getItem_x` を追加
  - `Note::attachFileNames_x` を追加
- include\npp\session.hpp
  - `class ThreadLocker` を追加
- include\npp\summary.hpp
  - `NamedItemTable::value` を追加
- include\npp\utils\csv\file.hpp
  - `File::writeRecord` の引数から `flush` を削除
- include\npp\utils\csv\filebase.hpp
  - `FileBase::writeRecord` の引数から `flush` を削除
- include\npp\utils\logger\logger.hpp
  - `DefaultFormatter` でスレッド番号を表示できるようにする
  - `Logger::output` を仮想関数化し、 `writer_.flush()` を呼び出すようにする
- include\npp\utils\writer\file.hpp
  - `File::fileMutex` を廃止
  - `File::write` の引数 `flush` を廃止
  - `File::_write` の引数 `flush` を廃止
  - `File::flush` を追加
- include\npp\utils\writer\filebase.hpp
  - `FileBase::fileMutex_` を廃止
  - `FileBase::readWriteMutex_` を廃止
  - `FileBase::fileMutex` を廃止
  - `FileBase::write` の引数 `flush` を廃止
  - `FileBase::_write` の引数 `flush` を廃止
  - `FileBase::flush` を追加
- include\npp\utils\writer\rotator\filescountrotator.hpp
  - `FilesCountRotator::rotate` のミューテックスロックを削除
- include\npp\utils\writer\rotator\timestamprotator.hpp
  - `TimestampRotator::rotate` のミューテックスロックを削除
- installer\libs.js
  - 次世代インストーラ作成用ライブラリを追加
- installer\package.json
  - 次世代インストーラ作成用ライブラリ用に追加

# 1.0.0.1-20220218

- インストーラの内包

# [next->v1.0] 1.0.0.0-20220218

- dev ブランチを削除(リモートには未アップ)
- master->v0.1 に移行
- next->v1.0 に移行
- next を master に統合
- next ローカルブランチを削除
- 以降、master で作業

# [next]0.0.15-20220210

- include\npp\utils\utils.hpp
  - `getSeparator<T>` 関数を追加

# [next]0.0.14-20220208

- include\npp\summary.hpp
  - `NamedItemTable` クラスを追加
- include\npp\type\lmbcslist.hpp
  - `LmbcsList` クラスに `toQStringList` メソッドを追加

# [next]0.0.13-20211220

- include\npp\envstd.hpp 追加
- include\npp\item.hpp
  - `extractAttachFile_x` 追加
- include\npp\note.hpp
  - `getTextItem` 追加
  - `setTextItem` 追加
  - `update` 追加
  - `create` 追加
- include\npp\extmgr\repository.hpp
  - `createRecursionID_x` 追加
- include\npp\http\authenticate.hpp
  - `getUserNameList_x` 追加
  - `getHeader_x` 追加
- include\npp\http\context.hpp
  - `writeClient_x` 追加
  - `serverSupport_x` 追加
  - `getAuthenticatedUser_x` 追加
- include\npp\http\directresponse.hpp
  - `render_x` 追加
- include\npp\http\exec.hpp
  - `exec_x` 追加
  - `Filter` 追加
- include\npp\http\install.hpp
  - `install_x` 追加
- include\npp\http\request.hpp
  - `QString::arg` の書き方変更
- include\npp\type\distinguishedname.hpp
  - `canonical_x` 追加
  - `getComponents_x` 追加
  - `orgName_x` 追加
  - `currentUserName_x` 追加
- include\npp\type\lmbcslist.hpp
  - `convert` 追加
- include\npp\utils\jwt\jsonwebtoken.hpp
  - `encode_x` 追加
  - `decode_x` 追加

# [next]0.0.12-20211029

- npp202009::npp::Compute::evaluate_x(...)
- npp202009::npp::Compute::compute_x(...)
- npp202009::npp::Database::open_x(...)
- npp202009::npp::Database::getAccessingUserName_x(...)
- npp202009::npp::Database::getPaths_x(...)
- npp202009::npp::Database::getInfo_x(...)
- npp202009::npp::Database::makeNetpath_x(...)
- npp202009::npp::ExtensionManager2
- npp202009::npp::Formula::compile_x(...)
- npp202009::npp::Note::getItem_x(...)
- npp202009::npp::Note::attachFileNames_x(...)
- npp202009::npp::Note::open_x(...)
- npp202009::npp::Search::ListIterator
- npp202009::npp::Search::reduce
- npp202009::npp::Search::callbackReducer

# [next]0.0.11-20210802

- エクステンションマネージャ回りを改良。
- Note クラスメンバーの一部を this から起動できるようにする。

# 0.0.10-20210628

- http::exec や http::install のヘルパーを修正。

# 0.0.9-20210318

- 仕様説明コメントの追加。
- 1 ファイルに複数クラスが定義されている場合の整理(部分的)。

# 0.0.8

- `std::tuple<WORD,Lmbcs,QByteArray>` の別名として　`Note::DefaultScanData` を定義する。
- `static STATUS LNPUBLIC Note::defaultScanItemsCallback(...)`　を追加する。
- `Note::scanItem` の仕様を以下のように変更する。

1. 型　`NSFITEMSCANPROC` の引数を 1 つ増やし、初期値を　`defaultScanItemsCallback` にする。
2. 戻り値型を `observable<Lmbcs>` から　`observable<T>` に変更し、　`T`　の初期値を　`DefaultScanData` にする。

- `Note::getFieldNames` が `scanItems` を呼び出す時、型 `T` が要指定になるので、 `Lmbcs` とする。

# 0.0.7

- Logger からレベル名が表示されていないバグを修正。

# 0.0.6

- FileRotator を FileRotator ベース、TimestampRotator 拡張に変更し、さらに FileCountRotator 拡張を増やす。
- Logger を追加する。

# 0.0.5-20200112

- Linux 最適化。

# 0.0.5-20200106

- その他すべての追加、変更、削除を含め、2021-01-07T08:16 時点でのスナップショットとしてマーク。

# 0.0.4

- addin::logMessage などを logmessage.hpp に移動する。
- Database::getReplicaInfo を追加する。
- Database::getAccessingUserName を追加する。
- env::has を追加する。
- env::getNumber<T>を追加する。
- extmgr 名前空間を追加する。
- extmgr::RegData クラスを追加する。
- extmgr::ExtensionManager クラスを追加する。
- handle::Lockable を lockable.hpp に移動する。
- handle::Object を object.hpp に移動する。
- handle::Block を block.hpp に移動する。
- http::Authenticate のコンストラクタの引数を `void*` から `const Context &` に変更する。
- http::Authenticate に setAuthName を追加する。
- http::Authenticate に setAuthType を追加する。
- http::Authenticate に getHeader を追加する。
- http::Authorize のコンストラクタの引数を `void*` から `const Context &` に変更する。
- http::Authorize に isAuthorized を追加する。
- http::UserNameList のコンストラクタの引数を `void*` から `const Context &` に変更する。
- http::Context 内の static メソッドを個別の関数として独立させる。
- http::DirectResponse に static メソッド reponse を追加する。
- http::exec を追加する。
- http::Request::matchUrl を追加する。
- kfm 名前空間を追加する。
- kfm::getUserName を追加する。
- formatId<T>を追加する。
- Note::getFieldNamesCallback を追加する。
- Note::getFieldNames を追加する。
- Note::scanItems を追加する。
- function::toStringList<T>を追加する。
- jwt::Bio を追加する。
- jwt::Json を追加する。
- jwt::Header を削除する。
- jwt::HeaderJson を追加する。
- jwt::Payload を削除する。
- jwt::PayloadJson を追加する。
- jwt::JsonWebToken の header*と payload*の型を変更する。
- jwt::MessageDigest::decode の設計を見直す。
- jwt::throwErrors を追加する。
- (utils)getProcessFilePath を追加する。
- (utils)getProcessFileInfo を追加する。

# 0.0.3

- `class http::Authenticate` を追加。
- `FilterContext *http:Context::ctx() const` を追加。

# 0.0.2

- doap202009 で認可コードプロセスを一通り作業できるようにする。

# 0.0.1

- `http::install` 関数オブジェクトを作成

# 0.0.0

- プロジェクトを立ち上げる。
