TARGET = npp_v
DEFINES += NPP2_LIBRARY

QT -= gui
TEMPLATE = lib
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

# 製品/バージョン設定
TargetVersion = 2.1.0.0
TargetCompany = Chiburu Systems
TargetDomain = chiburu.com
TargetCopyright = 2024 Chiburu Systems
TargetProduct = npp_library
TargetDescription = Notes++ Library on Notes C API
include(./version.pri)
include(./local.pri)
include(./global.pri)

HEADERS += \
  include/npp2/_private/block.hpp \
  include/npp2/_private/handleobject.hpp \
  include/npp2/_private/range.hpp \
  include/npp2/addin/logmessage.hpp \
  include/npp2/converter/base.hpp \
  include/npp2/converter/extintlformat.hpp \
  include/npp2/converter/intlformat.hpp \
  include/npp2/converter/number.hpp \
  include/npp2/converter/numberpair.hpp \
  include/npp2/converter/range.hpp \
  include/npp2/converter/time.hpp \
  include/npp2/converter/timepair.hpp \
  include/npp2/data/any.hpp \
  include/npp2/data/lmbcs.hpp \
  include/npp2/data/lmbcslist.hpp \
  include/npp2/data/number.hpp \
  include/npp2/data/numberrange.hpp \
  include/npp2/data/range.hpp \
  include/npp2/data/rfc822text.hpp \
  include/npp2/data/textlist.hpp \
  include/npp2/data/time.hpp \
  include/npp2/data/timedate.hpp \
  include/npp2/data/timedaterange.hpp \
  include/npp2/data/variant.hpp \
  include/npp2/format/number.hpp \
  include/npp2/format/time.hpp \
  include/npp2/logger/defaultformatter.hpp \
  include/npp2/logger/formatter.hpp \
  include/npp2/logger/index.hpp \
  include/npp2/logger/level.hpp \
  include/npp2/mime/codec.hpp \
  include/npp2/mime/decode.hpp \
  include/npp2/mime/quotedprintable.hpp \
  include/npp2/nls/info.hpp \
  include/npp2/nls/size.hpp \
  include/npp2/nls/status.hpp \
  include/npp2/nls/translate.hpp \
  include/npp2/utils/renamer/count.hpp \
  include/npp2/utils/renamer/index.hpp \
  include/npp2/utils/renamer/timestamp.hpp \
  include/npp2/utils/rotator/daily.hpp \
  include/npp2/utils/rotator/index.hpp \
  include/npp2/utils/rotator/size.hpp \
  include/npp2/utils/writer/filewriter.hpp \
  include/npp2/utils/writer/index.hpp \
  include/npp2/utils/writer/qdatafilewriter.hpp \
  include/npp2/utils/writer/qfilewriter.hpp \
  include/npp2/utils/writer/qtextfilewriter.hpp \
  include/npp2/utils/writer/stdfilewriter.hpp \
  include/npp2/utils/audit.hpp \
  include/npp2/utils/data.hpp \
  include/npp2/utils/function.hpp \
  include/npp2/utils/hexint.hpp \
  include/npp2/utils/index.hpp \
  include/npp2/utils/os.hpp \
  include/npp2/utils/process.hpp \
  include/npp2/utils/replicaid.hpp \
  include/npp2/utils/thread.hpp \
  include/npp2/utils/time.hpp \
  include/npp2/utils/unid.hpp \
  include/npp2/acl.h \
  include/npp2/attachmentitem.hpp \
  include/npp2/collection.hpp \
  include/npp2/compute.hpp \
  include/npp2/database.hpp \
  include/npp2/dname.hpp \
  include/npp2/env.hpp \
  include/npp2/extmgr.hpp \
  include/npp2/formula.hpp \
  include/npp2/idtable.hpp \
  include/npp2/item.hpp \
  include/npp2/itemtable.hpp \
  include/npp2/namelookup.hpp \
  include/npp2/nameslist.hpp \
  include/npp2/note.hpp \
  include/npp2/npp2_global.h \
  include/npp2/search.hpp \
  include/npp2/status.hpp \
  include/npp2/thread.hpp \
  include/npp2/unreadidtable.hpp

SOURCES += \
  src/npp2_acl.cpp \
  src/npp2_attachmentitem.cpp \
  src/npp2_collection.cpp \
  src/npp2_compute.cpp \
  src/npp2_converter_intlformat.cpp \
  src/npp2_converter_number.cpp \
  src/npp2_converter_numberpair.cpp \
  src/npp2_converter_time.cpp \
  src/npp2_converter_timepair.cpp \
  src/npp2_data_any.cpp \
  src/npp2_data_lmbcs.cpp \
  src/npp2_data_lmbcslist.cpp \
  src/npp2_data_number.cpp \
  src/npp2_data_numberrange.cpp \
  src/npp2_data_rfc822text.cpp \
  src/npp2_data_textlist.cpp \
  src/npp2_data_time.cpp \
  src/npp2_data_timedate.cpp \
  src/npp2_data_timedaterange.cpp \
  src/npp2_data_variant.cpp \
  src/npp2_database.cpp \
  src/npp2_dname.cpp \
  src/npp2_env.cpp \
  src/npp2_extmgr.cpp \
  src/npp2_format_number.cpp \
  src/npp2_format_time.cpp \
  src/npp2_formula.cpp \
  src/npp2_idtable.cpp \
  src/npp2_item.cpp \
  src/npp2_itemtable.cpp \
  src/npp2_logger_defaultformatter.cpp \
  src/npp2_logger_index.cpp \
  src/npp2_logger_level.cpp \
  src/npp2_mime_codec.cpp \
  src/npp2_mime_decode.cpp \
  src/npp2_mime_quotedprintable.cpp \
  src/npp2_namelookup.cpp \
  src/npp2_nameslist.cpp \
  src/npp2_nls_info.cpp \
  src/npp2_nls_size.cpp \
  src/npp2_nls_status.cpp \
  src/npp2_nls_translate.cpp \
  src/npp2_note.cpp \
  src/npp2_private_block.cpp \
  src/npp2_private_handleobject.cpp \
  src/npp2_private_range.cpp \
  src/npp2_search.cpp \
  src/npp2_status.cpp \
  src/npp2_thread.cpp \
  src/npp2_unreadidtable.cpp \
  src/npp2_utils_index.cpp \
  src/npp2_utils_process.cpp \
  src/npp2_utils_renamer_count.cpp \
  src/npp2_utils_renamer_index.cpp \
  src/npp2_utils_renamer_timestamp.cpp \
  src/npp2_utils_replicaid.cpp \
  src/npp2_utils_rotator_daily.cpp \
  src/npp2_utils_rotator_index.cpp \
  src/npp2_utils_rotator_size.cpp \
  src/npp2_utils_thread.cpp \
  src/npp2_utils_time.cpp \
  src/npp2_utils_unid.cpp \
  src/npp2_utils_writer_qdatafilewriter.cpp \
  src/npp2_utils_writer_qtextfilewriter.cpp \
  src/npp2_utils_writer_stdfilewriter.cpp
