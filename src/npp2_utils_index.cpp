#include "../include/npp2/utils/index.hpp"

#include <QUrlQuery>

namespace npp2::utils {

void addQueryItem(
  QUrl &url,
  const QString &key,
  const QString &value
) {
  QUrlQuery query(url);
  query.addQueryItem(key, value);
  url.setQuery(query);
}

} // namespace npp2::utils
