#include "../include/npp2/utils/writer/stdfilewriter.hpp"

namespace npp2 {

/**
 * @brief コンストラクタ
 * @param filePath ファイルパス
 */
StdFileWriter::StdFileWriter(
  const QString &filePath,
  std::function<void(const QString&)> fn
)
  : _FileWriter<StdStream>(filePath, fn)
{}

/**
 * @brief ファイルを開く。
 * @return true 成功
 * @return false 失敗
 */
bool StdFileWriter::open() {
#ifndef NT
  auto path = filePath_.toUtf8().toStdString();
#else
  auto path = filePath_.toStdWString();
#endif
  auto mode = std::ios_base::out | std::ios_base::app;
  auto p = new StdStream(path, mode);
  pStream_.reset(p);
  return isOpen();
}

/**
 * @brief ファイルは開いているか
 * @return true 開いている
 * @return false 閉じている
 */
bool StdFileWriter::isOpen() const {
  return pStream_ && pStream_->is_open();
}

/**
 * @return qint64 ファイルサイズを返す。
 */
qint64 StdFileWriter::size() const {
  QFileInfo fileInfo(filePath_);
  return fileInfo.size();
}

/**
 * @brief ファイルを閉じる。
 */
void StdFileWriter::close() {
  if (isOpen()) {
    pStream_->close();
    pStream_.reset();
  }
}

/**
 * @brief ファイルオープン後に実行する処理
 */
void StdFileWriter::afterOpenFile(bool)
{}

} // namespace npp2
