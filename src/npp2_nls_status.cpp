#include "../include/npp2/nls/status.hpp"

namespace npp2::nls {

/**
 * @brief コンストラクタ
 */
Status::Status()
  : std::exception()
  , raw_(NLS_SUCCESS)
{}

/**
 * @brief コンストラクタ
 * @param status NLSステータス
 */
Status::Status(NLS_STATUS status)
  : std::exception()
  , raw_(status)
{}

/**
 * @brief エラー値
 */
NLS_STATUS Status::raw() const {
  return raw_;
}

/**
 * @brief 成功していれば真
 */
Status::operator bool() const {
  return raw() == NLS_SUCCESS;
}

/**
 * @brief std::exception::whatをオーバーライド
 */
const char *Status::what() const noexcept {
  switch (raw_) {
  case NLS_SUCCESS: return "[npp2::nls] Success.";
  case NLS_BADPARM: return "[npp2::nls] Bad Parameters.";
  case NLS_BUFFERTOOSMALL: return "[npp2::nls] Buffer too small.";
  case NLS_CHARSSTRIPPED: return "[npp2::nls] Chars stripped.";
  case NLS_ENDOFSTRING: return "[npp2::nls] End of string.";
  case NLS_FALLBACKUSED: return "[npp2::nls] Fallback used.";
  case NLS_FILEINVALID: return "[npp2::nls] File invalid.";
  case NLS_FILENOTFOUND: return "[npp2::nls] File not found.";
  case NLS_FINDFAILED: return "[npp2::nls] Find failed.";
  case NLS_INVALIDCHARACTER: return "[npp2::nls] Invalid character.";
  case NLS_INVALIDDATA: return "[npp2::nls] Invalid data.";
  case NLS_INVALIDENTRY: return "[npp2::nls] Invalid entry.";
  case NLS_INVALIDTABLE: return "[npp2::nls] Invalid table.";
  case NLS_PROPNOTFOUND: return "[npp2::nls] Prop not found.";
  case NLS_STARTOFSTRING: return "[npp2::nls] Start of string.";
  case NLS_STRINGSIZECHANGED: return "[npp2::nls] String size changed.";
  case NLS_TABLEHEADERINVALID: return "[npp2::nls] Table header invalid.";
  case NLS_TABLENOTFOUND: return "[npp2::nls] Table not found.";
  default: return "[npp2::nls] Unknown NLS status.";
  }
}

} // namespace npp2::nls
