#include "../include/npp2/unreadidtable.hpp"
#include "../include/npp2/dname.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfdb.h>
#include <nsferr.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

UnreadIdTable::UnreadIdTable(DBHANDLE hDb, DHANDLE handle, DHANDLE hOrigin)
  : IdTable(handle)
  , hDb_(hDb)
  , hOriginalTable_(hOrigin)
{}

UnreadIdTable *UnreadIdTable::build(DBHANDLE hDb) {
  auto canonical = DName::currentUserName().value();
  DHANDLE handle = NULLHANDLE, hOrigin = NULLHANDLE;
  Status status = NSFDbGetUnreadNoteTable(
    hDb,
    const_cast<char*>(canonical.constData()),
    canonical.size(),
    FALSE,
    &handle
  );
  if (!status) { throw status; }
  if (handle == NULLHANDLE) {
    status = NSFDbGetUnreadNoteTable(
      hDb,
      const_cast<char*>(canonical.constData()),
      canonical.size(),
      TRUE,
      &handle
    );
    if (status.error() == ERR_NO_MODIFIED_NOTES) {
      status = IDCreateTable(sizeof(NOTEID), &handle);
    }
    if (!status) { throw status; }
  }

  status = IDTableCopy(handle, &hOrigin);
  if (!status) { throw status; }
  status = NSFDbUpdateUnread(hDb, handle);
  if (!status) { throw status; }
  return new UnreadIdTable(hDb, handle, hOrigin);
}

UnreadIdTable::~UnreadIdTable() {
  update();
  if (hOriginalTable_) {
    IDDestroyTable(hOriginalTable_);
    hOriginalTable_ = NULLHANDLE;
  }
}

void UnreadIdTable::update() {
  if (!isNull() && hOriginalTable_ != NULLHANDLE) {
    auto canonical = DName::currentUserName().value();
    Status status = NSFDbSetUnreadNoteTable(
      hDb_,
      const_cast<char*>(canonical.constData()),
      canonical.size(),
      FALSE,
      hOriginalTable_,
      rawHandle()
    );
  }
}

} // namespace npp2
