#include "../include/npp2/utils/thread.hpp"
#include <sstream>
// #include <QString>
// #include <thread>

namespace npp2 {

QString threadIdToText(const std::thread::id &id) {
  std::ostringstream strm;
  strm << id;
  return QString::fromStdString(strm.str());
}

} // namespace npp2
