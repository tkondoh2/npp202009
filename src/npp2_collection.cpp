#include "../include/npp2/collection.hpp"
#include "../include/npp2/status.hpp"
// #include "_private/handleobject.hpp"

// #if defined(NT)
// #pragma pack(push, 1)
// #endif

// #include <nif.h>

// #if defined(NT)
// #pragma pack(pop)
// #endif

namespace npp2 {

Entries::Entries()
  : npp2::LockableObject3<DHANDLE>()
{}

Entries::Entries(DHANDLE handle)
  : npp2::LockableObject3<DHANDLE>(handle)
{}

Entries::~Entries() {}

void NIFCloseCollectionDeleter::operator ()(HCOLLECTION hCol) {
  Status status = NIFCloseCollection(hCol);
  if (!status) { throw status; }
}

_Collection::_Collection(
  HCOLLECTION hCol,
  UNID unid,
  DHANDLE hCollapsedList,
  DHANDLE hSelectedList
)
  : LockableObject2<HCOLLECTION,NIFCloseCollectionDeleter>(hCol)
  , unid_(unid)
  , hCollapsedList_(hCollapsedList)
  , hSelectedList_(hSelectedList)
{}

_Collection::~_Collection()
{}

Collection _Collection::open(
  DBHANDLE hViewDb,
  DBHANDLE hDataDb,
  NOTEID viewNoteId,
  WORD openFlags,
  DHANDLE hUnread
) {
  HCOLLECTION hCol = NULLHCOLLECTION;
  UNID unid;
  DHANDLE hCollapsedList = NULLHANDLE;
  DHANDLE hSelectedList = NULLHANDLE;
  Status status = NIFOpenCollection(
    hViewDb,
    hDataDb,
    viewNoteId,
    openFlags,
    hUnread,
    &hCol,
    nullptr,
    &unid,
    &hCollapsedList,
    &hSelectedList
  );
  if (!status) { throw status; }
  return std::make_unique<_Collection>(hCol, unid, hCollapsedList, hSelectedList);
}

Collection _Collection::open(
  DBHANDLE hDb,
  NOTEID viewNoteId,
  WORD openFlags,
  DHANDLE hUnread
) {
  return open(hDb, hDb, viewNoteId, openFlags, hUnread);
}

DWORD _Collection::entriesCount(HCOLLECTION hCol) {
  COLLECTIONPOSITION pos { 0, 0, 0, { 1 } };
  DHANDLE hBuffer = NULLHANDLE;
  DWORD count = 0;
  Status status = NIFReadEntries(
    hCol,
    &pos,
    NAVIGATE_CURRENT,
    0,
    NAVIGATE_NEXT,
    MAXDWORD,
    0,
    &hBuffer,
    nullptr,
    nullptr,
    &count,
    nullptr
  );
  if (hBuffer) {
    OSMemFree(hBuffer);
  }
  if (!status) {
    throw status;
  }
  return count;
}

DWORD _Collection::entriesCount() const {
  return entriesCount(rawHandle());
}

void _Collection::readEntries(
  HCOLLECTION hCol,
  WORD skipNavigator,
  DWORD skipCount,
  WORD returnNavigator,
  DWORD returnCount,
  DWORD returnMask,
  ReadEntriesResult *result
) {
  DHANDLE hBuffer = NULLHANDLE;
  Status status = NIFReadEntries(
    hCol,
    &result->pos_,
    skipNavigator,
    skipCount,
    returnNavigator,
    returnCount,
    returnMask,
    &hBuffer,
    &result->bufferLength_,
    &result->numEntriesSkipped_,
    &result->numEntriesReturned_,
    &result->signalFlags_
  );
  if (hBuffer) {
    result->entries_.setHandle(hBuffer);
  }
  if (!status) { throw status; }
}

void _Collection::readEntries(
  WORD skipNavigator,
  DWORD skipCount,
  WORD returnNavigator,
  DWORD returnCount,
  DWORD returnMask,
  ReadEntriesResult *result
) const {
  readEntries(
    rawHandle(),
    skipNavigator,
    skipCount,
    returnNavigator,
    returnCount,
    returnMask,
    result
  );
}

} // namespace npp2
