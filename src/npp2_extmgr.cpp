#include "../include/npp2/extmgr.hpp"

namespace npp2::extmgr {

WORD createRecursionID() {
  WORD RecursionID = 0;
  Status status = EMCreateRecursionID(&RecursionID);
  if (!status) {
    throw status;
  }
  return RecursionID;
}

bool filter(EMRECORD *pRecord, EID eid, WORD notifyType) {
  // 処理対象イベントかどうか判定
#if defined(NT)
  auto _eid = pRecord->EId & 0xffff;
  auto notificationType = pRecord->EId >> 16;
#else
  auto _eid = pRecord->EId;
  auto notificationType = pRecord->NotificationType;
#endif
  return _eid == eid && notificationType == notifyType;
}

} // namespace npp2::extmgr
