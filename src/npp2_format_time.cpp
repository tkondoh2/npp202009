#include "../include/npp2/format/time.hpp"

// #ifdef NT
// #pragma pack(push, 1)
// #endif

// #include <global.h>
// #include <misc.h>

// #ifdef NT
// #pragma pack(pop)
// #endif

namespace npp2::format {

/**
 * @brief デフォルトコンストラクタ
 */
Time::Time()
  : value_({TDFMT_FULL,TTFMT_FULL,TZFMT_NEVER,TSFMT_DATETIME})
{}

/**
 * @brief コンストラクタ
 * @param date 日付フォーマット値(TDFMT_xxx)
 * @param time 時刻フォーマット値(TTFMT_xxx)
 * @param zone タイムゾーンフォーマット値(TZFMT_xxx)
 * @param structure 構成フォーマット値(TSFMT_xxx)
 */
Time::Time(BYTE date, BYTE time, BYTE zone, BYTE structure)
  : value_({date, time, zone, structure})
{}

/**
 * @return TFMTへのポインタ
 */
TFMT *Time::data() {
  return &value_;
}

/**
 * @return 日付フォーマット
 */
BYTE Time::date() const {
  return value_.Date;
}

/**
 * @return 時刻フォーマット
 */
BYTE Time::time() const {
  return value_.Time;
}

/**
 * @return タイムゾーンフォーマット
 */
BYTE Time::zone() const {
  return value_.Zone;
}

/**
 * @return 構成フォーマット
 */
BYTE Time::structure() const {
  return value_.Structure;
}

/**
 * @param v 設定する日付フォーマット値
 */
void Time::setDate(BYTE v) {
  value_.Date = v;
}

/**
 * @param v 設定する時刻フォーマット値
 */
void Time::setTime(BYTE v) {
  value_.Time = v;
}

/**
 * @param v 設定するタイムゾーンフォーマット値
 */
void Time::setZone(BYTE v) {
  value_.Zone = v;
}

/**
 * @param v 設定する構成フォーマット値
 */
void Time::setStructure(BYTE v) {
  value_.Structure = v;
}

} // namespace npp2::format
