#include "../include/npp2/attachmentitem.hpp"
#include "../include/npp2/utils/time.hpp"

namespace npp2 {

AttachmentItem::AttachmentItem()
  : Item()
  , header_({0,0})
  , fileNameLength_(0)
  , hostType_(0)
  , compressionType_(0)
  , fileAttributes_(0)
  , flags_(0)
  , fileSize_(0)
  , fileCreated_(getMinTimeDate())
  , fileModified_(getMinTimeDate())
  , fileName_()
{}

AttachmentItem::AttachmentItem(
    BLOCKID itemId,
    WORD dataType,
    BLOCKID valueId,
    DWORD valueSize
    )
  : Item(itemId, dataType, valueId, valueSize)
  , header_({0,0})
  , fileNameLength_(0)
  , hostType_(0)
  , compressionType_(0)
  , fileAttributes_(0)
  , flags_(0)
  , fileSize_(0)
  , fileCreated_(getMinTimeDate())
  , fileModified_(getMinTimeDate())
  , fileName_()
{
  Block valueBlock(valueId);
  if (valueBlock.isNull()) return;
  
  BlockLocker lock(valueBlock);
  auto pch = lock.ptr() + sizeof(WORD);
#ifdef NT
  auto pObjDesc = reinterpret_cast<OBJECT_DESCRIPTOR*>(pch);
  if (pObjDesc->ObjectType == OBJECT_FILE) {
    auto pFileObj = reinterpret_cast<FILEOBJECT*>(pObjDesc);
    header_ = pFileObj->Header;
    fileNameLength_ = pFileObj->FileNameLength;
    hostType_ = pFileObj->HostType;
    compressionType_ = pFileObj->CompressionType;
    fileAttributes_ = pFileObj->FileAttributes;
    flags_ = pFileObj->Flags;
    fileSize_ = pFileObj->FileSize;
    fileCreated_ = pFileObj->FileCreated;
    fileModified_ = pFileObj->FileModified;

    if (!(compressionType_ & ~COMPRESS_MASK)) {
      fileName_ = data::Lmbcs(
        pch + sizeof(FILEOBJECT),
        fileNameLength_
      );
    }
#else
  auto pObjDesc = pch;
  OBJECT_DESCRIPTOR objDesc;
  ODSReadMemory(&pObjDesc, _OBJECT_DESCRIPTOR, &objDesc, 1);
  if (objDesc.ObjectType == OBJECT_FILE) {
    FILEOBJECT fileObj;
    ODSReadMemory(&pch, _FILEOBJECT, &fileObj, 1); // pchはFILEOBJECT構造体分移動する

    fileNameLength_ = fileObj.FileNameLength;
    hostType_ = fileObj.HostType;
    compressionType_ = fileObj.CompressionType;
    fileAttributes_ = fileObj.FileAttributes;
    flags_ = fileObj.Flags;
    fileSize_ = fileObj.FileSize;
    fileCreated_ = fileObj.FileCreated;
    fileModified_ = fileObj.FileModified;

    if (!(compressionType_ & ~COMPRESS_MASK)) {
      fileName_ = data::Lmbcs(pch, fileNameLength_);
    }
#endif
  }
}

OBJECT_DESCRIPTOR AttachmentItem::header() const {
  return header_;
}

WORD AttachmentItem::fileNameLength() const {
  return fileNameLength_;
}

WORD AttachmentItem::hostType() const {
  return hostType_;
}

WORD AttachmentItem::compressionType() const {
  return compressionType_;
}

WORD AttachmentItem::fileAttributes() const {
  return fileAttributes_;
}

WORD AttachmentItem::flags() const {
  return flags_;
}

DWORD AttachmentItem::fileSize() const {
  return fileSize_;
}

TIMEDATE AttachmentItem::fileCreated() const {
  return fileCreated_;
}

TIMEDATE AttachmentItem::fileModified() const {
  return fileModified_;
}

data::Lmbcs AttachmentItem::fileName() const {
  return fileName_;
}

} // namespace npp2
