#include "../include/npp2/dname.hpp"
#include "../include/npp2/status.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <names.h>
// #include <dname.h>
#include <kfm.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp2 {

DistinguishedName::DistinguishedName()
  : value_()
{}

DistinguishedName::DistinguishedName(const data::Lmbcs &name) noexcept(false)
  : value_()
{
  char buffer[MAXUSERNAME] = "";
  WORD len = 0;
  Status status = DNCanonicalize(
    0L,
    nullptr,
    name.constData(),
    buffer,
    MAXUSERNAME,
    &len
  );
  if (!status) { throw status; }
  value_ = data::Lmbcs(buffer, len);
}

data::Lmbcs DistinguishedName::value() const {
  return value_;
}

data::Lmbcs DistinguishedName::abbreviated() const {
  char buffer[MAXUSERNAME] = "";
  WORD len = 0;
  Status status = DNAbbreviate(
    0,
    nullptr,
    const_cast<char*>(value_.constData()),
    buffer,
    MAXUSERNAME,
    &len
  );
  if (!status) throw status;
  return data::Lmbcs(buffer, static_cast<int>(len));
}

DN_COMPONENTS DistinguishedName::getComponents() const noexcept(false) {
  DN_COMPONENTS dncom;
  Status status = DNParse(
    0,
    nullptr,
    value_.constData(),
    &dncom,
    sizeof(dncom)
  );
  if (!status) { throw status; }
  return dncom;
}

data::Lmbcs DistinguishedName::orgName() const noexcept(false) {
  auto dncom = getComponents();
  return data::Lmbcs(dncom.O, dncom.OLength);
}

DistinguishedName DistinguishedName::currentUserName() noexcept(false) {
  char buffer[MAXUSERNAME] = "";
  Status status = SECKFMGetUserName(buffer);
  if (!status) { throw status; }
  return DistinguishedName(data::Lmbcs(buffer));
}

} // namespace npp2
