#include "../include/npp2/_private/handleobject.hpp"
#include "../include/npp2/status.hpp"

namespace npp2 {

void OSMemFreeDeleter::operator ()(DHANDLE handle) {
  Status status = OSMemFree(handle);
  if (!status) { throw status; }
}

} // namespace npp2
