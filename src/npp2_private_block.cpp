#include "../include/npp2/_private/block.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

Block::Block(BLOCKID id)
  : id_(id)
{}

/**
 * @return ブロックIDがヌルを示している場合はtrue
 */
bool Block::isNull() const {
  return ISNULLBLOCKID(id_);
}

/**
 * @brief ブロックIDが有効ならtrue
 */
Block::operator bool() const {
  return !isNull();
}

Block::operator BLOCKID() const {
  return id_;
}

BlockLocker::BlockLocker()
  : id_(NullBlockID)
  , ptr_(nullptr)
{}

BlockLocker::BlockLocker(BLOCKID id)
  : id_(id)
  , ptr_(OSLockBlock(char, id_))
{}

BlockLocker::~BlockLocker() {
  if (!ISNULLBLOCKID(id_)) {
    OSUnlockBlock(id_);
  }
}

} // namespace npp2
