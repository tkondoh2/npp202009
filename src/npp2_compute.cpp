#include "../include/npp2/compute.hpp"
// #include "formula.hpp"
// #include "data/any.hpp"

namespace npp2 {

void compute(
  FORMULAHANDLE hFormula,
  std::function<void(HCOMPUTE hCompute)> fn
) noexcept(false) {
  Locker<FORMULAHANDLE> lock(hFormula);
  HCOMPUTE hCompute = nullptr;
  Status status = NSFComputeStart(0, lock.ptr<void>(), &hCompute);
  if (!status) { throw status; }
  fn(hCompute);
  status = NSFComputeStop(hCompute);
  if (!status) { throw status; }
}

data::Any evaluate(
  HCOMPUTE hCompute,
  NOTEHANDLE hNote,
  bool *pNoteMatchesFormula,
  bool *pNoteShouldBeDeleted,
  bool *pNoteModified
) noexcept(false) {
  DHANDLE hValue = NULLHANDLE;
  WORD vLen = 0;
  Status status = NSFComputeEvaluate(
    hCompute,
    hNote,
    &hValue,
    &vLen,
    reinterpret_cast<BOOL*>(pNoteMatchesFormula),
    reinterpret_cast<BOOL*>(pNoteShouldBeDeleted),
    reinterpret_cast<BOOL*>(pNoteModified)
  );
  data::Any value;
  if (!status) { throw status; }
  LockableObject3 obj(hValue);
  Locker<DHANDLE> lock(obj.rawHandle());
  value = data::Any(lock.ptr<char>(), static_cast<size_t>(vLen));
  return value;
}

} // namespace npp2
