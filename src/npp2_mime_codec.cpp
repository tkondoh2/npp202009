#include "../include/npp2/mime/codec.hpp"
#include <QByteArrayList>
#include <QTextCodec>

namespace npp2::mime {

Codec::Codec(const QString &pattern)
  : pattern_(standardize(pattern))
{}

QOptionalString Codec::operator ()(const QByteArray &from) const {
  static const QByteArrayList keyNames = QTextCodec::availableCodecs();

  foreach (auto keyName, keyNames) {
    QString key = standardize(QString::fromLatin1(keyName));
    if (pattern_.compare(key, Qt::CaseInsensitive) == 0) {
      QTextCodec *codec = QTextCodec::codecForName(keyName);
      return codec->toUnicode(from);
    }
  }
  return std::nullopt;
}

bool Codec::equals(const QString &lhs, const QString &rhs) {
  auto l = standardize(lhs);
  auto r = standardize(rhs);
  return l.compare(r, Qt::CaseInsensitive) == 0;
}

QString Codec::standardize(const QString &s) {
  static const auto rx = QRegExp(R"rx([^a-zA-Z0-9])rx");
  auto r = s;
  r.replace(rx, QChar('-'));
  return r;
}

bool Codec::US_ASCII::compare(const QString &pattern) {
  return Codec::equals(pattern, "us-ascii");
}

QString Codec::US_ASCII::codec(const QByteArray &from) {
  return QString::fromLatin1(from);
}

bool Codec::UTF8::compare(const QString &pattern) {
  return Codec::equals(pattern, "utf-8");
}

QString Codec::UTF8::codec(const QByteArray &from) {
  return QString::fromUtf8(from);
}

bool Codec::ShiftJIS::compare(const QString &pattern) {
  return Codec::equals(pattern, "Shift-JIS")
      || Codec::equals(pattern, "SJIS")
      || Codec::equals(pattern, "x-sjis")
      || Codec::equals(pattern, "Windows-31J")
      || Codec::equals(pattern, "MS_Kanji")
      || Codec::equals(pattern, "cp932")
      || Codec::equals(pattern, "MS932");
}

QString Codec::ShiftJIS::codec(const QByteArray &from) {
  QTextCodec *codec = QTextCodec::codecForName("Shift-JIS");
  return codec->toUnicode(from);
}

bool Codec::JIS::compare(const QString &pattern) {
  return Codec::equals(pattern, "JIS") || Codec::equals(pattern, "ISO 2022-JP");
}

QString Codec::JIS::codec(const QByteArray &from) {
  QTextCodec *codec = QTextCodec::codecForName("ISO 2022-JP");
  return codec->toUnicode(from);
}

} // namespace npp2::mime {
