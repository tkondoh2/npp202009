#include "../include/npp2/logger/index.hpp"
#include "../include/npp2/logger/defaultformatter.hpp"

namespace npp2::logger {

/**
 * @brief デフォルトコンストラクタ
 */
Logger::Logger()
  : id_()
  , mutex_()
  , pWriter_(new LoggerWriter)
  , pFormatter_(new DefaultFormatter)
  , threshold_(Level::Info)
{}

/**
 * @brief コンストラクタ
 * @param id ログID
 */
Logger::Logger(const QString &id)
  : id_(id)
  , mutex_()
  , pWriter_(new LoggerWriter)
  , pFormatter_(new DefaultFormatter)
  , threshold_(Level::Info)
{}

/**
 * @brief 有効なログか
 * 
 * @return true 有効
 * @return false 無効
 */
bool Logger::isValid() const {
  return !id_.isEmpty();
}

/**
 * @return Level レベル閾値を返す。
 */
Level Logger::threshold() const {
  return threshold_;
}

/**
 * @param threshold 設定するレベル閾値
 */
void Logger::setThreshold(Level threshold) {
  threshold_ = threshold;
}

/**
 * @return WriterPtr& ライターを返す。
 */
WriterPtr &Logger::pWriter() {
  return pWriter_;
}

/**
 * @return FormatterPtr& フォーマッターを返す。
 */
FormatterPtr &Logger::pFormatter() {
  return pFormatter_;
}

/**
 * @brief 致命的なエラーレベルでメッセージを出力する。
 * @param getMessage メッセージを取得する関数
 */
void Logger::critical(GetMsgFunc getMessage) {
  output(Level::Critical, getMessage);
}

/**
 * @brief エラーレベルでメッセージを出力する。
 * @param getMessage メッセージを取得する関数
 */
void Logger::error(GetMsgFunc getMessage) {
  output(Level::Error, getMessage);
}

/**
 * @brief 警告レベルでメッセージを出力する。
 * @param getMessage メッセージを取得する関数
 */
void Logger::warning(GetMsgFunc getMessage) {
  output(Level::Warning, getMessage);
}

/**
 * @brief 情報レベルでメッセージを出力する。
 * @param getMessage メッセージを取得する関数
 */
void Logger::info(GetMsgFunc getMessage) {
  output(Level::Info, getMessage);
}

/**
 * @brief デバッグレベルでメッセージを出力する。
 * @param getMessage メッセージを取得する関数
 */
void Logger::debug(GetMsgFunc getMessage) {
  output(Level::Debug, getMessage);
}

/**
 * @brief トレースレベルでメッセージを出力する。
 * @param getMessage メッセージを取得する関数
 */
void Logger::trace(GetMsgFunc getMessage) {
  output(Level::Trace, getMessage);
}

/**
 * @return LoggerPtr& シングルトンインスタンスを返す。
 */
LoggerPtr &Logger::instance() {
  static LoggerPtr instance_(new Logger());
  return instance_;
}

/**
 * @brief ログファイルを作成する。
 * @param id ログID
 * @param filePath ログファイルパス
 * @param codecName コーデック名
 * @param bom BOMの有無
 * @return LoggerPtr インスタンス
 */
LoggerPtr Logger::createFile(
  const QString &id,
  const QString &filePath,
  std::function<void(const QString&)> fn,
  const QByteArray &codecName,
  bool bom
) {
  auto p = new Logger(id);
  p->pWriter().reset(new QTextFileWriter(filePath, fn, codecName, bom));
  instance().reset(p);
  return instance();
}

/**
 * @brief ログ情報を出力する。
 * @param level ログレベル
 * @param getMessage メッセージ取得関数
 */
void Logger::output(Level level, GetMsgFunc getMessage) {
  if (threshold_ >= level) {
    auto now = QDateTime::currentDateTime();
    auto tid = std::this_thread::get_id();
    auto msg = getMessage();
    std::thread thread([this, now, tid, level, msg] {
      QMutexLocker lock(&mutex_);

      // ログメッセージを作成
      auto text = (*pFormatter_)(id_, now, tid, level, msg);

      // ログを書き込み、フラッシュする
      (*pWriter_)() << text << Qt::endl;
    });
    thread.detach();
  }
}

} // namespace npp2::logger
