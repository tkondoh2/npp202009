#include "../include/npp2/utils/renamer/index.hpp"
#include <QFile>
#include <QFileInfo>
#include <QDir>

namespace npp2 {

/**
 * @brief コンストラクタ
 * @param format リネーム元テンプレート
 */
Renamer::Renamer(const QString &format)
  : format_(format)
{}

/**
 * @brief デストラクタ
 */
Renamer::~Renamer()
{}

/**
 * @brief リネーム処理をする。
 * @param filePath リネームするファイルのパス
 */
void Renamer::operator ()(const QString &filePath) const {
  QFile file(filePath);
  auto path = renamePath(filePath);
  file.rename(path);
}

/**
 * @brief 指定パスのディレクトリがない時に作成する。
 * @param path パス
 */
void Renamer::makeDir(const QString &path) {
  QFileInfo info(path);
  auto dir = info.dir();
  if (!dir.exists()) {
    dir.mkpath(dir.path());
  }
}

} // namespace npp2
