#include "../include/npp2/formula.hpp"
// #include "_private/handleobject.hpp"
// #include "status.hpp"
// #include "data/lmbcs.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

// #include <nsfsearc.h>
#include <nsferr.h>
// #include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

_Formula::_Formula()
  : LockableObject3<FORMULAHANDLE>()
{}

_Formula::_Formula(FORMULAHANDLE handle)
  : LockableObject3<FORMULAHANDLE>(handle)
{}

_Formula::~_Formula()
{}

Formula _Formula::compile(
  const data::Lmbcs &query,
  const data::Lmbcs &column
) noexcept(false) {
  FORMULAHANDLE handle = NULLHANDLE;
  STATUS compileStatus = NOERROR;
  WORD formulaLen = 0, line = 0, col = 0, offset = 0, length = 0;
  Status status = NSFFormulaCompile(
    column.isEmpty() ? nullptr : const_cast<char*>(column.constData()),
    static_cast<WORD>(column.size()),
    const_cast<char*>(query.constData()),
    static_cast<WORD>(query.size()),
    &handle,
    &formulaLen,
    &compileStatus,
    &line,
    &col,
    &offset,
    &length
  );
  if (!status && status.error() != ERR_FORMULA_COMPILATION) { throw status; }
  CompileError compileError(compileStatus, line, col, offset, length);
  if (!compileError) { throw compileError; }
  return std::make_unique<_Formula>(handle);
}

_Formula::CompileError::CompileError()
  : Status()
  , line_(0)
  , col_(0)
  , offset_(0)
  , length_(0)
{}

_Formula::CompileError::CompileError(
  STATUS status,
  WORD line,
  WORD col,
  WORD offset,
  WORD length
)
  : Status(status)
  , line_(line)
  , col_(col)
  , offset_(offset)
  , length_(length)
{}

WORD _Formula::CompileError::line() const {
  return line_;
}

WORD _Formula::CompileError::col() const {
  return col_;
}

WORD _Formula::CompileError::offset() const {
  return offset_;
}

WORD _Formula::CompileError::length() const {
  return length_;
}

} // namespace npp2
