#include "../include/npp2/mime/decode.hpp"
#include "../include/npp2/mime/quotedprintable.hpp"
#include "../include/npp2/mime/codec.hpp"
#include <QTextStream>
#include <stdexcept>

namespace npp2::mime {

const char EncodeBase64::pattern = 'B';

QByteArray EncodeBase64::decode(const QByteArray &bytes) {
  return QByteArray::fromBase64(bytes);
}

const char EncodeQuoted::pattern = 'Q';

QByteArray EncodeQuoted::decode(const QByteArray &bytes) {
  return QuotedPrintable::decode(bytes);
}

QString Decode::header(const QString &source) noexcept(false) {
  std::exception excep;
  try {
    return headerType1(source);
  }
  catch (std::exception &ex) {
    excep = ex;
  }
  try {
    return headerType2(source);
  }
  catch (std::exception &ex) {
    excep = ex;
  }
  throw excep;
}

QString Decode::headerType1(const QString &source) {
  // デリミターで分割
  auto list = source.split(QRegExp(R"(\r\n\s+)"));

  // 分割単位でデコード処理し、一つにまとめる
  QString buffer;
  QTextStream sout(&buffer, QIODevice::WriteOnly);
  foreach (auto s2, list) {
    int pos = 0;
    while (pos < s2.length()) {
      QRegExp rx(R"rx(=\?(\S+)\?(B|b|Q|q)\?(\S+)\?=)rx");
      auto offset = rx.indexIn(s2, pos);
      if (offset >= pos) {
        if (offset > pos) {
          sout << s2.mid(pos, offset - pos);
        }
        sout << encodingPart(rx.cap(1), rx.cap(2), rx.cap(3));
        pos = offset + rx.matchedLength();
      }
      else {
        if (s2.length() > pos) {
          sout << s2.mid(pos, s2.length() - pos);
        }
        break;
      }
    }
  }
  sout.flush();
  return buffer;
}

QString Decode::headerType2(const QString &source) {
  QString buffer;
  QTextStream sout(&buffer, QIODevice::WriteOnly);

  int pos = 0;
  while (pos < source.length()) {
    auto offset1 = source.indexOf("=?", pos);
    if (offset1 >= pos) {
      if (offset1 > pos) {
        sout << source.mid(pos, offset1 - pos);
      }
      auto offset2 = source.indexOf("?=", offset1 + 2);
      auto part = source.mid(offset1, offset2 + 2 - offset1);

      QRegExp rx(R"rx(=\?(\S+)\?(B|b|Q|q)\?(\S+)\?=)rx");
      if (rx.indexIn(part) == 0) {
        part = encodingPart(rx.cap(1), rx.cap(2), rx.cap(3));
      }
      sout << part;

      pos = offset2 + 2;
    }
    else {
      if (pos < source.length()) {
        sout << source.right(source.length() - pos);
      }
      break;
    }
  }
  sout.flush();
  return buffer;
}

QString Decode::encodingPart(
  const QString &charSet,
  const QString &encoding,
  const QString &source
) noexcept(false) {

  if (source.isEmpty()) return QString();

  QByteArray bytes1 = source.toLatin1();
  auto bytes2 = decodeByteArray<EncodeBase64>(encoding, bytes1);
  bytes2 = bytes2 ? bytes2 : decodeByteArray<EncodeQuoted>(encoding, bytes1);
  if (!bytes2) {
    throw std::runtime_error(
          QString("Unknown encoding pattern(%1).").arg(encoding)
          .toStdString()
          );
  }

  Codec codec(charSet);
  auto dest = codec.to<Codec::UTF8>(bytes2.value());
  dest = dest ? dest : codec.to<Codec::JIS>(bytes2.value());
  dest = dest ? dest : codec.to<Codec::ShiftJIS>(bytes2.value());
  dest = dest ? dest : codec(bytes2.value());
  dest = dest ? dest : codec.to<Codec::US_ASCII>(bytes2.value());
  if (!dest) {
    throw std::runtime_error(
          QString("Unknown character set(%1).").arg(charSet)
          .toStdString()
          );
  }
  return dest.value();
}

} // namespace npp2::mime
