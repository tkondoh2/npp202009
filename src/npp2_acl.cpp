#include "../include/npp2/acl.h"
#include "../include/npp2/status.hpp"

namespace npp2 {

AclEntry::AclEntry(
  const char *pName,
  WORD level,
  const ACL_PRIVILEGES *pPrivileges,
  WORD flags
)
  : name_(pName)
  , level_(level)
  , privileges_(*pPrivileges)
  , flags_(flags)
{}

_Acl::_Acl()
  : LockableObject3<DHANDLE>()
{
  Status status = ACLCreate(&handle_);
  if (!status) throw status;
}

_Acl::_Acl(DHANDLE hAcl)
  : LockableObject3<DHANDLE>(hAcl)
{}

_Acl::~_Acl()
{}

Acl _Acl::read(DBHANDLE hDB) {
  DHANDLE hAcl = NULLHANDLE;
  Status status = NSFDbReadACL(hDB, &hAcl);
  if (!status) throw status;
  return std::make_unique<_Acl>(hAcl);
}

bool _Acl::isUniformAccess(DHANDLE hAcl) {
  DWORD flags = 0;
  Status status = ACLGetFlags(hAcl, &flags);
  if (!status) throw status;
  return (flags & ACL_UNIFORM_ACCESS) != 0;
}

data::Lmbcs _Acl::getAdminServer(DHANDLE hAcl) {
  char serverName[MAXUSERNAME] = "";
  Status status = ACLGetAdminServer(hAcl, serverName);
  if (!status) throw status;
  return data::Lmbcs(serverName);
}

void LNCALLBACK aclEnumEntriesCallback(
  void *EnumFuncParam,
  char *Name,
  WORD AccessLevel,
  ACL_PRIVILEGES *Privileges,
  WORD AccessFlags
) {
  AclEntry entry(Name, AccessLevel, Privileges, AccessFlags);
  auto callback = static_cast<std::function<void(AclEntry &&)>*>(
    EnumFuncParam
  );
  (*callback)(std::move(entry));
}

void _Acl::getEntries(
  DHANDLE hAcl,
  std::function<void(AclEntry &&)> callback
) {
  ACLEnumEntries(hAcl, aclEnumEntriesCallback, &callback);
}

data::LmbcsList _Acl::getRoles(
  DHANDLE hAcl,
  const ACL_PRIVILEGES &privileges
) {
  data::LmbcsList list;
  for (WORD i = 0; i < ACL_PRIVCOUNT; ++i) {
    if (ACLIsPrivSet(privileges, i)) {
      char roleName[ACL_PRIVSTRINGMAX] = "";
      Status status = ACLGetPrivName(hAcl, i, roleName);
      if (!status) { throw status; }
      list.append(data::Lmbcs(roleName));
    }
  }
  return list;
}

void _Acl::getHistory(
  DHANDLE hAcl,
  std::function<void(data::Lmbcs &&)> callback
) {
  DHANDLE hHistory = NULLHANDLE;
  WORD count = 0;

  Status status = ACLGetHistory(hAcl, &hHistory, &count);
  if (!status || hHistory == NULLHANDLE) {
    throw status;
  }
  else {
    Locker<DHANDLE> locker(hHistory);
    auto pHistory = locker.ptr<char>();

    for (WORD i = 0; i < count; ++i) {
      data::Lmbcs history(pHistory);
      auto len = history.size();
      callback(std::move(history));
      pHistory += len + 1;
    }
  }
  OSMemFree(hHistory);
}

} // namespace npp2