#include "../include/npp2/note.hpp"
#include <QScopedArrayPointer>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <stdnames.h>
#include <nsferr.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

void NSFNoteCloseDeleter::operator ()(NOTEHANDLE hNote) {
  Status status = NSFNoteClose(hNote);
  if (!status) { throw status; }
}

/**
 * @brief デフォルトコンストラクタ
 */
_Note::_Note()
  : LockableObject2<NOTEHANDLE,NSFNoteCloseDeleter>()
{}

/**
 * @brief コンストラクタ
 * @param handle 文書ハンドル
 */
_Note::_Note(NOTEHANDLE handle)
  : LockableObject2<NOTEHANDLE,NSFNoteCloseDeleter>(handle)
{}

_Note::~_Note()
{}

template <WORD Type, class T>
static T getNoteInfo(NOTEHANDLE hNote) noexcept {
  T value;
  NSFNoteGetInfo(hNote, Type, &value);
  return value;
}

// template <WORD Type, class T>
// T getNoteInfo() noexcept {
//   return _Note::getNoteInfo<Type, T>(rawHandle());
// }

template <WORD Type, class T>
static void setNoteInfo(NOTEHANDLE hNote, T value) {
  NSFNoteSetInfo(hNote, Type, &value);
}

// template <WORD Type, class T>
// void setNoteInfo(T value) {
//   setNoteInfo<Type, T>(rawHandle() &value);
// }

UNID _Note::getUnid(NOTEHANDLE hNote) noexcept {
  auto oid = getNoteInfo<_NOTE_OID, OID>(hNote);
  UNID result;
  result.File = oid.File;
  result.Note = oid.Note;
  return result;
}

UNID _Note::getUnid() noexcept {
  return getUnid(rawHandle());
}

WORD _Note::getNoteClass(NOTEHANDLE hNote) noexcept {
  return getNoteInfo<_NOTE_CLASS, WORD>(hNote);
}

WORD _Note::getNoteClass() noexcept {
  return _Note::getNoteClass(rawHandle());
}

DBHANDLE _Note::getDbHandle(NOTEHANDLE hNote) noexcept {
  return getNoteInfo<_NOTE_DB, DBHANDLE>(hNote);
}

DBHANDLE _Note::getDbHandle() noexcept {
  return _Note::getDbHandle(rawHandle());
}

NOTEID _Note::getNoteId(NOTEHANDLE hNote) noexcept {
  return getNoteInfo<_NOTE_ID, NOTEID>(hNote);
}

NOTEID _Note::getNoteId() const noexcept {
  return _Note::getNoteId(rawHandle());
}

OID _Note::getOID(NOTEHANDLE hNote) noexcept {
  return getNoteInfo<_NOTE_OID, OID>(hNote);
}

OID _Note::getOID() const noexcept {
  return _Note::getOID(rawHandle());
}

Note _Note::open(
  const Database &dbPtr,
  NOTEID noteId,
  WORD flags
) noexcept(false) {
  NOTEHANDLE hNote = open(dbPtr->rawHandle(), noteId, flags);
  return std::make_unique<_Note>(hNote);
}

NOTEHANDLE _Note::open(
  DBHANDLE hDB,
  NOTEID noteId,
  WORD flags
) noexcept(false) {
  NOTEHANDLE hNote = NULLHANDLE;
  Status status = NSFNoteOpen(hDB, noteId, flags, &hNote);
  if (!status) { throw status; }
  return hNote;
}

void _Note::close(NOTEHANDLE hNote) noexcept(false) {
  NSFNoteCloseDeleter()(hNote);
}

data::Lmbcs _Note::text256(const data::Lmbcs &field) const noexcept {
  return text256(rawHandle(), field);
}

data::Lmbcs _Note::text256(NOTEHANDLE hNote, const data::Lmbcs &field) noexcept {
  constexpr size_t SIZE = 256;
  char buffer[SIZE + 1] = "";
  auto len = NSFItemGetText(hNote, field.constData(), buffer, SIZE);
  return data::Lmbcs(buffer, len);
}

template <class T = Item>
void _items(
  NOTEHANDLE hNote,
  const data::Lmbcs &name,
  std::function<bool(const T&)> fn
) noexcept(false) {
  BLOCKID itemId, valueId, prevId;
  WORD dataType = 0;
  DWORD valueSize = 0;
  Status status = NSFItemInfo(
    hNote,
    name.constData(),
    static_cast<WORD>(name.size()),
    &itemId,
    &dataType,
    &valueId,
    &valueSize
  );
  while (status.error() != ERR_ITEM_NOT_FOUND) {
    if (!status) { throw status; }
    T item(itemId, dataType, valueId, valueSize);
    if (!fn(item)) { break; }
    prevId = itemId;
    status = NSFItemInfoNext(
      hNote,
      prevId,
      name.constData(),
      static_cast<WORD>(name.size()),
      &itemId,
      &dataType,
      &valueId,
      &valueSize
    );
  }
}

void _Note::items(
  NOTEHANDLE hNote,
  const data::Lmbcs &name,
  std::function<bool(const Item&)> fn
) noexcept(false) {
  _items(hNote, name, fn);
}

WORD _Note::getTextListEntries(NOTEHANDLE hNote, const data::Lmbcs &name) noexcept {
  return NSFItemGetTextListEntries(hNote, name.constData());
}

WORD _Note::getTextListEntries(const data::Lmbcs &name) const noexcept {
  return getTextListEntries(rawHandle(), name);
}

data::Lmbcs _Note::getTextListEntry(
  NOTEHANDLE hNote,
  const data::Lmbcs &name,
  WORD index
) noexcept {
  constexpr size_t SIZE = 256;
  char buffer[SIZE + 1] = "";
  WORD len = NSFItemGetTextListEntry(hNote, name.constData(), index, buffer, SIZE);
  return data::Lmbcs(buffer, len);
}

data::Lmbcs _Note::getTextListEntry(
  const data::Lmbcs &name,
  WORD index
) const noexcept {
  return getTextListEntry(rawHandle(), name, index);
}

void _Note::attachmentItems(
    NOTEHANDLE hNote,
    std::function<bool(const AttachmentItem &attItem)> fn
    ) noexcept(false) {
  _items<AttachmentItem>(hNote, ITEM_NAME_ATTACHMENT, fn);
}

void _Note::items(
  const data::Lmbcs &name,
  std::function<bool(const Item&)> fn
) const noexcept(false) {
  items(rawHandle(), name, fn);
}

void _Note::attachmentItems(
  std::function<bool(const AttachmentItem &attItem)> fn
) const noexcept(false) {
  attachmentItems(rawHandle(), fn);
}

QByteArray _Note::extractAttachment(
  const AttachmentItem &attItem
) const noexcept(false) {
  QByteArray bytes;
  Status status = NSFNoteExtractWithCallback(
    rawHandle(),
    attItem.itemId(),
    nullptr,
    0,
    _extractAttachmentCallback,
    &bytes
  );
  if (!status) { throw status; }
  return bytes;
}

STATUS _Note::_extractAttachmentCallback(
  const BYTE *bytes,
  DWORD length,
  void *ptr
) {
  if (length > 0) {
    QByteArray *pBytes = static_cast<QByteArray*>(ptr);
    QByteArray fileChunk(
      reinterpret_cast<char*>(const_cast<BYTE*>(bytes)),
      static_cast<int>(length)
    );
    *pBytes += fileChunk;
  }
  return NOERROR;
}

bool _Note::isUnread(const Database &dbPtr) const {
  auto &col = dbPtr->getUnreadIdTable();
  return col.contains(getNoteId());
}

void _Note::setUnread(const Database &dbPtr, bool bUnread) {
  auto &col = dbPtr->getUnreadIdTable();
  auto noteId = getNoteId();
  if (bUnread) { col.insert(noteId); }
  else { col.remove(noteId); }
}

NOTEHANDLE _Note::copy(
  NOTEHANDLE hSrc,
  bool isReplica,
  DBHANDLE hDestDb
) {
  NOTEHANDLE hDest = NULLHANDLE;
  Status status = NSFNoteCopy(hSrc, &hDest);
  if (!status) { throw status; }
  setNoteInfo<_NOTE_ID, NOTEID>(hDest, 0);
  if (hDestDb != NULLHANDLE) {
    setNoteInfo<_NOTE_DB, DBHANDLE>(hDest, hDestDb);
  }
  DBHANDLE hDb = NULLHANDLE;
  if (hDestDb != NULLHANDLE) {
    hDb = hDestDb;
  } else {
    hDb = getNoteInfo<_NOTE_DB, DBHANDLE>(hSrc);
  }
  if (!isReplica) {
    auto oid = _Database::generateOid(hDb);
    setNoteInfo<_NOTE_OID, OID>(hDest, oid);
  }
  return hDest;
}

Note _Note::copy(
  bool isReplica,
  DBHANDLE hDestDb
) const {
  return std::make_unique<_Note>(copy(rawHandle(), isReplica, hDestDb));
}

void _Note::update(NOTEHANDLE hNote, WORD updateFlags) {
  Status status = NSFNoteUpdate(hNote, updateFlags);
  if (!status) throw status;
}

void _Note::update(WORD updateFlags) {
  update(rawHandle(), updateFlags);
}

void _Note::updateEx(NOTEHANDLE hNote, DWORD updateFlags) {
  Status status = NSFNoteUpdateExtended(hNote, updateFlags);
  if (!status) throw status;
}

void _Note::updateEx(DWORD updateFlags) {
  updateEx(rawHandle(), updateFlags);
}

void _Note::setText(
  NOTEHANDLE hNote,
  const data::Lmbcs &name,
  data::Lmbcs value,
  bool bSummary
) {
  value.replace("\n", "\0");
  Status status = NSFItemSetTextSummary(
    hNote,
    const_cast<char*>(name.constData()),
    const_cast<char*>(value.constData()),
    value.size(),
    bSummary ? TRUE : FALSE
  );
  if (!status) throw status;
}

void _Note::setText(
  const data::Lmbcs &name,
  const data::Lmbcs &value,
  bool bSummary
) {
  setText(rawHandle(), name, value, bSummary);
}

STATUS _Note::scanItemsCallback(
  WORD, WORD itemFlags,
  char *name, WORD nameLength,
  void *pValue, DWORD valueLength,
  void *routineParameter
) {
  auto fn = reinterpret_cast<ScanItemsFn*>(routineParameter);
  data::Lmbcs field(name, static_cast<int>(nameLength));
  data::Any value(reinterpret_cast<char*>(pValue), valueLength);
  return (*fn)(field, value, itemFlags);
}

void _Note::scanItems(NOTEHANDLE hNote, ScanItemsFn fn) {
  Status status = NSFItemScan(hNote, scanItemsCallback, &fn);
  if (!status) throw status;
}

void _Note::scanItems(const ScanItemsFn &fn) const {
  scanItems(rawHandle(), fn);
}

data::Lmbcs _Note::convertToText(
  const data::Lmbcs &name,
  char separator
) const {
  return convertToText(rawHandle(), name, separator);
}

data::Lmbcs _Note::convertToText(
  NOTEHANDLE hNote,
  const data::Lmbcs &name,
  char separator
) {
  QScopedArrayPointer<char> buffer(new char[MAXWORD]);
  auto len = NSFItemConvertToText(
    hNote,
    name.constData(),
    buffer.data(),
    MAXWORD,
    separator
  );
  return data::Lmbcs(buffer.data(), static_cast<int>(len));
}

bool _Note::hasItem(NOTEHANDLE hNote, const data::Lmbcs &field) {
  return NSFItemIsPresent(
    hNote,
    field.constData(),
    static_cast<WORD>(field.size())
  ) == TRUE;
}

bool _Note::hasItem(const data::Lmbcs &field) const {
  return hasItem(rawHandle(), field);
}

NoteClass::NoteClass()
  : raw_(NOTE_CLASS_DOCUMENT)
{}

NoteClass::NoteClass(WORD v)
  : raw_(v)
{}

WORD NoteClass::raw() const {
  return raw_;
}

bool NoteClass::match(WORD v) const {
  return raw_ & v;
}

bool NoteClass::isDesign() const {
  return raw_ & NOTE_CLASS_ALLNONDATA;
}

} // namespace npp2
