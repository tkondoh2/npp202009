#include "../include/npp2/utils/renamer/timestamp.hpp"

namespace npp2 {

/**
 * @brief コンストラクタ
 * @param format リネーム元テンプレート
 * @param fn フォーマット式(デフォルトあり)
 */
TimestampRenamer::TimestampRenamer(const QString &format, TimestampRenameFunc fn)
  : Renamer(format)
  , fn_(fn)
{}

/**
 * @brief デストラクタ
 */
TimestampRenamer::~TimestampRenamer() {}

/**
 * @brief リネームパスを作成
 * @param filePath リネームするファイルのパス
 * @return QString リネームパス
 */
QString TimestampRenamer::renamePath(const QString &) const {
  auto now = QDateTime::currentDateTime();
  auto path = fn_(format_, now);
  makeDir(path);
  return path;
}

} // namespace npp2
