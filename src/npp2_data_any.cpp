#include "../include/npp2/data/any.hpp"

namespace npp2::data {

template <class S, class P, class T>
static Lmbcs _rangeToString(const T &range) {
  auto slist = range.template reduceItems<LmbcsList>([](LmbcsList acc, typename S::type v) {
    acc.append(S(v).toString());
    return acc;
  }, LmbcsList());
  slist = range.template reducePairs<LmbcsList>([](LmbcsList acc, typename P::type v) {
    auto lower = S(v.Lower).toString();
    auto upper = S(v.Upper).toString();
    acc.append(lower + ".." + upper);
    return acc;
  }, slist);
  return slist.join(",");
}

Any::Any()
  : Variant()
  , type_(TYPE_INVALID_OR_UNKNOWN)
  , raw_()
{}

Any::Any(const char *ptr, size_t size, BOOL fPrefix)
  : Variant()
  , type_(TYPE_INVALID_OR_UNKNOWN)
  , raw_()
{
  if (fPrefix) {
    type_ = *reinterpret_cast<WORD*>(const_cast<char*>(ptr));
  }
  auto p = dataptr<char>(const_cast<char*>(ptr), fPrefix);
  auto len = static_cast<int>(size - (fPrefix ? sizeof(WORD) : 0));
  raw_ = QByteArray(p, len);
}

Any::Any(WORD type, const char *ptr, size_t size)
  : Variant()
  , type_(type)
  , raw_(ptr, static_cast<int>(size))
{}

WORD Any::dataType() const {
  return type_;
}

const QByteArray &Any::raw() const {
  return raw_;
}

Lmbcs Any::toText(bool fReplace) const {
  // 文字列
  if (type_ == TYPE_TEXT) {
    return Lmbcs(raw_.constData(), raw_.length(), FALSE, fReplace);
  }

  // 文字列リスト
  else if (type_ == TYPE_TEXT_LIST) {
    auto p = reinterpret_cast<LIST*>(const_cast<char*>(raw_.constData()));
    return _TextList::getFirst(p, FALSE);
  }

  // その他
  return Lmbcs();
}

TextList Any::toTextList(bool fReplace) const {
  auto result = std::make_unique<_TextList>();

  // 文字列リスト
  if (type_ == TYPE_TEXT_LIST) {
    result->duplicate(
      reinterpret_cast<LIST*>(const_cast<char*>(raw_.constData())),
      FALSE
    );
  }

  // 文字列
  else if (type_ == TYPE_TEXT) {
    Lmbcs item0(raw_.constData(), raw_.length(), FALSE, fReplace);
    if (!item0.isEmpty()) {
      result->append(item0);
    }
  }

  return result;
}

Number Any::toNumber() const {
  // 数値
  if (type_ == TYPE_NUMBER) {
    return Number(raw_.constData(), FALSE);
  }

  // 数値範囲
  else if (type_ == TYPE_NUMBER_RANGE) {
    return NumberRange::getFirstItem(
      reinterpret_cast<RANGE*>(const_cast<char*>(raw_.constData())),
      FALSE
    );
  }

  // その他
  return Number();
}

NumberRange Any::toNumberRange() const {
  // 数値範囲
  if (type_ == TYPE_NUMBER_RANGE) {
    return NumberRange(
      reinterpret_cast<RANGE*>(const_cast<char*>(raw_.constData())),
      FALSE
    );
  }

  //
  else if (type_ == TYPE_NUMBER) {
    NumberRange range;
    range.appendItem(Number(raw_.constData(), FALSE));
    return range;
  }
  return NumberRange();
}

TimeDate Any::toTimeDate() const {
  if (type_ == TYPE_TIME) {
    return TimeDate(raw_.constData(), FALSE);
  }
  else if (type_ == TYPE_TIME_RANGE) {
    return TimeDateRange::getFirstItem(
      reinterpret_cast<RANGE*>(const_cast<char*>(raw_.constData())),
      FALSE
    );
  }
  return TimeDate();
}

TimeDateRange Any::toTimeDateRange() const {
  if (type_ == TYPE_TIME_RANGE) {
    return TimeDateRange(
      reinterpret_cast<RANGE*>(const_cast<char*>(raw_.constData())),
      FALSE
    );
  }
  else if (type_ == TYPE_TIME) {
    TimeDateRange range;
    range.appendItem(TimeDate(raw_.constData(), FALSE));
    return range;
  }
  return TimeDateRange();
}

Rfc822Text Any::toRfc822Text() const {
  if (type_ == TYPE_RFC822_TEXT) {
    return Rfc822Text::fromBytes(raw_.constData(), FALSE);
  }
  return Rfc822Text();
}

Lmbcs Any::toString() const {
  switch (type_) {

  case TYPE_TEXT: {
    return toText();
  }

  case TYPE_TEXT_LIST: {
    auto list = this->toTextList();
    return (list->size() > 1)
      ? list->join(",")
      : ((list->size() == 1) ? list->first() : Lmbcs(""));
  }

  case TYPE_NUMBER: {
    return toNumber().toString();
  }

  case TYPE_NUMBER_RANGE: {
    auto range = toNumberRange();
    return _rangeToString<Number,NumberPair,NumberRange>(range);
  }

  case TYPE_TIME: {
    return toTimeDate().toString();
  }

  case TYPE_TIME_RANGE: {
    auto range = toTimeDateRange();
    return _rangeToString<TimeDate,TimeDatePair,TimeDateRange>(range);
  }

  case TYPE_RFC822_TEXT: {
    auto rfc822text = toRfc822Text();
    if (rfc822text.isDate()) {
      return rfc822text.date().toString();
    }
    else if (rfc822text.isTextList()) {
      return rfc822text.textList()->join(",");
    }
    return rfc822text.text();
  }

  default:
    return Lmbcs();
  }
}

} // namespace npp2::data
