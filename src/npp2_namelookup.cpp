#include "../include/npp2/namelookup.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <lookup.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

NameLookup::InnerList::InnerList(QByteArray &&bytes, int size)
  : bytes_(std::move(bytes))
  , size_(size)
{}

const char *NameLookup::InnerList::constData() const {
  return bytes_.constData();
}

NameLookup::InnerList NameLookup::serialize(const data::LmbcsList &list) {
  auto bufSize = list.size();
  foreach (auto item, list) {
    bufSize += item.size();
  }
  QByteArray buffer(bufSize, '\0');
  int pos = 0;
  foreach (auto item, list) {
    qstrcpy(buffer.data() + pos, item.constData());
    pos += item.size() + 1;
  }
  return NameLookup::InnerList(std::move(buffer), list.size());
}

NameLookup::NameLookup(const data::Lmbcs &server, const data::LmbcsList &viewList)
  : server_(server)
  , viewList_(viewList)
{}

QList<NameLookup::Result> NameLookup::operator ()(
  const data::LmbcsList &nameList,
  const data::LmbcsList &itemList,
  WORD flags
) {
  auto views = serialize(viewList_);
  auto names = serialize(nameList);
  auto items = serialize(itemList);

  DHANDLE handle = NULLHANDLE;
  Status status = NAMELookup(
    server_.isEmpty() ? nullptr : server_.constData(),
    flags,
    views.size<WORD>(), views.constData(),
    names.size<WORD>(), names.constData(),
    items.size<WORD>(), items.constData(),
    &handle
  );
  if (!status) { throw status; }

  LockableObject3 obj(handle);
  Locker<DHANDLE> lock(obj.rawHandle());
  auto pLookup = lock.ptr<void>();

  QList<Result> resultList;
  for (int vi = 0; vi < viewList_.size(); ++vi) {
    void *pName = nullptr;
    for (int ni = 0; ni < nameList.size(); ++ni) {
      WORD matchCount = 0;
      pName = NAMELocateNextName(pLookup, pName, &matchCount);
      void *pMatch = nullptr;
      for (int mi = 0; mi < matchCount; ++mi) {
        pMatch = NAMELocateNextMatch(pLookup, pName, pMatch);
        NameLookup::Result result {
          viewList_.at(vi),
          nameList.at(ni),
          mi,
          QMap<data::Lmbcs, data::Any>()
        };
        for (int ii = 0; ii < itemList.size(); ++ii) {
          WORD dataType = 0, dataSize = 0;
          auto pItem = NAMELocateItem(
            pMatch,
            static_cast<WORD>(ii),
            &dataType,
            &dataSize
          );
          data::Any value(
            static_cast<char*>(pItem),
            static_cast<size_t>(dataSize)
          );
          result.items_.insert(itemList.at(ii), value);
        }
        resultList.append(result);
      }
    }
  }
  return resultList;
}

} // namespace npp2
