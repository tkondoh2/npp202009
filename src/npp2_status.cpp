#include "../include/npp2/status.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <misc.h>
#include <osmisc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

/**
 * @brief デフォルトコンストラクタ
 */
Status::Status()
  : std::exception(),
  raw_(NOERROR)
{}

/**
 * @brief コンストラクタ
 * @param status ステータス値
 */
Status::Status(STATUS status)
  : std::exception(),
  raw_(status)
{}

/**
 * @return STATUS値
 */
STATUS Status::raw() const noexcept {
  return raw_;
}

/**
 * @return エラー値の取得
 */
STATUS Status::error() const noexcept {
  return ERR(raw_);
}

/**
 * @brief 表示済みかどうか
 * @return 表示済みなら真
 */
bool Status::isDisplayed() const noexcept {
  return ERROR_DISPLAYED(raw_);
}

/**
 * @brief リモート先のエラーかどうか
 * @return リモート先のエラーなら真
 */
bool Status::isRemote() const noexcept {
  return ERROR_REMOTE(raw_);
}

/**
 * @brief エラーが発生したパッケージ値を返す。
 * @return パッケージ値
 */
STATUS Status::package() const noexcept {
  return PKG(raw_);
}

/**
 * @brief エラー番号を返す。
 * @return エラー番号
 */
STATUS Status::errnum() const noexcept {
  return ERRNUM(raw_);
}

/**
 * @return エラーがなければ真を返す
 */
Status::operator bool() const noexcept {
  return error() == NOERROR;
}

/**
 * @brief 例外の内容を示す文字列を返す
 * @return Notes C APIエラー文字列(LMBCS)
 */
const char *Status::what() const noexcept {
  static char buffer[MAXSPRINTF + 1] = "";
  auto len = OSLoadString(NULLHANDLE, error(), buffer, MAXSPRINTF);
  buffer[len] = '\0';
  return buffer;
}

/**
 * @brief 例外内容をQByteArray型で返す
 * @return 例外メッセージ
 */
QByteArray Status::message() const noexcept {
  return QByteArray(what());
}

} // namespace npp2
