#include "../include/npp2/itemtable.hpp"
// #include "data/any.hpp"
// #include "data/lmbcs.hpp"
// #include <QVector>

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nsfnote.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace npp2 {

/**
 * @brief コンストラクタ
 * @param pTable ITEM_TABLE構造体ポインタ
 */
NamedItemTable::NamedItemTable(ITEM_TABLE *pTable)
  : pTable_(pTable)
  , names_()
{
  // 先頭のITEMポインタを取得
  ITEM *pItem = reinterpret_cast<ITEM*>(pTable_ + 1);

  // 先頭の名前ポインタを取得
  char *pName = reinterpret_cast<char*>(pItem + pTable_->Items);

  // アイテム数分の巡回
  for (USHORT i = 0; i < pTable_->Items; ++i) {

    // 現在のアイテム名を取得
    data::Lmbcs name(pName, pItem->NameLength);

    // 名前リストに追加
    names_.append(name);

    // 名前ポインタを次にアイテム名に移動
    pName += pItem->NameLength + pItem->ValueLength;

    // 次のITEMポインタに移動
    pItem += 1;
  }
}

/**
 * @brief 名前リストを取得
 * @return
 */
const QVector<data::Lmbcs> &NamedItemTable::names() const {
  return names_;
}

bool NamedItemTable::hasItem(const data::Lmbcs &name) const {
  return names_.contains(name);
}

/**
 * @brief 名前に対応した値をVariantで取得
 * @param name アイテム名
 * @return アイテム値
 */
data::Any NamedItemTable::value(const data::Lmbcs &name) const {
  return value(pTable_, name);
}

/**
 * @brief 名前に対応した値をVariantで取得
 * @param pTable テーブルポインタ
 * @param name アイテム名
 * @return アイテム値
 */
data::Any NamedItemTable::value(ITEM_TABLE *pTable, const data::Lmbcs &name) {
  char *pValue = nullptr;
  WORD len = 0, type = 0;
  if (NSFLocateSummaryValue(
        pTable,
        name.constData(),
        &pValue,
        &len,
        &type
        )) {
    return data::Any(type, pValue, len);
  }
  return data::Any();
}

} // namespace npp2
