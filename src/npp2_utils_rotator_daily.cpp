#include "../include/npp2/utils/rotator/daily.hpp"
#include <QFileInfo>
#include <QDateTime>

namespace npp2 {

/**
 * @brief コンストラクタ
 */
DailyRotator::DailyRotator()
  : Rotator()
{}

/**
 * @brief デストラクタ
 */
DailyRotator::~DailyRotator()
{}

/**
 * @brief ローテーション判定を実行する
 * @param filePath ファイルパス
 * @return true ファイルの最終更新日が昨日以前
 * @return false ファイルの最終更新日が今日
 */
bool DailyRotator::operator ()(const QString &filePath) {
  QFileInfo fileInfo(filePath);
  QDateTime modified = fileInfo.lastModified();
  QDateTime now = QDateTime::currentDateTime();
  return now.daysTo(modified) < 0; // 前日なら-1でfalseを返す
}

} // namespace npp2
