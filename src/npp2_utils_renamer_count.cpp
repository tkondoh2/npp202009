#include "../include/npp2/utils/renamer/count.hpp"
#include <QFileInfo>
#include <QDateTime>

namespace npp2 {

/**
 * @brief コンストラクタ
 * @param format リネーム元テンプレート
 * @param maxCount 最大カウント数
 * @param fn フォーマット式(デフォルトあり)
 */
CountRenamer::CountRenamer(const QString &format, int maxCount, CountRenameFunc fn)
  : Renamer(format)
  , maxCount_(maxCount)
  , fn_(fn)
{}

/**
 * @brief デストラクタ
 */
CountRenamer::~CountRenamer() {}

/**
 * @brief リネームパスを作成
 * @param filePath リネームするファイルのパス
 * @return QString リネームパス
 */
QString CountRenamer::renamePath(const QString &filePath) const {
  // リネーム前のファイルの更新日時を取得
  QFileInfo fileInfo(filePath);
  auto modified = fileInfo.lastModified();

  QString lastModifiedPath;
  for (int i = 0; i < maxCount_; ++i) {

    // カウンターでリネーム名を作成
    auto path = fn_(format_, i);
    makeDir(path);

    // リネーム名のファイルが存在していなければその名前を返す
    fileInfo = QFileInfo(path);
    if (!fileInfo.exists()) {
      return path;
    }

    // ファイルの更新日時がより古ければそれを候補にする
    if (fileInfo.lastModified() < modified) {
      lastModifiedPath = path;
      modified = fileInfo.lastModified();
    }
  }

  // 最も古いファイルを削除
  QFile file(lastModifiedPath);
  file.remove();

  // その名前を返す
  return lastModifiedPath;
}

} // namespace npp2
