#include "../include/npp2/data/textlist.hpp"
#include "../include/npp2/status.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <textlist.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::data {

/**
 * @brief デフォルトコンストラクタ
 */
_TextList::_TextList(BOOL fPrefix)
  : LockableObject3<DHANDLE>(NULLHANDLE)
  , Variant()
  , allocated_(false)
  , fPrefix_(fPrefix)
{}

_TextList::~_TextList() {
  // _TextList::free();
}

void _TextList::duplicate(const LIST *from, BOOL fPrefix) {
  if (!allocated_) {
    DHANDLE handle = NULLHANDLE;
    Status status = ListDuplicate(const_cast<LIST*>(from), fPrefix, &handle);
    if (!status) { throw status; }
    setHandle(handle);
    fPrefix_ = fPrefix;
    allocated_ = true;
  }
}

/**
 * @return データタイププレフィックスの有無
 */
BOOL _TextList::fPrefix() const {
  return fPrefix_;
}

/**
 * @return データタイプ
 */
WORD _TextList::dataType() const {
  Locker<DHANDLE> lock(rawHandle());
  return getDataType(lock.ptr<LIST>(), fPrefix_, TYPE_TEXT_LIST);
}

/**
 * @return 現在の要素数
 */
WORD _TextList::size() const {
  if (isNull()) return 0;
  Locker<DHANDLE> lock(rawHandle());
  return getSize(lock.ptr<LIST>(), fPrefix_);
}

bool _TextList::isEmpty() const {
  return size() == 0;
}

/**
 * @return 現在の全体のデータサイズ
 */
WORD _TextList::byteSize() const {
  Locker<DHANDLE> lock(rawHandle());
  return getByteSize(lock.ptr<LIST>(), fPrefix_);
}

/**
 * @brief 要素アクセス
 * @param index 要素インデックス
 * @return インデックスに該当する要素(テキスト)
 */
Lmbcs _TextList::at(WORD index, bool fReplace) const {
  Locker<DHANDLE> lock(rawHandle());
  return getAt(index, lock.ptr<LIST>(), fPrefix_, fReplace);
}

/**
 * @brief 要素アクセス(atと同じ)
 * @param index 要素インデックス
 * @return インデックスに該当する要素(テキスト)
 */
Lmbcs _TextList::operator [](WORD index) const {
  return at(index);
}

/**
 * @return 先頭要素
 */
Lmbcs _TextList::first() const {
  Locker<DHANDLE> lock(rawHandle());
  return getFirst(lock.ptr<LIST>(), fPrefix_);
}

/**
 * @return 末尾要素
 */
Lmbcs _TextList::last() const {
  Locker<DHANDLE> lock(rawHandle());
  return getLast(lock.ptr<LIST>(), fPrefix_);
}

/**
 * @param sep 区切り文字列
 * @return リスト中の文字列が1つに繋がった文字列
 */
Lmbcs _TextList::join(const Lmbcs &sep) const {
  Locker<DHANDLE> lock(rawHandle());
  return getJoin(sep, lock.ptr<LIST>(), fPrefix_);
}

/**
 * @brief 要素を指定した位置への挿入
 * @param index 挿入位置
 * @param item 挿入するテキスト
 * @return 例外以外の失敗はfalseを返す
 */
bool _TextList::insert(WORD index, const Lmbcs &item) noexcept(false) {
  if (isNull() && !allocate()) {
    return false;
  }
  if (index > size()) { return false; }
  auto curSize = byteSize();
  Status status = ListAddEntry(
    rawHandle(),
    fPrefix_,
    &curSize,
    index,
    item.constData(),
    static_cast<WORD>(item.size())
  );
  if (!status) { throw status; }
  return true;
}

/**
 * @brief 末尾への要素追加
 * @param item 追加するテキスト
 */
bool _TextList::append(const Lmbcs &item) noexcept(false) {
  return insert(size(), item);
}

/**
 * @brief 指定した位置の要素を削除
 * @param index 削除する要素のインデックス
 * @return 成功すればtrue
 */
bool _TextList::removeAt(WORD index) noexcept(false) {
  if (isNull() || index >= size()) { return false; }
  auto curSize = byteSize();
  Status status = ListRemoveEntry(
    rawHandle(),
    fPrefix_,
    &curSize,
    index
  );
  if (!status) { throw status; }
  return true;
}

/**
 * @brief 末尾の要素削除
 * @return 成功すればtrue
 */
bool _TextList::removeLast() noexcept(false) {
  return removeAt(size() - 1);
}

/**
 * @brief すべての要素を削除
 */
void _TextList::removeAll() noexcept(false) {
  if (isNull() || size() == 0) { return; }
  auto curSize = byteSize();
  Status status = ListRemoveAllEntries(
    rawHandle(),
    fPrefix_,
    &curSize
  );
  if (!status) { throw status; }
}

void _TextList::clear() noexcept(false) {
  removeAll();
}

/**
 * @param pList LISTへのポインタ
 * @param fPrefix データタイププレフィックス
 * @return データタイプ
 */
WORD _TextList::getDataType(
  LIST *pList,
  BOOL fPrefix,
  WORD defaultType
) {
  return fPrefix
    ? *reinterpret_cast<WORD*>(pList)
    : defaultType;
}

/**
 * @param pList LISTへのポインタ
 * @param fPrefix データタイププレフィックス
 * @return 現在の要素数
 */
WORD _TextList::getSize(LIST *pList, BOOL fPrefix) {
  return ListGetNumEntries(pList, fPrefix);
}

/**
 * @param pList LISTへのポインタ
 * @param fPrefix データタイププレフィックス
 * @return 現在の全体のデータサイズ
 */
WORD _TextList::getByteSize(LIST *pList, BOOL fPrefix) {
  return ListGetSize(pList, fPrefix);
}

/**
 * @brief 要素アクセス
 * @param index 要素インデックス
 * @param pList LISTへのポインタ
 * @param fPrefix データタイププレフィックス
 * @return インデックスに該当する要素(テキスト)
 */
Lmbcs _TextList::getAt(
  WORD index,
  LIST *pList,
  BOOL fPrefix,
  bool fReplace
) {
  auto count = getSize(pList, fPrefix);
  if (count == 0 || index >= count) {
    return Lmbcs();
  }
  char *ptr = nullptr;
  WORD len = 0;
  Status status = ListGetText(pList, fPrefix, index, &ptr, &len);
  if (!status) { throw status; }
  return Lmbcs(ptr, len, FALSE, fReplace);
}

/**
 * @return 先頭要素
 * @param pList LISTへのポインタ
 * @param fPrefix データタイププレフィックス
 */
Lmbcs _TextList::getFirst(LIST *pList, BOOL fPrefix, bool fReplace) {
  return getAt(0, pList, fPrefix, fReplace);
}

/**
 * @return 末尾要素
 * @param pList LISTへのポインタ
 * @param fPrefix データタイププレフィックス
 */
Lmbcs _TextList::getLast(LIST *pList, BOOL fPrefix, bool fReplace) {
  auto count = getSize(pList, fPrefix);
  if (count == 0) { return Lmbcs(); }
  return getAt(count - 1, pList, fPrefix, fReplace);
}

/**
 * @param sep 区切り文字列
 * @param pList LISTへのポインタ
 * @param fPrefix データタイププレフィックス
 * @return リスト中の文字列が1つに繋がった文字列
 */
Lmbcs _TextList::getJoin(const Lmbcs &sep, LIST *pList, BOOL fPrefix, bool fReplace) {
  auto count = getSize(pList, fPrefix);
  auto joined = getFirst(pList, fPrefix);
  for (auto i = 1; i < count; ++i) {
    joined += sep + getAt(i, pList, fPrefix, fReplace);
  }
  return joined;
}

LmbcsList _TextList::toLmbcsList() const {
  LmbcsList result;
  for (WORD i = 0; i < size(); ++i) {
    result << at(i);
  }
  return result;
}

/**
 * @brief LIST領域の確保
 * @param count 初期要素数
 * @param quota 初期割当サイズ
 * @return 成功すればTRUE
 */
bool _TextList::allocate(WORD count, WORD quota) {
  if (allocated_) {
    return true;
  }
  DHANDLE handle = NULLHANDLE;
  void *ptr = nullptr;
  WORD size = 0;
  Status status = ListAllocate(
    count,
    quota,
    fPrefix_,
    &handle,
    &ptr,
    &size
  );
  if (!status) { throw status; }
  auto pType = reinterpret_cast<WORD*>(ptr);
  if (fPrefix_) { *pType = TYPE_TEXT_LIST; }
  OSUnlockObject(handle);
  setHandle(handle);
  allocated_ = true;
  return true;
}

} // namespace npp2::data

QStringList fromTextList(const npp2::data::TextList &list) {
  QStringList result;
  for (WORD i = 0; i < list->size(); ++i) {
    result << fromLmbcs(list->at(i));
  }
  return result;
}

npp2::data::TextList toTextList(const QStringList &qlist) {
  // auto result = npp2::data::TextList::create();
  auto result = std::make_unique<npp2::data::_TextList>();
  foreach (auto qitem, qlist) {
    result->append(toLmbcs(qitem));
  }
  return result;
}

bool contains(
  const npp2::data::TextList &list,
  const npp2::data::Lmbcs &item,
  Qt::CaseSensitivity cs,
  bool fReplace
) {
  auto v = fromLmbcs(item);
  for (WORD i = 0; i < list->size(); ++i) {
    auto s = fromLmbcs(list->at(i, fReplace));
    if (s.compare(v, cs) == 0) return true;
  }
  return false;
}

void append(
  npp2::data::TextList &list,
  const npp2::data::TextList &others,
  std::function<npp2::data::Lmbcs(const npp2::data::Lmbcs &)> t
) {
  for (WORD i = 0; i < others->size(); ++i) {
    list->append(t(others->at(i)));
  }
}

npp2::data::TextList split(const npp2::data::Lmbcs &text, char sep) {
  // auto result = npp2::data::TextList::create();
  auto result = std::make_unique<npp2::data::_TextList>();
  auto list = text.raw().split(sep);
  foreach (auto item, list) {
    result->append(npp2::data::Lmbcs(item));
  }
  return result;
}

npp2::data::TextList createTextList(
  const std::initializer_list<npp2::data::Lmbcs> &list
) {
  // auto result = npp2::data::TextList::create();
  auto result = std::make_unique<npp2::data::_TextList>();
  for (auto it: list) {
    result->append(it);
  }
  return result;
}

npp2::data::TextList lmbcsToTextList(
  const npp2::data::LmbcsList &lmbcsList
) {
  // auto result = npp2::data::TextList::create();
  auto result = std::make_unique<npp2::data::_TextList>();
  foreach (auto item, lmbcsList) {
    result->append(item);
  }
  return result;
}
