#include "../include/npp2/search.hpp"
#include "../include/npp2/status.hpp"
#include "../include/npp2/formula.hpp"

namespace npp2 {

using SearchCallback = std::function<STATUS(const SEARCH_MATCH&,ITEM_TABLE*)>;

Search::Search()
{}

static STATUS NPP2CALLBACK _callback(
  void *ptr,
  SEARCH_MATCH *pMatch,
  ITEM_TABLE *pItemTable
) {
  SEARCH_MATCH match;
  memcpy(&match, pMatch, sizeof(SEARCH_MATCH));
  SearchCallback *pFn = reinterpret_cast<SearchCallback*>(ptr);
  return (*pFn)(match, pItemTable);
}

TIMEDATE Search::operator ()(
  DBHANDLE hDb,
  FORMULAHANDLE hFormula,
  const data::Lmbcs &viewTitle,
  WORD flags,
  WORD noteClassMask,
  SearchCallback fn,
  TIMEDATE *pSince
) noexcept(false) {
  TIMEDATE retUntil;
  Status status = NSFSearch(
    hDb,
    hFormula,
    viewTitle.isEmpty()
      ? nullptr
      : const_cast<char*>(viewTitle.constData()),
    flags,
    noteClassMask,
    pSince,
    _callback,
    &fn,
    &retUntil
  );
  if (!status) { throw status; }
  return retUntil;
}

TIMEDATE Search::operator ()(
  DBHANDLE hDb,
  const data::Lmbcs &query,
  const data::Lmbcs &viewTitle,
  WORD flags,
  WORD noteClassMask,
  SearchCallback fn,
  TIMEDATE *pSince
) noexcept(false) {
  auto fPtr = npp2::_Formula::compile(query);
  return operator ()(
    hDb,
    fPtr->rawHandle(),
    viewTitle,
    flags,
    noteClassMask,
    fn,
    pSince
  );
}

} // namespace npp2
