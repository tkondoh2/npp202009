#include "../include/npp2/converter/number.hpp"
#include "../include/npp2/status.hpp"

namespace npp2::converter {

/**
 * @brief デフォルトコンストラクタ
 */
NumberConverter::NumberConverter()
  : Base<format::Number>()
{}

/**
 * @brief コンストラクタ
 * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
 * @param nPtr NFMTラッパーの共有ポインタ
 */
NumberConverter::NumberConverter(IntlFormatPtr &&iPtr, format::NumberPtr &&nPtr)
  : Base<format::Number>(std::move(iPtr), std::move(nPtr))
{}

/**
 * @brief デフォルトコンストラクタ
 */
NumberToTextConverter::NumberToTextConverter()
  : NumberConverter()
{}

/**
 * @brief コンストラクタ
 * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
 * @param nPtr NFMTラッパーの共有ポインタ
 */
NumberToTextConverter::NumberToTextConverter(
  IntlFormatPtr &&iPtr,
  format::NumberPtr &&nPtr
)
  : NumberConverter(std::move(iPtr), std::move(nPtr))
{}

/**
 * @brief 関数呼び出し
 * @param pNum 変換元数値へのポインタ
 * @return フォーマット済み文字列
 */
data::Lmbcs NumberToTextConverter::operator () (NUMBER *pNum) const {
  char buffer[MAXALPHANUMBER] = "";
  WORD len = 0;
  Status status = ConvertFLOATToText(
        !intlFormatPtr_ ? nullptr : intlFormatPtr_->data(),
        !formatPtr_ ? nullptr : formatPtr_->data(),
        pNum,
        buffer,
        MAXALPHANUMBER,
        &len
        );
  if (!status) { throw status; }
  return data::Lmbcs(buffer, len);
}

/**
 * @brief デフォルトコンストラクタ
 */
TextToNumberConverter::TextToNumberConverter()
  : NumberConverter()
{}

/**
 * @brief コンストラクタ
 * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
 * @param nPtr NFMTラッパーの共有ポインタ
 */
TextToNumberConverter::TextToNumberConverter(
  IntlFormatPtr &&iPtr,
  format::NumberPtr &&nPtr
)
  : NumberConverter(std::move(iPtr), std::move(nPtr))
{}

/**
 * @brief 関数呼び出し
 * @param lmbcs 変換元テキスト
 * @return フォーマット済み文字列
 */
NUMBER TextToNumberConverter::operator () (const data::Lmbcs &lmbcs) const {
  char *ptr = const_cast<char*>(lmbcs.constData());
  NUMBER num = 0;
  Status status = ConvertTextToFLOAT(
        !intlFormatPtr_ ? nullptr : intlFormatPtr_->data(),
        !formatPtr_ ? nullptr : formatPtr_->data(),
        &ptr,
        static_cast<WORD>(lmbcs.size()),
        &num
        );
  if (!status) { throw status; }
  return num;
}

} // namespace npp2::converter
