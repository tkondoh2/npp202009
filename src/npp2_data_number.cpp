#include "../include/npp2/data/number.hpp"
#include "../include/npp2/status.hpp"
#include "../include/npp2/utils/data.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>
#include <misc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::data {

WORD Number::dataType() const {
  return TYPE_NUMBER;
}

NUMBER Number::defaultRaw() {
  return 0;
}

/**
 * @brief デフォルトコンストラクタ
 */
Number::Number()
  : Variant()
  , raw_(0)
{}

/**
 * @brief コンストラクタ
 * @param num 初期値
 */
Number::Number(NUMBER num)
  : Variant()
  , raw_(num)
{}

/**
 * @brief コンストラクタ(フィールドデータから構築)
 * @param ptr 数値型へのポインタ
 * @param fPrefix データタイププレフィックスの有無
 */
Number::Number(const void *ptr, BOOL fPrefix)
  : Variant()
  , raw_(*dataptr<NUMBER>(const_cast<void*>(ptr), fPrefix))
{}

/**
 * @brief NUMBER型へのキャスト演算子
 */
Number::operator NUMBER() const {
  return raw_;
}

/**
 * @brief アドレス取得演算子
 * @return NUMBERデータへのポインタ
 */
NUMBER *Number::operator &() {
  return &raw_;
}

Lmbcs Number::toString() const noexcept(false) {
  return toString(raw_);
}

Lmbcs Number::toString(NUMBER n) noexcept(false) {
  char buffer[MAXALPHANUMBER] = "";
  WORD len = 0;
  Status status = ConvertFLOATToText(
    nullptr,
    nullptr,
    &n,
    buffer,
    MAXALPHANUMBER,
    &len
  );
  if (!status) {
    throw status;
  }
  return Lmbcs(buffer, len);
}

} // namespace npp2::data
