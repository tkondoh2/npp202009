#include "../include/npp2/utils/replicaid.hpp"
#include "../include/npp2/utils/time.hpp"
#include "../include/npp2/utils/hexint.hpp"


// #if defined(NT)
// #pragma pack(push, 1)
// #endif

// #include <nsfdata.h>

// #if defined(NT)
// #pragma pack(pop)
// #endif

namespace npp2 {

ReplicaId::ReplicaId()
  : value_(getMinTimeDate())
{}

ReplicaId::ReplicaId(const DBREPLICAINFO &repInfo)
  : value_(repInfo.ID)
{}

ReplicaId::operator DBID() const {
  return value_;
}

QByteArray ReplicaId::toString() const {
  return formatHexInt<DWORD,uint>(value_.Innards[1])
    + formatHexInt<DWORD,uint>(value_.Innards[0]);
}

} // namespace npp2
