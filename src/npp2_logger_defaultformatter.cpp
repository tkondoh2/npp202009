#include "../include/npp2/logger/defaultformatter.hpp"
#include "../include/npp2/utils/thread.hpp"

namespace npp2::logger {

DefaultFormatter::DefaultFormatter(
  const QString &format,
  const std::function<QString(QDateTime)> &timeFormatter
)
  : format_(format)
  , timeFormatter_(timeFormatter)
{}

DefaultFormatter::~DefaultFormatter() {}

QString DefaultFormatter::operator ()(
  const QString &id,
  const QDateTime &time,
  const std::thread::id &tid,
  Level level,
  const QString &msg
) {
  auto str = format_;
  str.replace("${time}", timeFormatter_(time))
      . replace("${thread}", threadIdToText(tid))
      . replace("${id}", id)
      . replace("${level}", levelText(level))
      . replace("${message}", msg);
  return str;
}

} // namespace npp2::logger
