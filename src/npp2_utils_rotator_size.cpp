#include "../include/npp2/utils/rotator/size.hpp"
#include <QFileInfo>

namespace npp2 {

/**
 * @brief コンストラクタ
 * @param limit 上限サイズ
 */
SizeRotator::SizeRotator(qint64 limit)
  : Rotator()
  , limit_(limit)
{}

/**
 * @brief デストラクタ
 */
SizeRotator::~SizeRotator()
{}

/**
 * @return qint64 設定済み上限サイズ
 */
qint64 SizeRotator::limit() const {
  return limit_;
}

/**
 * @brief ローテーション判定を実行する
 * @param filePath ファイルパス
 * @return true ファイルが上限サイズに達している
 * @return false ファイルが上限サイズに達していない
 */
bool SizeRotator::operator ()(const QString &filePath) {
  QFileInfo fileInfo(filePath);
  return fileInfo.size() > limit_;
}

} // namespace npp2
