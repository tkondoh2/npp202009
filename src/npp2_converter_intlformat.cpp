#include "../include/npp2/converter/intlformat.hpp"

// #include "../data/lmbcs.hpp"
// #include <algorithm>

// #ifdef NT
// #pragma pack(push, 1)
// #endif

// #include <intl.h>

// #ifdef NT
// #pragma pack(pop)
// #endif

namespace npp2::converter {

/**
 * @brief デフォルトコンストラクタ
 */
IntlFormat::IntlFormat()
  : value_(getCurrent())
{}

/**
 * @return INTLFORMATへのポインタ
 */
INTLFORMAT *IntlFormat::data() { return &value_; }

/**
 * @brief フラグ値から該当フラグの真偽を調べる
 * @param F フラグ
 * @return フラグが真ならtrue
 */
bool IntlFormat::isFlags(WORD F) const {
  return value_.Flags & F;
}

/**
 * @brief フラグ値の真偽を設定する
 * @param F フラグ
 * @param f 真にするならtrue
 */
void IntlFormat::setFlags(WORD F, bool f) {
  if (f) { value_.Flags |= F; }
  else { value_.Flags &= (~F); }
}

// フラグの真偽取得
bool IntlFormat::isCurrencySuffix() const { return isFlags(CURRENCY_SUFFIX); }
bool IntlFormat::isCurrencySpace() const { return isFlags(CURRENCY_SPACE); }
bool IntlFormat::isNumberLeadingZero() const { return isFlags(NUMBER_LEADING_ZERO); }
bool IntlFormat::isClock24Hour() const { return isFlags(CLOCK_24_HOUR); }
bool IntlFormat::isDaylightSavings() const { return isFlags(DAYLIGHT_SAVINGS); }
bool IntlFormat::isDateMDY() const { return isFlags(DATE_MDY); }
bool IntlFormat::isDateDMY() const { return isFlags(DATE_DMY); }
bool IntlFormat::isDateYMD() const { return isFlags(DATE_YMD); }
bool IntlFormat::isDate4DigitYear() const { return isFlags(DATE_4DIGIT_YEAR); }
bool IntlFormat::isTimeAMPMPrefix() const { return isFlags(TIME_AMPM_PREFIX); }
bool IntlFormat::isDateAbbrev() const { return isFlags(DATE_ABBREV); }

// フラグの真偽設定
void IntlFormat::setCurrencySuffix(bool f) { setFlags(CURRENCY_SUFFIX, f); }
void IntlFormat::setCurrencySpace(bool f) { setFlags(CURRENCY_SPACE, f); }
void IntlFormat::setNumberLeadingZero(bool f) { setFlags(NUMBER_LEADING_ZERO, f); }
void IntlFormat::setClock24Hour(bool f) { setFlags(CLOCK_24_HOUR, f); }
void IntlFormat::setDaylightSavings(bool f) { setFlags(DAYLIGHT_SAVINGS, f); }
void IntlFormat::setDateMDY(bool f) { setFlags(DATE_MDY, f); }
void IntlFormat::setDateDMY(bool f) { setFlags(DATE_DMY, f); }
void IntlFormat::setDateYMD(bool f) { setFlags(DATE_YMD, f); }
void IntlFormat::setDate4DigitYear(bool f) { setFlags(DATE_4DIGIT_YEAR, f); }
void IntlFormat::setTimeAMPMPrefix(bool f) { setFlags(TIME_AMPM_PREFIX, f); }
void IntlFormat::setDateAbbrev(bool f) { setFlags(DATE_ABBREV, f); }

/**
 * @return 通貨の小数点桁数
 */
BYTE IntlFormat::currencyDigits() const { return value_.CurrencyDigits; }

/**
 * @param c 通貨の小数点桁数
 */
void IntlFormat::setCurrencyDigits(BYTE c) { value_.CurrencyDigits = c; }

/**
 * @return 構造体長
 */
BYTE IntlFormat::length() const { return value_.Length; }

/**
 * @param l 構造体長
 */
void IntlFormat::setLength(BYTE l) { value_.Length = l; }

/**
 * @return 時差(日本UTC+9なら-9)
 */
int IntlFormat::timeZone() const { return value_.TimeZone; }

/**
 * @param tz 時差(日本UTC+9なら-9)
 */
void IntlFormat::setTimeZone(int tz) { value_.TimeZone = tz; }

/**
 * @return 午前表記文字列
 */
data::Lmbcs IntlFormat::amString() const {
  return getString<ISTRMAX>(value_.AMString);
}

/**
 * @return 午後表記文字列
 */
data::Lmbcs IntlFormat::pmString() const {
  return getString<ISTRMAX>(value_.PMString);
}

/**
 * @return 通貨記号文字列
 */
data::Lmbcs IntlFormat::currencyString() const {
  return getString<ISTRMAX>(value_.CurrencyString);
}

/**
 * @return 3桁区切り文字列
 */
data::Lmbcs IntlFormat::thousandString() const {
  return getString<ISTRMAX>(value_.ThousandString);
}

/**
 * @return 小数点文字列
 */
data::Lmbcs IntlFormat::decimalString() const {
  return getString<ISTRMAX>(value_.DecimalString);
}

/**
 * @return 日付区切り文字列
 */
data::Lmbcs IntlFormat::dateString() const {
  return getString<ISTRMAX>(value_.DateString);
}

/**
 * @return 時間区切り文字列
 */
data::Lmbcs IntlFormat::timeString() const {
  return getString<ISTRMAX>(value_.TimeString);
}

/**
 * @return 「昨日」文字列
 */
data::Lmbcs IntlFormat::yesterdayString() const {
  return getString<YTSTRMAX>(value_.YesterdayString);
}

/**
 * @return 「今日」文字列
 */
data::Lmbcs IntlFormat::todayString() const {
  return getString<YTSTRMAX>(value_.TodayString);
}

/**
 * @return 「明日」文字列
 */
data::Lmbcs IntlFormat::tomorrowString() const {
  return getString<YTSTRMAX>(value_.TomorrowString);
}

/**
 * @param src 午前表記文字列
 */
void IntlFormat::setAmString(const data::Lmbcs &src) {
  setString<ISTRMAX>(value_.AMString, src);
}

/**
 * @param src 午後表記文字列
 */
void IntlFormat::setPmString(const data::Lmbcs &src) {
  setString<ISTRMAX>(value_.PMString, src);
}

/**
 * @param src 通貨記号文字列
 */
void IntlFormat::setCurrencyString(const data::Lmbcs &src) {
  setString<ISTRMAX>(value_.CurrencyString, src);
}

/**
 * @param src 3桁区切り文字列
 */
void IntlFormat::setThousandString(const data::Lmbcs &src) {
  setString<ISTRMAX>(value_.ThousandString, src);
}

/**
 * @param src 小数点文字列
 */
void IntlFormat::setDecimalString(const data::Lmbcs &src) {
  setString<ISTRMAX>(value_.DecimalString, src);
}

/**
 * @param src 日付区切り文字列
 */
void IntlFormat::setDateString(const data::Lmbcs &src) {
  setString<ISTRMAX>(value_.DateString, src);
}

/**
 * @param src 時間区切り文字列
 */
void IntlFormat::setTimeString(const data::Lmbcs &src) {
  setString<ISTRMAX>(value_.TimeString, src);
}

/**
 * @param src 「昨日」文字列
 */
void IntlFormat::setYesterdayString(const data::Lmbcs &src) {
  setString<YTSTRMAX>(value_.YesterdayString, src);
}

/**
 * @param src 「今日」文字列
 */
void IntlFormat::setTodayString(const data::Lmbcs &src) {
  setString<YTSTRMAX>(value_.TodayString, src);
}

/**
 * @param src 「明日」文字列
 */
void IntlFormat::setTomorrowString(const data::Lmbcs &src) {
  setString<YTSTRMAX>(value_.TomorrowString, src);
}

/**
 * @return デフォルトの設定
 */
INTLFORMAT IntlFormat::getCurrent() {
  INTLFORMAT intlformat;
  OSGetIntlSettings(&intlformat, sizeof(INTLFORMAT));
  return intlformat;
}

} // namespace npp2::converter
