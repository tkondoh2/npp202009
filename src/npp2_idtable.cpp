#include "../include/npp2/idtable.hpp"

namespace npp2 {

void IDDestroyTableDeleter::operator ()(DHANDLE handle) {
  Status status = IDDestroyTable(handle);
  if (!status) { throw status; }
}

IdTable::IdTable(DHANDLE handle)
  : LockableObject2<DHANDLE, IDDestroyTableDeleter>(handle)
{}

IdTable::~IdTable()
{}

DWORD IdTable::count() const {
  return isNull() ? 0 : IDEntries(rawHandle());
}

bool IdTable::isEmpty() const {
  return count() == 0;
}

void IdTable::scan(const std::function<bool(NOTEID)> &fn) const {
  if (isNull()) return;
  NOTEID retID = 0;
  BOOL first = TRUE;
  while (IDScan(rawHandle(), first, &retID)) {
    if (!fn(retID)) break;
    first = FALSE;
  }
}

bool IdTable::insert(NOTEID id) {
  if (isNull()) return false;
  BOOL inserted = FALSE;
  Status status = IDInsert(rawHandle(), id, &inserted);
  if (!status) { throw status; }
  return inserted ? false : true;
}

bool IdTable::remove(NOTEID id) {
  if (isNull()) return false;
  BOOL deleted = FALSE;
  Status status = IDDelete(rawHandle(), id, &deleted);
  if (!status) { throw status; }
  return deleted ? true : false;
}

bool IdTable::contains(NOTEID id) const {
  if (isNull()) return false;
  return IDIsPresent(rawHandle(), id) != FALSE;
}

// void IdTable::copyToFolder() {

// }

} // namespace npp2
