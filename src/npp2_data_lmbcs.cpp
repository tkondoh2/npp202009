#include "../include/npp2/data/lmbcs.hpp"
#include "../include/npp2/nls/translate.hpp"
#include "../include/npp2/status.hpp"
#include "../include/npp2/utils/data.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::data {

WORD Lmbcs::dataType() const {
  return TYPE_TEXT;
}

/**
 * @brief デフォルトコンストラクタ
 */
Lmbcs::Lmbcs()
  : Variant()
  , raw_()
{}

/**
 * @brief コンストラクタ
 * @param p ヌル終端文字列
 */
Lmbcs::Lmbcs(const char *p)
  : Variant()
  , raw_(p)
{}

/**
 * @brief コンストラクタ
 * @param p 文字列
 * @param n サイズ
 * @param fPrefix データタイププレフィックス分ずらす場合はTRUE
 * @param fReplace NULL文字を改行に置き換える時はtrue
 */
Lmbcs::Lmbcs(const char *p, int n, BOOL fPrefix, bool fReplace)
  : Variant()
  , raw_(dataptr<char>(const_cast<char*>(p), fPrefix), n)
{
  if (fReplace) {
    raw_.replace('\0', '\n');
  }
}

/**
 * @brief コンストラクタ
 * @param str QByteArrayデータ(あくまでLMBCS文字列が入っている前提)
 */
Lmbcs::Lmbcs(const QByteArray &str)
  : Variant()
  , raw_(str)
{}

Lmbcs::~Lmbcs()
{}

/**
 * @return QByteArrayデータ
 */
const QByteArray &Lmbcs::raw() const {
  return raw_;
}

/**
 * @return C文字列ポインタ
 */
char *Lmbcs::data() {
  return raw_.data();
}

/**
 * @return 定数C文字列ポインタ
 */
const char *Lmbcs::constData() const {
  return raw_.constData();
}

/**
 * @return データ長
 */
int Lmbcs::size() const {
  return raw_.size();
}

/**
 * @return データ長が0ならtrue
 */
bool Lmbcs::isEmpty() const {
  return raw_.isEmpty();
}

/**
 * @param str 検索文字列
 * @param from 検索開始位置
 * @return 文字列が見つかった位置
 */
int Lmbcs::indexOf(const Lmbcs &str, int from) const {
  return raw_.indexOf(str.raw_, from);
}

/**
 * @brief 部分文字列の置き換え
 * @param from 置き換え元の文字列
 * @param to 置き換え先の文字列
 * @return 置き換え済みの自身の参照
 */
Lmbcs &Lmbcs::replace(const Lmbcs &before, const Lmbcs &after) {
  raw_.replace(before.raw_, after.raw_);
  return *this;
}

/**
 * @brief 加算代入演算子(文字列結合)
 * @param rhs 結合する文字列
 * @return 結合済み文字列(自身)
 */
Lmbcs &Lmbcs::operator +=(const Lmbcs &rhs) {
  raw_ += rhs.raw_;
  return *this;
}

/**
 * @brief 加算演算子(文字列結合)
 * @param lhs 左辺値
 * @param rhs 右辺値
 * @return 左辺値と右辺値を結合した文字列
 */
Lmbcs operator +(const Lmbcs &lhs, const Lmbcs &rhs) {
  return Lmbcs(lhs.raw_ + rhs.raw_);
}

/**
 * @brief ＜(小なり)演算子
 * @param lhs 左辺値
 * @param rhs 右辺値
 * @return 左辺値が右辺値より小さい場合true
 */
bool operator <(const Lmbcs &lhs, const Lmbcs &rhs) {
  return lhs.raw_ < rhs.raw_;
}

/**
 * @brief 等価演算子
 * @param lhs 左辺値
 * @param rhs 右辺値
 * @return 左辺値と右辺値が等価の場合true
 */
bool operator ==(const Lmbcs &lhs, const Lmbcs &rhs) {
  return lhs.raw_ == rhs.raw_;
}

} // namespace npp2::data

namespace npp2 {

/**
 * @brief STATUS値からエラー内容のテキストに変換する
 * @param status ステータス値
 * @return エラーテキスト(LMBCS文字列)
 */
data::Lmbcs fromStatus(STATUS status) {
  return data::Lmbcs(Status(status).message());
}

} // namespace npp2

/**
 * @brief QString文字列をLMBCS文字列に変換する
 * @param qstr QString
 * @return LMBCS文字列
 */
npp2::data::Lmbcs toLmbcs(const QString &qstr) {
  QByteArray x(
    reinterpret_cast<const char*>(qstr.utf16()),
    qstr.size() * sizeof(ushort)
  );
  return npp2::nls::unicodeToLmbcs(x);
}

/**
 * @brief LMBCS文字列をQString文字列に変換する
 * @param lmbcs LMBCS文字列
 * @return QString
 */
QString fromLmbcs(const npp2::data::Lmbcs &lmbcs) {
  auto x = npp2::nls::lmbcsToUnicode(lmbcs.raw());
  return QString::fromUtf16(
    reinterpret_cast<const ushort*>(x.constData()),
    x.size() / sizeof(ushort)
  );
}

/**
 * @brief QString文字列をLMBCS文字列に変換する
 * @param qstr QString
 * @return LMBCS文字列
 */
npp2::data::Lmbcs nativeTo(const QByteArray &native) {
  return npp2::nls::nativeToLmbcs(native);
}

/**
 * @brief LMBCS文字列をQString文字列に変換する
 * @param lmbcs LMBCS文字列
 * @return QString
 */
QByteArray toNative(const npp2::data::Lmbcs &lmbcs) {
  return npp2::nls::lmbcsToNative(lmbcs.raw());
}
