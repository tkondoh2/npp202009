#include "../include/npp2/env.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osfile.h>
#include <names.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::env {

/**
 * @brief 環境変数から文字列値を取得する
 * @param key 環境変数名
 * @param fn 環境変数名が見つからない時にデフォルト値を返す式
 * @return 文字列値
 */
QByteArray getString(
  const char *key,
  std::function<QByteArray()> fn
) noexcept {
  char buffer[MAXENVVALUE + 1];
  return OSGetEnvironmentString(key, buffer, MAXENVVALUE)
    ? QByteArray(buffer)
    : fn();
}

/**
 * @brief 環境変数に文字列値を設定する
 * @param key 環境変数名
 * @param value 文字列値
 */
void setString(const char *key, const QByteArray &value) noexcept {
  OSSetEnvironmentVariable(key, value.constData());
}

/**
 * @brief 環境変数に変数が設定されているかテストする
 * @param key 環境変数名
 * @return 設定されていればtrue
 */
bool has(const char *key) noexcept {
  return !getString(key).isEmpty();
}

/**
 * @return データディレクトリ
 */
QByteArray getDataDirectory() noexcept {
  char path[MAXPATH] = "";
  WORD len = OSGetDataDirectory(path);
  return QByteArray(path, len);
}

/**
 * @return プログラムディレクトリ
 */
QByteArray getProgramDirectory() noexcept {
  char path[MAXPATH] = "";
  OSGetExecutableDirectory(path);
  return QByteArray(path);
}

} // namespace npp2::env
