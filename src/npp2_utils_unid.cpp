#include "../include/npp2/utils/unid.hpp"
#include "../include/npp2/utils/hexint.hpp"
#include "../include/npp2/utils/time.hpp"

namespace npp2 {

UniversalNoteId::UniversalNoteId()
  : value_({getMinTimeDate(), getMinTimeDate()})
{}

UniversalNoteId::UniversalNoteId(const UNID &value)
  : value_(value)
{}

UniversalNoteId::operator UNID() const {
  return value_;
}

QByteArray UniversalNoteId::toString(bool withColon) const {
  auto s = formatHexInt<DWORD,uint>(value_.File.Innards[1])
    + formatHexInt<DWORD,uint>(value_.File.Innards[0]);
  if (withColon) {
    s += ":";
  }
  return s + formatHexInt<DWORD,uint>(value_.Note.Innards[1])
    + formatHexInt<DWORD,uint>(value_.Note.Innards[0]);
}

} // namespace npp2
