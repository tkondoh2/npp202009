#include "../include/npp2/logger/level.hpp"

namespace npp2::logger {

QString levelText(Level level) {
  switch (level) {
  case Level::Off: return "Off";
  case Level::Critical: return "Critical";
  case Level::Error: return "Error";
  case Level::Warning: return "Warning";
  case Level::Info: return "Info";
  case Level::Debug: return "Debug";
  case Level::Trace: return "Trace";
  default: return "Max";
  }
}

Level textToLevel(const QString &text) {
  if (text.compare("Off", Qt::CaseInsensitive) == 0)
    return Level::Off;
  else if (text.compare("Critical", Qt::CaseInsensitive) == 0)
    return Level::Critical;
  else if (text.compare("Error", Qt::CaseInsensitive) == 0)
    return Level::Error;
  else if (text.compare("Warning", Qt::CaseInsensitive) == 0)
    return Level::Warning;
  else if (text.compare("Info", Qt::CaseInsensitive) == 0)
    return Level::Info;
  else if (text.compare("Debug", Qt::CaseInsensitive) == 0)
    return Level::Debug;
  else if (text.compare("Trace", Qt::CaseInsensitive) == 0)
    return Level::Trace;
  else return Level::Max;
}

} // namespace npp2::logger
