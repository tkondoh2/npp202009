#include "../include/npp2/item.hpp"
#include <QScopedPointer>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <ods.h>
#include <nsfnote.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

Item::Item()
  : itemId_(NullBlockID)
  , dataType_(TYPE_INVALID_OR_UNKNOWN)
  , valueId_(NullBlockID)
  , valueSize_(0)
{}

Item::Item(BLOCKID itemId, WORD dataType, BLOCKID valueId, DWORD valueSize)
  : itemId_(itemId)
  , dataType_(dataType)
  , valueId_(valueId)
  , valueSize_(valueSize)
{}

const BLOCKID Item::itemId() const {
  return itemId_;
}

WORD Item::dataType() const {
  return dataType_;
}

const BLOCKID Item::valueId() const {
  return valueId_;
}

DWORD Item::valueSize() const {
  return valueSize_;
}

bool Item::isNull() const {
  return ISNULLBLOCKID(itemId_);
}

Item::operator bool() const {
  return !isNull();
}

Block Item::itemBlock() const {
  return Block(itemId_);
}

Block Item::valueBlock() const {
  return Block(valueId_);
}

data::Any Item::value() const {
  BlockLocker lock(valueBlock());
  auto p = lock.ptr();
  return data::Any(p, valueSize());
}

data::Lmbcs Item::convertToText(
  const data::Lmbcs &lineDelimiter,
  WORD charsPerLine,
  BOOL fStripTabs
) const {
  DHANDLE hBuffer = NULLHANDLE;
  DWORD bufferLen = 0;
  Status status = ConvertItemToText(
    valueId_,
    valueSize_,
    lineDelimiter.constData(),
    charsPerLine,
    &hBuffer,
    &bufferLen,
    fStripTabs
  );
  npp2::LockableObject3<DHANDLE> buffer(hBuffer);
  if (!status) { throw status; }
  npp2::Locker<DHANDLE> lock(buffer.rawHandle());
  auto ptr = lock.ptr<const char>();
  return data::Lmbcs(ptr, static_cast<int>(bufferLen));
}

data::Lmbcs Item::convertToText(
  WORD valueType,
  char separator
) const {
  QScopedArrayPointer<char> buffer(new char[MAXWORD]);
  auto len = NSFItemConvertValueToText(
    valueType,
    valueId_,
    valueSize_,
    buffer.data(),
    MAXWORD,
    separator
  );
  return data::Lmbcs(buffer.data(), static_cast<int>(len));
}

} // namespace npp2
