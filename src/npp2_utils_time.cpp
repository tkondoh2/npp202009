#include "../include/npp2/utils/time.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <misc.h> // TimeConstant

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

TIMEDATE getMinTimeDate() {
  TIMEDATE td;
  TimeDateClear(&td);
  return td;
}

TIMEDATE getMaxTimeDate() {
  TIMEDATE td;
  TimeConstant(TIMEDATE_MAXIMUM, &td);
  return td;
}

} // namespace npp2
