#include "../include/npp2/database.hpp"
#include "../include/npp2/utils/time.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osfile.h> // OSPathNetConstruct
#include <miscerr.h>
#include <nif.h>
#include <foldman.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

void NSFDbCloseDeleter::operator ()(DBHANDLE hDb) {
  Status status = NSFDbClose(hDb);
  if (!status) { throw status; }
}

/**
 * @brief コンストラクタ
 * @param raw 生データ
 */
_Database::PathNet::PathNet(const data::Lmbcs &raw)
  : raw_(raw)
{}

std::tuple<data::Lmbcs,data::Lmbcs,data::Lmbcs> _Database::PathNet::parse() const {
  char path[MAXPATH] = "";
  char server[MAXPATH] = "";
  char port[MAXPATH] = "";
  Status status = OSPathNetParse(
    raw_.constData(),
    port,
    server,
    path
  );
  if (!status) throw status;
  return std::make_tuple(
    data::Lmbcs(path),
    data::Lmbcs(server),
    data::Lmbcs(port)
  );
}

/**
 * @brief パスネットをパーツから生成する
 * @param path パス
 * @param server サーバ名(省略可)
 * @param port ポート名(省略可)
 * @return パスネットオブジェクト
 */
_Database::PathNet _Database::PathNet::build(
    const data::Lmbcs &path,
    const data::Lmbcs &server,
    const data::Lmbcs &port
    ) noexcept(false) {
  char buffer[MAXPATH] = "";
  Status status = OSPathNetConstruct(
        port.isEmpty() ? nullptr : port.constData(),
        server.constData(),
        path.constData(),
        buffer
        );
  if (!status) { throw status; }
  return _Database::PathNet(data::Lmbcs(buffer));
}

/**
 * @brief デフォルトコンストラクタ
 */
_Database::_Database()
  : LockableObject2<DBHANDLE,NSFDbCloseDeleter>()
  , pUnreadIdTable_(nullptr)
{}

/**
 * @brief コンストラクタ
 * @param handle データベースハンドル
 */
_Database::_Database(DBHANDLE handle)
  : LockableObject2<DBHANDLE,NSFDbCloseDeleter>(handle)
  , pUnreadIdTable_(nullptr)
{}

_Database::~_Database()
{}

_Database::Paths _Database::getPaths() noexcept(false) {
  return getPaths(rawHandle());
}

data::Lmbcs _Database::getAccessingUserName() {
  return getAccessingUserName(rawHandle());
}

/**
 * @brief データベースをパスネットでオープンする
 * @param pathnet パスネットオブジェクト
 * @return データベーススマートポインタ
 */
Database _Database::open(const PathNet &pathnet) noexcept(false) {
  DBHANDLE handle = NULLHANDLE;
  Status status = NSFDbOpen(pathnet.raw_.constData(), &handle);
  if (!status) { throw status; }
  return std::make_unique<_Database>(handle);
}

/**
 * @brief データベースをパスパーツでオープンする
 * @param path パス
 * @param server サーバ名(省略可)
 * @param port ポート名(省略可)
 * @return データベーススマートポインタ
 */
Database _Database::open(
  const data::Lmbcs &path,
  const data::Lmbcs &server,
  const data::Lmbcs &port
) noexcept(false) {
  return open(PathNet::build(path, server, port));
}

/**
 * @brief データベースをパスネットでオープンする
 * @param pathnet パスネットオブジェクト
 * @return データベーススマートポインタ
 */
Database _Database::openEx(
  const NamesListPtr &namesList,
  WORD options,
  const PathNet &pathnet
) noexcept(false) {
  DBHANDLE handle = NULLHANDLE;
  TIMEDATE td = getMinTimeDate();
  Status status = NSFDbOpenExtended(
    pathnet.raw_.constData(),
    options,
    // NULLHANDLE,
    namesList->rawHandle(),
    &td,
    &handle,
    &td,
    &td
  );
  if (!status) { throw status; }
  return std::make_unique<_Database>(handle);
}

Database _Database::openEx(
    const NamesListPtr &namesList,
    WORD options,
    const data::Lmbcs &path,
    const data::Lmbcs &server,
    const data::Lmbcs &port
    ) noexcept(false) {
  return openEx(namesList, options, PathNet::build(path, server, port));
}

_Database::Paths _Database::getPaths(DBHANDLE hDB) noexcept(false) {
  char canonical[MAXPATH] = "";
  char expanded[MAXPATH] = "";
  Status status = NSFDbPathGet(hDB, canonical, expanded);
  if (!status) { throw status; }
  return _Database::Paths({
    data::Lmbcs(canonical),
    data::Lmbcs(expanded)
  });
}

data::Lmbcs _Database::getAccessingUserName(DBHANDLE hDB) noexcept(false) {
  char user[MAXUSERNAME + 1] = "";
  Status status = NSFDbUserNameGet(hDB, user, MAXUSERNAME);
  if (!status) { throw status; }
  return data::Lmbcs(user);
}

data::Lmbcs _Database::getInfo(DBHANDLE hDB, WORD what) noexcept(false) {
  char buffer[NSF_INFO_SIZE] = "";
  Status status = NSFDbInfoGet(hDB, buffer);
  if (!status) { throw status; }
  buffer[NSF_INFO_SIZE - 1] = '\0';
  char buf[NSF_INFO_SIZE] = "";
  NSFDbInfoParse(buffer, what, buf, NSF_INFO_SIZE - 1);
  buf[NSF_INFO_SIZE - 1] = '\0';
  return data::Lmbcs(buf);
}

data::Lmbcs _Database::getInfoTitle(DBHANDLE hDB) noexcept(false) {
  return getInfo(hDB, INFOPARSE_TITLE);
}

data::Lmbcs _Database::getInfoCategories(DBHANDLE hDB) noexcept(false) {
  return getInfo(hDB, INFOPARSE_CATEGORIES);
}

data::Lmbcs _Database::getInfoClass(DBHANDLE hDB) noexcept(false) {
  return getInfo(hDB, INFOPARSE_CLASS);
}

data::Lmbcs _Database::getInfoDesignClass(DBHANDLE hDB) noexcept(false) {
  return getInfo(hDB, INFOPARSE_DESIGN_CLASS);
}

data::Lmbcs _Database::getInfo(WORD what) noexcept(false) {
  return getInfo(rawHandle(), what);
}

data::Lmbcs _Database::getInfoTitle() noexcept(false) {
  return getInfoTitle(rawHandle());
}

data::Lmbcs _Database::getInfoCategories() noexcept(false) {
  return getInfoCategories(rawHandle());
}

data::Lmbcs _Database::getInfoClass() noexcept(false) {
  return getInfoClass(rawHandle());
}

data::Lmbcs _Database::getInfoDesignClass() noexcept(false) {
  return getInfoDesignClass(rawHandle());
}

DBID _Database::getId(DBHANDLE hDB) noexcept(false) {
  DBID dbId;
  Status status = NSFDbIDGet(hDB, &dbId);
  if (!status) { throw status; }
  return dbId;
}

DBID _Database::getId() noexcept(false) {
  return getId(rawHandle());
}

DBREPLICAINFO _Database::getReplicaInfo(DBHANDLE hDB) {
  DBREPLICAINFO result;
  Status status = NSFDbReplicaInfoGet(hDB, &result);
  if (!status) { throw status; }
  return result;
}

DBREPLICAINFO _Database::getReplicaInfo() const {
  return getReplicaInfo(rawHandle());
}

NOTEID _Database::findDesign(DBHANDLE hDb, const char *name, WORD noteClass) {
  NOTEID noteId = 0;
  Status status = NIFFindDesignNote(hDb, const_cast<char*>(name), noteClass, &noteId);
  if (!status) {
    if (status.error() != ERR_NOT_FOUND)
      throw status;
  }
  return noteId;
}

NOTEID _Database::findDesign(const char *name, WORD noteClass) {
  return findDesign(rawHandle(), name, noteClass);
}

UnreadIdTable &_Database::getUnreadIdTable() {
  if (!pUnreadIdTable_) {
    pUnreadIdTable_.reset(UnreadIdTable::build(rawHandle()));
  }
  return *pUnreadIdTable_;
}

OID _Database::generateOid(DBHANDLE hDb) {
  OID oid;
  Status status = NSFDbGenerateOID(hDb, &oid);
  if (!status) { throw status; }
  return oid;
}

OID _Database::generateOid() {
  return generateOid(rawHandle());
}

NOTEID _Database::createFolder(
  DBHANDLE hDb,
  const data::Lmbcs &name,
  NOTEID formatNoteId,
  DBHANDLE hFormatDb,
  DESIGN_TYPE folderType
) {
  NOTEID folderId = 0;
  Status status = FolderCreate(
    hDb,
    NULLHANDLE,
    formatNoteId,
    (hFormatDb != NULLHANDLE) ? hFormatDb : hDb,
    const_cast<char*>(name.constData()),
    name.size(),
    folderType,
    0,
    &folderId
  );
  if (!status) { throw status; }
  return folderId;
}

NOTEID _Database::createFolder(
  const data::Lmbcs &name,
  NOTEID formatNoteId,
  DBHANDLE hFormatDb,
  DESIGN_TYPE folderType
) const {
  return createFolder(rawHandle(), name, formatNoteId, hFormatDb, folderType);
}

void _Database::addToFolder(
  DBHANDLE hDb,
  NOTEID folderNoteId,
  DHANDLE hIdTable
) {
  Status status = FolderDocAdd(hDb, NULLHANDLE, folderNoteId, hIdTable, 0);
  if (!status) { throw status; }
}

void _Database::addToFolder(
  NOTEID folderNoteId,
  DHANDLE hIdTable
) {
  addToFolder(rawHandle(), folderNoteId, hIdTable);
}

} // namespace npp2
