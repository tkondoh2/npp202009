#include "../include/npp2/converter/numberpair.hpp"

namespace npp2::converter {

/**
 * @brief デフォルトコンストラクタ
 */
NumberPairToTextConverter::NumberPairToTextConverter()
  : NumberToTextConverter()
{}

/**
 * @brief コンストラクタ
 * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
 * @param nPtr NFMTラッパーの共有ポインタ
 */
NumberPairToTextConverter::NumberPairToTextConverter(
  IntlFormatPtr &&iPtr,
  format::NumberPtr &&nPtr
)
  : NumberToTextConverter(std::move(iPtr), std::move(nPtr))
{}

/**
 * @brief 関数呼び出し
 * @param pPair 変換元数値ペアへのポインタ
 * @return フォーマット済み文字列
 */
data::Lmbcs NumberPairToTextConverter::operator () (NUMBER_PAIR *pPair) const {
  return NumberToTextConverter::operator()(&pPair->Lower)
    + " - "
    + NumberToTextConverter()(&pPair->Upper);
}

} // namespace npp2::converter
