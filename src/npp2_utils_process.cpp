#include "../include/npp2/utils/process.hpp"

#if defined(NT)
#include <Windows.h>
#else
#include <unistd.h>
#endif

namespace npp2 {

QByteArray getProcessFilePath() noexcept {
#if defined(NT)
  char buffer[MAX_PATH] = "";
  return GetModuleFileNameA(nullptr, buffer, MAX_PATH)
      ? QByteArray(buffer)
      : QByteArray();
#else
  char path[1024] = "";
  ssize_t len = readlink("/proc/self/exe", path, sizeof(path) - 1);
  return QByteArray(path, static_cast<int>(len));
#endif
}

QFileInfo getProcessFileInfo() noexcept {
  return QFileInfo(QString::fromLocal8Bit(getProcessFilePath()));
}

} // namespace npp2
