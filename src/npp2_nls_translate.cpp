#include "../include/npp2/nls/translate.hpp"
#include "../include/npp2/nls/size.hpp"

namespace npp2::nls {

/**
 * @brief KiBをByteにします。
 * @tparam T 数値型
 * @param n KiB
 * @return Byte
 */
template <typename T>
T KiBytesToBytes(T n) {
  return n * 1024;
}

const auto MAX_TRANSLATE_BUFFER_SIZE = KiBytesToBytes(60);

/**
 * @brief translateMax60kib関数の結果データ
 */
struct TranslateResult {
  QByteArray target; ///< 変換済み文字列
  int translatedSize; ///< 変換済みサイズ
};

/**
 * @brief 変換済み文字列が64KiBに制限される文字列変換をする
 * @tparam MAX_RATIO 変換後に想定される最大倍率
 * @tparam CTRL_FLAGS NLS_translate関数に適用する制御フラグ
 * @param source 変換元文字列バイト列
 * @param offset 変換を始める開始位置
 * @param srcInfo 変換元に適用されるNLS情報
 * @param targetInfo 変換先に適用されるNLS情報
 * @return TranslateResult型
 */
template <int MAX_RATIO, int CTRL_FLAGS>
TranslateResult translateMax60kib(
  const QByteArray &source,
  int offset,
  const Info &srcInfo,
  const Info &targetInfo
) noexcept(false) {
  // 最大のバッファサイズを求める
  const auto maxSrcSize = MAX_TRANSLATE_BUFFER_SIZE / MAX_RATIO;

  // 変換元文字列のサイズからオフセット位置まで差し引いたサイズを求める
  auto orgSrcSize = source.size() - offset;

  // 最大サイズと変換サイズを比較して小さい方を対象範囲にする
  auto srcSize = std::min(maxSrcSize, orgSrcSize);

  // 対象範囲末尾が文字境界をまたいでいたら範囲調整する
  srcSize = static_cast<int>(
    adjustByteSize(source.constData() + offset, srcSize, srcInfo.get())
  );

  // サイズが1バイトに満たない場合は処理しない
  // ただし処理済みサイズは処理したとしてサイズ分返す
  if (srcSize <= 0) {
    return TranslateResult { QByteArray(), orgSrcSize };
  }

  // バッファサイズを求める
  auto bufferSize = static_cast<WORD>(srcSize * MAX_RATIO);

  // バッファを作成する
  QByteArray buffer(bufferSize, '\0');

  // 変換API関数を実行する
  Status status = NLS_translate(
    reinterpret_cast<BYTE*>(const_cast<char*>(source.data()) + offset),
    static_cast<WORD>(srcSize),
    reinterpret_cast<BYTE*>(buffer.data()),
    &bufferSize,
    CTRL_FLAGS,
    targetInfo.get()
  );
  if (!status) throw status;

  // 結果を返す
  return TranslateResult { buffer.left(bufferSize), srcSize };
}

/**
 * @brief 文字列を別の文字コードに変換する
 * @tparam MAX_RATIO 変換後に想定される最大倍率
 * @tparam CTRL_FLAGS NLS_translate関数に適用する制御フラグ
 * @param source 変換元文字列バイト列
 * @param srcInfo 変換元に適用されるNLS情報
 * @param targetInfo 変換先に適用されるNLS情報
 * @return 変換済み文字列
 */
template <int MAX_RATIO, int CTRL_FLAGS>
QByteArray translate(
    const QByteArray &source,
    const Info &srcInfo,
    const Info &targetInfo
    ) noexcept(false) {
  QByteArray buffer;
  for (auto offset = 0; offset < source.size(); ) {
    auto result = translateMax60kib<MAX_RATIO, CTRL_FLAGS>(
      source,
      offset,
      srcInfo,
      targetInfo
    );
    buffer += result.target;
    offset += result.translatedSize;
  }
  return buffer;
}

/**
 * @brief Unicode文字列をLMBCS文字列に変換する
 * @param unicode Unicode文字列
 * @returns 変換後のLMBCS文字列
 */
QByteArray unicodeToLmbcs(const QByteArray &unicode) noexcept(false) {
  LoadedInfo<NLS_CS_UNICODE> unicodeInfo;
  LmbcsInfo lmbcsInfo;
  return translate<
    NLS_MAXRATIO_XLATE_TO_LMBCS,
    NLS_NONULLTERMINATE | NLS_SOURCEISUNICODE | NLS_TARGETISLMBCS
  >(
    unicode,
    unicodeInfo,
    lmbcsInfo
  );
}

/**
 * @brief LMBCS文字列をUnicode文字列に変換する
 * @param lmbcs LMBCS文字列
 * @returns 変換後のUnicode文字列
 */
QByteArray lmbcsToUnicode(const QByteArray &lmbcs) noexcept(false) {
  LoadedInfo<NLS_CS_UNICODE> unicodeInfo;
  LmbcsInfo lmbcsInfo;
  return translate<
    NLS_MAXRATIO_XLATE_FROM_LMBCS,
    NLS_NONULLTERMINATE | NLS_SOURCEISLMBCS | NLS_TARGETISUNICODE
  >(
    lmbcs,
    lmbcsInfo,
    unicodeInfo
  );
}

/**
 * @brief Native文字列をLMBCS文字列に変換する
 * @param native Native文字列
 * @returns 変換後のLMBCS文字列
 */
QByteArray nativeToLmbcs(const QByteArray &native) noexcept(false) {
  NativeInfo nativeInfo;
  LmbcsInfo lmbcsInfo;
  return translate<
    NLS_MAXRATIO_XLATE_TO_LMBCS,
    NLS_NONULLTERMINATE | NLS_SOURCEISPLATFORM | NLS_TARGETISLMBCS
  >(
    native,
    nativeInfo,
    lmbcsInfo
  );
}

/**
 * @brief LMBCS文字列をNative文字列に変換する
 * @param lmbcs LMBCS文字列
 * @returns 変換後のNative文字列
 */
QByteArray lmbcsToNative(const QByteArray &lmbcs) noexcept(false) {
  NativeInfo nativeInfo;
  LmbcsInfo lmbcsInfo;
  return translate<
    NLS_MAXRATIO_XLATE_FROM_LMBCS,
    NLS_NONULLTERMINATE | NLS_SOURCEISLMBCS | NLS_TARGETISPLATFORM
  >(
    lmbcs,
    lmbcsInfo,
    nativeInfo
  );
}

} // namespace npp2::nls
