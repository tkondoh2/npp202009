#include "../include/npp2/utils/writer/qdatafilewriter.hpp"

namespace npp2 {

/**
 * @brief コンストラクタ
 * @param filePath ファイルパス
 */
QDataFileWriter::QDataFileWriter(
  const QString &filePath,
  std::function<void(const QString&)> fn
)
  : _QFileWriter<QDataStream>(filePath, fn)
{}

/**
 * @brief ファイルオープン後に実行する処理
 */
void QDataFileWriter::afterOpenFile(bool)
{}

} // namespace npp2
