#include "../include/npp2/nls/size.hpp"

namespace npp2::nls {

/**
 * @brief バイト配列の文字数相当のバイト数を取得します。
 * @param s 対象のバイト配列
 * @param chars バイト数として測りたい文字数
 * @param pInfo バイト配列の文字セット情報
 * @return バイト数
 * @throw Status NLS_STATUSを内包したクラス
 */
WORD getStringBytes(
  const char *str,
  WORD chars,
  NLS_PINFO pInfo
) noexcept(false) {
  if (chars == 0) return 0;
  WORD bytes = 0;
  Status status = NLS_string_bytes(
    reinterpret_cast<const BYTE*>(str),
    chars,
    &bytes,
    pInfo
  );
  if (!status) throw status;
  return static_cast<WORD>(bytes);
}

/**
 * @brief バイト配列の文字数を取得します。
 * @param s 対象のバイト配列
 * @param bytes 文字数として測りたいバイト数
 * @param pInfo バイト配列の文字セット情報
 * @return 文字数
 * @throw Status NLS_STATUSを内包したクラス
 */
WORD getStringChars(
  const char *str,
  WORD bytes,
  NLS_PINFO pInfo
) noexcept(false) {
  if (bytes == 0) return 0;
  WORD chars = 0;
  Status status = NLS_string_chars(
    reinterpret_cast<const BYTE*>(str),
    bytes,
    &chars,
    pInfo
  );
  // 無効なデータ以外の失敗の時
  if (!status && status.raw() != NLS_INVALIDDATA) throw status;
  return static_cast<WORD>(chars);
}

/**
 * @brief 文字セットに応じて、マルチバイト文字列の区切り位置を調整したバイト数を求めます。
 * @param s 対象のバイト配列
 * @param bytes 計測対象のバイト数
 * @param pInfo バイト配列の文字セット情報
 * @return 調整したバイト数
 * @throw Status NLS_STATUSを内包したクラス
 */
WORD adjustByteSize(
  const char *str,
  WORD bytes,
  NLS_PINFO pInfo
) noexcept(false) {
  auto chars = getStringChars(str, bytes, pInfo);
  return getStringBytes(str, chars, pInfo);
}

} // namespace npp2::nls
