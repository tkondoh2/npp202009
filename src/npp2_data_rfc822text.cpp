#include "../include/npp2/data/rfc822text.hpp"
#include "../include/npp2/mime/decode.hpp"
#include "../include/npp2/utils/data.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <ods.h> // _RFC822ITEMDESC

#ifdef NT
#pragma pack(pop)
#endif

namespace npp2::data {

WORD Rfc822Text::dataType() const {
  return TYPE_RFC822_TEXT;
}

Rfc822Text::Rfc822Text()
  : Variant()
  , desc_()
  , native_()
  , name_()
  , delim_()
  , body_()
{}

WORD Rfc822Text::version() const {
  return desc_.wVersion;
}

DWORD Rfc822Text::flags() const {
  return desc_.dwFlags;
}

DWORD Rfc822Text::type() const {
  return flags() & RFC822_ITEM_FORMAT_MASK;
}

WORD Rfc822Text::nativeLen() const {
  return desc_.wNotesNativeLen;
}

WORD Rfc822Text::nameLen() const {
  return desc_.w822NameLen;
}

WORD Rfc822Text::delimLen() const {
  return desc_.w822DelimLen;
}

WORD Rfc822Text::bodyLen() const {
  return desc_.w822BodyLen;
}

const QByteArray &Rfc822Text::native() const {
  return native_;
}

const QByteArray &Rfc822Text::name() const {
  return name_;
}

const QByteArray &Rfc822Text::delim() const {
  return delim_;
}

const QByteArray &Rfc822Text::body() const {
  return body_;
}

bool Rfc822Text::isAddress() const {
  return type() == RFC822_ITEM_FORMAT_ADDR;
}

bool Rfc822Text::isDate() const {
  return type() == RFC822_ITEM_FORMAT_DATE;
}

bool Rfc822Text::isText() const {
  return type() == RFC822_ITEM_FORMAT_TEXT;
}

bool Rfc822Text::isStorageStrict() const {
  return flags() & RFC822_ITEM_STORAGE_STRICT;
}

bool Rfc822Text::isTextList() const {
  return flags() & RFC822_ITEM_TEXT_LIST;
}

bool Rfc822Text::isUnused() const {
  return flags() & RFC822_TEXT_UNUSED;
}

Lmbcs Rfc822Text::text() const {
  if (nativeLen() > 0) {
    return Lmbcs(native_);
  }
  else if (bodyLen() > 0) {
    return toLmbcs(mime::Decode::header(QString::fromLatin1(body_)));
  }
  else {
    return Lmbcs();
  }
}

TimeDate Rfc822Text::date() const {
  if (nativeLen() > 0) {
    return TimeDate(native_.constData(), FALSE);
  }
  if (bodyLen() > 0) {
    return TimeDate::fromQDateTime(
      QDateTime::fromString(QString::fromLocal8Bit(body_), Qt::RFC2822Date)
    );
  }
  return TimeDate();
}

TextList Rfc822Text::textList() const {
  auto result = std::make_unique<_TextList>();
  if (isTextList()) {
    result->duplicate(
      dataptr<LIST>(const_cast<char*>(native_.constData()), FALSE),
      FALSE
    );
  }
  return result;
}

Rfc822Text Rfc822Text::fromBytes(const void *ptr, BOOL fPrefix) {
  Rfc822Text result;

  char *p = dataptr<char>(const_cast<void*>(ptr), fPrefix);

  copyODStoRFC822ITEMDESC(&p, &result.desc_, 1);
  // p += ODSLength(_RFC822ITEMDESC);

  result.native_ = getAndInc(&p, result.nativeLen());
  result.name_ = getAndInc(&p, result.nameLen());
  result.delim_ = getAndInc(&p, result.delimLen());
  result.body_ = trimLastCRLF(getAndInc(&p, result.bodyLen()));

  return result;
}

void *Rfc822Text::copyODStoRFC822ITEMDESC(
  void *ppSrc,
  RFC822ITEMDESC *pDest,
  WORD iterations
) {
  char** pRef = reinterpret_cast<char**>(ppSrc);
  for (WORD i = 0; i < iterations; ++i) {
#ifdef NT
    static const auto size = ODSLength(_RFC822ITEMDESC);
    memcpy_s(pDest, size, *pRef, size);
    *pRef += size;
#else
    *pRef = _copy<WORD>(*pRef, &pDest->wVersion);
    *pRef = _copy<DWORD>(*pRef, &pDest->dwFlags);
    *pRef = _copy<WORD>(*pRef, &pDest->wNotesNativeLen);
    *pRef = _copy<WORD>(*pRef, &pDest->w822NameLen);
    *pRef = _copy<WORD>(*pRef, &pDest->w822DelimLen);
    *pRef = _copy<WORD>(*pRef, &pDest->w822BodyLen);
#endif
  }
  return pRef;
}

QByteArray Rfc822Text::getAndInc(char **pp, WORD len) {
  QByteArray result(*pp, static_cast<int>(len));
  *pp += len;
  return result;
}

QByteArray Rfc822Text::trimLastCRLF(const QByteArray &bytes) {
  const QByteArray crlf("\x0d\x0a");
  if (bytes.right(2) == crlf) {
    return bytes.left(bytes.length() - 2);
  }
  return bytes;
}

} // namespace npp2::data
