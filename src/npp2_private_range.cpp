﻿#include "../include/npp2/_private/range.hpp"

namespace npp2 {

/**
 * @brief 範囲型シングルリストの要素数を取得
 * @param pRange 範囲型のロック済みポインタ
 * @param fPrefix データタイププレフィックスの有無
 * @return 範囲型シングルリストの要素数
 */
WORD RangeGetNumItem(void *pRange, BOOL fPrefix) {
  return dataptr<RANGE>(pRange, fPrefix)->ListEntries;
}

/**
 * @brief 範囲型ペアリストの要素数を取得
 * @param pRange 範囲型のロック済みポインタ
 * @param fPrefix データタイププレフィックスの有無
 * @return 範囲型ペアリストの要素数
 */
WORD RangeGetNumPair(void *pRange, BOOL fPrefix) {
  return dataptr<RANGE>(pRange, fPrefix)->RangeEntries;
}

/**
 * @brief 範囲型からシングル/ペア要素すべての削除する。
 * @param hRange 範囲型ハンドル
 * @param fPrefix データタイププレフィックスの有無
 * @return API関数が返したステータス値
 */
STATUS RangeRemoveAll(DHANDLE hRange, BOOL fPrefix) {
  // メモリハンドルをロックしてポインタを取得
  char *p0 = reinterpret_cast<char*>(OSLockObject(hRange));

  // RANGE型データポインタを取得
  DWORD offset = sizeof(WORD) * (fPrefix ? 1 : 0);
  auto pRange = dataptr<RANGE>(p0, fPrefix);

  // カウンターを0にする。
  pRange->ListEntries = 0;
  pRange->RangeEntries = 0;

  // メモリハンドルをアンロックする
  OSUnlockObject(hRange);

  // メモリを最小限にする。
  return OSMemRealloc(hRange, offset + sizeof(RANGE));
}

} // namespace npp2
