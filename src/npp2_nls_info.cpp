#include "../include/npp2/nls/info.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osmisc.h> // OSGetLMBCSCLS

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::nls {

/**
 * @brief コンストラクタ
 * @param ptr NLS情報へのポインタ
 */
Info::Info(NLS_PINFO ptr)
  : ptr_(ptr)
{}

/**
 * @brief NLS情報へのポインタを返します。
 * @return NLS情報へのポインタ
 */
NLS_PINFO Info::get() const noexcept {
  return ptr_;
}

/**
 * @brief コンストラクタ
 */
LmbcsInfo::LmbcsInfo()
  : Info(OSGetLMBCSCLS())
{}

/**
 * @brief コンストラクタ
 */
NativeInfo::NativeInfo()
  : Info(OSGetNativeCLS())
{}

} // namespace npp2::nls
