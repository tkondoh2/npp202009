#include "../include/npp2/nameslist.hpp"
#include "../include/npp2/status.hpp"
// #include "_private/handleobject.hpp"
// #include "data/lmbcs.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdb.h>
// #include <acl.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2 {

NamesList::NamesList()
  : LockableObject3<DHANDLE>()
{}

NamesList::NamesList(DHANDLE handle)
  : LockableObject3<DHANDLE>(handle)
{}

NamesList::~NamesList()
{}

NamesListPtr NamesList::build(const data::Lmbcs &name) {
  DHANDLE handle = NULLHANDLE;
  Status status = NSFBuildNamesList(const_cast<char*>(name.constData()), 0, &handle);
  return std::make_unique<NamesList>(handle);
}

} // namespace npp2
