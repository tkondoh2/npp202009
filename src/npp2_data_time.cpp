#include "../include/npp2/data/time.hpp"

// #ifdef NT
// #pragma pack(push, 1)
// #endif

// #include <global.h>
// #include <misc.h>

// #ifdef NT
// #pragma pack(pop)
// #endif

namespace npp2::data {

/**
 * @brief デフォルトコンストラクタ
 */
Time::Time()
  : year_(0)
  , month_(0)
  , day_(0)
  , hour_(0)
  , minute_(0)
  , second_(0)
  , millisecond_(0)
  , isDaylightTime_(false)
  , offsetSecond_(0)
{}

/**
 * @brief コンストラクタ
 * @param year 年
 * @param month 月
 * @param day 日
 * @param hour 時
 * @param minute 分
 * @param second 秒
 * @param millisecond ミリ秒
 * @param isDaylightTime 夏時間の有無
 * @param offsetSecond UTCオフセット秒
 */
Time::Time(
  int year,
  int month,
  int day,
  int hour,
  int minute,
  int second,
  int millisecond,
  bool isDaylightTime,
  int offsetSecond
)
  : year_(year)
  , month_(month)
  , day_(day)
  , hour_(hour)
  , minute_(minute)
  , second_(second)
  , millisecond_(millisecond)
  , isDaylightTime_(isDaylightTime)
  , offsetSecond_(offsetSecond)
{}

/**
 * @return 年
 */
int Time::year() const {
  return year_;
}

/**
 * @return 月
 */
int Time::month() const {
  return month_;
}

/**
 * @return 日
 */
int Time::day() const {
  return day_;
}

/**
 * @return 時
 */
int Time::hour() const {
  return hour_;
}

/**
 * @return 分
 */
int Time::minute() const {
  return minute_;
}

/**
 * @return 秒
 */
int Time::second() const {
  return second_;
}

/**
 * @return ミリ秒
 */
int Time::millisecond() const {
  return millisecond_;
}

/**
 * @return 夏時間の有無
 */
bool Time::isDaylightTime() const {
  return isDaylightTime_;
}

/**
 * @return UTCオフセット秒
 */
int Time::offsetSeconds() const {
  return offsetSecond_;
}

TIME Time::toTIME() const {
  TIME time;
  time.year = year_;
  time.month = month_;
  time.day = day_;
  time.hour = hour_;
  time.minute = minute_;
  time.second = second_;
  time.hundredth = millisecond_ / 10;
  time.dst = isDaylightTime_ ? TRUE : FALSE;
  time.zone = offsetSecondsToZone(offsetSecond_, isDaylightTime_);
  return time;
}

Time Time::fromTIME(const TIME &time) {
  return Time(
    time.year,
    time.month,
    time.day,
    time.hour,
    time.minute,
    time.second,
    time.hundredth * 10,
    time.dst == TRUE,
    zoneToOffsetSeconds(time.zone)
  );

}

/**
 * @brief UTCオフセット秒(夏時間を含む)からTIME::zone値(夏時間を含まない)に変換する
 * @param secs UTCオフセット秒
 * @param isDaylightTime 夏時間の有無
 * @return TIME::zone値
 */
int Time::offsetSecondsToZone(int secs, bool isDaylightTime) {
  int mins = secs / 60;
  int minsEx = mins % 60;
  int dst = isDaylightTime ? TRUE : FALSE;
  return (mins / -60 + dst) + (minsEx == 0 ? 0 : minsEx * -100);
}

/**
 * @brief TIME::zone値(夏時間を含まない)からUTCオフセット秒(夏時間を含む)に変換する
 * @param zone TIME::zone値
 * @param isDaylightTime 夏時間の有無
 * @return UTCオフセット秒
 */
int Time::zoneToOffsetSeconds(int zone, bool isDaylightTime) {
  int hours = zone;
  int mins = 0;
  int dst = isDaylightTime ? TRUE : FALSE;
  if (hours < 100 && hours > -100) {
    mins = (hours - dst) * -60;
  }
  else {
    mins = hours / 100;
    hours = hours % 100;
    mins = (hours - dst) * -60 - mins;
  }
  return mins * 60;
}

} // namespace npp2::data
