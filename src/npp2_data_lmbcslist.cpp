#include "../include/npp2/data/lmbcslist.hpp"

namespace npp2::data {

LmbcsList::LmbcsList()
  : QList<Lmbcs>()
{}

LmbcsList::LmbcsList(const std::initializer_list<npp2::data::Lmbcs> &list)
  : QList<Lmbcs>()
{
  for (auto it: list) {
    append(it);
  }
}

QString LmbcsList::atAsQStr(int i) const {
  return fromLmbcs(at(i));
}

bool LmbcsList::containsByQStr(
  const Lmbcs &search,
  Qt::CaseSensitivity cs
) const {
  auto v = fromLmbcs(search);
  for (auto i = 0; i < size(); ++i) {
    auto item = atAsQStr(i);
    if (item.compare(v, cs) == 0) return true;
  }
  return false;
}

Lmbcs LmbcsList::join(const Lmbcs &sep) const {
  if (!isEmpty()) {
    Lmbcs result = isEmpty() ? Lmbcs() : first();
    for (int i = 1; i < size(); ++i) {
      result += (sep + at(i));
    }
    return result;
  }
  return Lmbcs();
}

QStringList LmbcsList::toQStrList() const {
  return map<QStringList, QString>(fromLmbcs);
}

LmbcsList LmbcsList::split(const Lmbcs &target, char sep) {
  LmbcsList result;
  auto lmbcsList = target.raw().split(sep);
  foreach (auto item, lmbcsList) {
    result.append(item);
  }
  return result;
}

} // namespace npp2::data
