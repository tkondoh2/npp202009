#include "../include/npp2/utils/writer/qtextfilewriter.hpp"

namespace npp2 {

/**
 * @brief コンストラクタ
 * @param filePath ファイルパス
 * @param codecName コーデック名(デフォルトUTF-8)
 * @param bom BOMの有無(デフォルトなし)
 */
QTextFileWriter::QTextFileWriter(
  const QString &filePath,
  std::function<void(const QString&)> fn,
  const QByteArray &codecName,
  bool bom
)
  : _QFileWriter<QTextStream>(filePath, fn)
  , codecName_(codecName)
  , bom_(bom)
{}

/**
 * @return QTextCodec* コーデックへのポインタを返す。
 */
QTextCodec *QTextFileWriter::codecPtr() const {
  return codecName_.isEmpty()
    ? nullptr
    : QTextCodec::codecForName(codecName_);
}

/**
 * @brief ファイルオープン後に実行する処理
 * ストリームに対し、コーデック設定、BOM設定を追加する。
 */
void QTextFileWriter::afterOpenFile(bool emptyFile) {
  auto codec = codecPtr();
  if (codec != nullptr) {
    pStream_->setCodec(codec);
  } else {
    pStream_->setAutoDetectUnicode(false);
  }
  pStream_->setGenerateByteOrderMark(bom_ && emptyFile);
}

} // namespace npp2
