#include "../include/npp2/converter/time.hpp"
#include "../include/npp2/status.hpp"
#include "../include/npp2/utils/time.hpp"

namespace npp2::converter {

/**
 * @brief デフォルトコンストラクタ
 */
TimeConverter::TimeConverter()
  : Base<format::Time>()
{}

/**
 * @brief コンストラクタ
 * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
 * @param tPtr NFMTラッパーの共有ポインタ
 */
TimeConverter::TimeConverter(IntlFormatPtr &&iPtr, format::TimePtr &&tPtr)
  : Base<format::Time>(std::move(iPtr), std::move(tPtr))
{}

/**
 * @brief デフォルトコンストラクタ
 */
TimeToTextConverter::TimeToTextConverter()
  : TimeConverter()
{}

/**
 * @brief コンストラクタ
 * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
 * @param tPtr NFMTラッパーの共有ポインタ
 */
TimeToTextConverter::TimeToTextConverter(
  IntlFormatPtr &&iPtr,
  format::TimePtr &&tPtr
)
  : TimeConverter(std::move(iPtr), std::move(tPtr))
{}

/**
 * @brief 関数呼び出し
 * @param pTime 変換元数値へのポインタ
 * @return フォーマット済み文字列
 */
data::Lmbcs TimeToTextConverter::operator () (TIMEDATE *pTime) const {
  char buffer[MAXALPHATIMEDATE] = "";
  WORD len = 0;
  Status status = ConvertTIMEDATEToText(
    !intlFormatPtr_ ? nullptr : intlFormatPtr_->data(),
    !formatPtr_ ? nullptr : formatPtr_->data(),
    pTime,
    buffer,
    MAXALPHATIMEDATE,
    &len
  );
  if (!status) { throw status; }
  return data::Lmbcs(buffer, len);
}

/**
 * @brief デフォルトコンストラクタ
 */
TextToTimeConverter::TextToTimeConverter()
  : TimeConverter()
{}

/**
 * @brief コンストラクタ
 * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
 * @param tPtr NFMTラッパーの共有ポインタ
 */
TextToTimeConverter::TextToTimeConverter(
  IntlFormatPtr &&iPtr,
  format::TimePtr &&tPtr
)
  : TimeConverter(std::move(iPtr), std::move(tPtr))
{}

/**
 * @brief 関数呼び出し
 * @param lmbcs 変換元テキスト
 * @return フォーマット済み文字列
 */
TIMEDATE TextToTimeConverter::operator () (const data::Lmbcs &lmbcs) const {
  char *ptr = const_cast<char*>(lmbcs.constData());

  TIMEDATE td = getMinTimeDate();
  Status status = ConvertTextToTIMEDATE(
        !intlFormatPtr_ ? nullptr : intlFormatPtr_->data(),
        !formatPtr_ ? nullptr : formatPtr_->data(),
        &ptr,
        static_cast<WORD>(lmbcs.size()),
        &td
        );
  if (!status) { throw status; }
  return td;
}

} // namespace npp2::converter
