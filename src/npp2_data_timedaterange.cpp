#include "../include/npp2/data/timedaterange.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::data {

TIMEDATE_PAIR TimeDatePair::defaultRaw() {
  return {
    TimeDate::getMinimum(),
    TimeDate::getMaximum(),
  };
}

WORD TimeDateRange::dataType() const {
  return TYPE_TIME_RANGE;
}

TimeDateRange::TimeDateRange()
  : Range<TimeDate, TimeDatePair>()
{}

TimeDateRange::TimeDateRange(RANGE *pRange, BOOL fPrefix)
  : Range<TimeDate, TimeDatePair>(pRange, fPrefix)
{}

} // namespace npp2::data
