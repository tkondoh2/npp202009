#include "../include/npp2/data/timedate.hpp"
#include "../include/npp2/utils/data.hpp"
#include "../include/npp2/status.hpp"
#include <QDate>
#include <QTime>
#include <QTimeZone>
#include <stdexcept>

// #include "variant.hpp"
// #include "time.hpp"
// #include "lmbcs.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>
#include <ostime.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::data {

WORD TimeDate::dataType() const { return TYPE_TIME; }

TIMEDATE TimeDate::defaultRaw() { return getMinimum(); }

/**
 * @brief デフォルトコンストラクタ
 */
TimeDate::TimeDate(): Variant(), raw_(getMinimum()) {}

TimeDate::TimeDate(const TIMEDATE &td): Variant(), raw_(td) {}

/**
 * @brief コンストラクタ(フィールドデータから構築)
 * @param ptr 数値型へのポインタ
 * @param fPrefix データタイププレフィックスの有無
 */
TimeDate::TimeDate(const void *ptr, BOOL fPrefix)
  : Variant()
  , raw_(*dataptr<TIMEDATE>(const_cast<void*>(ptr), fPrefix))
{}

/**
 * @brief TIMEDATE型へのキャスト演算子
 */
TimeDate::operator TIMEDATE() const { return raw_; }

/**
 * @brief アドレス取得演算子
 * @return TIMEDATEデータへのポインタ
 */
TIMEDATE *TimeDate::operator &() { return &raw_; }

/**
 * @brief 現在の日時を指定したUTCオフセットの時間に変換する
 * @param offsetSeconds UTCオフセット秒(夏時間込み)
 * @param isDaylightTime 夏時間の有無
 * @return TIMEDATE型のデータ
 */
TIMEDATE TimeDate::toTimeZone(int offsetSeconds, bool isDaylightTime) const {
  TIME time;
  time.GM = raw_;
  time.zone = Time::offsetSecondsToZone(offsetSeconds, isDaylightTime);
  time.dst = isDaylightTime ? TRUE : FALSE;
  if (TimeGMToLocalZone(&time) != FALSE) {
    return getWildcard();
  }
  if (TimeLocalToGM(&time) != FALSE) {
    return getWildcard();
  }
  return time.GM;
}

Time TimeDate::toTime() const {
  auto time = fromTIMEDATEToTIME(raw_);
  return Time::fromTIME(time);
}

/**
 * @brief 現在の日時を取得
 * @return 現在の日時
 */
TIMEDATE TimeDate::getCurrent() {
  TIMEDATE td;
  OSCurrentTIMEDATE(&td);
  return td;
}

Lmbcs TimeDate::toString() const noexcept(false) {
  return toString(raw_);
}

Lmbcs TimeDate::toString(TIMEDATE td) noexcept(false) {
  char buffer[MAXALPHATIMEDATE] = "";
  WORD len = 0;
  Status status = ConvertTIMEDATEToText(
        nullptr,
        nullptr,
        &td,
        buffer,
        MAXALPHATIMEDATE,
        &len
        );
  if (!status) { throw status; }
  return Lmbcs(buffer, len);
}

/**
 * @return ミニマム定数を返す
 */
TIMEDATE TimeDate::getMinimum() {
  return getConstant(TIMEDATE_MINIMUM);
}

/**
 * @return マキシマム定数を返す
 */
TIMEDATE TimeDate::getMaximum() {
  return getConstant(TIMEDATE_MAXIMUM);
}

/**
 * @return ワイルドカード定数を返す
 */
TIMEDATE TimeDate::getWildcard() {
  return getConstant(TIMEDATE_WILDCARD);
}

TimeDate TimeDate::fromTime(const Time &t) {
  auto time = t.toTIME();
  return fromTIMEToTIMEDATE(time);
}

TIMEDATE TimeDate::fromTIMEToTIMEDATE(const TIME &time) {
  TIME t = time;
  if (TimeLocalToGM(&t) != FALSE) { // Success=FALSE
    return TimeDate::getMinimum();
  }
  return t.GM;
}

TIME TimeDate::fromTIMEDATEToTIME(const TIMEDATE &td) {
  TIME time;
  time.GM = td;
  TimeGMToLocal(&time);
  return time;
}

TimeDate TimeDate::fromQDateTime(const QDateTime &dateTime) noexcept(false) {
  QDate qDate = dateTime.date();
  QTime qTime = dateTime.time();
  TIME time;
  time.year = qDate.year();
  time.month = qDate.month();
  time.day = qDate.day();
  time.hour = qTime.hour();
  time.minute = qTime.minute();
  time.second = qTime.second();
  time.hundredth = qTime.msec() / 10;
  QTimeZone timeZone = dateTime.timeZone();
  time.dst = dateTime.isDaylightTime() ? 1 : 0;
  time.zone = timeZone.offsetFromUtc(dateTime) / -3600 + time.dst;
  if (TimeLocalToGM(&time) == FALSE) { // 成功時FALSE
    return TimeDate(time.GM);
  }
  throw std::runtime_error("TimeLocalToGM failed.");
}

/**
 * @brief 日時定数の取得
 * @tparam T 取得する定数の設定値
 * @return 指定した日時定数
 */
TIMEDATE TimeDate::getConstant(WORD type) {
  TIMEDATE td;
  TimeConstant(type, &td);
  return td;
}

} // namespace npp2::data
