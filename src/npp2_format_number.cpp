#include "../include/npp2/format/number.hpp"

// #ifdef NT
// #pragma pack(push, 1)
// #endif

// #include <global.h>
// #include <misc.h>

// #ifdef NT
// #pragma pack(pop)
// #endif

namespace npp2::format {

/**
 * @brief デフォルトコンストラクタ
 */
Number::Number() : value_({2,NFMT_GENERAL,0,0}) {}

/**
 * @brief コンストラクタ
 * @param digits 小数点以下の桁数
 * @param format フォーマット値(NFMT_xxx)
 * @param attributes フォーマット属性(NATTR_xxx)
 */
Number::Number(BYTE digits, BYTE format, BYTE attributes)
  : value_({digits, format, attributes, 0})
{}

/**
 * @return NFMTへのポインタ
 */
NFMT *Number::data() {
  return &value_;
}

/**
 * @return 小数点以下の桁数
 */
BYTE Number::digits() const {
  return value_.Digits;
}

/**
 * @param v 設定する小数点以下の桁数
 */
void Number::setDigits(BYTE v) {
  value_.Digits = v;
}

/**
 * @return フォーマット値(NFMT_xxx)
 */
BYTE Number::format() const {
  return value_.Format;
}

/**
 * @param v 設定するフォーマット値(NFMT_xxx)
 */
void Number::setFormat(BYTE v) {
  value_.Format = v;
}

/**
 * @brief 属性値から該当属性の真偽を調べる
 * @param A 属性
 * @return 属性が真ならtrue
 */
bool Number::isAttribute(WORD A) const {
  return value_.Attributes & A;
}

/**
 * @brief 属性値の真偽を設定する
 * @param A 属性
 * @param a 真にするならtrue
 */
void Number::setAttribute(WORD A, bool a) {
  if (a) {
    value_.Attributes |= A;
  }
  else {
    value_.Attributes &= (~A);
  }
}

/**
 * @return 3桁ごとに分離記号を付けるか
 */
bool Number::isPunctuated() const {
  return isAttribute(NATTR_PUNCTUATED);
}

/**
 * @return 負のときに括弧を付けるか
 */
bool Number::isParen() const {
  return isAttribute(NATTR_PARENS);
}

/**
 * @return パーセント表示をするか
 */
bool Number::isPercent() const {
  return isAttribute(NATTR_PERCENT);
}

/**
 * @return 小数点位置を可変にするか
 */
bool Number::isVarying() const {
  return isAttribute(NATTR_VARYING);
}

/**
 * @return バイト表示するか
 */
bool Number::isBytes() const {
  return isAttribute(NATTR_BYTES);
}

/**
 * @param a 3桁ごとに分離記号を付けるかを設定
 */
void Number::setPunctuated(bool a) {
  setAttribute(NATTR_PUNCTUATED, a);
}

/**
 * @param a 負のときに括弧を付けるかを設定
 */
void Number::setParen(bool a) {
  setAttribute(NATTR_PARENS, a);
}

/**
 * @param a パーセント表示をするかを設定
 */
void Number::setPercent(bool a) {
  setAttribute(NATTR_PERCENT, a);
}

/**
 * @param a 小数点位置を可変にするかを設定
 */
void Number::setVarying(bool a) {
  setAttribute(NATTR_VARYING, a);
}

/**
 * @param a バイト表示するかを設定
 */
void Number::setBytes(bool a) {
  setAttribute(NATTR_BYTES, a);
}

} // namespace npp2::format
