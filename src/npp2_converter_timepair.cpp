#include "../include/npp2/converter/timepair.hpp"
#include "../include/npp2/utils/time.hpp"
#include "../include/npp2/status.hpp"

namespace npp2::converter {

/**
 * @brief デフォルトコンストラクタ
 */
TimeDatePairToTextConverter::TimeDatePairToTextConverter()
  : TimeToTextConverter()
{}

/**
 * @brief コンストラクタ
 * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
 * @param nPtr NFMTラッパーの共有ポインタ
 */
TimeDatePairToTextConverter::TimeDatePairToTextConverter(
  IntlFormatPtr &&iPtr,
  format::TimePtr &&nPtr
)
  : TimeToTextConverter(std::move(iPtr), std::move(nPtr))
{}

/**
 * @brief 関数呼び出し
 * @param pPair 変換元数値ペアへのポインタ
 * @return フォーマット済み文字列
 */
data::Lmbcs TimeDatePairToTextConverter::operator () (
  TIMEDATE_PAIR *pPair
) const {
  char buffer[MAXALPHATIMEDATEPAIR] = "";
  WORD len = 0;
  Status status = ConvertTIMEDATEPAIRToText(
    !intlFormatPtr_ ? nullptr : intlFormatPtr_->data(),
    !formatPtr_ ? nullptr : formatPtr_->data(),
    pPair,
    buffer,
    MAXALPHATIMEDATEPAIR,
    &len
  );
  if (!status) { throw status; }
  return data::Lmbcs(buffer, len);
}

/**
 * @brief デフォルトコンストラクタ
 */
TextToTimeDatePairConverter::TextToTimeDatePairConverter()
  : TimeConverter()
{}

/**
 * @brief コンストラクタ
 * @param iPtr i18nフォーマットオブジェクトの共有ポインタ
 * @param tPtr TFMTラッパーの共有ポインタ
 */
TextToTimeDatePairConverter::TextToTimeDatePairConverter(
  IntlFormatPtr &&iPtr,
  format::TimePtr &&tPtr
)
  : TimeConverter(std::move(iPtr), std::move(tPtr))
{}

TIMEDATE_PAIR TextToTimeDatePairConverter::operator () (
  const data::Lmbcs &lmbcs
) const {
  char *ptr = const_cast<char*>(lmbcs.constData());
  TIMEDATE_PAIR td;
  td.Lower = getMinTimeDate();
  td.Upper = getMinTimeDate();
  Status status = ConvertTextToTIMEDATEPAIR(
    !intlFormatPtr_ ? nullptr : intlFormatPtr_->data(),
    !formatPtr_ ? nullptr : formatPtr_->data(),
    &ptr,
    static_cast<WORD>(lmbcs.size()),
    &td
  );
  if (!status) { throw status; }
  return td;
}

} // namespace npp2::converter
