#include "../include/npp2/data/numberrange.hpp"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace npp2::data {

NUMBER_PAIR NumberPair::defaultRaw() {
  return {0, 0};
}

WORD NumberRange::dataType() const {
  return TYPE_NUMBER_RANGE;
}

NumberRange::NumberRange()
  : Range<Number, NumberPair>()
{}

NumberRange::NumberRange(RANGE *pRange, BOOL fPrefix)
  : Range<Number, NumberPair>(pRange, fPrefix)
{}

} // namespace npp2::data
