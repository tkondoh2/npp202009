#include "../include/npp2/thread.hpp"

namespace npp2 {

ThreadLocker::ThreadLocker()
  : status_(NotesInitThread())
{}

ThreadLocker::~ThreadLocker() {
  if (status_) {
    NotesTermThread();
  }
}

} // namespace npp2
