# DOXYGENの使い方

1. Path環境変数にDoxygenの実行パスが通っているか確認し、通っていなければ通す。
2. doxyfileのあるパスでコマンドプロンプトを開く(npp202009ならC:\Users\tkondoh\Documents\QtProjects\npp202009）
3. `doxygen npp202009.doxyfile` を実行する。

# Doxyfileの変更箇所

PROJECT_NAME: プロジェクト名、npp202009
OUTPUT_DIRECTORY: 出力先、doc
OUTPUT_LANGUAGE: 出力言語、Japanese
TAB_SIZE: タブサイズ、2
EXTRACT_ALL: ドキュメントがなくてもドキュメント化する、YES
EXTRACT_PRIVATE: プライベートメンバーもドキュメント化する、YES
EXTRACT_STATIC: 静的メンバーもドキュメント化する、YES
INPUT: 入力ファイル/ディレクトリ、include/npp
RECURSIVE: 再帰検索、YES
GENERATE_LATEX: LATEX形式で出力、NO